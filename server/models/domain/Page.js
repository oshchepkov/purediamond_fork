var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/pagesSql');
var pool = config.db.pool;

var PageDto = require('../../models/dto/PageDto');

function sqlRowToDto(row){
    var dto = new PageDto(row.id, row.slug, row.title, row.subtitle, row.menu_title, row.published)
    return dto;
}

function findAll(callback){
    var pages = [];
    pool.query(sql.select.pages, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
		for (var i in rows) {
            pageDto = sqlRowToDto(rows[i]);
            pages.push(pageDto);
		}
        callback(null, pages);
    });
};

function findBySlug(slug, callback){
    var pages = [];
    pool.query(sql.select.pagesBySlug, [slug], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        async.each(rows, function(row, eachCallback) {
            var page = sqlRowToDto(row)

        }, function(err){
            if( err ) {
                callback(err, null);
            } else {
                callback(null, pages);
              //res.send(rir);
            }
        });
    });
};


////////////////////
module.exports = function () {
    return {
        findAll : findAll,
        findBySlug : findBySlug
    }
};
