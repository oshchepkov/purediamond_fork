var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/stoneSearchSql');
var pool = config.db.pool;

var CutDto = require('../../models/dto/CutDto');

function sqlRowToDto(row){
    var dto = new CutDto(row.id, row.rank, row.name, row.friendly_name, row.description);
    return dto;
}

function findAll(callback){
    var cuts = [];
    pool.query(sql.select.cuts, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
		for (var i in rows) {
            cuts.push(sqlRowToDto(rows[i]));
		}
        callback(null, cuts);
    });
};

////////////////////
module.exports = function () {
    return {
        findAll : findAll
    }
};