var async = require('async');
var config = require('../../config/config');
//var sql = require('../../api/sql/ringSearchSql');
var inventorySql = require('../../api/sql/ringsInventorySql');
var pool = config.db.pool;

var ImageDto = require('../../models/dto/ImageDto');

var imageUrl = config.path.ringsImagesUrl;

function sqlRowToDto(row, ringId){
    var dto = new ImageDto(row.id, row.file_name, imageUrl+'/'+ringId+'/'+row.file_name, imageUrl+'/'+ringId+'/thumb/'+row.file_name, row.description, Boolean(row.favorite));
    return dto;
}

function findAllByRing(ringId, callback){
    var images = [];
    pool.query(inventorySql.select.ringImages, [ringId], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        for (var i in rows) {
            imageDto = sqlRowToDto(rows[i], ringId);
            images.push(imageDto);
        }
        callback(null, images);
    });
};

////////////////////
module.exports = function () {
    return {
        findAllByRing : findAllByRing
    }
};
