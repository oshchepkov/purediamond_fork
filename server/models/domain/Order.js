var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/ordersSql');
var pool = config.db.pool;

//Importing models
var Product = require('./Product');
var Stone = require('./Stone');
var Ring = require('./Ring');
var User = require('./User');

var product = new Product();
var stone = new Stone();
var ring = new Ring();
var user = new User();

var orderItem;

//native dto
function OrderDto(id, date, subtotal, tax, discount, total, contents, status, ringSize, billing_address, shipping_address, extras) {
    this.id = id
    this.date = date
    this.subtotal = subtotal
    this.tax = tax
    this.discount = discount
    this.total = total
    this.contents = contents
    this.status = status
    this.ringSize = ringSize
    this.stone = null
    this.ring = null
    this.billing_address = null
    this.shipping_address = null
    this.extras = extras
    this.products = []
}

function sqlRowToDto(row){
    var dto = new OrderDto(row.id, row.date, row.subtotal, row.tax, row.discount, row.total, row.contents,
                        row.status, row.ring_size, row.billing_address, row.shipping_address, row.extras)
    return dto;
}


getSubtotal = function(orderDto){
    var subtotal = 0;
    var itemPrice = orderItem.price - getDiscount();
    // if (orderDto && orderDto.stone && orderDto.stone.price){
    //     subtotal += orderDto.stone.price
    // }
    // if (orderDto && orderDto.ring && orderDto.ring.price){
    //     subtotal += orderDto.ring.price - orderDto.ring.discount
    // }
    if(orderDto.subtotal > 0) {
      subtotal = orderDto.subtotal;
    }
    if(true === orderItem.remove && subtotal > 0) {
        subtotal = subtotal - itemPrice;
    } else {
      subtotal = subtotal + itemPrice;
    }
    if(!orderDto.products.length) {
      subtotal = 0
    }

    return subtotal
}
getTax = function(orderDto){
    var tax = 0
    tax += getSubtotal(orderDto) * 0.12
    return tax
}
getTotal = function(orderDto){
    var total = getSubtotal(orderDto)
    // if(orderDto.ring) {
    //     total = total - orderDto.ring.discount;
    // }
    total += getTax(orderDto)
    return total
}
getDiscount = function(orderDto){
    var discount = 0
    if(orderItem.discount) {
        discount = orderItem.discount;
    }
    return discount
}


function findById(orderId, callback){
    //console.log('findById');
    var orderDto = null;
    var stoneId;
    var product;
    async.series({
        findOrder : function(seriesCallback){
           pool.query(sql.select.orderById, [orderId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                }
                if (rows.length > 0){
                    var order = rows[0]
                    orderDto = sqlRowToDto(order)
                    // getFullProducts(orderId, function(err, result){
                    //     if (err){
                    //         seriesCallback(err, 'success');
                    //     }else{
                    //         orderDto.productsFull = result
                    //         //orderDto.user = result.user
                    //         seriesCallback(null, 'success');
                    //     }
                    // });
                    seriesCallback(null, 'success');
                }else{
                    seriesCallback(null, 'success');
                }
            });
        },//findOrder
        orderProduct : function(seriesCallback){
          //var products = {};
          pool.query(sql.select.orderProducts, [orderId], function (err, rows, fields) {
            if (err) {
                console.log(err);
            }
            if (rows.length > 0){
              for (var i in rows) {
                product = {
                  'id':rows[i].id,
                  'product_id':rows[i].product_id,
                  'category':rows[i].category,
                  'stone_id':rows[i].stone_id,
                  'size':rows[i].size,
                  'metal_id':rows[i].metal_id,
                  'metal':rows[i].metal
                };
                orderDto.products.push(product);
          		}
              seriesCallback(null, 'success');
            } else {
              seriesCallback(null, 'success');
            }
          });
        }
    },//final callback
    function(err, results) {
        callback(null, orderDto);
    });//series
};

function findBySessionId(sessionId, callback){
	//console.log('findBySessionId');
    var orderDto = null;
    var stoneId;
    var ringId;
    var orderId;
    var product;
    async.series({
        findOrder : function(seriesCallback){
    	   pool.query(sql.select.orderBySessionId, [sessionId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                }
                if (rows.length > 0){
                    var order = rows[0]
                    orderDto = sqlRowToDto(order)
                    getOrderProperties(order, function(err, result){
                        if (err){
                            seriesCallback(err, 'success');
                        }else{
                            orderDto.stone = result.stone
                            orderDto.ring = result.ring
                            orderDto.user = result.user

                            if(order.extras) {
                               extras = JSON.parse(order.extras)
                               orderDto.ring.additionalPrice = extras.metalPrice
                            }

                            orderId = orderDto.id;
                            seriesCallback(null, 'success');
                        }
                    });

                }else{
                    seriesCallback(null, 'success');
                }

            });
            //orderId = 'jhg';
        },//findOrder
        orderProduct : function(seriesCallback){
          //var products = {};
          pool.query(sql.select.orderProducts, [orderId], function (err, rows, fields) {
            if (err) {
                console.log(err);
            }
            if (rows.length > 0){
              for (var i in rows) {
                product = {
                  'id':rows[i].id,
                  'product_id':rows[i].product_id,
                  'category':rows[i].category,
                  'stone_id':rows[i].stone_id,
                  'size':rows[i].size,
                  'metal_id':rows[i].metal_id,
                  'metal':rows[i].metal
                };
                orderDto.products.push(product);
          		}
              seriesCallback(null, 'success');
            } else {
              seriesCallback(null, 'success');
            }
          });
        }
    },//final callback
    function(err, results) {
        callback(null, orderDto);
    });//series
};

function updateOrder(order, product, callback){
    var updatedOrder;
    orderItem = product;
    async.series({
        updateOrder : function(seriesCallback){
          if(true === product.remove) {
            pool.query(sql.delete.orderRemoveProduct, [product.order_product_id, order.id], function (err, rows, fields) {
                if (err) {
                    console.log('remove ERR', err);
                    seriesCallback(err, 'success');
                }
                console.log('remove', product.id);
                seriesCallback(null, 'success');
            });
          } else if(true === product.update) {
            pool.query(sql.update.orderUpdateProduct, [product.stone, parseFloat(product.size), product.metal_id, product.id], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    seriesCallback(err, 'success');
                }
                seriesCallback(null, 'success');
            });
          } else {
            pool.query(sql.insert.orderAddProduct, [product.id, order.id, product.category, parseFloat(product.size), product.metal_id], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    seriesCallback(err, 'success');
                }
                seriesCallback(null, 'success');
            });
          }
        },//addStone
        findOrder : function(seriesCallback){
            findById(order.id, function(err, result){
                updatedOrder = result;
                seriesCallback(null, 'success');
            });
        },//findOrder
        updatePrice : function(seriesCallback){
           updatePrice(updatedOrder, function(err, result){
                if (err){
                    seriesCallback(err, 'success');
                }else{
                    seriesCallback(null, 'success');
                }
            });
        },//updatePrice
    },//final callback
    function(err, results) {
        callback(null, results);
    });//series
};

function deleteOrder(orderId, callback){
    async.series({
        deleteOrder : function(seriesCallback){
            pool.query(sql.delete.order, [orderId], function (err) {
                if (err) {
                    console.log('remove ERR', err);
                    seriesCallback(err, 'success');
                }
                console.log('remove order', orderId);
                seriesCallback(null, 'success');
            });          
        },//addStone
        deleteOrderProducts : function(seriesCallback){
            pool.query(sql.delete.orderProducts, [orderId], function (err) {
                if (err) {
                    console.log('remove ERR', err);
                    seriesCallback(err, 'success');
                }
                seriesCallback(null, 'success');
            });
        }
    },//final callback
    function(err, results) {
        callback(null, results);
    });//series
};

//Get order products
function getFullProducts(productList, callback) {
  var products = [];
  var stones = [];
  var prodIds = [];
  var stonesIds = [];

  if (typeof productList !== 'undefined' && productList.length > 0){
    for(var i in productList) {
      if(productList[i].category != 'diamonds') {
        prodIds.push(productList[i].product_id);
      } else {
        stonesIds.push(productList[i].product_id);
      }
    }
    async.series({
      prod: function(seriesCallback){
        if(typeof prodIds !== 'undefined' && prodIds.length > 0) {
          product.findById(prodIds, function(err, result){
              products = result;
              seriesCallback(null, 'success');
          });
        } else {
          seriesCallback(null, 'success');
        }
      },
      stone: function(seriesCallback){
        if(typeof stonesIds !== 'undefined' && stonesIds.length > 0) {
          stone.findById(stonesIds, function(err, result){
              stones = result;
              products = products.concat(stones);
              seriesCallback(null, 'success');
          });
        } else {
          seriesCallback(null, 'success');
        }
      }
    },
    function(err, results) {
        callback(null, products);
    });//series
  } else {
    callback(null, null);
  }

};


function addRing(order, ring, callback) {
    var updatedOrder;
    var ring_id;
    if(true === ring.remove) {
        ring_id = null;
    } else {
        ring_id = ring.id;
    }
    async.series({
        addRing : function(seriesCallback){
            pool.query(sql.update.addRingToOrder, [ring_id, order.id], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    seriesCallback(err, 'success');
                }
                seriesCallback(null, 'success');
            });
        },//addRing
        addExtras : function(seriesCallback){
            var extObj = {}
            var extras = null
            if(ring.additionalPrice > 0) {
                extObj.metalPrice = ring.additionalPrice
                extras = JSON.stringify(extObj)
            }
            pool.query(sql.update.addExtras, [extras, order.id], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    seriesCallback(err, 'success');
                }
                seriesCallback(null, 'success');
            });
        },
        findOrder : function(seriesCallback){
            findById(order.id, function(err, result){
                updatedOrder = result;
                seriesCallback(null, 'success');
            });
        },//findOrder
        updatePrice : function(seriesCallback){
            if(ring.additionalPrice > 0) {
                updatedOrder.ring.price += ring.additionalPrice;
                updatedOrder.subtotal += ring.additionalPrice;
            }
           updatePrice(updatedOrder, function(err, result){
                if (err){
                    seriesCallback(err, 'success');
                } else{
                    seriesCallback(null, 'success');
                }
            });
        },//updatePrice
    },//final callback
    function(err, results) {
        callback(null, results);
    });//series
};

function create(sessionId, callback){
    //console.log('create');
    var newOrderId;
    var newOrder;
    async.series({
        createOrder : function(seriesCallback){
            pool.query(sql.insert.createOrder, [new Date(), 0, 0, 0, null, 'new'], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                newOrderId = rows.insertId;
                seriesCallback(null, 'success');
            });
        },//createOrder
        linkOrderWithSession : function(seriesCallback){
            pool.query(sql.insert.linkOrderWithSession, [newOrderId, sessionId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                }
                seriesCallback(null, 'success');
            });
        },//linkOrderWithSession
        returnNewOrder : function(seriesCallback){
            findById(newOrderId, function(err, result){
                newOrder = result;
                seriesCallback(null, 'success');
            });
        }//returnNewOrder
    },//final callback
    function(err, results) {
        callback(null, newOrder);
    });//series
};//create



function findAll(callback){
    //console.log('findAll');
    var orders = [];

    pool.query(sql.select.orders, function (err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }

        async.each(rows, function(row, eachCallback) {

            var orderDto = sqlRowToDto(row);

            getOrderProperties(row, function(err, result){
                orderDto.stone = result.stone;
                orderDto.ring = result.ring;
                orderDto.user = result.user;
                orderDto.date = row.date;
                orderDto.billing_address = JSON.parse(row.billing_address);
                orderDto.shipping_address = JSON.parse(row.shipping_address);

                orders.push(orderDto);
                eachCallback();
            });

        }, function(err){
            if( err ) {
                callback(err, null);
            } else {
                callback(null, orders);
            }
        });

    });

};


function updateRingSize(order, ringSize, callback) {
    pool.query(sql.update.updateRingSize, [ringSize, order.id], function (err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, rows.insertId);
    });
};

function updateStatus(order, status, callback) {
    pool.query(sql.update.updateStatus, [status, order.id], function (err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, rows.insertId);
    });
};

updatePrice = function(orderDto, callback){
    //console.log('updatePrice: ', orderDto);
    async.series({
        updatePrice : function(seriesCallback){
            pool.query(sql.update.price, [getSubtotal(orderDto), getTax(orderDto), getDiscount(orderDto), getTotal(orderDto), orderDto.id], function (err, rows, fields) {
              if (err) {
                  console.log('updErr', err);
                  seriesCallback(err, 'success');
              }
              seriesCallback(null, 'success');
            });
        },//updatePrice
    },//final callback
    function(err, results) {
        callback(null, orderDto);
    });//series
};


getOrderProperties = function(order, callback){
    var orderDto = new OrderDto();
    var products = [];
    async.parallel({
        getProducts : function(seriesCallback){
            seriesCallback(null, 'success');
              // stone.findById(order.stone_id, function(err, result){
              //     orderDto.stone = result
              //     seriesCallback(null, 'success');
              // });

        },
        // getStone : function(seriesCallback){
        //     if (order.stone_id != null){
        //         stone.findById(order.stone_id, function(err, result){
        //             orderDto.stone = result
        //             seriesCallback(null, 'success');
        //         });
        //     }else{
        //         seriesCallback(null, 'success');
        //     }
        // },//getStone
        // getRing : function(seriesCallback){
        //     if (order.ring_id != null){
        //         ring.findById(order.ring_id, function(err, result){
        //             orderDto.ring = result
        //             seriesCallback(null, 'success');
        //         });
        //     }else{
        //         seriesCallback(null, 'success');
        //     }
        // },//getRing
        getUser : function(seriesCallback){
            if (order.user_id != null){
                user.findById(order.user_id, function(err, result){
                    orderDto.user = result
                    seriesCallback(null, 'success');
                });
            }else{
                seriesCallback(null, 'success');
            }
        }//getUser
    },
    function(err, results) {
        if (err){
            callback(err, null);
        }
        callback(null, orderDto);
    });
};

//updateAddresses
function updateShippingDetails(currentBillingAddress, currentShippingAddress, userId, orderId, callback){
    console.log('updateAddresses');
    //var newUserId;
    var updatedOrder;
    async.series({
        updateDetails : function(seriesCallback){
            pool.query(sql.update.addresses, [JSON.stringify(currentBillingAddress),
                                            JSON.stringify(currentShippingAddress), userId, orderId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                seriesCallback(null, 'success');
            });
        },//updateDetails
        updateStatus : function(seriesCallback){
            pool.query(sql.update.updateStatus, ['placed', orderId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                seriesCallback(null, 'success');
            });
        },
        returnUpdatedOrder : function(seriesCallback){
            findById(orderId, function(err, result){
                updatedOrder = result;
                seriesCallback(null, 'success');
            });
        }//returnUpdatedOrder
    },//final callback
    function(err, results) {
        callback(null, updatedOrder);
    });//series
};//updateShippingDetails


////////////////////
module.exports = function () {
    return {
        findAll : findAll,
        findById : findById,
		findBySessionId : findBySessionId,
        create : create,
        getFullProducts : getFullProducts,
        updateOrder : updateOrder,
        deleteOrder : deleteOrder,
        updateRingSize : updateRingSize,
        updateStatus : updateStatus,
        updateShippingDetails : updateShippingDetails
    }
};
