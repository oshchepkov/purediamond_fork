var async = require('async');
var config = require('../../config/config');
//var sql = require('../../api/sql/ringSearchSql');
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport('smtps://purediamond.ca@gmail.com:dzscbqnqimgznbce@smtp.gmail.com');
var pool = config.db.pool;

var toAddress = 'info@purediamond.ca';

function BookAppointment(form, callback){

    console.log(form);
    if (typeof form.name != 'undefined' && typeof form.email != 'undefined') {
    
    var mailBody = '<h2>Schedule an appointment</h2>'+
                  '<p>Name: <b>'+ form.name + '</b></p>'+
                  '<p>Email: <b>'+ form.email + '</b></p>'+
                  '<p>Phone: <b>'+ form.phone + '</b></p>'+
                  '<div>Notes: '+ form.notes + '</div>';

    var mailOptions = {
      from: '"PureDiamond.ca" <info@purediamond.ca>',
      to: toAddress,
      subject: 'Appointment request',
      html: mailBody
    };

    transporter.sendMail(mailOptions, function(error, info){
      if(error){
          console.log(error);
          callback(error, null);
      } else {
          console.log('Message sent: ' + info.response);
          callback(null, null);
      };
    });   

    }
};

function OrderPlaced(form, order, callback){
    if (typeof form.name != 'undefined' && typeof form.email != 'undefined' && typeof order != 'undefined') {
    
    var mailBody = '<p>Name: <b>'+ form.name + '</b></p>'+
                  '<p>Email: <b>'+ form.email + '</b></p>'+
                  '<p style="padding-bottom: 20px">Phone: <b>'+ form.phone + '</b></p>';

    mailBody += '<table style="border-collapse: collapse;">'+
    '<tr style="text-align:left; border-bottom:3px solid #eee;">'+
    '<th style="padding: 8px">Lot#</th>'+
    '<th style="padding: 8px">Title</th>'+
    '<th style="padding: 8px">Size</th>'+
    '<th style="padding: 8px">Metal</th>'+
    '<th style="padding: 8px">Price</th></tr>';
    for(var i in order.fullProducts){
        var size = getProp(order.products, 'product_id', order.fullProducts[i].id, 'size');
        var metal = getProp(order.products, 'product_id', order.fullProducts[i].id, 'metal');
        mailBody += '<tr>';
        mailBody += '<td style="border-bottom:1px solid #eee; padding: 8px 24px 8px 8px">'+
        (order.fullProducts[i].lot_number ? order.fullProducts[i].lot_number : order.fullProducts[i].stock_number)
        +'</td>';
        mailBody += '<td style="border-bottom:1px solid #eee; padding: 8px 24px 8px 8px">'+
        (order.fullProducts[i].title ? order.fullProducts[i].title : 'Stone - ' + order.fullProducts[i].shape + ', '
        +order.fullProducts[i].weight + ' Carat')
        +'</td>';
        mailBody += '<td style="border-bottom:1px solid #eee; padding: 8px 24px 8px 8px">'+(!size ? '' : size)+'</td>';
        mailBody += '<td style="border-bottom:1px solid #eee; padding: 8px 24px 8px 8px">'+(!metal ? '' : metal)+'</td>';
        mailBody += '<td style="border-bottom:1px solid #eee; padding: 8px 24px 8px 8px">C$ '+order.fullProducts[i].price+'</td>';
        mailBody += '</tr>';
    }
    mailBody += '</table>';
    mailBody += '<div style="padding: 28px 24px 8px 8px"><b>Total:</b> C$ '+order.total+'</div>';

    var mailOptions = {
      from: '"PureDiamond.ca" <info@purediamond.ca>',
      to: toAddress,
      subject: 'New Order!',
      html: mailBody
    }; 
 
    transporter.sendMail(mailOptions, function(error, info){
      if(error){
          callback(error, null);
      } else {
          console.log('Message sent: ' + info.response);
          callback(null, null);
      };
    });   

    }
};

getProp = function(array, key, val, prop) {
    if(item = array.filter(function(a){ return a[key] == val })[0]) {
        return item[prop];
    } else {
        return false;
    }    
}

////////////////////
module.exports = function () {
    return {
        BookAppointment : BookAppointment,
        OrderPlaced : OrderPlaced
    }
};