var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/stoneSearchSql');
var pool = config.db.pool;

var ColorDto = require('../../models/dto/ColorDto');

var seletableColors = ['D', 'E', 'F','G','H','I','J','K','L','M']

function sqlRowToDto(row){
    var dto = new ColorDto(row.id, row.rank, row.name, row.friendly_name, row.description);
    return dto;
}

function findAll(callback){
    var colors = [];
    pool.query(sql.select.colors, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
		for (var i in rows) {
            colors.push(sqlRowToDto(rows[i]));
		}
        callback(null, colors);
    });
};

function findAllByNames(callback){
    var colors = [];
    pool.query(sql.select.colorsByNames, [seletableColors], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        for (var i in rows) {
            colors.push(sqlRowToDto(rows[i]));
        }
        callback(null, colors);
    });
};

////////////////////
module.exports = function () {
    return {
        findAll : findAll,
        findAllByNames : findAllByNames
    }
};