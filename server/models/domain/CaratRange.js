var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/stoneSearchSql');
var pool = config.db.pool;

var CaratRangeDto = require('../../models/dto/CaratRangeDto');

function sqlRowToDto(row){
    var dto = new CaratRangeDto(row.min_weight, row.max_weight);
    return dto;
}

function find(callback){
    var caratRange;
    pool.query(sql.select.carat_range, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        caratRange = sqlRowToDto(rows[0]);
        callback(null, caratRange);
    });
};

////////////////////
module.exports = function () {
    return {
        find : find
    }
};