var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/stoneSearchSql');
var pool = config.db.pool;

var StoneDto = require('../../models/dto/StoneDto');
var PriceRangeDto = require('../../models/dto/PriceRangeDto');
var CaratRangeDto = require('../../models/dto/CaratRangeDto');

var storageUrl = 'images/';

function sqlRowToDto(row){
    var dto = new StoneDto(row.id, row.stock_number, row.shape, row.shape_class_name, row.weight, row.color,
            row.clarity, row.cut, row.polish, row.symmetry,
            row.measurements, row.table_percent, row.lab, row.certificate_number,
            storageUrl + row.certificate_filename, storageUrl + row.diamond_image,
            row.price, row.depth, row.girdle, row.fluorescence, row.category);
    return dto;
}

function findById(stoneId, callback){
    var stones = [];
    pool.query(sql.select.stones + sql.where.byId, [stoneId], function(err, rows, fields) {
        if (err) {
            console.log(err);
        }
        if (rows.length > 0){
          async.each(rows, function(row, eachCallback) {
            row.category = 'diamonds';
            stones.push(sqlRowToDto(row));
            eachCallback();
          }, function(err){
  	            if( err ) {
  	                callback(err, null);
  	            } else {
  	                callback(null, stones);
  	            }
  	        });
            //callback(null, sqlRowToDto(rows[0]));
        }else{
            callback(null, null);
        }
    });
};

function findAllBySearchCriteria(getCountOnly, searchCriteria, callback){

    //parse cuts
    var cuts_for_sql = [];
    for (var i in searchCriteria.cuts) {
        cuts_for_sql.push(searchCriteria.cuts[i].rank)
    }
    //console.log("cuts_for_sql: "+cuts_for_sql);

    //parse clarities
    var clarities_for_sql = [];
    for (var i in searchCriteria.clarities) {
        clarities_for_sql.push(searchCriteria.clarities[i].rank)
    }
    //console.log("clarities_for_sql: "+clarities_for_sql);

    //parse colors
    var colors_for_sql = [];
    for (var i in searchCriteria.colors) {
        colors_for_sql.push(searchCriteria.colors[i].rank)
    }
    //console.log("colors_for_sql: "+colors_for_sql);

    //parse shapes
    var shapes_for_sql = [];
    for (var i in searchCriteria.shapes) {
        shapes_for_sql.push(searchCriteria.shapes[i].rank)
    }

    var labs_for_sql = [];
    for (var i in searchCriteria.labs) {
        labs_for_sql.push(searchCriteria.labs[i].rank)
    }

    console.log('labs_for_sql', labs_for_sql);
    //console.log("shapes_for_sql: "+shapes_for_sql);

    //parse priceRange
    var priceRange_for_sql = new PriceRangeDto(searchCriteria.priceRange.min, searchCriteria.priceRange.max);
    //console.log("priceRange_for_sql: "+priceRange_for_sql.min+' - '+priceRange_for_sql.max);

    //parse caratRange
    var caratRange_for_sql = new CaratRangeDto(searchCriteria.caratRange.min, searchCriteria.caratRange.max);
    //console.log("caratRange_for_sql: "+caratRange_for_sql.min+' - '+caratRange_for_sql.max);

    var currentSortingOrder
    //switching through user sorting selection
    switch(searchCriteria.sortingOrder) {
    case 'shape':
        currentSortingOrder = 'shapes.rank'
        break;
    case 'carat':
        currentSortingOrder = 'weight'
        break;
    case 'color':
        currentSortingOrder = 'colors.rank'
        break;
    case 'clarity':
        currentSortingOrder = 'clarities.rank'
        break;
    case 'cut':
        currentSortingOrder = 'cuts.rank'
        break;
    case 'report':
        currentSortingOrder = 'labs.rank'
        break;
    case 'polish':
        currentSortingOrder = 'polishes.rank'
        break;
    case 'price':
        currentSortingOrder = 'total_price'
        break;
    default:
        currentSortingOrder = 'total_price'
        break;
    }
    //console.log('currentSortingOrder',currentSortingOrder)

    //this is a workaround for inability to inject both order by and direction into sql query (using current node-mysql connector)
    var orderBy = searchCriteria.sortingOrderDirection == 'DESC' ? sql.where.orderByDesc : sql.where.orderByAsc

    if (!getCountOnly){
        pool.query(sql.select.stones + sql.where.forSearch + orderBy + sql.where.limit,
            [   priceRange_for_sql.min, priceRange_for_sql.max,
                caratRange_for_sql.min, caratRange_for_sql.max,
                cuts_for_sql,
                clarities_for_sql,
                colors_for_sql,
                shapes_for_sql,
                labs_for_sql,
                currentSortingOrder,
                searchCriteria.limitFrom , searchCriteria.limitTo],
            function (err, rows, fields) {
                if (err) {
                    console.log(err)
                    callback(err, null);
                }
                //console.log("results: " + rows);
                var storageUrl = 'images/';
                var stones = []
                for (var i in rows) {
                    stones.push(sqlRowToDto(rows[i]));
                }
                callback(null, stones);
        });
    }else{
       pool.query(sql.select.stonesCount + sql.where.forSearch,
            [   priceRange_for_sql.min, priceRange_for_sql.max,
                caratRange_for_sql.min, caratRange_for_sql.max,
                cuts_for_sql,
                clarities_for_sql,
                colors_for_sql,
                shapes_for_sql,
                labs_for_sql],
            function (err, rows, fields) {
                if (err) {
                    console.log(err)
                    callback(err, null);
                }
                var count = 0
                for (var i in rows) {
                    count = rows[i].count
                }
                callback(null, count);
        });
    }
};

////////////////////
module.exports = function () {
    return {
        findById : findById,
        findAllBySearchCriteria : findAllBySearchCriteria
    }
};
