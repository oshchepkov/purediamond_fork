var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/ringSearchSql');
var pool = config.db.pool;

var ShapeDto = require('../../models/dto/ShapeDto');

function sqlRowToDto(row){
    var dto = new ShapeDto(row.id, row.rank, row.name, row.friendly_name, row.description, row.class_name, false);
    return dto;
}

function findAll(callback){
    var shapes = [];
    pool.query(sql.select.shapes, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
		for (var i in rows) {
            shapeDto = sqlRowToDto(rows[i]);
            shapes.push(shapeDto);
		}
        callback(null, shapes);
    });
};

function findAllByRing(ringId, callback){
    var shapes = [];
    pool.query(sql.select.shapesByRing, [ringId], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        for (var i in rows) {
            shapeDto = sqlRowToDto(rows[i]);
            shapes.push(shapeDto);
        }
        callback(null, shapes);
    });
};


////////////////////
module.exports = function () {
    return {
        findAll : findAll,
        findAllByRing : findAllByRing
    }
};