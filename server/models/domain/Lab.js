var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/stoneSearchSql');
var pool = config.db.pool;

var LabDto = require('../../models/dto/LabDto');

function sqlRowToDto(row){
    var dto = new LabDto(row.id, row.rank, row.name, row.friendly_name, row.description);
    return dto;
}

function findAll(callback){
    var labs = [];
    pool.query(sql.select.labs, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
		for (var i in rows) {
            labs.push(sqlRowToDto(rows[i]));
		}
        callback(null, labs);
    });
};

////////////////////
module.exports = function () {
    return {
        findAll : findAll
    }
};