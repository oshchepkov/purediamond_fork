var async = require('async');
var https = require('https');
var config = require('../../config/config');
var sql = require('../../api/sql/ringSearchSql');
var pool = config.db.pool;

var RingDto = require('../../models/dto/RingDto');
var PriceRangeDto = require('../../models/dto/PriceRangeDto');

var Designer = require('../../models/domain/Designer');
var Metal = require('../../models/domain/Metal');
var Shape = require('../../models/domain/Shape');
var Collection = require('../../models/domain/Collection');
var Image = require('../../models/domain/Image');

var designer = new Designer();
var metal = new Metal();
var shape = new Shape();
var collection = new Collection();
var image = new Image();

function sqlRowToDto(row){
	var dto = new RingDto(row.id, row.title, row.lot_number, row.price, row.stone_weight_min, row.stone_weight_max, row.description, row.instagram, row.published, row.category, row.features, row.details, row.discount, row.sale)
    return dto;
}

//getPriceRange
function getPriceRange(callback){
    pool.query(sql.select.price_range, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        if (rows.length > 0){
            var priceRangeDto = new PriceRangeDto(rows[0].min_price, rows[0].max_price);
            callback(null, priceRangeDto);
        }else{
            callback(null, null);
        }
    });
};

function findAllBySearchCriteria(searchCriteria, callback){
    //console.log(searchCriteria)
    var metalIDs = [];
    for (var i in searchCriteria.metals) {
        metalIDs.push(searchCriteria.metals[i].id)
    }
    var designerIDs = [];
    for (var i in searchCriteria.designers) {
        designerIDs.push(searchCriteria.designers[i].id)
    }
    var shapeIDs = [];
    for (var i in searchCriteria.shapes) {
        shapeIDs.push(searchCriteria.shapes[i].id)
    }
    //var priceRange_for_sql = new PriceRangeDto(searchCriteria.priceRange.min, searchCriteria.priceRange.max);
    var priceRangeSql =" AND (";
    for (var i in searchCriteria.priceRanges) {
        priceRangeSql += "(price BETWEEN "+searchCriteria.priceRanges[i].min+" AND "+searchCriteria.priceRanges[i].max+")";
        if(i < searchCriteria.priceRanges.length-1) priceRangeSql += " OR ";
    }
		priceRangeSql += ")";

    var rings = [];
    pool.query(sql.select.rings + sql.where.forSearch + priceRangeSql + ' ORDER BY rings.id DESC', [metalIDs, designerIDs, shapeIDs],
        function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        async.each(rows, function(row, eachCallback) {
            var ring = sqlRowToDto(row)

            getRingProperties(ring.id, function(err, result){
                if (err){
                    callback(err, null);
                }else{
                    ring.metals = result.metals
                    ring.designers = result.designers
                    ring.shapes = result.shapes
                    ring.collections = result.collections
                    ring.images = result.images
                    rings.push(ring);
                    eachCallback();
                }
            });
        }, function(err){
            if( err ) {
                callback(err, null);
            } else {
                callback(null, rings);
              //res.send(rir);
            }
        });
    });
}//findAllBySearchCriteria

function findFeatured(callback) {
    var rings = [];
    pool.query(sql.select.rings + sql.where.featured, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        async.each(rows, function(row, eachCallback) {
            var ring = sqlRowToDto(row)

            getRingProperties(ring.id, function(err, result){
                if (err){
                    callback(err, null);
                }else{
                    ring.metals = result.metals
                    ring.designers = result.designers
                    ring.shapes = result.shapes
                    ring.collections = result.collections
                    ring.images = result.images
                    rings.push(ring);
                    eachCallback();
                }
            });
        }, function(err){
            if( err ) {
                callback(err, null);
            } else {
                callback(null, rings);
              //res.send(rir);
            }
        });
    });
}

function findSale(callback) {
    var rings = [];
    pool.query(sql.select.rings + sql.where.sale, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        async.each(rows, function(row, eachCallback) {
            var ring = sqlRowToDto(row)

            getRingProperties(ring.id, function(err, result){
                if (err){
                    callback(err, null);
                }else{
                    ring.metals = result.metals
                    ring.designers = result.designers
                    ring.shapes = result.shapes
                    ring.collections = result.collections
                    ring.images = result.images
                    rings.push(ring);
                    eachCallback();
                }
            });
        }, function(err){
            if( err ) {
                callback(err, null);
            } else {
                callback(null, rings);
              //res.send(rir);
            }
        });
    });
}

function findById(ringId, callback){
    pool.query(sql.select.rings + sql.where.byId, [ringId], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        if (rows.length > 0){
            var ring = sqlRowToDto(rows[0]);

            getRingProperties(ring.id, function(err, result){
                if (err){
                    callback(err, null);
                }else{
                    ring.metals = result.metals
                    ring.designers = result.designers
                    ring.shapes = result.shapes
                    ring.collections = result.collections
                    ring.images = result.images

                    if(ring.instagram && ring.instagram.substring(0,4) == 'http') {
                        https.get('https://api.instagram.com/oembed/?url='+ring.instagram+'&omitscript=true&hidecaption=true', function(res) {
                          var body = '';
                          res.setEncoding('utf8');
                          res.on('data', function(d) {
                            body += d;
                          });
                          res.on('end', function() {
                          console.log('BODY', body);
                          console.log('INSTAGRAM', ring.instagram);
                            var resp = JSON.parse(body);
                            ring.instagram = resp.html;
                            callback(null, ring);
                          });

                        }).on('error', function(e) {
                            ring.instagram = false;
                            callback(null, ring);
                          console.error(e);
                        });
                    } else {
                        callback(null, ring);
                    }
                }
            });


        }else{
            callback(null, null);
        }
    });
};


getRingProperties = function(ringId, callback){
    var ringDto = new RingDto();
    async.parallel({
        designers: function(parallelCallback){
            designer.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.designers = result
                    parallelCallback(null, 'success');
                }
            });
        },//designers
        metals: function(parallelCallback){
            metal.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.metals = result
                    parallelCallback(null, 'success');
                }
            });
        },//metals
        shapes: function(parallelCallback){
            shape.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.shapes = result
                    parallelCallback(null, 'success');
                }
            });
        },//shapes
        collections: function(parallelCallback){
            collection.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.collections = result
                    parallelCallback(null, 'success');
                }
            });
        },//collections
        images: function(parallelCallback){
            image.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.images = result
                    parallelCallback(null, 'success');
                }
            });
        },//images
    },
    function(err, results) {
        if (err){
            callback(err, null);
        }
        callback(null, ringDto);
    });

};




////////////////////
module.exports = function () {
    return {
        getPriceRange : getPriceRange,
        findAllBySearchCriteria : findAllBySearchCriteria,
        findFeatured : findFeatured,
				findSale : findSale,
        findById : findById
    }
};
