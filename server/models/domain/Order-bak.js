var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/ordersSql');
var pool = config.db.pool;

//Importing models
var Stone = require('./Stone');
var Ring = require('./Ring');
var User = require('./User');

var stone = new Stone();
var ring = new Ring();
var user = new User();

//native dto
function OrderDto(id, date, subtotal, tax, discount, total, contents, status, ringSize, billing_address, shipping_address, extras) {
    this.id = id
    this.date = date
    this.subtotal = subtotal
    this.tax = tax
    this.discount = discount
    this.total = total
    this.contents = contents
    this.status = status
    this.ringSize = ringSize
    this.stone = null
    this.ring = null
    this.billing_address = null
    this.shipping_address = null
    this.extras = extras
    this.user
}

function sqlRowToDto(row){
    var dto = new OrderDto(row.id, row.date, row.subtotal, row.tax, row.discount, row.total, row.contents, 
                        row.status, row.ring_size, row.billing_address, row.shipping_address, row.extras)
    return dto;
}

getSubtotal = function(orderDto){
    var subtotal = 0
    if (orderDto && orderDto.stone && orderDto.stone.price){
        subtotal += orderDto.stone.price
    }
    if (orderDto && orderDto.ring && orderDto.ring.price){
        subtotal += orderDto.ring.price - orderDto.ring.discount
    }
    return subtotal
}
getTax = function(orderDto){
    var tax = 0
    tax += (getSubtotal(orderDto)-getDiscount(orderDto)) * 0.12
    return tax
}
getTotal = function(orderDto){
    var total = getSubtotal(orderDto)
    // if(orderDto.ring) {
    //     total = total - orderDto.ring.discount;
    // }
    total += getTax(orderDto)
    return total
}
getDiscount = function(orderDto){ 
    var discount = null
    if(orderDto.ring) {
        discount = orderDto.ring.discount;
    }
    return discount
}


function findById(orderId, callback){
    //console.log('findById');
    var orderDto = null;
    var stoneId;
    async.series({
        findOrder : function(seriesCallback){
           pool.query(sql.select.orderById, [orderId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                }
                if (rows.length > 0){
                    var order = rows[0]
                    orderDto = sqlRowToDto(order)
                    getOrderProperties(order, function(err, result){
                        if (err){
                            seriesCallback(err, 'success');
                        }else{
                            orderDto.stone = result.stone
                            orderDto.ring = result.ring
                            orderDto.user = result.user                            
                            seriesCallback(null, 'success');
                        }
                    });
                }else{
                    seriesCallback(null, 'success');
                }
            });
        },//findOrder
    },//final callback
    function(err, results) {
        callback(null, orderDto);
    });//series    
};

function findBySessionId(sessionId, callback){
	//console.log('findBySessionId');
    var orderDto = null;
    var stoneId;
    var ringId;
    async.series({
        findOrder : function(seriesCallback){
    	   pool.query(sql.select.orderBySessionId, [sessionId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                }
                if (rows.length > 0){
                    var order = rows[0]
                    orderDto = sqlRowToDto(order)
                    getOrderProperties(order, function(err, result){
                        if (err){
                            seriesCallback(err, 'success');
                        }else{
                            orderDto.stone = result.stone
                            orderDto.ring = result.ring
                            orderDto.user = result.user
                            
                            if(order.extras) {
                               extras = JSON.parse(order.extras) 
                               orderDto.ring.additionalPrice = extras.metalPrice
                            }
                            seriesCallback(null, 'success');
                        }
                    });
                }else{
                    seriesCallback(null, 'success');
                }
            });
        }//findOrder
    },//final callback
    function(err, results) {
        callback(null, orderDto);
    });//series    
};

function addStone(order, stone, callback){
    var updatedOrder;
    var stone_id;
    if(true === stone.remove) {
        stone_id = null;
    } else {
        stone_id = stone.id;
    }
    async.series({
        addStone : function(seriesCallback){
            pool.query(sql.update.addStoneToOrder, [stone_id, order.id], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    seriesCallback(err, 'success');
                }
                seriesCallback(null, 'success');
            });
        },//addStone
        findOrder : function(seriesCallback){
            findById(order.id, function(err, result){
                updatedOrder = result;
                seriesCallback(null, 'success');
            });
        },//findOrder        
        updatePrice : function(seriesCallback){
           updatePrice(updatedOrder, function(err, result){
                if (err){
                    seriesCallback(err, 'success');
                }else{
                    seriesCallback(null, 'success');
                }
            });
        },//updatePrice
    },//final callback
    function(err, results) {
        callback(null, results);
    });//series
};

function addRing(order, ring, callback) {
    var updatedOrder;
    var ring_id;
    if(true === ring.remove) {
        ring_id = null;
    } else {
        ring_id = ring.id;
    }
    async.series({
        addRing : function(seriesCallback){
            pool.query(sql.update.addRingToOrder, [ring_id, order.id], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    seriesCallback(err, 'success');
                }
                seriesCallback(null, 'success');
            });
        },//addRing
        addExtras : function(seriesCallback){
            var extObj = {}
            var extras = null
            if(ring.additionalPrice > 0) {
                extObj.metalPrice = ring.additionalPrice
                extras = JSON.stringify(extObj)
            }
            pool.query(sql.update.addExtras, [extras, order.id], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    seriesCallback(err, 'success');
                }
                seriesCallback(null, 'success');
            });
        },
        findOrder : function(seriesCallback){
            findById(order.id, function(err, result){
                updatedOrder = result;
                seriesCallback(null, 'success');
            });
        },//findOrder         
        updatePrice : function(seriesCallback){
            if(ring.additionalPrice > 0) {
                updatedOrder.ring.price += ring.additionalPrice;
                updatedOrder.subtotal += ring.additionalPrice;
            }
           updatePrice(updatedOrder, function(err, result){
                if (err){
                    seriesCallback(err, 'success');
                } else{
                    seriesCallback(null, 'success');
                }
            });
        },//updatePrice
    },//final callback
    function(err, results) {
        callback(null, results);
    });//series
};

function create(sessionId, callback){
    //console.log('create');
    var newOrderId;
    var newOrder;
    async.series({
        createOrder : function(seriesCallback){
            pool.query(sql.insert.createOrder, [new Date(), 0, 0, 0, null, 'new'], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                newOrderId = rows.insertId;
                seriesCallback(null, 'success');
            });
        },//createOrder
        linkOrderWithSession : function(seriesCallback){
            pool.query(sql.insert.linkOrderWithSession, [newOrderId, sessionId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                }
                seriesCallback(null, 'success');
            });
        },//linkOrderWithSession
        returnNewOrder : function(seriesCallback){
            findById(newOrderId, function(err, result){
                newOrder = result;
                seriesCallback(null, 'success');
            });
        }//returnNewOrder
    },//final callback
    function(err, results) {
        callback(null, newOrder);
    });//series
};//create



function findAll(callback){
    //console.log('findAll');
    var orders = [];

    pool.query(sql.select.orders, function (err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }

        async.each(rows, function(row, eachCallback) {

            var orderDto = sqlRowToDto(row);

            getOrderProperties(row, function(err, result){
                orderDto.stone = result.stone;
                orderDto.ring = result.ring;
                orderDto.user = result.user;
                orderDto.date = row.date;
                orderDto.billing_address = JSON.parse(row.billing_address);
                orderDto.shipping_address = JSON.parse(row.shipping_address);
                
                orders.push(orderDto);
                eachCallback();
            });

        }, function(err){
            if( err ) {
                callback(err, null);
            } else {
                callback(null, orders);
            }
        });
                
    });

};


function updateRingSize(order, ringSize, callback) {
    pool.query(sql.update.updateRingSize, [ringSize, order.id], function (err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, rows.insertId);
    });
};

function updateStatus(order, status, callback) {
    pool.query(sql.update.updateStatus, [status, order.id], function (err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        callback(null, rows.insertId);
    });
};

updatePrice = function(orderDto, callback){
    //console.log('updatePrice: ', orderDto);
    async.series({
        updatePrice : function(seriesCallback){
            pool.query(sql.update.price, [getSubtotal(orderDto), getTax(orderDto), getDiscount(orderDto), getTotal(orderDto), orderDto.id], function (err, rows, fields) {
              if (err) {
                  console.log(err);
                  seriesCallback(err, 'success');
              }
              seriesCallback(null, 'success');
            });
        },//updatePrice
    },//final callback
    function(err, results) {
        callback(null, orderDto);
    });//series
};


getOrderProperties = function(order, callback){
    var orderDto = new OrderDto();
    async.parallel({
        getStone : function(seriesCallback){
            if (order.stone_id != null){
                stone.findById(order.stone_id, function(err, result){
                    orderDto.stone = result
                    seriesCallback(null, 'success');
                });
            }else{
                seriesCallback(null, 'success');
            }
        },//getStone
        getRing : function(seriesCallback){
            if (order.ring_id != null){
                ring.findById(order.ring_id, function(err, result){
                    orderDto.ring = result
                    seriesCallback(null, 'success');
                });
            }else{
                seriesCallback(null, 'success');
            }
        },//getRing
        getUser : function(seriesCallback){
            if (order.user_id != null){
                user.findById(order.user_id, function(err, result){
                    orderDto.user = result
                    seriesCallback(null, 'success');
                });
            }else{
                seriesCallback(null, 'success');
            }
        }//getUser        
    },
    function(err, results) {
        if (err){
            callback(err, null);    
        }
        callback(null, orderDto);
    });
};

//updateAddresses
function updateShippingDetails(currentBillingAddress, currentShippingAddress, userId, orderId, callback){
    console.log('updateAddresses');
    //var newUserId;
    var updatedOrder;
    async.series({
        updateDetails : function(seriesCallback){
            pool.query(sql.update.addresses, [JSON.stringify(currentBillingAddress), 
                                            JSON.stringify(currentShippingAddress), userId, orderId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                seriesCallback(null, 'success');
            });
        },//updateDetails
        returnUpdatedOrder : function(seriesCallback){
            findById(orderId, function(err, result){
                updatedOrder = result;
                seriesCallback(null, 'success');
            });
        }//returnUpdatedOrder
    },//final callback
    function(err, results) {
        callback(null, updatedOrder);
    });//series
};//updateShippingDetails


////////////////////
module.exports = function () {
    return {
        findAll : findAll, 
        findById : findById,
		findBySessionId : findBySessionId,
        create : create,
        addStone : addStone,
        addRing : addRing,
        updateRingSize : updateRingSize,
        updateStatus : updateStatus,
        updateShippingDetails : updateShippingDetails
    }
};