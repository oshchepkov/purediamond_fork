var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/ringSearchSql');
var pool = config.db.pool;

var DesignerDto = require('../../models/dto/DesignerDto');
var RingDto = require('../../models/dto/ProductDto');

function sqlRowToDto(row){
    var dto = new DesignerDto(row.id, row.name, row.slug, row.image, row.description, row.ring_id, false)
    return dto;
}

function sqlRowToRingDto(row){
    var dto = new RingDto(row.id, row.title, row.lot_number, row.price, row.stone_weight_min, row.stone_weight_max, row.description, row.instagram, row.youtube, row.published, row.category, row.features, row.details, row.discount, row.sale, row.metal_id)
    return dto;
}

function findAll(callback){
    var designers = [];
    pool.query(sql.select.designers, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
		for (var i in rows) {
            designerDto = sqlRowToDto(rows[i]);
            designers.push(designerDto);
		}
        callback(null, designers);
    });
};

function findBySlug(slug, callback){
    var rings = [];
    pool.query(sql.select.ringsByDesignerSlug, [slug], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        } 


        async.each(rows, function(row, eachCallback) {
            var ring = sqlRowToRingDto(row)
            
            getRingProperties(ring.id, function(err, result){
                if (err){
                    callback(err, null);
                }else{
                    ring.metals = result.metals
                    ring.designers = result.designers
                    ring.shapes = result.shapes
                    ring.collections = result.collections
                    ring.images = result.images


                    if (!ring.image && ring.images.length) {
                        ring.image = ring.images[0].file_name;
                    } else {
                        ring.image = '';
                    }
                    rings.push(ring);
                    eachCallback();
                }
            });
        }, function(err){
            if( err ) {
                callback(err, null);
            } else {
                callback(null, rings);              
              //res.send(rir);
            }
        });            
        
        // for (var i in rows) {
        //     var ring = new RingDto(
        //             rows[i].id,
        //             rows[i].title,
        //             rows[i].lot_number,
        //             rows[i].price,
        //             rows[i].stone_weight_min,
        //             rows[i].stone_weight_max,
        //             rows[i].description,                    
        //             rows[i].published,
        //             rows[i].featured,
        //             rows[i].details,
        //             rows[i].image
        //     );

        //     getRingProperties(ring.id, function(err, result){
        //         if (err){
        //             callback(err, null);
        //         }else{
        //             ring.metals = result.metals
        //             ring.designers = result.designers
        //             ring.shapes = result.shapes
        //             ring.collections = result.collections
        //             ring.images = result.images

        //             callback(null, ring);
        //         }
        //     });

        //     rings.push(ring);
        // }
        // callback(null, rings);
    });
};

function findAllByRing(ringId, callback){
    var designers = [];
    pool.query(sql.select.designersByRing, [ringId], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        for (var i in rows) {
            designerDto = sqlRowToDto(rows[i]);
            designers.push(designerDto);
        }
        callback(null, designers);
    });
};


getRingProperties = function(ringId, callback){
    var ringDto = new RingDto();
    async.parallel({
        designers: function(parallelCallback){
            designer.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.designers = result
                    parallelCallback(null, 'success');
                }
            });
        },//designers
        metals: function(parallelCallback){
            metal.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.metals = result
                    parallelCallback(null, 'success');
                }
            });
        },//metals
        shapes: function(parallelCallback){
            shape.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.shapes = result
                    parallelCallback(null, 'success');
                }
            });
        },//shapes
        collections: function(parallelCallback){
            collection.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.collections = result
                    parallelCallback(null, 'success');
                }
            });
        },//collections  
        images: function(parallelCallback){
            image.findAllByRing(ringId, function(err, result){
                if (err){
                    parallelCallback(err, 'failure');
                }else{
                    ringDto.images = result
                    parallelCallback(null, 'success');
                }
            });
        },//images               
    },
    function(err, results) {
        if (err){
            callback(err, null);    
        }
        callback(null, ringDto);
    });

};

////////////////////
module.exports = function () {
    return {
        findAll : findAll,
        findAllByRing : findAllByRing,
        findBySlug : findBySlug
    }
};