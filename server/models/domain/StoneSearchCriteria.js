var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/stoneSearchSql');
var pool = config.db.pool;

var SearchCriteriaDto = require('../../models/dto/SearchCriteriaDto');

//Importing models
var Shape = require('../../models/domain/Shape');
var Cut = require('../../models/domain/Cut');
var Clarity = require('../../models/domain/Clarity');
var Color = require('../../models/domain/Color');
var Lab = require('../../models/domain/Lab');
var PriceRange = require('../../models/domain/PriceRange');
var CaratRange = require('../../models/domain/CaratRange');

var ErrorForResponse = require('../../models/responses/ErrorForResponse');

var shape = new Shape();
var cut = new Cut();
var clarity = new Clarity();
var color = new Color();
var lab = new Lab();
var priceRange = new PriceRange();
var caratRange = new CaratRange();

function find(userSelection, callback){
    var stonesSearchUserSelection = userSelection;
    var searchCriteriaDto = new SearchCriteriaDto();

    searchCriteriaDto.userSelection = stonesSearchUserSelection

    async.parallel({
        shapes: function(callback){
            shape.findAll(function(err, result){
                if (err){
                    searchCriteriaDto.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
                }else{
                    searchCriteriaDto.shapes = result  
                }
                callback(null, 'success');
            });

        },
        cuts: function(callback){
            cut.findAll(function(err, result){
                if (err){
                    searchCriteriaDto.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
                }else{
                    searchCriteriaDto.cuts = result  
                }
                callback(null, 'success');
            });
        },
        clarities: function(callback){
            clarity.findAll(function(err, result){
                if (err){
                    searchCriteriaDto.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
                }else{
                    searchCriteriaDto.clarities = result  
                }
                callback(null, 'success');
            });            
        },
        colors: function(callback){
            color.findAllByNames(function(err, result){
                if (err){
                    searchCriteriaDto.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
                }else{
                    searchCriteriaDto.colors = result  
                }
                callback(null, 'success');
            });              
        },
        labs: function(callback){
            lab.findAll(function(err, result){
                if (err){
                    searchCriteriaDto.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
                }else{
                    searchCriteriaDto.labs = result  
                }
                callback(null, 'success');
            });              
        },
        price_range: function(callback){
            priceRange.find(function(err, result){
                if (err){
                    searchCriteriaDto.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
                }else{
                    searchCriteriaDto.priceRange = result  
                }
                callback(null, 'success');
            });            
        },
        carat_range: function(callback){
            caratRange.find(function(err, result){
                if (err){
                    searchCriteriaDto.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
                }else{
                    searchCriteriaDto.caratRange = result  
                }
                callback(null, 'success');
            });            
        }
    },
    function(err, results) {
        //initial sorting order and 
        searchCriteriaDto.sortingOrder = config.stoneSearch.defaultSortingOrder
        searchCriteriaDto.sortingOrderDirection = 'ASC'
        searchCriteriaDto.limitFrom = config.stoneSearch.defaultLimitFrom
        searchCriteriaDto.limitTo = config.stoneSearch.defaultLimitTo
        searchCriteriaDto.resultsPerPage = config.stoneSearch.defaultLimitTo
        callback(null, searchCriteriaDto);
    });
};

////////////////////
module.exports = function () {
    return {
        find : find
    }
};