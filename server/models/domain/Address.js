var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/addressesSql');
var pool = config.db.pool;

var AddressDto = require('../../models/dto/AddressDto');

function sqlRowToDto(row){
    var dto = new AddressDto(row.id, row.first_name, row.last_name, row.line1, row.line2, row.country, row.province, row.city, row.postal_code, row.phone)
    return dto;
}

function findById(addressId, callback){
    pool.query(sql.select.findById, [addressId], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        if (rows.length > 0){
            var addressDto = sqlRowToDto(rows[0]);
            callback(null, addressDto);
		}else{
            callback(null, null);
        }
    });
};

function create(addressDetails, callback){
    console.log('create');
    var newAddressId;
    var newAddress;
    async.series({
        createOrder : function(seriesCallback){
            pool.query(sql.insert.createAddress, [addressDetails.firstName, addressDetails.lastName, addressDetails.addressLine1, 
                                            addressDetails.addressLine2, addressDetails.country, addressDetails.province, 
                                            addressDetails.city, addressDetails.postalCode, addressDetails.phone]
                                            ,function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                newAddressId = rows.insertId;
                seriesCallback(null, 'success');
            });
        },//createOrder
        returnNewAddress : function(seriesCallback){
            findById(newAddressId, function(err, result){
                newAddress = result;
                seriesCallback(null, 'success');
            });
        }//returnNewAddress
    },//final callback
    function(err, results) {
        callback(null, newAddress);
    });//series
};//create

////////////////////
module.exports = function () {
    return {
        findById : findById,
        create : create
    }
};