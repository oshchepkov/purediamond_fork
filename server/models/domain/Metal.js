var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/ringSearchSql');
var pool = config.db.pool;

var MetalDto = require('../../models/dto/MetalDto');

function sqlRowToDto(row){
    var dto = new MetalDto(row.id, row.rank, row.name, false)
    return dto;
}

function findAll(callback){
    //console.log('findAll');
    var metals = [];
    pool.query(sql.select.metals, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
		for (var i in rows) {
            metalDto = sqlRowToDto(rows[i]);
            metals.push(metalDto);
            //rings.push(sqlRowToDto(rows[i]));
		}//for i in rows
        callback(null, metals);
    });
};

function findAllByRing(ringId, callback){
    var metals = [];
    pool.query(sql.select.metalsByRing, [ringId], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        for (var i in rows) {
            metalDto = sqlRowToDto(rows[i]);
            metals.push(metalDto);
        }//for i in rows
        callback(null, metals);
    });
};


////////////////////
module.exports = function () {
    return {
        findAll : findAll,
        findAllByRing : findAllByRing
    }
};