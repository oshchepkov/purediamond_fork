var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/stoneSearchSql');
var pool = config.db.pool;

var ClarityDto = require('../../models/dto/ClarityDto');

function sqlRowToDto(row){
    var dto = new ClarityDto(row.id, row.rank, row.name, row.friendly_name, row.description);
    return dto;
}

function findAll(callback){
    var clarities = [];
    pool.query(sql.select.clarities, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
		for (var i in rows) {
            clarities.push(sqlRowToDto(rows[i]));
		}
        callback(null, clarities);
    });
};

////////////////////
module.exports = function () {
    return {
        findAll : findAll
    }
};