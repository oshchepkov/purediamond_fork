var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/stoneSearchSql');
var pool = config.db.pool;

var PriceRangeDto = require('../../models/dto/PriceRangeDto');

function sqlRowToDto(row){
    var dto = new PriceRangeDto(row.min_price, row.max_price);
    return dto;
}

function find(callback){
    var priceRange;
    pool.query(sql.select.price_range, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        priceRange = sqlRowToDto(rows[0]);
        callback(null, priceRange);
    });
};

////////////////////
module.exports = function () {
    return {
        find : find
    }
};