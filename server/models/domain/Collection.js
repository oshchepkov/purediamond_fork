var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/ringSearchSql');
var pool = config.db.pool;

var CollectionDto = require('../../models/dto/CollectionDto');

function sqlRowToDto(row){
    var dto = new CollectionDto(row.id, row.rank, row.name, row.description, false)
    return dto;
}

function findAll(callback){
    var collections = [];
    pool.query(sql.select.collections, function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
		for (var i in rows) {
            collectionDto = sqlRowToDto(rows[i]);
            collections.push(collectionDto);
		}
        callback(null, collections);
    });
};

function findAllByRing(ringId, callback){
    var collections = [];
    pool.query(sql.select.collectionsByRing, [ringId], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        for (var i in rows) {
            collectionDto = sqlRowToDto(rows[i]);
            collections.push(collectionDto);
        }
        callback(null, collections);
    });
};

////////////////////
module.exports = function () {
    return {
        findAll : findAll,
        findAllByRing : findAllByRing
    }
};