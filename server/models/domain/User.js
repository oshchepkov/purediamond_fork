var async = require('async');
var config = require('../../config/config');
var sql = require('../../api/sql/usersSql');
var pool = config.db.pool;

var UserDto = require('../../models/dto/UserDto');

function sqlRowToDto(row){
    var dto = new UserDto(row.id, row.username, row.firstname, row.lastname, row.phone, row.subscribe)
    return dto;
}

function findById(userId, callback){
    pool.query(sql.select.findById, [userId], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        if (rows.length > 0){
            var userDto = sqlRowToDto(rows[0]);
            callback(null, userDto);
        }else{
            callback(null, null);
        }
    });
};

function findByUserName(username, callback){
    pool.query(sql.select.findByUserName, [username], function(err, rows, fields) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        if (rows.length > 0){
            var userDto = sqlRowToDto(rows[0]);
            callback(null, userDto);
		}else{
            callback(null, null);
        }
    });
};

function create(userDetails, callback){
    console.log('create', userDetails);
    var newUserId;
    var newUser;

    userDetails.email = !userDetails.email ? '' : userDetails.email;
    userDetails.password = !userDetails.password ? '' : userDetails.password;
    userDetails.firstName = !userDetails.name ? '' : userDetails.name;
    userDetails.lastName = !userDetails.lastName ? '' : userDetails.lastName;
    userDetails.phone = !userDetails.phone ? '' : userDetails.phone;
    userDetails.subscribe = !userDetails.subscribe ? '' : userDetails.subscribe;
    
    async.series({
        createOrder : function(seriesCallback){
            pool.query(sql.insert.createUser, [userDetails.email, userDetails.password, userDetails.firstName, 
                                            userDetails.lastName, userDetails.phone, userDetails.subscribe]
                                            ,function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                newUserId = rows.insertId;
                seriesCallback(null, 'success');
            });
        },//createOrder
        returnNewUser : function(seriesCallback){
            findById(newUserId, function(err, result){
                newUser = result;
                seriesCallback(null, 'success');
            });
        }//returnnewUser
    },//final callback
    function(err, results) {
        callback(null, newUser);
    });//series
};//create


//
function updateAddresses(billingAddressId, shippingAddressId, userId, callback){
    console.log('updateAddresses');
    //var newUserId;
    var updatedUser;
    async.series({
        createOrder : function(seriesCallback){
            pool.query(sql.update.addresses, [billingAddressId, shippingAddressId, userId], function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                }
                //newUserId = rows.insertId;
                seriesCallback(null, 'success');
            });
        },//createOrder
        returnUpdatedUser : function(seriesCallback){
            findById(userId, function(err, result){
                updatedUser = result;
                seriesCallback(null, 'success');
            });
        }//returnUpdatedUser
    },//final callback
    function(err, results) {
        callback(null, updatedUser);
    });//series
};//updateAddresses
////////////////////
module.exports = function () {
    return {
        findById : findById,
        findByUserName : findByUserName,
        create : create,
        updateAddresses : updateAddresses
    }
};