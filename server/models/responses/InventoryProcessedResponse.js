module.exports = function InventoryProcessedResponse() {
    this.status;
    this.totalItems;
    this.noCertItems;
    this.lowQualityItems;
    this.eglIsraelItems;
    this.itemsImported;
    this.duplicateItems = [];
    this.errors = [];
}