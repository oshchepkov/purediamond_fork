module.exports = function Clarity(id, rank, name, friendly_name, description, assigned) {
    this.id = id;
    this.rank = rank;
    this.name = name;
    this.friendly_name = friendly_name;
    this.description = description;
    this.assigned = assigned;
}