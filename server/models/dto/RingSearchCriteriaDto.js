module.exports = function RingSearchCriteriaDto() {
	this.collections = [];
	this.designers = [];
	this.metals = [];
    this.shapes = [];
    this.priceRange;

    this.errors = [];
}