module.exports = function Ring(id, title, lot_number, price, stone_weight_min, stone_weight_max, description, instagram, published, category, featured, details, discount, sale, image) {
    this.id = id;
    this.title = title;
    this.lot_number = lot_number;
    this.price = price;
    this.stone_weight_min = stone_weight_min;
    this.stone_weight_max = stone_weight_max;
    this.description = description;
    this.instagram = instagram;
    this.instagramHeight = 0;
    this.published = published;
    this.category = category;
    this.featured = featured;
    this.details = details;
    this.image = image;
    this.discount = discount;
    this.sale = sale;

    this.designers = [];
    this.metals = [];
    this.shapes = [];
    this.collections = [];
    this.images = [];

}
