module.exports = function SearchCriteria() {
    this.shapes = [];
    this.priceRange;
    this.caratRange;
    this.cuts = [];
    this.clarities = [];
    this.colors = [];
    this.errors = [];
    this.sortingOrder;
    this.sortingOrderDirection; //asc, desc
    this.resultsPerPage;
    this.limitFrom;
    this.limitTo;
    this.userSelection;
}