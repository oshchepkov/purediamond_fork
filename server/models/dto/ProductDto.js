module.exports = function Product(id, title, lot_number, price, stone_weight_min, stone_weight_max, description, instagram, youtube, published, category, featured, details, discount, sale, image, metal_id) {
    this.id = id;
    this.title = title;
    this.lot_number = lot_number;
    this.price = price;
    this.stone_weight_min = stone_weight_min;
    this.stone_weight_max = stone_weight_max;
    this.description = description;
    this.instagram = instagram;
    this.youtube = youtube;
    this.instagramHeight = 0;
    this.published = published;
    this.category = category;
    this.featured = featured;
    this.details = details;
    this.image = image;
    this.discount = discount;
    this.sale = sale;
    this.metal_id = metal_id;

    this.designers = [];
    this.metals = [];
    this.shapes = [];
    this.collections = [];
    this.images = [];

}
