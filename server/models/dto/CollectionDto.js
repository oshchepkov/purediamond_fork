module.exports = function Collection(id, rank, name, description, assigned) {
    this.id = id;
    this.rank = rank;
    this.name = name;
    this.description = description;
    this.assigned = assigned;
}
