module.exports = function Page(id, slug, title, subtitle, menu_title, published) {
    this.id = id;
    this.slug = slug;
    this.title = title;
    this.subtitle = subtitle;
    this.menu_title = menu_title;
    this.published = published;
}
