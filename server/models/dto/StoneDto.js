module.exports = function Stone(
    id, 
    stock_number, 
    shape, 
    shape_class_name, 
    weight, 
    color, 
    clarity, 
    cut, 
    polish, 
    symmetry, 
    measurements, 
    table, 
    lab, 
    cert_number, 
    cert_file, 
    image, 
    price, 
    depth, 
    girdle, 
    fluorescence, 
    category, 
    cert_link,
    specials,
    discount,
    youtube,
    rapnet_price, 
    rapnet_discount,
    country
    ) {
        this.id = id;
        this.stock_number = stock_number;
        this.shape = shape;
        this.shape_class_name = shape_class_name;
        this.weight = weight;
        this.color = color;
        this.clarity = clarity;
        this.cut = cut;
        this.polish = polish;
        this.symmetry = symmetry;
        this.measurements = measurements;
        this.table = table;
        this.lab = lab;
        this.cert_number = cert_number;
        this.cert_file = cert_file;
        this.image = image;
        this.price = price;
        this.depth = depth;
        this.girdle = girdle;
        this.fluorescence = fluorescence;
        this.category = category;
        this.rapnet_price = rapnet_price;
        this.rapnet_discount = rapnet_discount;
        this.country = country;
        this.specials = specials;
        this.discount = discount;
        this.youtube = youtube;

        this.shapes = [];
        this.colors = [];
        this.clarities = [];
        this.cuts = [];
        this.labs = [];
        this.polishes = [];
        this.symmetries = [];
        this.fluorescences = [];
        this.images = [];

        //Optional
        this.cert_link = cert_link;
}
