module.exports = function Image(id, file_name, url, thumb_url, description, favorite) {
    this.id = id;
    this.file_name = file_name;
    this.url = url;
    this.thumb_url = thumb_url;
    this.description = description;
    this.favorite = favorite;
}
