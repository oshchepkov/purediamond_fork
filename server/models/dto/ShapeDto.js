module.exports = function Shape(id, rank, name, friendly_name, description, class_name, assigned) {
    this.id = id;
    this.rank = rank;
    this.name = name;
    this.friendly_name = friendly_name;
    this.description = description;
    this.class_name = class_name;
    this.assigned = assigned;
}
