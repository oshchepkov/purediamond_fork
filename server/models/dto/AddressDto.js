module.exports = function AddressDto(id, firstName, lastName, line1, line2, country, province, city, postalCode, phone) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.line1 = line1;
    this.line2 = line2;
    this.country = country;
    this.province = province;
    this.city = city
    this.postalCode = postalCode;
    this.phone = phone;
}
