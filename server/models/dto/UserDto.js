module.exports = function UserDto(id, userName, firstName, lastName, phone, subscribed) {
    this.id = id;
    this.userName = userName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phone = phone;
    this.subscribed = subscribed;
	this.billingAddress;
    this.shippingAddress;

}
