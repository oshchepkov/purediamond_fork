module.exports = function Order(id, date, subtotal, tax, total, contents, status, stone_id, ring_id, user_id, ring_size, billing_address, shipping_address, products) {
    this.id = id;
    this.date = date;
    this.subtotal = subtotal;
    this.tax = tax;
    this.total = total;
    this.contents = contents;
    this.status = status;
    this.stone_id = stone_id;
    this.ring_id = ring_id;
    this.user_id = user_id;
    this.ring_size = ring_size;
    this.billing_address = billing_address;
    this.shipping_address = shipping_address;
    this.products = products;
}
