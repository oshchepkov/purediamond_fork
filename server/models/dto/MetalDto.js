module.exports = function Metal(id, rank, name, assigned) {
    this.id = id;
    this.rank = rank;
    this.name = name;
    this.assigned = assigned;
}
