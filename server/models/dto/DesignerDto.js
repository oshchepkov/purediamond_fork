module.exports = function Designer(id, name, slug, image, description, ring_id, ring_category, assigned) {
    this.id = id;
    this.name = name;
    this.slug = slug;
    this.image = image;
    this.description = description;
    this.ring_id = ring_id;
	this.ring_category = ring_category;
    this.assigned = assigned;
}
