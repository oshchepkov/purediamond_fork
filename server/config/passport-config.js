var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;
var User = require('../api/auth/User');
var app = require('../server')


var myUser = new User();
module.exports = passport;



 passport.serializeUser(function(user, done) {
    done(null, user.id)
  })

  passport.deserializeUser(function(id, done) {
    myUser.deserializeUser(id, function (err, user) {
      done(err, user)
    })
  })

//console.log('passport-config');
//console.log(myUser);

passport.use(new LocalStrategy(
  function(username, password, done) {
  	console.log('Looking for user');
    myUser.getUser(username, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
      	console.log('Invalid username');
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!myUser.validPassword(password)) {
      	console.log('Invalid password');
        return done(null, false, { message: 'Incorrect password.' });
      }
      console.log('All good');
      return done(null, user);
    });
  }
));//strategy
