var path = require('path');
var mysql = require('mysql');

var config = {}

config.db = {};

//Production setup
config.db.host = 'localhost';
config.db.port = '3306';
config.db.user = 'purediamond_node';
config.db.password = 'Sd2hfSCYhZXCBZWK';
config.db.database = 'purediamond_node';


// //MAMP setup
// config.db.host = 'localhost';
// config.db.user = 'purediamond_node';
// config.db.password = 'Sd2hfSCYhZXCBZWK';
// config.db.database = 'purediamond_node';
// config.db.socketPath = '/Applications/MAMP/tmp/mysql/mysql.sock';


//configuring mysql connection pool
config.db.pool = mysql.createPool({
    connectionLimit: 10,
    host: config.db.host,
    port: config.db.port,
    user: config.db.user,
    socketPath: config.db.socketPath,
    password: config.db.password,
    database: config.db.database
});

config.session = {}
config.session.options = {
	host: config.db.host,
    port: config.db.port,
    user: config.db.user,
    socketPath: config.db.socketPath,
    password: config.db.password,
    database: config.db.database,
    checkExpirationInterval: 900000, //15min
    expiration: 86400000 //24hrs
}

config.path = {};

//This is a temporary folder where inventory files are being uploaded
config.path.stonesInventoryStorage = path.join(__dirname, '../storage/inventory/temporary/');

//this is where server writes files
config.path.productsImagesStorage = path.join(__dirname, '../static/images/products/');
config.path.ImagesStorage = path.join(__dirname, '../static/images/');

config.path.stonesImagesStorage = path.join(__dirname, '../static/images/stones/');

//this is how images will be accessed from client app
config.path.ringsImagesUrl = '/static/images/products';// + filename.jpg
config.path.stonesImagesUrl = '/static/images/stones';// + filename.jpg

config.stoneSearch = {}
config.stoneSearch.defaultSortingOrder = 'total_price' //exact name of a field in DB
config.stoneSearch.defaultLimitFrom = 0
config.stoneSearch.defaultLimitTo = 20

module.exports = config;
