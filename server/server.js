var express = require('express');
var busboy = require('connect-busboy');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport')
var MySQLSessionStore = require('express-mysql-session')
var config = require('./config/config');

var api = require('./api/api');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(busboy());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(cookieParser('bUYWE827@*T@*'));
app.use(session({key:'pd.session',
                 secret: 'Hftwu662%JHSjg*8%5', 
                 saveUninitialized: true,
                 resave: true,
                 store: new MySQLSessionStore(config.session.options)
                }));
app.use(passport.initialize());
app.use(passport.session());


//app.use(cookieParser());
//app.use('/api', api); //!!!! change it to root '/' when deploying to production !!!!
app.use('/', api); //!!!uncomment for production!!!
//the line below is for dev purposes only! The server when in dev is intended to serve web apps to avoid x-domain requests
//app.use(express.static(path.join(__dirname, '../web_app'))); //uncomment for running web app locally
//app.use(express.static(path.join(__dirname, '../web_app/admin'))); //uncomment for running admin app locally
app.use('/static', express.static(path.join(__dirname, '/static'))); //uncomment for running admin app locally





/// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
/// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    console.log('Running in development mode')
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
