var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/pagesSql');

module.exports = router;

//Importing models
var Page = require('../models/dto/PageDto');
var ErrorForResponse = require('../models/responses/ErrorForResponse');
var PageResponse = require('../models/responses/PageResponse');
var PagesResponse = require('../models/responses/PagesResponse');
var GenericResponse = require('../models/responses/GenericResponse');


//get all existing pages

router.get('/', function(req, res) {

	console.log('getting pages');

  var dir = new PagesResponse();
  dir.status = 'success';


  pool.query(sql.select.pages, function(err, rows, fields) {
      if (err) {
        dir.errors.push(new ErrorForResponse(err.message, 'Error selecting data from the database'));
      }
      for (var i in rows) {
        var page = new Page(rows[i].id, rows[i].slug, rows[i].title, rows[i].subtitle, rows[i].menu_title, Boolean(rows[i].published))
        dir.pages.push(page);
      }
      res.send(dir);

    });


}); //router

router.get('/page', function(req, res) {
  //console.log(req.query);
  var dtr = new PageResponse();
  dtr.status = 'success';
  var pageSlug = req.query.slug;
  if (typeof pageId === 'undefined') {
    pageId = null;
    var page = new Page(null, null, null, null, null, null);
    page.id = null;
    dtr.page = page;
  }
  async.series({
      page: function(seriesCallback) {
        pool.query(sql.select.pageBySlug, [pageSlug], function(err, rows, fields) {
          if (err) {
            dtr.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
            dtr.status = 'failure';
          }
          //console.log(rows);
          for (var i in rows) {
            var page = new Page(rows[i].id, rows[i].slug, rows[i].title, rows[i].subtitle, rows[i].menu_title, Boolean(rows[i].published))
            dtr.page = page;
          } //for i in rows
          seriesCallback(null, 'success');
        });
      }
    }, //final callback
    function(err, results) {
      res.send(dtr);
    }); //series
}); //router

router.put('/page', function(req, res) {
  //console.log(req.query);
  var gr = new GenericResponse();
  gr.status = 'success';
  var updatedPage = req.body;
  var pageId = req.query.id;

  async.series({
      updatePage: function(seriesCallback) {
        pool.query(sql.update.page, [updatedPage.slug, updatedPage.title, updatedPage.subtitle, updatedPage.menu_title, Boolean(updatedPage.published), pageId], function(err, rows, fields) {
          if (err) {
            gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
            gr.status = 'failure';
            res.send(gr);
          } else {
            seriesCallback(null, 'success');
          }
        });
      },
    }, //final callback
    function(err, results) {
      gr.message = 'Page successfully updated'
      res.send(gr);
    }); //series
}); //router
