var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/stoneSearchSql');

module.exports = router; 

//Importing models
var Shape = require('../models/domain/Shape');
var Cut = require('../models/domain/Cut');
var Clarity = require('../models/domain/Clarity');
var Color = require('../models/domain/Color');
var Lab = require('../models/domain/Lab');
var PriceRange = require('../models/domain/PriceRange');
var CaratRange = require('../models/domain/CaratRange');
var StoneSearchCriteria = require('../models/domain/StoneSearchCriteria');
var Stone = require('../models/domain/Stone');
var StoneDto = require('../models/dto/StoneDto');

var ErrorForResponse = require('../models/responses/ErrorForResponse');
var SearchStonesResponse = require('../models/responses/SearchStonesResponse');


/*********************************************************************
 *GET searchCriteria.
 **********************************************************************/
router.get('/', function (req, res) {
    var stoneSearchCriteria = new StoneSearchCriteria()
    stoneSearchCriteria.find(req.session.stonesSearchUserSelection, function(err, result){
        if (err){
            console.log(err)
        }
        res.send(result);
    });
});//GET searchCriteria

/*********************************************************************
 POST searchCriteria.
 *********************************************************************/
 router.post('/', function (req, res) {

    var searchStonesResponse = new SearchStonesResponse();
    var stone = new Stone();
    var searchCriteria = req.body;

    //recording user selection to the session, make it null first so it doesn't cascade inside the object
    req.session.stonesSearchUserSelection = null;
    req.session.stonesSearchUserSelection = searchCriteria;

    async.series({
        getSearchResultsCount : function(seriesCallback){
            stone.findAllBySearchCriteria(true, searchCriteria, function(err, result){
                if (err){
                    console.log(err)
                    searchStonesResponse.errors.push(new ErrorForResponse(err.message, 'Error selecting data from the database'));
                }
                searchStonesResponse.searchResultsCount = result
                seriesCallback(null, 'success');
            });
        },//getSearchResultsCount

        findStones : function(seriesCallback){
            stone.findAllBySearchCriteria(false, searchCriteria, function(err, result){
                if (err){
                    console.log(err)
                    searchStonesResponse.errors.push(new ErrorForResponse(err.message, 'Error selecting data from the database'));
                }
                searchStonesResponse.stones = result
                seriesCallback(null, 'success');
            });
        }//findStones
    },//final callback
    function(err, results) {
        res.send(searchStonesResponse);
    });//series

});//POST searchCriteria



router.get('/property', function (req, res) {

     var stoneResp = {
        errors: [],
        status: 'success',
        stones: []
    };
    var stone = new Stone();
    var property = req.query.property;
    //var propValue = req.query.propValue;
    var query = 'SELECT stones.*, '+
        'shapes.name AS shape, shapes.class_name AS shape_class_name, '+ 
        'colors.name AS color, '+
        'clarities.name AS clarity, '+
        'polishes.friendly_name AS polish, '+
        'cuts.friendly_name AS cut, '+
        'symmetries.name AS symmetry, '+
        'labs.friendly_name AS lab, total_price AS price '+
        'FROM stones '+
        'LEFT JOIN (shapes, colors, clarities, polishes, symmetries, labs, cuts) '+
        'ON (shapes.id=stones.shape_id '+
            'AND colors.id=stones.color_id '+
            'AND clarities.id=stones.clarity_id '+
            'AND polishes.id=stones.polish_id '+
            'AND symmetries.id=stones.symmetry_id '+
            'AND labs.id=stones.lab_id '+
            'AND cuts.id=stones.cut_id) '+       
        'WHERE stones.'+property+' = 1 ';



    async.series({
        findStones : function(seriesCallback){
            pool.query(query+sql.where.orderByDesc, ['stones.id'], function (err, rows, fields) {
                if (err) {
                    console.log('sql err ', err);
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                
                async.each(rows, function(row, eachCallback) {
                    var stone = new StoneDto(
                        row.id, 
                        row.stock_number, 
                        row.shape, 
                        row.shape_class_name, 
                        row.weight, 
                        row.color, 
                        row.clarity, 
                        row.cut, 
                        row.polish, 
                        row.symmetry, 
                        row.measurements, 
                        row.table_percent, 
                        row.lab, 
                        row.certificate_number, 
                        row.certificate_filename, 
                        row.diamond_image, 
                        row.price, 
                        row.depth, 
                        row.girdle,
                        row.fluorescence,
                        'diamonds',
                        '',
                        Boolean(row.specials),
                        row.discount,
                        row.rapnet_price,
                        row.rapnet_discount,
                        row.country
                    );
                    pool.query('SELECT stones_images.*, images.* FROM stones_images JOIN images ON images.id=stones_images.image_id WHERE stones_images.stone_id='+row.id+'',function (err, rows, fields) {
                        stone.images = rows; 
                        stoneResp.stones.push(stone); 
                        eachCallback();                     
                    })
                    
                }, function(err){
                    seriesCallback(null, 'success');
                })
                
            });
        }//findStones
    },//final callback
    function(err, results) {
        res.send(stoneResp);
    });//series

});//POST searchCriteria


