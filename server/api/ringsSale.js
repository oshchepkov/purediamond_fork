var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/ringSearchSql');

module.exports = router;

//Importing models
var Ring = require('../models/domain/Ring');
var SearchRingsResponse = require('../models/responses/SearchRingsResponse');

/*********************************************************************
 *GET stoneDetails
 **********************************************************************/

router.get('/', function(req, res) {
  var ring = new Ring();
  var srr = new SearchRingsResponse();
  srr.status = 'success'

  ring.findSale(function(err, result){
      if (err){
          srr.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
          srr.status = 'failure'
      }else{
          srr.rings = result
      }
      //console.log(srr)
      res.send(srr)
  });


});
