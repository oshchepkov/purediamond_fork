var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/stoneSearchSql');

module.exports = router;

//Importing models
var Stone = require('../models/dto/StoneDto');
var Image = require('../models/dto/ImageDto');
var StoneDetailsResponse = require('../models/responses/StoneDetailsResponse');

var imagePath = config.path.stonesImagesStorage;
var imageUrl = config.path.stonesImagesUrl;

/*********************************************************************
 *GET stoneDetails
 **********************************************************************/

router.get('/', function(req, res) {

  var stoneDetailsResponse = new StoneDetailsResponse();

  //console.log(req.query);
  //console.log("Connecting to a database...");
  async.series({
      stone : function(seriesCallback){
        pool.query(sql.select.stones + sql.where.forDetails, [req.query.lot],
          function(err, rows, fields) {
          if (err) {
            stoneDetailsResponse.errors.push(new ErrorForResponse(err.message, 'Error selecting data from the database'));
          }
          //console.log(rows);
          var storageUrl = '';
          for (var i in rows) {
            var certUrl;

            var cert_link = '';
            if(rows[i].certificate_number) {
              switch (rows[i].lab) {
                  case "GIA":
                      cert_link = 'http://www.gia.edu/report-check?reportno=' + rows[i].certificate_number;
                  break;

                  case "EGL USA":
                      cert_link = 'http://www.eglusa.com/verify-a-report-results/?st_num=' + rows[i].certificate_number;
                  break;

                  case "HRD":
                      cert_link = 'http://www.hrdantwerplink.be/?record_number=' + rows[i].certificate_number + '&weight=' + rows[i].weight + '&L=';
                  break;

                  case "IGI":
                      cert_link = 'http://www.igiworldwide.com/verify.php?r=' + rows[i].certificate_number;
                  break;

                  default:
                  cert_link = '';
              }
            }

            var stone = new Stone(rows[i].id, rows[i].stock_number, rows[i].shape, rows[i].shape_class_name, rows[i].weight, rows[i].color,
              rows[i].clarity, rows[i].cut, rows[i].polish, rows[i].symmetry,
              rows[i].measurements, rows[i].table_percent, rows[i].lab, rows[i].certificate_number,
              storageUrl + rows[i].certificate_filename, storageUrl + rows[i].diamond_image,
              rows[i].price, rows[i].depth, rows[i].girdle, rows[i].fluorescence, 'diamonds', cert_link, rows[i].specials, rows[i].discount, rows[i].youtube);

            stoneDetailsResponse.stone = stone;
            seriesCallback(null, 'success');

          }
        })
      },
      images : function(seriesCallback) {
        var stoneId = stoneDetailsResponse.stone.id;
        pool.query(sql.select.stoneImages, [stoneId], function (err, rows, fields) {
            if (err) {
                stoneDetailsResponse.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                stoneDetailsResponse.status = 'failure';
            }
            for (var i in rows) {
                //console.log(JSON.stringify(rows[i]));
                var image = new Image(rows[i].id, rows[i].file_name, imageUrl+'/'+stoneId+'/'+rows[i].file_name, imageUrl+'/'+stoneId+'/thumb/'+rows[i].file_name, rows[i].description, Boolean(rows[i].favorite));
                stoneDetailsResponse.stone.images.push(image);
            }              
            seriesCallback(null, 'success');
        });
        
      }
    },
    function(err, results) {
        stoneDetailsResponse.message = 'Stone';
        res.send(stoneDetailsResponse);
    });//series
  


      //res.send(stoneDetailsResponse);



});