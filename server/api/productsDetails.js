var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/ringSearchSql');

module.exports = router;

//Importing models
var Product = require('../models/domain/Product');
var RingDetailsResponse = require('../models/responses/RingDetailsResponse');

/*********************************************************************
 *GET stoneDetails
 **********************************************************************/

router.get('/', function(req, res) {
  var product = new Product();
  var rdr = new RingDetailsResponse();
  rdr.status = 'success'
  var productId = req.query.id

  product.findById(productId, function(err, result){
      if (err){
          rdr.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
          rdr.status = 'failure'
      }else{
          rdr.ring = result
      }
      //console.log(rdr)
      res.send(rdr)
  });


});
