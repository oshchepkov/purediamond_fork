var config = require('../../config/config');
//var sql = require('../sql/stoneSearchSql');
var pool = config.db.pool;

function getUser(username, callback){
	console.log('getUser '+username);
	pool.query("SELECT * FROM users WHERE username = ?", [username],
        function (err, rows, fields) {
            if (err) {
                console.log(err);
            }
            console.log(rows[0]);
			callback(null, rows[0]);
        });
};

function saveUser(data, callback){
    console.log(data.body);
    pool.query('INSERT INTO users (username, password, firstname, lastname) VALUES (?, ?, ?, ?)', [data.body.email, data.body.password, data.body.firstName, data.body.lastName],
        function (err, rows, fields) {
            if (err) {
                console.log(err);
            }
            console.log(rows);
            callback(null, rows);
        });
};

function validPassword(user, password) {
    console.log('validPassword '+ user);
    return password == user.password;
}

function serializeUser(user, callback) {
    //console.log('serializeUser');
  callback(null, user.id);
};

function deserializeUser(id, callback) {
        //console.log('deserializeUser');
        pool.query('SELECT * FROM users WHERE id = ?', [id],
            function (err, rows, fields) {
                if (err) {
                    console.log(err);
                }
                //console.log(rows[0]);
                callback(null, rows[0]);
            });


};

module.exports = function () {
    return {
        serializeUser: serializeUser,
        deserializeUser: deserializeUser,
        validPassword: validPassword,
        getUser: getUser,
        saveUser: saveUser
    }
};