var express = require('express');
var router = express.Router();
var passport = require('../../config/passport-config')
var User = require('../../api/auth/User')

module.exports = router; 


//console.log('auth');

//Importing models
//var Stone = require('../models/Stone');
//var StoneDetailsResponse = require('../models/responses/StoneDetailsResponse');

/*********************************************************************
 *Auth
 **********************************************************************/

router.get('/api/login', function (req, res) {
    res.send('login');
});

router.post('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
		if (err || !user) {
			res.send({err:info});
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

			req.login(user, function(err) {
				if (err) {
					res.status(400).send(err);
				} else {
					res.jsonp(user);
				}
			});
		}
	})(req, res, next);
});

router.post('/logout', function(req, res){
  req.logout();
  //res.redirect('/');
  res.send('Logged out');
});

router.post('/signup', function(req, res) {
  // For security measurement we remove the roles from the req.body object
	delete req.body.roles;

	// Init Variables
	var user = new User(req.body);
	var message = null;

	// Then save the user 
	user.saveUser(req, function(err) {
		if (err) {
			return res.status(400).send({
				message: err
			});
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

			req.login(user, function(err) {
				if (err) {
					res.status(400).send(err);
				} else {
					res.jsonp(user);
				}
			});
		}
	});
});




