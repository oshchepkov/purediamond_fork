var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/designersInventorySql');

module.exports = router;

//Importing models
var Designer = require('../models/dto/DesignerDto');
var ErrorForResponse = require('../models/responses/ErrorForResponse');
var DesignersInventoryResponse = require('../models/responses/DesignersInventoryResponse');
var DesignerTemplateResponse = require('../models/responses/DesignerTemplateResponse');
var GenericResponse = require('../models/responses/GenericResponse');


//get all existing designers 

router.get('/', function(req, res) {

	console.log('getting designers');

  //1. Get all rings from DB
  //2. Assing designer, metals, shapes, collections

  var dir = new DesignersInventoryResponse();
  dir.status = 'success';


  pool.query(sql.select.designers, function(err, rows, fields) {
      if (err) {
        dir.errors.push(new ErrorForResponse(err.message, 'Error selecting data from the database'));
      }
      for (var i in rows) {
        var designer = new Designer(rows[i].id, rows[i].designer_name, rows[i].designer_slug, rows[i].ring_image, rows[i].designer_description, rows[i].ring_id)
        dir.designers.push(designer);
      }
      res.send(dir);

    });

  
}); //router





router.get('/designer', function(req, res) {
  //console.log(req.query);
  var dtr = new DesignerTemplateResponse();
  dtr.status = 'success';
  var designerId = req.query.id;
  if (typeof designerId === 'undefined') {
    designerId = null;
    var designer = new Designer('null', null, null, null, null);
    designer.id = null;
    dtr.designer = designer;
  }
  async.series({
      designer: function(seriesCallback) {
        pool.query(sql.select.designerById, [designerId], function(err, rows, fields) {
          if (err) {
            dtr.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
            dtr.status = 'failure';
          }
          //console.log(rows);
          for (var i in rows) {
            var designer = new Designer(rows[i].id, rows[i].name, rows[i].slug, rows[i].image, rows[i].description, rows[i].id, rows[i].category, rows[i].assigned);
            dtr.designer = designer;
          } //for i in rows
          seriesCallback(null, 'success');
        });
      }
    }, //final callback
    function(err, results) {
      res.send(dtr);
    }); //series
}); //router
//adding a new designer
router.post('/designer', function(req, res) {
  //console.log(req.body);
  var gr = new GenericResponse();
  gr.status = 'success';
  var newDesigner = req.body;
  //setting default values to be added to the database
  var newDesignerId;
  async.series({
      insertDesigner: function(seriesCallback) {
        pool.query(sql.insert.newDesigner, [newDesigner.name, newDesigner.slug, newDesigner.image, newDesigner.description], function(err, rows, fields) {
          if (err) {
            gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
            gr.status = 'failure';
            res.send(gr);
          } else {
            //console.log(rows);
            newDesignerId = rows.insertId;
            //console.log('newDesignerId: '+newDesignerId);
            seriesCallback(null, 'success');
          }
        });
      }, //designer
    }, //final callback
    function(err, results) {
      gr.message = 'Designer successfully added'
      res.send(gr);
    }); //series
}); //router
//editing the designer
router.put('/designer', function(req, res) {
  //console.log(req.query);
  var gr = new GenericResponse();
  gr.status = 'success';
  var updatedDesigner = req.body;
  var designerId = req.query.id;

  async.series({
      updateDesigner: function(seriesCallback) {
        pool.query(sql.update.designer, [updatedDesigner.name, updatedDesigner.slug, updatedDesigner.image, updatedDesigner.description, designerId], function(err, rows, fields) {
          if (err) {
            gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
            gr.status = 'failure';
            res.send(gr);
          } else {
            seriesCallback(null, 'success');
          }
        });
      }, //updateDesigner
    }, //final callback
    function(err, results) {
      gr.message = 'Designer successfully added'
      res.send(gr);
    }); //series
}); //router
//deleting the designer
router.delete('/designer', function(req, res) {
  var gr = new GenericResponse();
  gr.status = 'success';
  var designerId = req.query.id;
  if (typeof designerId === 'undefined') {
    gr.errors.push(new ErrorForResponse(err.message, 'Designer ID is not provided'));
    res.send(400, gr);
  }
  pool.query(sql.delete.designer, [designerId], function(err, rows, fields) {
    if (err) {
      gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
      gr.status = 'failure';
    }
    res.send(gr);
  });
}); //router