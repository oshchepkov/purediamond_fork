var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');

module.exports = router;

//Importing models
var Ring = require('../models/domain/Ring');
var Designer = require('../models/domain/Designer');
var ErrorForResponse = require('../models/responses/ErrorForResponse');
var SearchRingsResponse = require('../models/responses/SearchRingsResponse');

/*********************************************************************
 *GET stoneDetails
 **********************************************************************/

router.get('/', function(req, res) {
  var designer = new Designer();
  var slug = req.query.slug;
  var srr = new SearchRingsResponse();
  srr.status = 'success'

  designer.findBySlug(slug, function(err, result){
    if (err){
      srr.errors.push(new ErrorForResponse(err, 'Error reading data from database'))
      srr.status = 'failure';
    } else {
      srr.rings = result;
    }
    res.send(srr);
  });
  

});