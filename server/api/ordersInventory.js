var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;

module.exports = router;

//Importing models
var Order = require('../models/domain/Order');
var User = require('../models/domain/User');
var OrdersInventoryResponse = require('../models/responses/OrdersInventoryResponse');
var order = new Order();

//get all existing orders 

router.get('/', function(req, res) {

	console.log('getting orders');

  var oir = new OrdersInventoryResponse();

  order.findAll( function(err, result){
    
    if (err) {
      oir.errors.push('Error selecting data from the database');
    }
    oir.orders = result;
    
    res.send(oir);
  });

  
}); //router

//deleting the designer
router.delete('/order', function(req, res) {
  var data = {};
  data.status = 'Ok';
  var orderId = req.query.id;
  if (typeof orderId === 'undefined') {
    //gr.errors.push(new ErrorForResponse(err.message, 'Designer ID is not provided'));
    res.send(400, 'Order ID is not provided');
  }
  order.deleteOrder(orderId, function(err){    
    if (err) {
      data.status = 'Fail';
    }    
    res.send(data);
  })
  // pool.query(sql.delete.order, [orderId], function(err) {
  //   if (err) {
  //     //gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
  //     order.status = 'failure';
  //   }
  //   res.send(order);
  // });
}); //router