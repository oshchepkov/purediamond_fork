var sql = {};

/*************
* SELECT FROM
**************/
sql.select = {};

sql.select.collections = 'SELECT * FROM collections ORDER BY rank asc';
sql.select.collectionById = 'SELECT * FROM collections WHERE id = ?';
sql.select.collectionsByRing = 'SELECT collections.* FROM collections '+
							'LEFT JOIN (rings_collections) '+
							'ON (rings_collections.collection_id = collections.id) '+
							'WHERE rings_collections.ring_id = ? '+
							'ORDER BY rank asc';

sql.select.designers = 'SELECT * FROM designers';
sql.select.designersByRing = 'SELECT designers.* FROM designers '+
							'LEFT JOIN (rings_designers) '+
							'ON (rings_designers.designer_id = designers.id) '+
							'WHERE rings_designers.ring_id = ? '+
							'ORDER BY id asc';

sql.select.metals = 'SELECT * FROM metals ORDER BY rank asc';
sql.select.metalsByRing = 'SELECT metals.* FROM metals '+
							'LEFT JOIN (rings_metals) '+
							'ON (rings_metals.metal_id = metals.id) '+
							'WHERE rings_metals.ring_id = ? '+
							'ORDER BY rank asc';

sql.select.shapes = 'SELECT * FROM shapes WHERE visible = true ORDER BY rank asc';
sql.select.shapesByRing = 'SELECT shapes.* FROM shapes '+
							'LEFT JOIN (rings_shapes) '+
							'ON (rings_shapes.shape_id = shapes.id) '+
							'WHERE rings_shapes.ring_id = ? '+
							'ORDER BY rank asc';

sql.select.price_range = 'SELECT MIN(price) AS min_price, MAX(price) AS max_price FROM rings';


sql.select.rings = 'SELECT distinct rings.* FROM rings '+
			        'LEFT JOIN (rings_metals, rings_designers, rings_shapes) '+
			        'ON (rings_metals.ring_id = rings.id '+
			        	'AND rings_designers.ring_id = rings.id '+
			        	'AND rings_shapes.ring_id = rings.id) ';

sql.select.ringsByDesignerSlug = 'SELECT distinct rings.*, images.file_name as image FROM rings '+
					'LEFT JOIN (rings_designers) '+
					'ON (SELECT id FROM designers WHERE LOWER(designers.slug) LIKE ?) = rings_designers.designer_id '+
					'LEFT JOIN (images) ON (SELECT image_id FROM rings_images WHERE ring_id = rings.id AND favorite = 1) = images.id '+
					'WHERE rings.id IN (rings_designers.ring_id)';

/*******
* WHERE
********/
sql.where = {};

sql.where.forSearch ='WHERE rings.published = 1 '+
					'AND rings_metals.metal_id IN (?) '+
					'AND rings_designers.designer_id IN (?) '+
					'AND rings_shapes.shape_id IN (?)';
					//'AND price BETWEEN ? AND ?';

sql.where.byId = 'WHERE rings.id = ?';

sql.where.featured = 'WHERE rings.featured = 1 AND rings.published = 1';

sql.where.sale = 'WHERE rings.sale = 1 AND rings.published = 1';

/*************
* INSERTS
**************/
sql.insert = {};

/**********
* UPDATES
***********/
sql.update = {};

/**********
* DELETES
***********/
sql.delete = {};

///////////////////////////////////
module.exports = sql;
