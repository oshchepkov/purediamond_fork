var sql = {};

/*************
* SELECT FROM
**************/
sql.select = {};

sql.select.designers = 'SELECT designers.id, designers.name as `designer_name`, designers.slug as `designer_slug`, designers.description as `designer_description`, images.file_name as `ring_image`, rings_designers.designer_id,	rings_designers.ring_id, rings_designers.designer_id FROM designers '+
						'LEFT JOIN rings_designers '+
						'ON (designers.id = rings_designers.designer_id) '+
						'LEFT JOIN rings_images '+
						'ON (rings_designers.ring_id = rings_images.ring_id) '+
						'LEFT JOIN images '+
						'ON (rings_images.image_id = images.id) '+
						'GROUP BY designers.id ORDER BY designers.id DESC';

//sql.select.designerById = 'SELECT * FROM designers WHERE id = ?';
sql.select.designerById = 'SELECT distinct rings.*, images.file_name as image FROM rings '+
					'LEFT JOIN (rings_designers) '+
					'ON rings_designers.designer_id = ? '+
					'LEFT JOIN (images) ON (SELECT image_id FROM rings_images WHERE ring_id = rings.id AND favorite = 1) = images.id '+
					'WHERE rings.id IN (rings_designers.ring_id)';

/*******
* WHERE
********/
sql.where = {};


/*************
* INSERTS
**************/
sql.insert = {};

sql.insert.newDesigner ='INSERT INTO designers (name, slug, image, description) '+
					'VALUES (?, ?, ?, ?)';


/**********
* UPDATES
***********/
sql.update = {};

sql.update.designer = 	'UPDATE designers SET name = ?, slug = ?, image = ?, description = ? WHERE id = ?';


/**********
* DELETES
***********/
sql.delete = {};

sql.delete.designer = 'DELETE FROM designers WHERE id = ?';

///////////////////////////////////
module.exports = sql;
