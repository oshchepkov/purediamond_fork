var sql = {};

/*************
* SELECT FROM
**************/
sql.select = {};

sql.select.shapes = 'SELECT * FROM shapes WHERE visible = TRUE ORDER BY rank ASC';
sql.select.cuts = 'SELECT * FROM cuts WHERE rank < 100 ORDER BY rank ASC';
sql.select.clarities = 'SELECT * FROM clarities WHERE rank < 100 ORDER BY rank ASC';
sql.select.colors = 'SELECT * FROM colors WHERE rank < 100 ORDER BY rank ASC';
sql.select.labs = 'SELECT * FROM labs WHERE rank < 100 ORDER BY rank ASC';

sql.select.colorsByNames = 'SELECT * FROM colors WHERE name IN (?) ORDER BY rank ASC';
sql.select.price_range = 'SELECT MIN(total_price) AS min_price, MAX(total_price) AS max_price from stones WHERE total_price > 0';
sql.select.carat_range = 'SELECT MIN(weight) AS min_weight, MAX(weight) AS max_weight from stones';

sql.select.stones = 'SELECT stones.id, stock_number, specials, discount, youtube, '+
        'shapes.name AS shape, '+
        'shapes.class_name AS shape_class_name, '+
        'colors.name AS color, '+
        'clarities.name AS clarity, '+
        'polishes.friendly_name AS polish, '+
        'cuts.friendly_name AS cut, '+
        'symmetries.name AS symmetry, '+
        'fluorescences.name AS fluorescence, '+
        'depth, girdle, '+
        'weight, measurements, table_percent, '+
        'labs.friendly_name AS lab, certificate_number, certificate_filename, diamond_image, total_price AS price '+
        'FROM stones '+
        'LEFT JOIN (shapes, colors, clarities, polishes, symmetries, labs, cuts, fluorescences) '+
        'ON (shapes.id=stones.shape_id '+
            'AND colors.id=stones.color_id '+
            'AND clarities.id=stones.clarity_id '+
            'AND polishes.id=stones.polish_id '+
            'AND symmetries.id=stones.symmetry_id '+
            'AND labs.id=stones.lab_id '+
            'AND cuts.id=stones.cut_id '+
            'AND fluorescences.id=stones.fluorescence_id) ';

sql.select.stonesCount = 'SELECT count(stones.id) as count '+
        'FROM stones '+
        'LEFT JOIN (shapes, colors, clarities, polishes, symmetries, labs, cuts, fluorescences) '+
        'ON (shapes.id=stones.shape_id '+
            'AND colors.id=stones.color_id '+
            'AND clarities.id=stones.clarity_id '+
            'AND polishes.id=stones.polish_id '+
            'AND symmetries.id=stones.symmetry_id '+
            'AND labs.id=stones.lab_id '+
            'AND cuts.id=stones.cut_id '+
            'AND fluorescences.id=stones.fluorescence_id) ';

sql.select.image = 'SELECT * FROM images WHERE id = ?';

//Properties for stone
sql.select.stoneImages = 'SELECT images.*, stones_images.favorite as favorite FROM images LEFT JOIN (stones_images) '+
        'ON (images.id = stones_images.image_id) WHERE stones_images.stone_id = ? '+
        'ORDER BY favorite DESC, images.id ASC';

sql.select.shapesForStone = 'SELECT distinct shapes.*, '+
        'BIT_OR((SELECT IF(stones.id = ?,true,false))) as assigned '+
        'FROM shapes '+
        'LEFT JOIN (stones) '+
        'ON (shapes.id = stones.shape_id) '+
        'GROUP BY shapes.id '+
        'ORDER BY shapes.rank ASC';

sql.select.colorsForStone = 'SELECT distinct colors.*, '+
        'BIT_OR((SELECT IF(stones.id = ?,true,false))) as assigned '+
        'FROM colors '+
        'LEFT JOIN (stones) '+
        'ON (colors.id = stones.color_id) '+
        'GROUP BY colors.id '+
        'ORDER BY colors.rank ASC';

sql.select.claritiesForStone = 'SELECT distinct clarities.*, '+
        'BIT_OR((SELECT IF(stones.id = ?,true,false))) as assigned '+
        'FROM clarities '+
        'LEFT JOIN (stones) '+
        'ON (clarities.id = stones.clarity_id) '+
        'GROUP BY clarities.id '+
        'ORDER BY clarities.rank ASC';

sql.select.cutsForStone = 'SELECT distinct cuts.*, '+
        'BIT_OR((SELECT IF(stones.id = ?,true,false))) as assigned '+
        'FROM cuts '+
        'LEFT JOIN (stones) '+
        'ON (cuts.id = stones.cut_id) '+
        'GROUP BY cuts.id '+
        'ORDER BY cuts.rank ASC';

sql.select.labsForStone = 'SELECT distinct labs.*, '+
        'BIT_OR((SELECT IF(stones.id = ?,true,false))) as assigned '+
        'FROM labs '+
        'LEFT JOIN (stones) '+
        'ON (labs.id = stones.lab_id) '+
        'GROUP BY labs.id '+
        'ORDER BY labs.rank ASC';

sql.select.polishesForStone = 'SELECT distinct polishes.*, '+
        'BIT_OR((SELECT IF(stones.id = ?,true,false))) as assigned '+
        'FROM polishes '+
        'LEFT JOIN (stones) '+
        'ON (polishes.id = stones.polish_id) '+
        'GROUP BY polishes.id '+
        'ORDER BY polishes.rank ASC'; 

sql.select.symmetriesForStone = 'SELECT distinct symmetries.*, '+
        'BIT_OR((SELECT IF(stones.id = ?,true,false))) as assigned '+
        'FROM symmetries '+
        'LEFT JOIN (stones) '+
        'ON (symmetries.id = stones.symmetry_id) '+
        'GROUP BY symmetries.id '+
        'ORDER BY symmetries.rank ASC'; 

sql.select.fluorescencesForStone = 'SELECT distinct fluorescences.*, '+
        'BIT_OR((SELECT IF(stones.id = ?,true,false))) as assigned '+
        'FROM fluorescences '+
        'LEFT JOIN (stones) '+
        'ON (fluorescences.id = stones.fluorescence_id) '+
        'GROUP BY fluorescences.id '+
        'ORDER BY fluorescences.rank ASC';


/*************
* INSERTS
**************/
sql.insert = {};

sql.insert.newStone ='INSERT INTO stones (stock_number, certificate_number, total_price, weight, lab_id, shape_id, color_id, clarity_id, cut_id, polish_id, symmetry_id, fluorescence_id, measurements, depth, table_percent, specials, discount, youtube) '+
    'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

sql.insert.newImage = 'INSERT INTO images (file_name, description) VALUES (?, ?)';
sql.insert.stones_images = 'INSERT INTO stones_images (stone_id, image_id, favorite) VALUES (?, ?, ?)';


//UPDATE

sql.update = {};

sql.update.stone =   'UPDATE stones SET stock_number = ?, certificate_number = ?, total_price = ?, weight = ?, lab_id = ?, '+
                    'shape_id = ?, color_id = ?, clarity_id = ?, cut_id = ?, polish_id = ?, symmetry_id = ?, fluorescence_id = ?, measurements = ?, depth = ?, table_percent = ?, specials = ?, discount = ?, youtube = ? WHERE id = ?';

sql.update.clear_stone_image_primary = 'UPDATE stones_images SET favorite = false '+
                                'WHERE stone_id = ?';

sql.update.stone_image_primary = 'UPDATE stones_images, stones SET stones_images.favorite = true, stones.diamond_image = ? '+
                                'WHERE (stones_images.stone_id = ? AND stones_images.image_id = ?) AND stones.id = ? '; 


/**********
* DELETES
***********/
sql.delete = {};
sql.delete.stone = 'DELETE FROM stones WHERE id = ?';
sql.delete.image = 'DELETE FROM images WHERE id = ?';

sql.delete.images_of_stone = 'DELETE FROM images, stones_images WHERE (images.id, stones_images.image_id) IN ( '+
        'SELECT image_id FROM stones_images '+
        'WHERE stone_id = ?)'; 



/*******
* WHERE
********/
sql.where = {};

sql.where.forSearch = 'WHERE total_price BETWEEN ? AND ? '+
        'AND weight BETWEEN ? AND ? '+
        'AND cuts.rank IN (?) '+
        'AND clarities.rank IN (?) '+
        'AND colors.rank IN (?) '+
        'AND shapes.rank IN (?) ' +
        'AND labs.rank IN (?) ';

sql.where.orderByAsc = 'ORDER BY ?? ASC ' //field names are escaped with double ??
sql.where.orderByDesc = 'ORDER BY ?? DESC ' //field names are escaped with double ??
sql.where.limit = 'LIMIT ?, ?'

sql.where.forDetails = 'WHERE stock_number = ?';

sql.where.byId = 'WHERE stones.id IN (?)';

module.exports = sql;
