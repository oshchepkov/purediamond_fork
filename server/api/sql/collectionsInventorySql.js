var sql = {};

/*************
* SELECT FROM
**************/
sql.select = {};

sql.select.collections = 'SELECT * FROM collections ORDER BY id DESC';

sql.select.collectionById = 'SELECT * FROM collections WHERE id = ?';



/*******
* WHERE
********/
sql.where = {};


/*************
* INSERTS
**************/
sql.insert = {};

sql.insert.newCollection ='INSERT INTO collections (name, description, rank) '+
					'VALUES (?, ?, 0)';


/**********
* UPDATES
***********/
sql.update = {};

sql.update.collection = 	'UPDATE collections SET name = ?, description = ? WHERE id = ?';


/**********
* DELETES
***********/
sql.delete = {};

sql.delete.collection = 'DELETE FROM collections WHERE id = ?';

///////////////////////////////////
module.exports = sql;
