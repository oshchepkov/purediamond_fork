var sql = {};

/*************
* SELECT FROM
**************/
sql.select = {};
sql.select.findById = 'SELECT * FROM users WHERE id = ?';
sql.select.findByUserName = 'SELECT * FROM users WHERE username = ?';

/*******
* WHERE
********/
sql.where = {};


/*************
* INSERTS
**************/
sql.insert = {};

sql.insert.createUser = 'INSERT INTO users (username, password, firstname, lastname, phone, subscribe) '+
						'VALUES (?,?,?,?,?,?)';


/**********
* UPDATES
***********/
sql.update = {};

sql.update.addresses = 'UPDATE users SET billing_address_id = ?, shipping_address_id = ? WHERE id = ?';


/**********
* DELETES
***********/
sql.delete = {};


///////////////////////////////////
module.exports = sql;
