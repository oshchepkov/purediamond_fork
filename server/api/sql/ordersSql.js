var sql = {};

/*************
* SELECT FROM
**************/
sql.select = {};

sql.select.orders = 'SELECT * FROM orders ORDER BY UNIX_TIMESTAMP(date) DESC';

sql.select.orderBySessionId = 'SELECT orders.* FROM orders '+
								'LEFT JOIN (sessions, orders_sessions, order_products) '+
								'ON (orders.id = orders_sessions.order_id '+
								'	AND sessions.session_id = orders_sessions.session_id) '+
								'WHERE sessions.session_id = ? '+
								'ORDER BY date LIMIT 1';

sql.select.orderById = 'SELECT * FROM orders WHERE id = ?';
sql.select.orderProducts = 'SELECT order_products.*, metals.name metal FROM order_products '+
							'LEFT JOIN (metals) '+
							'ON (order_products.metal_id = metals.id) '+ 
							'WHERE order_id = ?';

/*******
* WHERE
********/
sql.where = {};


/*************
* INSERTS
**************/
sql.insert = {};

sql.insert.createOrder = 'INSERT INTO orders (date, subtotal, tax, total, contents, status) '+
						'VALUES (?,?,?,?,?,?)';

sql.insert.linkOrderWithSession = 'INSERT INTO orders_sessions (order_id, session_id) '+
								'VALUES (?,?)';

sql.insert.orderAddProduct = 'INSERT INTO order_products (product_id, order_id, category, size, metal_id) VALUES (?,?,?,?,?)';

/**********
* UPDATES
***********/
sql.update = {};

sql.update.orderUpdateProduct = 'UPDATE order_products SET stone_id = ?, size = ?, metal_id = ? WHERE id = ?';

sql.update.addStoneToOrder = 'UPDATE orders SET stone_id = ? WHERE id = ?';

sql.update.addRingToOrder = 'UPDATE orders SET ring_id = ? WHERE id = ?';

sql.update.addExtras = 'UPDATE orders SET extras = ? WHERE id = ?';

sql.update.updateRingSize = 'UPDATE orders SET ring_size = ? WHERE id = ?';

sql.update.updateStatus = 'UPDATE orders SET status = ? WHERE id = ?';

sql.update.price = 'UPDATE orders SET subtotal = ?, tax = ?, discount = ?, total = ? WHERE id = ?';

sql.update.addresses = 'UPDATE orders SET billing_address = ?, shipping_address = ?, user_id = ? WHERE id = ?';

/**********
* DELETES
***********/
sql.delete = {};

sql.delete.order = 'DELETE FROM orders WHERE id = ?';
sql.delete.orderProducts = 'DELETE FROM order_products WHERE order_id = ?';

sql.delete.orderRemoveProduct = 'DELETE FROM order_products WHERE id = ? AND order_id = ?';


///////////////////////////////////
module.exports = sql;
