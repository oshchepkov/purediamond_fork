var sql = {};

/*************
* SELECT FROM
**************/
sql.select = {};

sql.select.pages = 'SELECT * FROM pages WHERE published = 1 ORDER BY id DESC';

sql.select.pageById = 'SELECT * FROM pages WHERE id = ?';

sql.select.pageBySlug = 'SELECT * FROM pages WHERE slug = ?';

/*******
* WHERE
********/
sql.where = {};


/*************
* INSERTS
**************/
sql.insert = {};

sql.insert.newPage ='INSERT INTO pages (slug, title, subtitle, menu_title, published) '+
					'VALUES (?, ?, ?, ?, ?)';


/**********
* UPDATES
***********/
sql.update = {};

sql.update.page = 'UPDATE pages SET slug = ?, title = ?, subtitle = ?, menu_title = ?, published = ? WHERE id = ?';


/**********
* DELETES
***********/
sql.delete = {};

sql.delete.page = 'DELETE FROM pages WHERE id = ?';

///////////////////////////////////
module.exports = sql;
