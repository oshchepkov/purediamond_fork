var sql = {};

/*************
* SELECT FROM
**************/
sql.select = {};
sql.select.findById = 'SELECT * FROM addresses WHERE id = ?';

/*******
* WHERE
********/
sql.where = {};


/*************
* INSERTS
**************/
sql.insert = {};

sql.insert.createAddress = 'INSERT INTO addresses (first_name, last_name, line1, line2, country, province, city, postal_code, phone) '+
						'VALUES (?,?,?,?,?,?,?,?,?)';


/**********
* UPDATES
***********/
sql.update = {};



/**********
* DELETES
***********/
sql.delete = {};


///////////////////////////////////
module.exports = sql;
