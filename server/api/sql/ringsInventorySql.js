var sql = {};

/*************
* SELECT FROM
**************/
sql.select = {};

sql.select.rings = 'SELECT * FROM rings ORDER BY id DESC';

sql.select.ringById = 'SELECT * FROM rings WHERE id = ?';

//SELECTS for stored rings
sql.select.metalsForRing = 	'SELECT metals.* FROM metals LEFT JOIN (rings_metals) '+
							'ON (metals.id = rings_metals.metal_id) WHERE rings_metals.ring_id = ? '+
							'ORDER BY metals.rank ASC';

sql.select.shapesForRing = 	'SELECT shapes.* FROM shapes LEFT JOIN (rings_shapes) '+
							'ON (shapes.id = rings_shapes.shape_id) WHERE rings_shapes.ring_id = ? '+
							'ORDER BY shapes.rank ASC';

sql.select.collectionsForRing =	'SELECT collections.* FROM collections LEFT JOIN (rings_collections) '+
								'ON (collections.id = rings_collections.collection_id) WHERE rings_collections.ring_id = ? '+
								'ORDER BY collections.rank ASC';

sql.select.designersForRing ='SELECT designers.* FROM designers LEFT JOIN (rings_designers) '+
							'ON (designers.id = rings_designers.designer_id) WHERE rings_designers.ring_id = ? '+
							'ORDER BY designers.id ASC';

sql.select.ringImages = 'SELECT images.*, rings_images.favorite as favorite FROM images LEFT JOIN (rings_images) '+
						'ON (images.id = rings_images.image_id) WHERE rings_images.ring_id = ? '+
						'ORDER BY favorite DESC, images.id ASC';



/*
* Ring properties for editing form
*/
sql.select.metalsForRingEdit = 	'SELECT distinct metals.*, '+
								'	BIT_OR((SELECT IF(rings_metals.ring_id = ?,true,false))) as assigned '+
								'FROM metals '+
								'LEFT JOIN (rings_metals) '+
								'ON (metals.id = rings_metals.metal_id) '+
								'GROUP BY metals.id '+
								'ORDER BY metals.rank ASC';

sql.select.shapesForRingEdit = 	'SELECT distinct shapes.*, '+
								'	BIT_OR((SELECT IF(rings_shapes.ring_id = ?,true,false))) as assigned '+
								'FROM shapes '+
								'LEFT JOIN (rings_shapes) '+
								'ON (shapes.id = rings_shapes.shape_id) '+
								'GROUP BY shapes.id '+
								'ORDER BY shapes.rank ASC';

sql.select.collectionsForRingEdit =	'SELECT distinct collections.*, '+
									'	BIT_OR((SELECT IF(rings_collections.ring_id = ?,true,false))) as assigned '+
									'FROM collections '+
									'LEFT JOIN (rings_collections) '+
									'ON (collections.id = rings_collections.collection_id) '+
									'GROUP BY collections.id '+
									'ORDER BY collections.rank ASC';

sql.select.designersForRingEdit ='SELECT distinct designers.*, '+
								'	BIT_OR((SELECT IF(rings_designers.ring_id = ?,true,false))) as assigned '+
								'FROM designers '+
								'LEFT JOIN (rings_designers) '+
								'ON (designers.id = rings_designers.designer_id) '+
								'GROUP BY designers.id '+
								'ORDER BY designers.id ASC';


sql.select.image = 'SELECT * FROM images WHERE id = ?';

/*
//SELECTS for a ring template
sql.select.metals = 'SELECT * FROM metals '+
					'ORDER BY rank ASC';

sql.select.shapes = 'SELECT * FROM shapes '+
					'ORDER BY rank ASC';

sql.select.collections = 'SELECT * FROM collections '+
						'ORDER BY rank ASC';

sql.select.designers ='SELECT * FROM designers '+
					'ORDER BY id ASC';
*/


/*******
* WHERE
********/
sql.where = {};


/*************
* INSERTS
**************/
sql.insert = {};

sql.insert.newRing ='INSERT INTO rings (title, lot_number, price, stone_weight_min, stone_weight_max, description, instagram, youtube, published, category, featured, details, discount, sale) '+
					'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
sql.insert.rings_shapes = 	'INSERT INTO rings_shapes (ring_id, shape_id) VALUES (?, ?)';
sql.insert.rings_metals = 	'INSERT INTO rings_metals (ring_id, metal_id) VALUES (?, ?)';
sql.insert.rings_collections = 'INSERT INTO rings_collections (ring_id, collection_id) VALUES (?, ?)';
sql.insert.rings_designers = 'INSERT INTO rings_designers (ring_id, designer_id) VALUES (?, ?)';

sql.insert.newImage ='INSERT INTO images (file_name, description) VALUES (?, ?)';
sql.insert.rings_images = 'INSERT INTO rings_images (ring_id, image_id, favorite) VALUES (?, ?, ?)';

/**********
* UPDATES
***********/
sql.update = {};

sql.update.ring = 	'UPDATE rings SET title = ?, lot_number = ?, price = ?, stone_weight_min = ?, '+
					'stone_weight_max = ?, description = ?, instagram = ?, youtube = ?, published = ?, category = ?, featured = ?, details = ?, discount = ?, sale = ? WHERE id = ?';

sql.update.clear_ring_image_primary = 'UPDATE rings_images SET favorite = false '+
								'WHERE ring_id = ?';

sql.update.ring_image_primary = 'UPDATE rings_images SET favorite = true '+
								'WHERE ring_id = ? AND image_id = ?';

/**********
* DELETES
***********/
sql.delete = {};

sql.delete.ring = 'DELETE FROM rings WHERE id = ?';
sql.delete.shapes_references = 'DELETE FROM rings_shapes WHERE ring_id = ?';
sql.delete.metals_references = 'DELETE FROM rings_metals WHERE ring_id = ?';
sql.delete.collections_references = 'DELETE FROM rings_collections WHERE ring_id = ?';
sql.delete.designers_references = 'DELETE FROM rings_designers WHERE ring_id = ?';
sql.delete.image = 'DELETE FROM images WHERE id = ?';

sql.delete.images_of_ring = 'DELETE FROM images WHERE id IN ( '+
							'SELECT image_id FROM rings_images '+
							'	WHERE ring_id = ?)';

///////////////////////////////////
module.exports = sql;
