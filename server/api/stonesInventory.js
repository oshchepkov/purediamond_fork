var express = require('express');
var router = express.Router();
var async = require('async');
var fs = require('fs-extra');
var path = require('path');
var csv = require('csv');
var config = require('../config/config');
var sql = require('./sql/stoneSearchSql');
var im = require('imagemagick');
var pool = config.db.pool;

module.exports = router; 

var Clarity = require('../models/dto/ClarityDto');
var Shape = require('../models/dto/ShapeDto');
var Color = require('../models/dto/ColorDto');
var Fluorescence = require('../models/dto/FluorescenceDto');
var Cut = require('../models/dto/CutDto');
var Polish = require('../models/dto/PolishDto');
var Symmetry = require('../models/dto/SymmetryDto');
var Lab = require('../models/dto/LabDto');
var Stone = require('../models/dto/StoneDto');
var Image = require('../models/dto/ImageDto');

var InventoryUploadResponse = require('../models/responses/InventoryUploadResponse');
var InventoryProcessedResponse = require('../models/responses/InventoryProcessedResponse');
var ErrorForResponse = require('../models/responses/ErrorForResponse');
var InventorySubmitResponse = require('../models/responses/InventorySubmitResponse');


//This is a temporary folder where inventory files are being uploaded
// two dots in the beginning mean one level up
//var inventory_storage_temp = path.join(__dirname, '../storage/inventory/temporary/');
var inventory_storage_temp = config.path.stonesInventoryStorage;

var imagePath = config.path.stonesImagesStorage;
var imageUrl = config.path.stonesImagesUrl;


router.get('/getInventory', function (req, res) {
    var stones = [];
    var query = 'SELECT stones.id, stock_number, specials, discount, '+ 
        'shapes.name AS shape, '+
        'shapes.class_name AS shape_class_name, '+
        'colors.name AS color, '+
        'clarities.name AS clarity, '+
        'polishes.friendly_name AS polish, '+
        'cuts.friendly_name AS cut, '+
        'symmetries.name AS symmetry, '+
        'fluorescences.friendly_name AS fluorescence, '+
        'depth, girdle, '+
        'weight, measurements, table_percent, '+
        'labs.friendly_name AS lab, certificate_number, certificate_filename, diamond_image, total_price AS price '+
        'FROM stones '+
        'LEFT JOIN (shapes, colors, clarities, polishes, symmetries, labs, cuts, fluorescences) '+
        'ON (shapes.id=stones.shape_id '+
            'AND colors.id=stones.color_id '+
            'AND clarities.id=stones.clarity_id '+
            'AND polishes.id=stones.polish_id '+
            'AND symmetries.id=stones.symmetry_id '+
            'AND labs.id=stones.lab_id '+
            'AND cuts.id=stones.cut_id '+
            'AND fluorescences.id=stones.fluorescence_id)';

    pool.query(query, function (err, rows, fields) {
        if (err) {
            console.error(err);
            res.statusCode = 500;
            res.send({result: 'error', err: err.code});
        }
        for (var i in rows) {
            var stone = new Stone(
                rows[i].id, 
                rows[i].stock_number, 
                rows[i].shape, 
                rows[i].shape_class_name, 
                rows[i].weight, 
                rows[i].color, 
                rows[i].clarity, 
                rows[i].cut, 
                rows[i].polish, 
                rows[i].symmetry, 
                rows[i].measurements, 
                rows[i].table_percent, 
                rows[i].lab, 
                rows[i].certificate_number, 
                rows[i].certificate_filename, 
                rows[i].diamond_image, 
                rows[i].price, 
                rows[i].depth, 
                rows[i].girdle,
                rows[i].fluorescence,
                'diamonds',
                '',
                Boolean(rows[i].specials), 
                rows[i].discount                
            );
            stones.push(stone);
        }
        res.send({result: stones});
    });
});

router.get('/stone', function (req, res) {
    //console.log(req.query);
    var stoneResp = {
        errors: [],
        status: 'success',
        stone: {}
    };
    //rtr.status = 'success';

    var stoneId = req.query.id;
    if(typeof stoneId === 'undefined'){
        stoneId = null;
        var stone = new Stone();
        stone.id = null;
        stoneResp.stone = stone;
    }

    async.series({
        stone: function(seriesCallback){
            pool.query('SELECT * FROM stones WHERE id ='+stoneId,function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                console.log(rows);
                for (var i in rows) {
                    var stone = new Stone(
                        rows[i].id, 
                        rows[i].stock_number, 
                        rows[i].shape, 
                        rows[i].shape_class_name, 
                        rows[i].weight, 
                        rows[i].color, 
                        rows[i].clarity, 
                        rows[i].cut, 
                        rows[i].polish, 
                        rows[i].symmetry, 
                        rows[i].measurements, 
                        rows[i].table_percent, 
                        rows[i].lab, 
                        rows[i].certificate_number, 
                        rows[i].certificate_filename, 
                        rows[i].diamond_image, 
                        rows[i].total_price, 
                        rows[i].depth, 
                        rows[i].girdle,
                        rows[i].fluorescence,
                        'diamonds',
                        '',
                        Boolean(rows[i].specials),
                        rows[i].discount,
                        rows[i].youtube,
                        rows[i].rapnet_price,
                        rows[i].rapnet_discount,
                        rows[i].country
                    );
                    stoneResp.stone = stone;
                }//for i in rows
                seriesCallback(null, 'success');
            });

        },//rings
        shapes: function(seriesCallback){
            pool.query(sql.select.shapesForStone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var shape = new Shape(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, rows[i].class_name, Boolean(rows[i].assigned));
                        stoneResp.stone.shapes.push(shape);
                    }
                    seriesCallback(null, 'success');
                });
        },
        colors: function(seriesCallback){
            pool.query(sql.select.colorsForStone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var color = new Color(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, Boolean(rows[i].assigned));
                        stoneResp.stone.colors.push(color);
                    }
                    seriesCallback(null, 'success');
                });
        },
        clarities: function(seriesCallback){
            pool.query(sql.select.claritiesForStone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var clarity = new Clarity(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, Boolean(rows[i].assigned));
                        stoneResp.stone.clarities.push(clarity);
                    }
                    seriesCallback(null, 'success');
                });
        },
        cuts: function(seriesCallback){
            pool.query(sql.select.cutsForStone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var cut = new Cut(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, Boolean(rows[i].assigned));
                        stoneResp.stone.cuts.push(cut);
                    }
                    seriesCallback(null, 'success');
                });
        },
        labs: function(seriesCallback){
            pool.query(sql.select.labsForStone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var lab = new Lab(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, Boolean(rows[i].assigned));
                        stoneResp.stone.labs.push(lab);
                    }
                    seriesCallback(null, 'success');
                });
        },
        polishes: function(seriesCallback){
            pool.query(sql.select.polishesForStone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var polish = new Polish(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, Boolean(rows[i].assigned));
                        stoneResp.stone.polishes.push(polish);
                    }
                    seriesCallback(null, 'success');
                });
        },
        symmetries: function(seriesCallback){
            pool.query(sql.select.symmetriesForStone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var symmetry = new Symmetry(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, Boolean(rows[i].assigned));
                        stoneResp.stone.symmetries.push(symmetry);
                    }
                    seriesCallback(null, 'success');
                });
        },
        fluorescences: function(seriesCallback){
            pool.query(sql.select.fluorescencesForStone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var fluorescence = new Fluorescence(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, Boolean(rows[i].assigned));
                        stoneResp.stone.fluorescences.push(fluorescence);
                    }
                    seriesCallback(null, 'success');
                });
        },
        images: function(seriesCallback){
            pool.query(sql.select.stoneImages, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    stoneResp.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var image = new Image(rows[i].id, rows[i].file_name, imageUrl+'/'+stoneId+'/'+rows[i].file_name, imageUrl+'/'+stoneId+'/thumb/'+rows[i].file_name, rows[i].description, Boolean(rows[i].favorite));
                        stoneResp.stone.images.push(image);
                    }
                    seriesCallback(null, 'success');
                });
        }

    },//final callback
    function(err, results) {
        res.send(stoneResp);
    });//series
});

router.post('/stone', function (req, res) {
    //console.log(req.body);
    //var rtr = new GenericResponse();
    var stoneResp = {
        errors: [],
        status: 'success',
        stone: {}
    };
    var newStone = req.body;
    //setting default values to be added to the database
    newStone.stock_number = newStone.stock_number != null ? newStone.stock_number : 0;
    newStone.price = newStone.price != null ? newStone.price : 0;
    newStone.weight = newStone.weight != null ? newStone.weight : 0;
    newStone.lab_id = newStone.lab_id != null ? newStone.lab_id : 0;
    newStone.shape_id = newStone.shape_id != null ? newStone.shape_id : 0;
    newStone.color_id = newStone.color_id != null ? newStone.color_id : 0;
    newStone.clarity_id = newStone.clarity_id != null ? newStone.clarity_id : 0;
    newStone.cut_id = newStone.cut_id != null ? newStone.cut_id : 0;
    newStone.polish_id = newStone.polish_id != null ? newStone.polish_id : 0;
    newStone.symmetry_id = newStone.symmetry_id != null ? newStone.symmetry_id : 0;
    newStone.fluorescence_id = newStone.fluorescence_id != null ? newStone.fluorescence_id : 0;
    newStone.measurements = newStone.measurements != null ? newStone.measurements : 0;
    newStone.depth = newStone.depth != null ? newStone.depth : 0;
    newStone.table = newStone.table != null ? newStone.table : 0;
    newStone.youtube = newStone.youtube != null ? newStone.youtube : '';
    newStone.specials = newStone.specials != null ? newStone.specials : false;

    var newStoneId;
    pool.query(sql.insert.newStone, [
        newStone.stock_number, 
        newStone.cert_number, 
        newStone.price, 
        newStone.weight, 
        newStone.lab_id, 
        newStone.shape_id, 
        newStone.color_id, 
        newStone.clarity_id, 
        newStone.cut_id, 
        newStone.polish_id, 
        newStone.symmetry_id, 
        newStone.fluorescence_id, 
        newStone.measurements, 
        newStone.depth,
        newStone.table,
        newStone.specials,
        newStone.discount,
        newStone.youtube
        ],
        function (err, rows, fields) {
            if (err) {
                stoneResp.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
                stoneResp.status = 'failure';
                console.log('new stone', stoneResp);
                res.send(stoneResp);

            }else{
                //console.log('new stone', rows);
                newStoneId = rows.insertId;
                newStone.id = newStoneId;
                stoneResp.stone = newStone;
                //console.log('newstoneId: '+newRingId);
                stoneResp.message = 'Stone added';
                res.send(stoneResp);
            }
        }
    );

});


router.put('/stone', function (req, res) {
    //console.log(req.query);
    //var gr = new GenericResponse();
    var stoneResp = {
        errors: [],
        status: 'success'
    };
    var updatedStone = req.body;
    var stoneId = req.query.id;

    //console.log('stone', updatedStone);

    async.series({
        updateStone : function(seriesCallback){
            pool.query(sql.update.stone, [
                updatedStone.stock_number, 
                updatedStone.cert_number, 
                updatedStone.price, 
                updatedStone.weight, 
                updatedStone.lab_id, 
                updatedStone.shape_id, 
                updatedStone.color_id, 
                updatedStone.clarity_id, 
                updatedStone.cut_id, 
                updatedStone.polish_id, 
                updatedStone.symmetry_id, 
                updatedStone.fluorescence_id, 
                updatedStone.measurements, 
                updatedStone.depth,
                updatedStone.table,
                updatedStone.specials,
                updatedStone.discount,
                updatedStone.youtube,
                stoneId
                ],
                function (err, rows, fields) {
                    if (err) {
                        stoneResp.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
                        stoneResp.status = 'failure';
                        res.send(stoneResp);
                    }else{
                        seriesCallback(null, 'success');
                    }
                }
                );

        },//updateRing

    },//final callback
    function(err, results) {
        stoneResp.message = 'Stone updated'
        res.send(stoneResp);
    });//series

});


router.delete('/stone', function (req, res) {
    var stoneResp = {
        errors: [],
        status: 'success'
    };
    var stoneId = req.query.id;
    if(typeof stoneId === 'undefined'){
        stoneResp.errors.push(new ErrorForResponse('BAD REQUEST', 'Stone ID is not provided'));
        res.send(400, stoneResp);
    }
    async.series({
        deleteImagesFromDisk : function(seriesCallback){
            fs.remove(imagePath +'/'+stoneId, function (err) {
                if (err) {
                    console.log('Could not delete ' + imagePath +'/'+stoneId + ' ' + err);
                    stoneResp.errors.push(JSON.stringify(err));
                    stoneResp.status = 'failure';
                    //res.send(ir);
                }
                seriesCallback(null, 'success');
            });
        },//deleteImagesFromDisk
        deleteImagesFromDB : function(seriesCallback){
            pool.query(sql.delete.images_of_stone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error deleting data to the database'));
                    stoneResp.status = 'failure';
                }
                seriesCallback(null, 'success');
            });
        },//deleteImagesFromDB
        deleteStone : function(seriesCallback){
            pool.query(sql.delete.stone, [stoneId], function (err, rows, fields) {
                if (err) {
                    stoneResp.errors.push(new ErrorForResponse(err.message, 'Error deleting data to the database'));
                    stoneResp.status = 'failure';
                }
                seriesCallback(null, 'success');
            });
        },//deleteRing
    },//final callback
    function(err, results) {
        res.send(stoneResp);
    });//series

});//router




router.post('/upload/images', function (req, res) {
    var stoneResp = {
        errors: [],
        status: 'success'
    };
    var stoneId = req.query.id;
    if(typeof stoneId === 'undefined'){
        stoneResp.errors.push(new ErrorForResponse('BAD REQUEST', 'Stone ID is not provided'));
        res.send(400, stoneResp);
    }
    var newName = new Date().getTime();
    var newFileName;
    var newPath;
    req.pipe(req.busboy);

    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        fs.ensureDir(imagePath+'/'+stoneId+'/thumb', function (err) {
            //console.log('uploading ' + filename);
            newFileName = newName + '.' + filename.split('.').pop();
            
            var wr = fs.createWriteStream(imagePath+'/'+stoneId+'/'+path.basename(newFileName));
            
            wr.on('pipe', function(src) {
                
            });
            file.pipe(wr);

        });         
    });
    req.busboy.on('finish', function (filename) {
        //console.log('finish');
        console.log(newFileName);
        //res.send(stoneResp);
        var newImageId;
        var newPath = imagePath+stoneId+'/'+path.basename(newFileName);
        var thumbPath = imagePath+stoneId+'/thumb/'+path.basename(newFileName);
        var thumbArgs = [
                newPath,
                '-filter',
                'Triangle',
                '-fuzz', 
                '1%', 
                '-trim', 
                '+repage',
                '-define',
                'filter:support=2',
                '-thumbnail',                       
                '450x',
                '-unsharp',
                '0.25x0.25+8+0.065',
                '-dither',
                'None',
                '-posterize',
                '136',
                '-quality',
                '82',
                '-define',
                'jpeg:fancy-upsampling=off',
                '-define',
                'png:compression-filter=5',
                '-define',
                'png:compression-level=9',
                '-define',
                'png:compression-strategy=1',
                '-define',
                'png:exclude-chunk=all',
                '-interlace',
                'none',
                '-colorspace',
                'sRGB',
                '-strip',
                thumbPath
            ];
        async.series({
            insertImage : function(seriesCallback){
                pool.query(sql.insert.newImage, [newFileName, null],
                    function (err, rows, fields) {
                        if (err) {
                            stoneResp.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
                            stoneResp.status = 'failure';
                            res.send(stoneResp);
                        }else{
                                //console.log(rows);
                                newImageId = rows.insertId;
                                console.log('newImageId: '+newImageId);
                                seriesCallback(null, 'success');
                            }
                        }
                        );
                },//ring
                insertStoneImages: function(seriesCallback){
                    pool.query(sql.insert.stones_images, [stoneId, newImageId, false], function (err, rows, fields) {
                        if (err) {
                            stoneResp.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
                            stoneResp.status = 'failure';
                        }
                        seriesCallback(null, 'success');
                    });
                }//RingsImages
            },//final callback
            function(err, results) {
                im.convert(thumbArgs, function(err, stdout, stderr){
                    if (err) throw err;
                });
                stoneResp.message = 'Image successfully added'
                res.send(stoneResp);
            });//series
   
        

    });

});//router

router.delete('/stone/image', function (req, res) {
    var ir = {
        status: 'success',
        images: [],
        errors: [],
    };
    var stoneId = req.query.stoneId;
    var imageId = req.query.imageId;
    var imageFileName = null;

    if(typeof stoneId === 'undefined'){
        ir.errors.push(new ErrorForResponse('BAD REQUEST', 'Stone ID is not provided'));
        res.send(400, ir);
    }
    if(typeof imageId === 'undefined'){
        ir.errors.push(new ErrorForResponse('BAD REQUEST', 'Image ID is not provided'));
        res.send(400, ir);
    }

    async.series({
        findImageInDB: function(seriesCallback){
            pool.query(sql.select.image, [imageId], function (err, rows, fields) {
                if (err) {
                    ir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    ir.status = 'failure';
                    res.send(ir);
                }
                for (var i in rows) {
                    imageFileName = rows[i].file_name
                }
                seriesCallback(null, 'success');
            });
        },//findImageInDB
        deleteImageFromDisk : function(seriesCallback){
            fs.remove(imagePath +'/'+stoneId+'/'+ imageFileName, function (err) {
                if (err) {
                    console.log('Could not delete ' + imagePath +'/'+stoneId+'/'+ imageFileName + ' ' + err);
                    ir.errors.push(JSON.stringify(err));
                    ir.status = 'failure';
                    res.send(ir);
                }
                fs.remove(imagePath +'/'+stoneId+'/thumb/'+ imageFileName, function (err) {});
                seriesCallback(null, 'success');
            });
        },//deleteImageFromDisk
        deleteImageFromDB : function(seriesCallback){
            pool.query(sql.delete.image, [imageId], function (err, rows, fields) {
                if (err) {
                    ir.errors.push(new ErrorForResponse(err.message, 'Error deleting data to the database'));
                    ir.status = 'failure';
                }
                seriesCallback(null, 'success');
            });
        },//deleteImageFromDB
        images: function(seriesCallback){
            pool.query(sql.select.stoneImages, [stoneId], function (err, rows, fields) {
                if (err) {
                    ir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    ir.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var image = new Image(rows[i].id, rows[i].file_name, imageUrl+'/'+stoneId+'/'+rows[i].file_name, imageUrl+'/'+stoneId+'/thumb/'+rows[i].file_name, rows[i].description, Boolean(rows[i].favorite));
                        ir.images.push(image);
                    }
                    seriesCallback(null, 'success');
                });
        } //images
    },//final callback
    function(err, results) {
        res.send(ir);
    });//series
});//router

router.put('/stone/image/primary', function (req, res) {
    var ir = {
        status: 'success',
        images: [],
        errors: [],
    };
    var stoneId = req.query.stoneId;
    var imageId = req.query.imageId;

    if(typeof stoneId === 'undefined'){
        ir.errors.push(new ErrorForResponse('BAD REQUEST', 'Stone ID is not provided'));
        res.send(400, ir);
    }
    if(typeof imageId === 'undefined'){
        ir.errors.push(new ErrorForResponse('BAD REQUEST', 'Image ID is not provided'));
        res.send(400, ir);
    }
    async.series({
        clearPrimaryFlag: function(seriesCallback){
            pool.query(sql.update.clear_stone_image_primary, [stoneId], function (err, rows, fields) {
                if (err) {
                    ir.errors.push(new ErrorForResponse(err.message, 'Error updating data in a database'));
                    ir.status = 'failure';
                }
                seriesCallback(null, 'success');
            });
        },//clearPrimaryFlag
        makeImagePrimary: function(seriesCallback){
            pool.query(sql.update.stone_image_primary, [imageId, stoneId, imageId, stoneId], function (err, rows, fields) {
                if (err) {
                    ir.errors.push(new ErrorForResponse(err.message, 'Error updating data in a database'));
                    ir.status = 'failure';
                }
                seriesCallback(null, 'success');
            });
        },//makeImagePrimary
        images: function(seriesCallback){
            pool.query(sql.select.stoneImages, [stoneId], function (err, rows, fields) {
                if (err) {
                    ir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
                    ir.status = 'failure';
                }
                for (var i in rows) {
                        //console.log(JSON.stringify(rows[i]));
                        var image = new Image(rows[i].id, rows[i].file_name, imageUrl+'/'+stoneId+'/'+rows[i].file_name, imageUrl+'/'+stoneId+'/thumb/'+rows[i].file_name, rows[i].description, Boolean(rows[i].favorite));
                        ir.images.push(image);
                    }
                    seriesCallback(null, 'success');
                });
        } //images
    },//final callback
    function(err, results) {
        res.send(ir);
    });//series

});//router




/*********************************************************************
 * the following methods are for uploading csv files and their prosessing
 *********************************************************************/

/*
 * Get all the files that was uploaded to a special temporary folder
 * and was not processed yet
 */
 router.get('/getUploadedFiles', function (req, res) {
 	var iur = new InventoryUploadResponse();
    fs.readdir(inventory_storage_temp, function (err, files) {
    	if (err){
        	iur.errors.push(new ErrorForResponse(err.message, 'Error writing data into database'));
		}
        for (var i in files) {
            iur.files.push({filename: files[i]});
        }
        iur.status = 'success';
        res.send(iur);
    });
});


/*
 * Upload file, save it to the special temporary folder
 * (returns list of files, same as getUploadedFiles)
 */
 router.post('/upload', function (req, res) {
    req.pipe(req.busboy);
    
    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        fs.ensureDir(inventory_storage_temp, function (err) {
            console.log('uploading ' + filename);
            file.pipe(fs.createWriteStream(inventory_storage_temp + path.basename(filename)));
        });
    });
    
    req.busboy.on('finish', function () {
        console.log('Done parsing form!');
        //read from temporary folder to get all the files uploaded and return the list to the client
        setTimeout(function () {
            var iur = new InventoryUploadResponse();
            fs.readdir(inventory_storage_temp, function (err, files) {
                if (err){
                    iur.errors.push(new ErrorForResponse(err.message, 'Error reading '+inventory_storage_temp));
                }
                for (var i in files) {
                    iur.files.push({filename: files[i]});
                }
                iur.status = 'success';
                res.send(iur);
            });
        }, 500);
    });
});

/*
 * Deletes temporary folder in case user needs to start from scratch
 */
 router.delete('/clear', function (req, res) {
    fs.remove(inventory_storage_temp, function (err) {
        if (err) {
            console.log('Could not delete ' + inventory_storage_temp + ' ' + err);
        }
        console.log('successfully deleted ' + inventory_storage_temp);
        var iur = new InventoryUploadResponse();
        iur.status = 'success';
        res.send(iur);
    });
});



/*
 * Process uploaded files. This includes converting CSVs to objects
 * and then saving data to the database
 */
 router.post('/process', function (req, res) {
	var ipr = new InventoryProcessedResponse();

    var inventoryData = req.body.inventoryData;

    async.parallel({
        truncateStonesTable: function (callback) {
            pool.query('TRUNCATE TABLE stones_temp', function (err, rows, fields) {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.send({result: 'error', err: err.code});
                }
				callback(null, rows);
                });
        },
        inventory: function (callback) {
                //var e = new EventEmitter();
                var inventory = [];
                fs.readdir(inventory_storage_temp, function (err, files) {
                    var filesCount = files.length;
                    var parseCompleted = 0;
                    //console.log('filesCount: '+filesCount);
                    for (var i in files) {
                        console.log('reading: ' + files[i]);
                        csv()
                        .from(inventory_storage_temp + files[i], {columns: true})
                        .to.array(function (data) {
                                inventory = inventory.concat(data);
                                parseCompleted++;
                                if (parseCompleted == filesCount) {
                                    callback(null, inventory);
                                }
                            }
                         );
                    }
                });
        },
        clarities: function (callback) {
            var clarities = [];
            pool.query('SELECT * FROM clarities', function (err, rows, fields) {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.send({result: 'error', err: err.code});
                }
                for (var i in rows) {
                    var clarity = new Clarity(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description);
                    clarities.push(clarity);
                }
                callback(null, clarities);
            });
        },
        colors: function (callback) {
            var colors = [];
            pool.query('SELECT * FROM colors', function (err, rows, fields) {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.send({result: 'error', err: err.code});
                }
                for (var i in rows) {
                    var color = new Color(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description);
                    colors.push(color);
                }
                callback(null, colors);
            });
        },
        cuts: function (callback) {
            var cuts = [];
            pool.query('SELECT * FROM cuts', function (err, rows, fields) {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.send({result: 'error', err: err.code});
                }
                for (var i in rows) {
                    var cut = new Cut(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description);
                    cuts.push(cut);
                }
                callback(null, cuts);
            });
        },
        fluorescences: function (callback) {
            var fluorescences = [];
            pool.query('SELECT * FROM fluorescences', function (err, rows, fields) {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.send({result: 'error', err: err.code});
                }
                for (var i in rows) {
                    var fluorescence = new Fluorescence(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description);
                    fluorescences.push(fluorescence);
                }
                callback(null, fluorescences);
            });
        },
        polishes: function (callback) {
            var polishes = [];
            pool.query('SELECT * FROM polishes', function (err, rows, fields) {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.send({result: 'error', err: err.code});
                }
                for (var i in rows) {
                    var polish = new Polish(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description);
                    polishes.push(polish);
                }
                callback(null, polishes);
            });
        },
        shapes: function (callback) {
            var shapes = [];
            pool.query('SELECT * FROM shapes', function (err, rows, fields) {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.send({result: 'error', err: err.code});
                }
                for (var i in rows) {
                    var shape = new Shape(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, rows[i].class_name);
                    shapes.push(shape);
                }
                callback(null, shapes);
            });
        },
        symmetries: function (callback) {
            var symmetries = [];
            pool.query('SELECT * FROM symmetries', function (err, rows, fields) {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.send({result: 'error', err: err.code});
                }
                for (var i in rows) {
                    var symmetry = new Symmetry(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description);
                    symmetries.push(symmetry);
                }
                callback(null, symmetries);
            });
        },
        labs: function (callback) {
            var labs = [];
            pool.query('SELECT * FROM labs', function (err, rows, fields) {
                if (err) {
                    console.error(err);
                    res.statusCode = 500;
                    res.send({result: 'error', err: err.code});
                }
                for (var i in rows) {
                    var lab = new Lab(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description);
                    labs.push(lab);
                }
                callback(null, labs);
            });
        },

        },
        function (err, results) {
                    //console.log('results: '+JSON.stringify(results.inventory));

                    var inventory = results.inventory;
                    var clarities = results.clarities;
                    var colors = results.colors;
                    var cuts = results.cuts;
                    var fluorescences = results.fluorescences;
                    var polishes = results.polishes;
                    var shapes = results.shapes;
                    var symmetries = results.symmetries;
                    var labs = results.labs;
                    var sqlResponseCount = 0;
                    var successfulSqlResponseCount = 0;
                    var itemsProcessedCount = 0;
                    var noCertItemsCount = 0;
                    var lowQualityItemsCount = 0;
                    var eglIsraelItemsCount = 0;

                    
                    var certsArray = [];
                    var duplicateCertsArray = [];
                    console.log("inventory.length: " + inventory.length);

                    for (var i in inventory) {

                        //skip records with empty cert number
                        if (inventory[i]['Stock #'] == ''){
                            noCertItemsCount ++;
                            continue;
                        }

                        //skip low quality stones. Asked by client
                        if (inventory[i]['Clarity'] == 'I3'){
                            lowQualityItemsCount ++;
                            continue;
                        }


                        if (inventory[i]['Lab'] == 'NONE') {
                            inventory[i]['Lab'] = 'EGL USA';
                        };

                        if (inventory[i]['Lab'] == 'EGL ISRAEL') {
                            eglIsraelItemsCount ++;
                            continue;
                        }

                        if (inventory[i]['Measurements'].substring(0, 4) == '0.00') {
                            noCertItemsCount ++;
                            continue;
                        };



                        //skip records with rapnet discount = 0. This means that the price on rapnet is not final and we don't want to sell it at a wrong price
                        if (inventory[i]['Rapnet  Discount %'] == '0'){
                            continue;
                        }

                        

                        if (certsArray.indexOf(inventory[i]['Certificate #']) > -1 && inventory[i]['Pair Stock #'] == '') {
                            //console.log("Found duplicate: " + inventory[i]['Certificate #']);
                            duplicateCertsArray.push(inventory[i]['Certificate #']);
                            //continue;
                        } else {
                            certsArray.push(inventory[i]['Certificate #']);
                        }

                        var certExists = fs.existsSync(imagePath + inventory[i]['Certificate #'] + '.jpg');
                        var imageExists = fs.existsSync(imagePath + inventory[i]['Certificate #'] + '-2.jpg');


                        var totalPrice = Math.round(inventory[i]['RapNet Price'] * inventory[i]['Weight'] * inventoryData.exchange * inventoryData.markup);




                        


                        pool.query('INSERT INTO stones_temp (' +
                                'stock_number,        shape_id,             color_id,                   clarity_id,           cut_id' +//
                                ',polish_id,           symmetry_id,          fluorescence_id,            weight,               measurements' +//
                                ',certificate_number,  certificate_filename, diamond_image,              rapnet_price,         cash_price' +//
                                ',rapnet_discount,     cash_price_discount,  lab_id,                        fluorescence_color,   treatment' +//
                                ',availability,        fancy_color,          fancy_color_intensity,      fancy_color_overtone, depth' +//
                                ',table_percent,       girdle_thin,          girdle_thick,               girdle,               girdle_condition' +//
                                ',cullet_size,         cullet_condition,     crown_height,               crown_angle,          pavilion_depth' +//
                                ',pavilion_angle,      laser_inscription,    cert_comment,               country,              state' +//
                                ',city,                time_to_location,     is_matched_pair_separable,  pair_stock_number,    allow_raplink_feed' +//
                                ',parcel_stones,       trade_show,           key_to_symbols,             shade,                star_length' +
                                ',center_inclusion,    black_inclusion,      member_comment,             report_issue_date,    report_type' +
                                ',lab_location,        brand,                milky,      total_price' +
                                ')' +
        'VALUES ( ?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?,?' +
            ',?,?,?,?' +
            ')',
        [
        inventory[i]['Stock #'], getItemId(shapes, inventory[i]['Shape']), getItemId(colors, inventory[i]['Color']), getItemId(clarities, inventory[i]['Clarity']), getCutId(cuts, inventory[i]['Cut Grade'])
        , getItemId(polishes, inventory[i]['Polish']), getItemId(symmetries, inventory[i]['Symmetry']), getItemId(fluorescences, inventory[i]['Fluorescence Intensity']), inventory[i]['Weight'] != '' ? inventory[i]['Weight'] : 0, inventory[i]['Measurements']
        , inventory[i]['Certificate #'], 

        //certificate_filename
        (certExists) ? (inventory[i]['Certificate #'] + '\.jpg') : '',

        //diamond_image
        (imageExists) ? (inventory[i]['Certificate #'] + '-2\.jpg') : '',

        inventory[i]['RapNet Price'] != '' ? inventory[i]['RapNet Price'] : 0, inventory[i]['Cash Price'] != '' ? inventory[i]['Cash Price'] : 0
        , inventory[i]['Rapnet  Discount %'] != '' ? inventory[i]['Rapnet  Discount %'] : 0, inventory[i]['Cash Price Discount %'] != '' ? inventory[i]['Cash Price Discount %'] : 0, getItemId(labs, inventory[i]['Lab']), inventory[i]['Fluorescence Color'], inventory[i]['Treatment']
        , inventory[i]['Availability'], inventory[i]['Fancy Color'], inventory[i]['Fancy Color Intensity'], inventory[i]['Fancy Color Overtone'], inventory[i]['Depth %'] != '' ? inventory[i]['Depth %'] : 0
        , inventory[i]['Table %'] != '' ? inventory[i]['Table %'] : 0, inventory[i]['Girdle Thin'], inventory[i]['Girdle Thick'], inventory[i]['Girdle %'] != '' ? inventory[i]['Girdle %'] : 0, inventory[i]['Girdle Condition']
        , inventory[i]['Culet Size'], inventory[i]['Culet Condition'], inventory[i]['Crown Height'] != '' ? inventory[i]['Crown Height'] : 0, inventory[i]['Crown Angle'] != '' ? inventory[i]['Crown Angle'] : 0, inventory[i]['Pavilion Depth'] != '' ? inventory[i]['Pavilion Depth'] : 0
        , inventory[i]['Pavilion Angle'] != '' ? inventory[i]['Pavilion Angle'] : 0 , inventory[i]['Laser Inscription'], inventory[i]['Cert comment'], inventory[i]['Country'], inventory[i]['State']
        , inventory[i]['City'], inventory[i]['Time to location'], inventory[i]['Is Matched Pair Separable'], inventory[i]['Pair Stock #'], inventory[i]['Allow RapLink Feed'] != '' ? inventory[i]['Allow RapLink Feed'] : 0
        , inventory[i]['Parcel Stones'], inventory[i]['Trade Show'], inventory[i]['Key to symbols'], inventory[i]['Shade'], inventory[i]['Star Length']
        , inventory[i]['Center Inclusion'], inventory[i]['Black Inclusion'], inventory[i]['Member Comment'], inventory[i]['Report Issue Date'], inventory[i]['Report Type']
        , inventory[i]['Lab Location'], inventory[i]['Brand'], inventory[i]['Milky'], totalPrice ? totalPrice : 0
        ], function (err, rows, fields) {
            if (err) {
                console.log(getItemId(shapes, inventory[i]['Shape']), inventory[i]['Certificate #']);
                ;
                ipr.errors.push(new ErrorForResponse(err.message, 'Error writing data into database'));
            }else{
                successfulSqlResponseCount ++;
            }
            sqlResponseCount++;
  
                                //console.log(sqlResponseCount+" = results = " +i + ' k='+itemsProcessedCount);
                                if (sqlResponseCount == itemsProcessedCount) {
                                    console.log('DONE');
                                    ipr.status = 'success';
                                    ipr.totalItems = parseInt(i)+1;
                                    ipr.itemsImported = successfulSqlResponseCount;
                                    ipr.noCertItems = noCertItemsCount;
                                    ipr.lowQualityItems = lowQualityItemsCount;
                                    ipr.eglIsraelItems = eglIsraelItemsCount;
                                    ipr.duplicateItems = duplicateCertsArray;
                                    res.send(ipr);
                                }
                            });
        
        itemsProcessedCount++;
        }
        ;
        });
});



router.post('/submit', function (req, res) {

    // 1. Drop stones table
    // 2. Recreate stones table LIKE stones_temp
    // 2. Copy all data from stones_temp into stones table
    // 3. truncate stones_temp table
    // 4. Remove processed CSV inventory files

    var isr = new InventorySubmitResponse();
    isr.status = 'success';
    async.series({
        drop_stones: function(callback){
            //pool.query('DROP TABLE stones', function (err, rows, fields) {
            pool.query('DELETE FROM stones WHERE specials != 1', function (err, rows, fields) {
                if (err) {
                    isr.errors.push(JSON.stringify(err));
                    isr.status = 'failure';
                }
                callback(null, 'success');
            });
        },

        /*create_stones: function(callback){
            pool.query('CREATE TABLE stones LIKE stones_temp', function (err, rows, fields) {
                if (err) {
                    isr.errors.push(JSON.stringify(err));
                    isr.status = 'failure';
                }
                //console.log(rows);
                callback(null, 'success');
            });
        },*/
        copy_stones: function(callback){
            pool.query('INSERT INTO stones (' + 
                'stock_number,        shape_id,             color_id,                   clarity_id,           cut_id' +//
                ',polish_id,           symmetry_id,          fluorescence_id,            weight,               measurements' +//
                ',certificate_number,  certificate_filename, diamond_image,              rapnet_price,         cash_price' +//
                ',rapnet_discount,     cash_price_discount,  lab_id,                        fluorescence_color,   treatment' +//
                ',availability,        fancy_color,          fancy_color_intensity,      fancy_color_overtone, depth' +//
                ',table_percent,       girdle_thin,          girdle_thick,               girdle,               girdle_condition' +//
                ',cullet_size,         cullet_condition,     crown_height,               crown_angle,          pavilion_depth' +//
                ',pavilion_angle,      laser_inscription,    cert_comment,               country,              state' +//
                ',city,                time_to_location,     is_matched_pair_separable,  pair_stock_number,    allow_raplink_feed' +//
                ',parcel_stones,       trade_show,           key_to_symbols,             shade,                star_length' +
                ',center_inclusion,    black_inclusion,      member_comment,             report_issue_date,    report_type' +
                ',lab_location,        brand,                milky,      total_price) ' + 
                'SELECT '+
                'stock_number,        shape_id,             color_id,                   clarity_id,           cut_id' +//
                ',polish_id,           symmetry_id,          fluorescence_id,            weight,               measurements' +//
                ',certificate_number,  certificate_filename, diamond_image,              rapnet_price,         cash_price' +//
                ',rapnet_discount,     cash_price_discount,  lab_id,                        fluorescence_color,   treatment' +//
                ',availability,        fancy_color,          fancy_color_intensity,      fancy_color_overtone, depth' +//
                ',table_percent,       girdle_thin,          girdle_thick,               girdle,               girdle_condition' +//
                ',cullet_size,         cullet_condition,     crown_height,               crown_angle,          pavilion_depth' +//
                ',pavilion_angle,      laser_inscription,    cert_comment,               country,              state' +//
                ',city,                time_to_location,     is_matched_pair_separable,  pair_stock_number,    allow_raplink_feed' +//
                ',parcel_stones,       trade_show,           key_to_symbols,             shade,                star_length' +
                ',center_inclusion,    black_inclusion,      member_comment,             report_issue_date,    report_type' +
                ',lab_location,        brand,                milky,      total_price ' + 
            'FROM stones_temp', function (err, rows, fields) {
                if (err) {
                    isr.errors.push(JSON.stringify(err));
                    isr.status = 'failure';
                }
                //console.log(rows);
                callback(null, 'success');
            });
        },
        truncate_staging: function(callback){
            pool.query('TRUNCATE TABLE stones_temp', function (err, rows, fields) {
                if (err) {
                    isr.errors.push(JSON.stringify(err));
                    isr.status = 'failure';
                }
                //console.log(rows);
                callback(null, 'success');
            });
        },
        truncate_sessions: function(callback){
            pool.query('TRUNCATE TABLE sessions', function (err, rows, fields) {
                if (err) {
                    console.log(err);
                    isr.errors.push(JSON.stringify(err));
                    isr.status = 'failure';
                }
                req.session = null //also invalidate your own session (as it was not deleted for some reason)
                callback(null, 'success');
            });
        },
        remove_files: function(callback){
            fs.remove(inventory_storage_temp, function (err) {
                if (err) {
                    console.log('Could not delete ' + inventory_storage_temp + ' ' + err);
                    isr.errors.push(JSON.stringify(err));
                    isr.status = 'failure';
                }
                callback(null, 'success');
            });
        }

    },
    function(err, results) {
        res.send(isr);
    });



});


/*****************************************************************************************
 * Functions
 *
 ******************************************************************************************/
 var getItemId = function (items, name) {
    if (name == '') name = 'other';
    for (var i in items) {
        if (items[i].name == name) {
            return items[i].id;
        }
    }
    //name is not found in the table. Make it 'other' and iterate through array one more time
    name = 'other';
    for (var i in items) {
        if (items[i].name == name) {
            return items[i].id;
        }
    }
}

var getCutId = function (items, name) {
    if (name == '') name = 'VG';

    for (var i in items) {
        if (items[i].name == name) {
            return items[i].id;
        }
    }
    //name is not found in the table. Make it 'other' and iterate through array one more time
    name = 'other';
    for (var i in items) {
        if (items[i].name == name) {
            return items[i].id;
        }
    }
}