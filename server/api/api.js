/*
 * This module provides api resourses
 * for "Pure Diamond" client applications
 * Author: Dmitry Oshchepkov
 * July 2014
 *
 */
var express = require('express');
var router = express.Router();
var stonesSearch = require('./stonesSearch');
var stonesInventory = require('./stonesInventory');
var stonesDetails = require('./stonesDetails');
var ringsDetails = require('./ringsDetails');
var ringsInventory = require('./ringsInventory');
var designersInventory = require('./designersInventory');
var collectionsInventory = require('./collectionsInventory');
var orders = require('./orders');
var ordersInventory = require('./ordersInventory');
var auth = require('./auth/auth');
var ringsSearch = require('./ringsSearch');
var ringsFeatured = require('./ringsFeatured');
var ringsSale = require('./ringsSale');
var ringsDesigner = require('./ringsDesigner');
var stripe = require('./stripe');
var pages = require('./pages');
var productsDetails = require('./productsDetails');
var productsSearch = require('./productsSearch');
var notifications = require('./notifications');

//console.log('api');
module.exports = router;
router.use('/stones/search', stonesSearch);
router.use('/stones/inventory', stonesInventory);
router.use('/stones/details', stonesDetails);
router.use('/rings/details', ringsDetails);
router.use('/rings/featured', ringsFeatured);
router.use('/rings/sale', ringsSale);
router.use('/rings/designer', ringsDesigner);
router.use('/rings/inventory', ringsInventory);
router.use('/designers/inventory', designersInventory);
router.use('/collections/inventory', collectionsInventory);
router.use('/orders', orders);
router.use('/orders/inventory', ordersInventory);
router.use('/rings/search', ringsSearch);
router.use('/auth', auth);
router.use('/stripe', stripe);
router.use('/pages', pages);
router.use('/products', productsSearch);
router.use('/products/details', productsDetails);
router.use('/notifications', notifications);

router.get('/', function (req, res) {
    res.send('Welcome to API');
});
