var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/ringSearchSql');

module.exports = router;

//Importing models
var Ring = require('../models/domain/Product');
var RingDetailsResponse = require('../models/responses/RingDetailsResponse');

/*********************************************************************
 *GET stoneDetails
 **********************************************************************/

router.get('/', function(req, res) {
  var ring = new Ring();
  var rdr = new RingDetailsResponse();
  rdr.status = 'success'
  var ringId = req.query.id

  ring.findById(ringId, function(err, result){
      if (err){
          rdr.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
          rdr.status = 'failure'
      }else{
          rdr.ring = result  
      }
      //console.log(rdr)
      res.send(rdr)
  });
  

});