var express = require('express');
var router = express.Router();
var fs = require('fs-extra');
var path = require('path');
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/ringsInventorySql');
var im = require('imagemagick');

module.exports = router;



//Importing models
var Ring = require('../models/dto/ProductDto');
var Shape = require('../models/dto/ShapeDto');
var Metal = require('../models/dto/MetalDto');
var Collection = require('../models/dto/CollectionDto');
var Designer = require('../models/dto/DesignerDto');
var Image = require('../models/dto/ImageDto');

var ErrorForResponse = require('../models/responses/ErrorForResponse');
var RingsInventoryResponse = require('../models/responses/RingsInventoryResponse');
var RingTemplateResponse = require('../models/responses/RingTemplateResponse');
var GenericResponse = require('../models/responses/GenericResponse');
var InventoryUploadResponse = require('../models/responses/InventoryUploadResponse');
var ImagesResponse = require('../models/responses/ImagesResponse');



var imageStorage = config.path.productsImagesStorage;
var imageUrl = config.path.ringsImagesUrl;

//get all existing rings inventory
router.get('/', function (req, res) {

	//1. Get all rings from DB
	//2. Assing designer, metals, shapes, collections

	var rir = new RingsInventoryResponse();
	rir.status = 'success';

	async.series({
		rings : function(callback){
			pool.query(sql.select.rings, function (err, rows, fields) {
				if (err) {
					rir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
					rir.status = 'failure';
				}
        //console.log(rows);
        for (var i in rows) {
        	var ring = new Ring(rows[i].id, rows[i].title, rows[i].lot_number, rows[i].price, rows[i].stone_weight_min,
        		rows[i].stone_weight_max, rows[i].description, rows[i].instagram, rows[i].youtube, Boolean(rows[i].published), rows[i].category, Boolean(rows[i].featured), rows[i].details);
        	rir.rings.push(ring);
        }//for i in rows
        callback(null, 'success');
    });

		}//rings
	},//final callback
	function(err, results) {

		async.each(rir.rings, function(ring, eachCallback) {

			async.series({
				shapes: function(seriesCallback){
					pool.query(sql.select.shapesForRing, [ring.id], function (err, rows, fields) {
						if (err) {
							rir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
						}
						for (var i in rows) {
			                    //console.log(JSON.stringify(rows[i]));
			                    var shape = new Shape(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, rows[i].class_name, true);
			                    ring.shapes.push(shape);
			                }
			                seriesCallback(null, 'success');
			            });
				},
				metals: function(seriesCallback){
					pool.query(sql.select.metalsForRing, [ring.id], function (err, rows, fields) {
						if (err) {
							rir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
						}
						for (var i in rows) {
			                    //console.log(JSON.stringify(rows[i]));
			                    var metal = new Metal(rows[i].id, rows[i].rank, rows[i].name, true);
			                    ring.metals.push(metal);
			                }
			                seriesCallback(null, 'success');
			            });
				}, 
				collections: function(seriesCallback){
					pool.query(sql.select.collectionsForRing, [ring.id], function (err, rows, fields) {
						if (err) {
							rir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
						}
						for (var i in rows) {
			                    //console.log(JSON.stringify(rows[i]));
			                    var collection = new Collection(rows[i].id, rows[i].rank, rows[i].name, rows[i].description, true);
			                    ring.collections.push(collection);
			                }
			                seriesCallback(null, 'success');
			            });
				},
				designers: function(seriesCallback){
					pool.query(sql.select.designersForRing, [ring.id], function (err, rows, fields) {
						if (err) {
							rir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
						}
						for (var i in rows) {
			                    //console.log(JSON.stringify(rows[i]));
			                    var designer = new Designer(rows[i].id, rows[i].name, rows[i].image, rows[i].description, ring.id, ring.category, true);
			                    ring.designers.push(designer);
			                }
			                seriesCallback(null, 'success');
			            });
				}

        	},//final callback
        	function(err, results) {
        		eachCallback();
			});//series
		}, function(err){
			if( err ) {
				rir.errors.push(new ErrorForResponse(err.message, 'Error processing the request'));
			} else {
		      //console.log('All have been processed successfully');
		      res.send(rir);
		  }
		});

    });//series

});//router


router.get('/ring', function (req, res) {
	//console.log(req.query);
	var rtr = new RingTemplateResponse();
	rtr.status = 'success';

	var ringId = req.query.id;
	if(typeof ringId === 'undefined'){
		ringId = null;
		var ring = new Ring('null', null, null, null, null, null, null);
		ring.id = null;
		rtr.ring = ring;
	}

	async.series({
		ring : function(seriesCallback){
			pool.query(sql.select.ringById, [ringId],function (err, rows, fields) {
				if (err) {
					rtr.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
					rtr.status = 'failure';
				}
	            //console.log(rows);
	            for (var i in rows) {
	            	var ring = new Ring(rows[i].id, rows[i].title, rows[i].lot_number, rows[i].price, rows[i].stone_weight_min,
	            		rows[i].stone_weight_max, rows[i].description, rows[i].instagram, rows[i].youtube, Boolean(rows[i].published), rows[i].category, Boolean(rows[i].featured), rows[i].details, rows[i].discount, Boolean(rows[i].sale), rows[i].image, rows[i].metal_id);
	            	rtr.ring = ring;
	            }//for i in rows
	            seriesCallback(null, 'success');
	        });

		},//rings

		shapes: function(seriesCallback){
			pool.query(sql.select.shapesForRingEdit, [ringId], function (err, rows, fields) {
				if (err) {
					rtr.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
					rtr.status = 'failure';
				}
				for (var i in rows) {
	                    //console.log(JSON.stringify(rows[i]));
	                    var shape = new Shape(rows[i].id, rows[i].rank, rows[i].name, rows[i].friendly_name, rows[i].description, rows[i].class_name, Boolean(rows[i].assigned));
	                    rtr.ring.shapes.push(shape);
	                }
	                seriesCallback(null, 'success');
	            });
		},
		metals: function(seriesCallback){
			pool.query(sql.select.metalsForRingEdit, [ringId], function (err, rows, fields) {
				if (err) {
					rtr.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
					rtr.status = 'failure';
				}
				for (var i in rows) {
	                    //console.log(JSON.stringify(rows[i]));
	                    var metal = new Metal(rows[i].id, rows[i].rank, rows[i].name, Boolean(rows[i].assigned));
	                    rtr.ring.metals.push(metal);
	                }
	                seriesCallback(null, 'success');
	            });
		},
		collections: function(seriesCallback){
			pool.query(sql.select.collectionsForRingEdit, [ringId], function (err, rows, fields) {
				if (err) {
					rtr.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
					rtr.status = 'failure';
				}
				for (var i in rows) {
	                    //console.log(JSON.stringify(rows[i]));
	                    var collection = new Collection(rows[i].id, rows[i].rank, rows[i].name, rows[i].description, Boolean(rows[i].assigned));
	                    rtr.ring.collections.push(collection);
	                }
	                seriesCallback(null, 'success');
	            });
		},
		designers: function(seriesCallback){
			pool.query(sql.select.designersForRingEdit, [ringId], function (err, rows, fields) {
				if (err) {
					rtr.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
					rtr.status = 'failure';
				}
				for (var i in rows) {
	                    //console.log(JSON.stringify(rows[i]));
	                    var designer = new Designer(rows[i].id, rows[i].name, rows[i].slug, rows[i].image, rows[i].description, ringId, false, Boolean(rows[i].assigned));
	                    rtr.ring.designers.push(designer);
	                }
	                seriesCallback(null, 'success');
	            });
		},
		images: function(seriesCallback){
			pool.query(sql.select.ringImages, [ringId], function (err, rows, fields) {
				if (err) {
					rtr.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
					rtr.status = 'failure';
				}
				for (var i in rows) {
	                    //console.log(JSON.stringify(rows[i]));
	                    var image = new Image(rows[i].id, rows[i].file_name, imageUrl+'/'+ringId+'/'+rows[i].file_name, imageUrl+'/'+ringId+'/thumb/'+rows[i].file_name, rows[i].description, Boolean(rows[i].favorite));
	                    rtr.ring.images.push(image);
	                }
	                seriesCallback(null, 'success');
	            });
		}


	},//final callback
	function(err, results) {
		res.send(rtr);
	});//series

});//router

//adding a new ring
router.post('/ring', function (req, res) {
	//console.log(req.body);
	//var rtr = new GenericResponse();
	var rtr = new RingTemplateResponse();
	rtr.status = 'success';
	var newRing = req.body;
	//setting default values to be added to the database
	newRing.published = newRing.published != null ? newRing.published : false;
	newRing.featured = newRing.featured != null ? newRing.featured : false;
	newRing.lot_number = newRing.lot_number != null ? newRing.lot_number : 0;
	newRing.price = newRing.price != null ? newRing.price : 0;
	newRing.stone_weight_min = newRing.stone_weight_min != null ? newRing.stone_weight_min : 0;
	newRing.stone_weight_max = newRing.stone_weight_max != null ? newRing.stone_weight_max : 0;
	newRing.description = newRing.description != null ? newRing.description : '';
	newRing.details = newRing.details != null ? newRing.details : '';
	newRing.discount = newRing.discount != null ? newRing.discount : 0;
	newRing.sale = newRing.sale != null ? newRing.sale : 0;
	newRing.instagram = newRing.instagram != null ? newRing.instagram : '';
	newRing.youtube = newRing.youtube != null ? newRing.youtube : '';

	var newRingId;
	async.series({
		insertRing : function(seriesCallback){
			pool.query(sql.insert.newRing, [newRing.title, newRing.lot_number, newRing.price, newRing.stone_weight_min, newRing.stone_weight_max, newRing.description, newRing.instagram, newRing.youtube, newRing.published, newRing.category, newRing.featured, newRing.details, newRing.discount, newRing.sale],
				function (err, rows, fields) {
					if (err) {
						rtr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
						rtr.status = 'failure';
						res.send(rtr);
					}else{
			            //console.log(rows);
			            newRingId = rows.insertId;
			            newRing.id = newRingId;
			            rtr.ring = newRing;
			            //console.log('newRingId: '+newRingId);
			            seriesCallback(null, 'success');
			        }
			    }
			    );

		},//ring
		insertShapes: function(seriesCallback){
			async.each(newRing.shapes, function(item, eachCallback) {
				pool.query(sql.insert.rings_shapes, [newRingId, item.id], function (err, rows, fields) {
					if (err) {
						rtr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
						rtr.status = 'failure';
					}
					eachCallback();
				});
           	},//async each callback
           	function(err, results) {
           		seriesCallback(null, 'success');
   			});//series
    	},//insertShapes
    	insertMetals: function(seriesCallback){
    		async.each(newRing.metals, function(item, eachCallback) {
    			pool.query(sql.insert.rings_metals, [newRingId, item.id], function (err, rows, fields) {
    				if (err) {
    					rtr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    					rtr.status = 'failure';
    				}
    				eachCallback();
    			});
           	},//async each callback
           	function(err, results) {
           		seriesCallback(null, 'success');
   			});//series
    	},//insertMetals
    	insertCollections: function(seriesCallback){
    		async.each(newRing.collections, function(item, eachCallback) {
    			pool.query(sql.insert.rings_collections, [newRingId, item.id], function (err, rows, fields) {
    				if (err) {
    					rtr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    					rtr.status = 'failure';
    				}
    				eachCallback();
    			});
           	},//async each callback
           	function(err, results) {
           		seriesCallback(null, 'success');
   			});//series
    	},//insertCollections
    	insertDesigners: function(seriesCallback){
    		async.each(newRing.designers, function(item, eachCallback) {
    			pool.query(sql.insert.rings_designers, [newRingId, item.id], function (err, rows, fields) {
    				if (err) {
    					rtr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    					rtr.status = 'failure';
    				}
    				eachCallback();
    			});
           	},//async each callback
           	function(err, results) {
           		seriesCallback(null, 'success');
   			});//series
    	}//insertDesigners
   	},//final callback
   	function(err, results) {
   		rtr.message = 'Ring successfully added'
   		res.send(rtr);
	});//series

});//router



//editing the ring
router.put('/ring', function (req, res) {
	//console.log(req.query);
	var gr = new GenericResponse();
	gr.status = 'success';
	var updatedRing = req.body;
	var ringId = req.query.id;

	console.log('ring', updatedRing);

	async.series({
		updateRing : function(seriesCallback){
			pool.query(sql.update.ring, [updatedRing.title, updatedRing.lot_number, updatedRing.price, updatedRing.stone_weight_min, updatedRing.stone_weight_max, updatedRing.description, updatedRing.instagram, updatedRing.youtube, updatedRing.published, updatedRing.category, updatedRing.featured, updatedRing.details, updatedRing.discount, updatedRing.sale, ringId],
				function (err, rows, fields) {
					if (err) {
						gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
						gr.status = 'failure';
						res.send(gr);
					}else{
						seriesCallback(null, 'success');
					}
				}
				);

		},//updateRing
		deleteShapesReferences: function(seriesCallback){
			pool.query(sql.delete.shapes_references, [ringId], function (err, rows, fields) {
				if (err) {
					gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
					gr.status = 'failure';
					console.log('delete');
					console.log(err.message);
				}
				seriesCallback(null, 'success');
			});
    	},//deleteShapesReferences
    	insertShapes: function(seriesCallback){
    		async.each(updatedRing.shapes, function(item, eachCallback) {
    			pool.query(sql.insert.rings_shapes, [ringId, item.id], function (err, rows, fields) {
    				if (err) {
    					gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    					gr.status = 'failure';
    				}
    				eachCallback();
    			});
           	},//async each callback
           	function(err, results) {
           		seriesCallback(null, 'success');
   			});//series
    	},//insertShapes
    	deleteMetalsReferences: function(seriesCallback){
    		pool.query(sql.delete.metals_references, [ringId], function (err, rows, fields) {
    			if (err) {
    				gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    				gr.status = 'failure';
    			}
    			seriesCallback(null, 'success');
    		});
    	},//deleteMetalsReferences
    	insertMetals: function(seriesCallback){
    		async.each(updatedRing.metals, function(item, eachCallback) {
    			pool.query(sql.insert.rings_metals, [ringId, item.id], function (err, rows, fields) {
    				if (err) {
    					gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    					gr.status = 'failure';
    				}
    				eachCallback();
    			});
           	},//async each callback
           	function(err, results) {
           		seriesCallback(null, 'success');
   			});//series
    	},//insertMetals
    	deleteCollectionsReferences: function(seriesCallback){
    		pool.query(sql.delete.collections_references, [ringId], function (err, rows, fields) {
    			if (err) {
    				gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    				gr.status = 'failure';
    			}
    			seriesCallback(null, 'success');
    		});
    	},//deleteCollectionsReferences
    	insertCollections: function(seriesCallback){
    		async.each(updatedRing.collections, function(item, eachCallback) {
    			pool.query(sql.insert.rings_collections, [ringId, item.id], function (err, rows, fields) {
    				if (err) {
    					gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    					gr.status = 'failure';
    				}
    				eachCallback();
    			});
           	},//async each callback
           	function(err, results) {
           		seriesCallback(null, 'success');
   			});//series
    	},//insertCollections
    	deleteDesignersReferences: function(seriesCallback){
    		pool.query(sql.delete.designers_references, [ringId], function (err, rows, fields) {
    			if (err) {
    				gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    				gr.status = 'failure';
    			}
    			seriesCallback(null, 'success');
    		});
    	},//deleteCollectionsReferences
    	insertDesigners: function(seriesCallback){
    		async.each(updatedRing.designers, function(item, eachCallback) {
    			pool.query(sql.insert.rings_designers, [ringId, item.id], function (err, rows, fields) {
    				if (err) {
    					gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    					gr.status = 'failure';
    				}
    				eachCallback();
    			});
           	},//async each callback
           	function(err, results) {
           		seriesCallback(null, 'success');
   			});//series
    	}//insertDesigners
   	},//final callback
   	function(err, results) {
   		gr.message = 'Ring successfully added'
   		res.send(gr);
	});//series


});//router


//deleting the ring
router.delete('/ring', function (req, res) {
	var gr = new GenericResponse();
	gr.status = 'success';
	var ringId = req.query.id;
	if(typeof ringId === 'undefined'){
		gr.errors.push(new ErrorForResponse('BAD REQUEST', 'Ring ID is not provided'));
		res.send(400, gr);
	}
	async.series({
		deleteImagesFromDisk : function(seriesCallback){
			fs.remove(imageStorage +'/'+ringId, function (err) {
				if (err) {
					console.log('Could not delete ' + imageStorage +'/'+ringId + ' ' + err);
					gr.errors.push(JSON.stringify(err));
					gr.status = 'failure';
                    //res.send(ir);
                }
                seriesCallback(null, 'success');
            });
		},//deleteImagesFromDisk
		deleteImagesFromDB : function(seriesCallback){
			pool.query(sql.delete.images_of_ring, [ringId], function (err, rows, fields) {
				if (err) {
					gr.errors.push(new ErrorForResponse(err.message, 'Error deleting data to the database'));
					gr.status = 'failure';
				}
				seriesCallback(null, 'success');
			});
		},//deleteImagesFromDB
		deleteRing : function(seriesCallback){
			pool.query(sql.delete.ring, [ringId], function (err, rows, fields) {
				if (err) {
					gr.errors.push(new ErrorForResponse(err.message, 'Error deleting data to the database'));
					gr.status = 'failure';
				}
				seriesCallback(null, 'success');
			});
		},//deleteRing
	},//final callback
	function(err, results) {
		res.send(gr);
	});//series

});//router


router.post('/upload/images', function (req, res) {
	var gr = new GenericResponse();
	gr.status = 'success';
	var ringId = req.query.id;
	if(typeof ringId === 'undefined'){
		gr.errors.push(new ErrorForResponse('BAD REQUEST', 'Ring ID is not provided'));
		res.send(400, gr);
	}
	var newName = new Date().getTime();
	var newFileName;
	var newPath;
	req.pipe(req.busboy);

	req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
		fs.ensureDir(imageStorage+'/'+ringId+'/thumb', function (err) {
	      	//console.log('uploading ' + filename);
	      	newFileName = newName + '.' + filename.split('.').pop();
	      	
	      	var wr = fs.createWriteStream(imageStorage+'/'+ringId+'/'+path.basename(newFileName));
	      	
	      	wr.on('pipe', function(src) {
	      		
	      	});
	      	file.pipe(wr);

      	});      	
	});
	req.busboy.on('finish', function (filename) {
    	//console.log('finish');
    	console.log(newFileName);
    	//res.send(gr);
    	var newImageId;
    	var newPath = imageStorage+ringId+'/'+path.basename(newFileName);
	    var thumbPath = imageStorage+ringId+'/thumb/'+path.basename(newFileName);
    	var thumbArgs = [
				newPath,
				'-filter',
				'Triangle',
				'-fuzz', 
				'1%', 
				'-trim', 
				'+repage',
				'-define',
				'filter:support=2',
				'-thumbnail',						
				'450x',
				'-unsharp',
				'0.25x0.25+8+0.065',
				'-dither',
				'None',
				'-posterize',
				'136',
				'-quality',
				'82',
				'-define',
				'jpeg:fancy-upsampling=off',
				'-define',
				'png:compression-filter=5',
				'-define',
				'png:compression-level=9',
				'-define',
				'png:compression-strategy=1',
				'-define',
				'png:exclude-chunk=all',
				'-interlace',
				'none',
				'-colorspace',
				'sRGB',
				'-strip',
				thumbPath
			];
    	async.series({
    		insertImage : function(seriesCallback){
    			pool.query(sql.insert.newImage, [newFileName, null],
    				function (err, rows, fields) {
    					if (err) {
    						gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
    						gr.status = 'failure';
    						res.send(gr);
    					}else{
					            //console.log(rows);
					            newImageId = rows.insertId;
					            console.log('newImageId: '+newImageId);
					            seriesCallback(null, 'success');
					        }
					    }
					    );
				},//ring
				insertRingsImages: function(seriesCallback){
					pool.query(sql.insert.rings_images, [ringId, newImageId, false], function (err, rows, fields) {
						if (err) {
							gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
							gr.status = 'failure';
						}
						seriesCallback(null, 'success');
					});
				}//RingsImages
		   	},//final callback
		   	function(err, results) {
		   		im.convert(thumbArgs, function(err, stdout, stderr){
	      			if (err) throw err;
	      		});
		   		gr.message = 'Image successfully added'
		   		res.send(gr);
			});//series
   
      	

    });

});//router


//deleting the ring
router.delete('/ring/image', function (req, res) {
	var ir = new ImagesResponse();
	ir.status = 'success';
	var ringId = req.query.ringId;
	var imageId = req.query.imageId;
	var imageFileName = null;

	if(typeof ringId === 'undefined'){
		ir.errors.push(new ErrorForResponse('BAD REQUEST', 'Ring ID is not provided'));
		res.send(400, ir);
	}
	if(typeof imageId === 'undefined'){
		ir.errors.push(new ErrorForResponse('BAD REQUEST', 'Image ID is not provided'));
		res.send(400, ir);
	}

	async.series({
		findImageInDB: function(seriesCallback){
			pool.query(sql.select.image, [imageId], function (err, rows, fields) {
				if (err) {
					ir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
					ir.status = 'failure';
					res.send(ir);
				}
				for (var i in rows) {
					imageFileName = rows[i].file_name
				}
				seriesCallback(null, 'success');
			});
    	},//findImageInDB
    	deleteImageFromDisk : function(seriesCallback){
    		fs.remove(imageStorage +'/'+ringId+'/'+ imageFileName, function (err) {
    			if (err) {
    				console.log('Could not delete ' + imageStorage +'/'+ringId+'/'+ imageFileName + ' ' + err);
    				ir.errors.push(JSON.stringify(err));
    				ir.status = 'failure';
    				res.send(ir);
    			}
    			fs.remove(imageStorage +'/'+ringId+'/thumb/'+ imageFileName, function (err) {});
    			seriesCallback(null, 'success');
    		});
		},//deleteImageFromDisk
		deleteImageFromDB : function(seriesCallback){
			pool.query(sql.delete.image, [imageId], function (err, rows, fields) {
				if (err) {
					ir.errors.push(new ErrorForResponse(err.message, 'Error deleting data to the database'));
					ir.status = 'failure';
				}
				seriesCallback(null, 'success');
			});
		},//deleteImageFromDB
		images: function(seriesCallback){
			pool.query(sql.select.ringImages, [ringId], function (err, rows, fields) {
				if (err) {
					ir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
					ir.status = 'failure';
				}
				for (var i in rows) {
	                    //console.log(JSON.stringify(rows[i]));
	                    var image = new Image(rows[i].id, rows[i].file_name, imageUrl+'/'+ringId+'/'+rows[i].file_name, imageUrl+'/'+ringId+'/thumb/'+rows[i].file_name, rows[i].description, Boolean(rows[i].favorite));
	                    ir.images.push(image);
	                }
	                seriesCallback(null, 'success');
	            });
    	} //images
	},//final callback
	function(err, results) {
		res.send(ir);
	});//series
});//router


router.put('/ring/image/primary', function (req, res) {
	var ir = new ImagesResponse();
	ir.status = 'success';
	var ringId = req.query.ringId;
	var imageId = req.query.imageId;

	if(typeof ringId === 'undefined'){
		ir.errors.push(new ErrorForResponse('BAD REQUEST', 'Ring ID is not provided'));
		res.send(400, ir);
	}
	if(typeof imageId === 'undefined'){
		ir.errors.push(new ErrorForResponse('BAD REQUEST', 'Image ID is not provided'));
		res.send(400, ir);
	}
	async.series({
		clearPrimaryFlag: function(seriesCallback){
			pool.query(sql.update.clear_ring_image_primary, [ringId], function (err, rows, fields) {
				if (err) {
					ir.errors.push(new ErrorForResponse(err.message, 'Error updating data in a database'));
					ir.status = 'failure';
				}
				seriesCallback(null, 'success');
			});
    	},//clearPrimaryFlag
    	makeImagePrimary: function(seriesCallback){
    		pool.query(sql.update.ring_image_primary, [ringId, imageId], function (err, rows, fields) {
    			if (err) {
    				ir.errors.push(new ErrorForResponse(err.message, 'Error updating data in a database'));
    				ir.status = 'failure';
    			}
    			seriesCallback(null, 'success');
    		});
    	},//makeImagePrimary
    	images: function(seriesCallback){
    		pool.query(sql.select.ringImages, [ringId], function (err, rows, fields) {
    			if (err) {
    				ir.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
    				ir.status = 'failure';
    			}
    			for (var i in rows) {
              //console.log(JSON.stringify(rows[i]));
              var image = new Image(rows[i].id, rows[i].file_name, imageUrl+'/'+ringId+'/'+rows[i].file_name, imageUrl+'/'+ringId+'/thumb/'+rows[i].file_name, rows[i].description, Boolean(rows[i].favorite));
              ir.images.push(image);
          }
          seriesCallback(null, 'success');
      });
    	} //images
	},//final callback
	function(err, results) {
		res.send(ir);
	});//series


});//router


// Rebuild thumbs for images
router.post('/rebuild_thumbs', function (req, res) {

	var rootDir = config.path.ImagesStorage+'rings';
	var prodsDir = imageStorage;
	var dirs = fs.readdirSync(rootDir);
	var folder = prodsDir;

	fs.ensureDirSync(prodsDir);

	var queue = async.queue(function(data, callback) {
				var path = prodsDir+data.from+ '/';
				if(data.file.charAt(0) != '.' && !fs.statSync(data.dir + '/' + data.file).isDirectory()) {
						
						fs.ensureDirSync(path+'thumb'); 

						var args = [
						data.dir + '/' + data.file,
						'-filter',
						'Triangle',
						'-fuzz', 
						'1%', 
						'-trim', 
						'+repage',
						'-define',
						'filter:support=2',
						'-thumbnail',
						'1000x',
						'-unsharp',
						'0.25x0.25+8+0.065',
						'-dither',
						'None',
						'-posterize',
						'136',
						'-quality',
						'82',
						'-define',
						'jpeg:fancy-upsampling=off',
						'-define',
						'png:compression-filter=5',
						'-define',
						'png:compression-level=9',
						'-define',
						'png:compression-strategy=1',
						'-define',
						'png:exclude-chunk=all',
						'-interlace',
						'none',
						'-colorspace',
						'sRGB',
						'-strip',
						path + data.file
						];

						
						
						var args2 = [
						data.dir + '/' + data.file,
						'-filter',
						'Triangle',
						'-fuzz', 
						'1%', 
						'-trim', 
						'+repage',
						'-define',
						'filter:support=2',
						'-thumbnail',						
						'450x',
						'-unsharp',
						'0.25x0.25+8+0.065',
						'-dither',
						'None',
						'-posterize',
						'136',
						'-quality',
						'82',
						'-define',
						'jpeg:fancy-upsampling=off',
						'-define',
						'png:compression-filter=5',
						'-define',
						'png:compression-level=9',
						'-define',
						'png:compression-strategy=1',
						'-define',
						'png:exclude-chunk=all',
						'-interlace',
						'none',
						'-colorspace',
						'sRGB',
						'-strip',
						path +'thumb/' + data.file
						];
						im.convert(args, function(err, stdout, stderr){
							if (err) throw err;						
							im.convert(args2, function(err, stdout, stderr){
								if (err) throw err; 
								callback();
							});	
						});	
					
				} else { callback(); }

	});

	dirs.forEach(function(dir) {
		
		if(dir.charAt(0) != '.' ) { 
			fs.ensureDirSync(prodsDir+dir); 
			folder = rootDir + '/' + dir;
			files = fs.readdirSync(folder);
			files.forEach(function(f) {
				queue.push({dir:folder,from:dir,file:f});
			});
		}

	});

	res.send(200);

});//router
