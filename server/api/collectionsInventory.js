var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/collectionsInventorySql');

module.exports = router;

//Importing models
var Collection = require('../models/dto/CollectionDto');
var ErrorForResponse = require('../models/responses/ErrorForResponse');
var CollectionsInventoryResponse = require('../models/responses/CollectionsInventoryResponse');
var CollectionTemplateResponse = require('../models/responses/CollectionTemplateResponse');
var GenericResponse = require('../models/responses/GenericResponse');


//get all existing collections 

router.get('/', function(req, res) {

	console.log('getting collections');

  //1. Get all rings from DB
  //2. Assing collection, metals, shapes, collections

  var dir = new CollectionsInventoryResponse();
  dir.status = 'success';


  pool.query(sql.select.collections, function(err, rows, fields) {
      if (err) {
        dir.errors.push(new ErrorForResponse(err.message, 'Error selecting data from the database'));
      }
      //console.log(rows);
      for (var i in rows) {
        var collection = new Collection(rows[i].id, rows[i].rank, rows[i].name, rows[i].description, rows[i].assigned)
        dir.collections.push(collection);
      }
      res.send(dir);

    });

  
}); //router





router.get('/collection', function(req, res) {
  //console.log(req.query);
  var dtr = new CollectionTemplateResponse();
  dtr.status = 'success';
  var collectionId = req.query.id;
  if (typeof collectionId === 'undefined') {
    collectionId = null;
    var collection = new Collection('null', null, null, null, null);
    collection.id = null;
    dtr.collection = collection;
  }
  async.series({
      collection: function(seriesCallback) {
        pool.query(sql.select.collectionById, [collectionId], function(err, rows, fields) {
          if (err) {
            dtr.errors.push(new ErrorForResponse(err.message, 'Error reading data from database'));
            dtr.status = 'failure';
          }
          //console.log(rows);
          for (var i in rows) {
            var collection = new Collection(rows[i].id, rows[i].rank, rows[i].name, rows[i].description, rows[i].assigned);
            dtr.collection = collection;
          } //for i in rows
          seriesCallback(null, 'success');
        });
      }
    }, //final callback
    function(err, results) {
      res.send(dtr);
    }); //series
}); //router
//adding a new collection
router.post('/collection', function(req, res) {
  //console.log(req.body);
  var gr = new GenericResponse();
  gr.status = 'success';
  var newCollection = req.body;
  //setting default values to be added to the database
  var newCollectionId;
  async.series({
      insertCollection: function(seriesCallback) {
        pool.query(sql.insert.newCollection, [newCollection.name,, newCollection.description], function(err, rows, fields) {
          if (err) {
            gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
            gr.status = 'failure';
            res.send(gr);
          } else {
            //console.log(rows);
            newCollectionId = rows.insertId;
            //console.log('newCollectionId: '+newCollectionId);
            seriesCallback(null, 'success');
          }
        });
      }, //collection
    }, //final callback
    function(err, results) {
      gr.message = 'Collection successfully added'
      res.send(gr);
    }); //series
}); //router
//editing the collection
router.put('/collection', function(req, res) {
  //console.log(req.query);
  var gr = new GenericResponse();
  gr.status = 'success';
  var updatedCollection = req.body;
  var collectionId = req.query.id;

  async.series({
      updateCollection: function(seriesCallback) {
        pool.query(sql.update.collection, [updatedCollection.name, updatedCollection.description, collectionId], function(err, rows, fields) {
          if (err) {
            gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
            gr.status = 'failure';
            res.send(gr);
          } else {
            seriesCallback(null, 'success');
          }
        });
      }, //updateCollection
    }, //final callback
    function(err, results) {
      gr.message = 'Collection successfully added'
      res.send(gr);
    }); //series
}); //router
//deleting the collection
router.delete('/collection', function(req, res) {
  var gr = new GenericResponse();
  gr.status = 'success';
  var collectionId = req.query.id;
  if (typeof collectionId === 'undefined') {
    gr.errors.push(new ErrorForResponse(err.message, 'Collection ID is not provided'));
    res.send(400, gr);
  }
  pool.query(sql.delete.collection, [collectionId], function(err, rows, fields) {
    if (err) {
      gr.errors.push(new ErrorForResponse(err.message, 'Error inserting data to the database'));
      gr.status = 'failure';
    }
    res.send(gr);
  });
}); //router