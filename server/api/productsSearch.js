var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;
var sql = require('./sql/stoneSearchSql');

module.exports = router;


//Importing models
var Product = require('../models/domain/Product');
var Collection = require('../models/domain/Collection');
var Designer = require('../models/domain/Designer');
var Metal = require('../models/domain/Metal');
var Shape = require('../models/domain/Shape');
var RingSearchCriteriaDto = require('../models/dto/RingSearchCriteriaDto');
var ErrorForResponse = require('../models/responses/ErrorForResponse');
var SearchRingsResponse = require('../models/responses/SearchRingsResponse');


/*********************************************************************
 *GET searchCriteria.
 **********************************************************************/
router.get('/', function (req, res) {
  var product = new Product();
	var collection = new Collection();
	var designer = new Designer();
	var metal = new Metal();
	var shape = new Shape();

	var searchCriteria = new RingSearchCriteriaDto();

    async.parallel({
        collections: function(parallelCallback){
        	collection.findAll(function(err, result){
        		if (err){
        			searchCriteria.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
        		}else{
        			searchCriteria.collections = result
        		}
        		parallelCallback(null, 'success');
        	});

		},//collections
        designers: function(parallelCallback){
        	designer.findAll(function(err, result){
        		if (err){
        			searchCriteria.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
        		}else{
        			searchCriteria.designers = result
        		}
        		parallelCallback(null, 'success');
        	});
		},//designers
        metals: function(parallelCallback){
        	metal.findAll(function(err, result){
        		if (err){
        			searchCriteria.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
        		}else{
        			searchCriteria.metals = result
        		}
        		parallelCallback(null, 'success');
        	});
		},//metals
        shapes: function(parallelCallback){
        	shape.findAll(function(err, result){
        		if (err){
        			searchCriteria.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
        		}else{
        			searchCriteria.shapes = result
        		}
        		parallelCallback(null, 'success');
        	});
		},//shapes
        price_range: function(parallelCallback){
            product.getPriceRange(function(err, result){
                if (err){
                    searchCriteria.errors.push(new ErrorForResponse(err, 'Error reading data from database'));
                }else{
                    searchCriteria.priceRanges = []
                    if(result.min < 499) { searchCriteria.priceRanges.push({ name: 'CAD 0 - CAD 499', min: 0, max: 499}) }
                    if(result.min < 999) { searchCriteria.priceRanges.push({ name: 'CAD 500 - CAD 999', min: 500, max: 999 }) }
                    if(result.min < 1999 || result.max < 1999) { searchCriteria.priceRanges.push({ name: 'CAD 1,000 - CAD 1,999', min: 1000, max: 1999}) }
                    if(result.min < 2999 || result.max < 2999) { searchCriteria.priceRanges.push({ name: 'CAD 2,000 - CAD 2,999', min: 2000, max: 2999}) }
                    if(result.max > 2999) { searchCriteria.priceRanges.push({ name: 'CAD 3,000 +', min: 3000, max: 999999}) }
                    //searchCriteria.priceRange = result
                }
                parallelCallback(null, 'success');
            });
        }//price_range




    },
    function(err, results) {
        //console.log('searchCriteria: ', searchCriteria)
        res.send(searchCriteria);

    });

});//GET searchCriteria

router.get('/list', function (req, res) {
  var category = req.query.category;
  var product = new Product();
  var products = [];
  product.getByCategory(category, function(err, result){
      if (err){
          console.log('failure get products by category');
      } else {
          products = result;
      }
      res.send(products);
  });
});

router.post('/', function (req, res) {
    var srr = new SearchRingsResponse();
    srr.status = 'success'
    var product = new Product();
    //console.log('searchCriteria: ', req.body)

    product.findAllBySearchCriteria(req.body, function(err, result){
        if (err){
            srr.status = 'failure'
            srr.errors.push(new ErrorForResponse(err, 'Error searching for rings'));
        }else{
            srr.rings = result
        }
        //console.log(srr)
        res.send(srr)
    });

});//POST searchCriteria
