var express = require('express');
var router = express.Router();
var async = require('async');
var config = require('../config/config');
var pool = config.db.pool;

module.exports = router;

//Importing models
var Order = require('../models/domain/Order');
var User = require('../models/domain/User');
var Address = require('../models/domain/Address');
var Notifications = require('../models/domain/Notifications');
var notifications = new Notifications();
var order = new Order();


router.get('/', function(req, res) {
	order.findAll( function(err, result){
		//console.log(result)
		res.send(result)
	});
}); //router

router.get('/session', function(req, res) {
	//console.log(req.sessionID)
	order.findBySessionId(req.sessionID, function(err, result){
		if (result == null){
			res.send(204)
		}else{
			res.send(result)
		}

	});
	//res.send(req.sessionID)

}); //router

router.post('/getProducts', function(req, res) {
	order.getFullProducts(req.body, function(err, result){
		if (result == null){
			res.send(204)
		}else{
			res.send(result)
		}
	});
}); //router

router.post('/updateOrder', function(req, res) {
	//1. check if order exists for this session
	//2. if yes, check if stone is already added
	//3. if no, create new order, add the stone
	//4. return order

	var product = req.body;
	var sessionOrder;
	//console.log("addStone : ",stone)
	async.series({
		checkOrder : function(seriesCallback){
			order.findBySessionId(req.sessionID, function(err, result){
				if (err){console.log('fail');
					seriesCallback(err, 'failure');

				}
				if (result != null){
					sessionOrder = result;
					console.log('check-ok');
					seriesCallback(null, 'success');
				}else{
					order.create(req.sessionID, function(err, result){
						sessionOrder = result;
						console.log('create');
						seriesCallback(null, 'success');
					});
				}
			});
		},//checkOrder
		updateOrder : function(seriesCallback){
			order.updateOrder(sessionOrder, product, function(err, result){
				if (err){
					console.log('fail');
					seriesCallback(err, 'failure');
				} else {
					console.log('upd-ok');
					seriesCallback(null, 'success');
				}

			});
		},//checkOrder
		getOrder : function(seriesCallback){
			order.findBySessionId(req.sessionID, function(err, result){
				if (err){
					console.log('fail');
					seriesCallback(err, 'failure');
				}
				sessionOrder = result;
				console.log('get-ok');
				seriesCallback(null, 'success');
			});
		}//getOrder

	},//final callback
	function(err, results) {
		if (err){
			res.send(500, err);
		}
		res.send(sessionOrder)
	});//series

}); //router


router.post('/addRing', function(req, res) {

	//1. check if order exists for this session
	//2. if yes, check if ring is already added
	//3. if no, create new order, add the ring
	//4. return order

	var ring = req.body;
	var sessionOrder;

	async.series({
		checkOrder : function(seriesCallback){
			order.findBySessionId(req.sessionID, function(err, result){
				if (err){
					seriesCallback(err, 'failure');
				}
				if (result != null){
					sessionOrder = result;
					seriesCallback(null, 'success');
				}else{
					order.create(req.sessionID, function(err, result){
						sessionOrder = result;
						seriesCallback(null, 'success');
					});
				}
			});
		},//checkOrder
		addRing : function(seriesCallback){
			order.addRing(sessionOrder, ring, function(err, result){
				if (err){
					seriesCallback(err, 'failure');
				}
				seriesCallback(null, 'success');
			});
		},//checkOrder
		getOrder : function(seriesCallback){
			order.findBySessionId(req.sessionID, function(err, result){
				if (err){
					seriesCallback(err, 'failure');
				}
				sessionOrder = result;
				seriesCallback(null, 'success');
			});
		}//getOrder

	},//final callback
	function(err, results) {
		if (err){
			res.send(500, err);
		}
		res.send(sessionOrder)
	});//series

}); //router

/*
*
*/
router.put('/checkout', function(req, res) {
	// console.log(req.body)

	var request = req.body;
	//var ringSize = request.ringSize
	var curentStatus = 'checkout'
	// console.log("ringSize: ",ringSize)
	var sessionOrder;

	async.series({
		checkOrder : function(seriesCallback){
			order.findBySessionId(req.sessionID, function(err, result){
				if (err){
					seriesCallback(err, 'failure');
				}
				if (result != null){
					sessionOrder = result;
					seriesCallback(null, 'success');
				}else{
					seriesCallback(err, 'failure');
				}
			});
		},//checkOrder
		// updateRingSize : function(seriesCallback){
		// 	order.updateRingSize(sessionOrder, ringSize, function(err, result){
		// 		if (err){
		// 			seriesCallback(err, 'failure');
		// 		}
		// 		seriesCallback(null, 'success');
		// 	});
		// },//updateRingSize
		updateStatus : function(seriesCallback){
			order.updateStatus(sessionOrder, curentStatus, function(err, result){
				if (err){
					seriesCallback(err, 'failure');
				}
				seriesCallback(null, 'success');
			});
		},//updateStatus
		getOrder : function(seriesCallback){
			order.findBySessionId(req.sessionID, function(err, result){
				if (err){
					seriesCallback(err, 'failure');
				}
				sessionOrder = result;
				seriesCallback(null, 'success');
			});
		}//getOrder

	},//final callback
	function(err, results) {
		if (err){
			res.send(500, err);
		}
		res.send(sessionOrder)
	});//series

}); //router


router.post('/placeOrder', function(req, res) {


	var user = new User();
	// var address = new Address();
	var formData = req.body;
	// var shippingAddress = req.body.shipping;
	console.log("form: ", formData);
	//console.log("shippingAddress: ",shippingAddress)
	//var ringSize = request.ringSize
	//var curentStatus = 'checkout'
	// console.log("ringSize: ",ringSize)
	var sessionOrder;
	var currentUser;
	var currentBillingAddress = formData
	var currentShippingAddress = {}

	async.series({
		checkOrder : function(seriesCallback){
			order.findBySessionId(req.sessionID, function(err, result){
				if (err){
					seriesCallback(err, 'failure');
				}
				if (result != null){
					sessionOrder = result;
					seriesCallback(null, 'success');
				}else{
					seriesCallback(err, 'failure');
				}
			});
		},//checkOrder
		checkUser : function(seriesCallback){
			user.findByUserName(formData.email, function(err, result){
				if (err){
					seriesCallback(err, 'failure');
				}
				if (result != null){
					currentUser = result;
					console.log("user found: ", currentUser)
					seriesCallback(null, 'success');
				}else{
					//if user with this username doesn't exist , create
					user.create(formData, function(err, result){
						if (err){
							seriesCallback(err, 'failure');
						} else {
							currentUser = result;
							console.log("user created: ", currentUser)
							seriesCallback(null, 'success');
						}
					});
					//seriesCallback(null, 'success');
				}
			});
		},//checkUser
		// createBillingAddress : function(seriesCallback){
		// 	address.create(billingAddress, function(err, result){
		// 		if (err){
		// 			seriesCallback(err, 'failure');
		// 		}
		// 		currentBillingAddress = result
		// 		seriesCallback(null, 'success');
		// 	});
		// },//createBillingAddress
		// createShippingAddress : function(seriesCallback){
		// 	if(!billingAddress.shippingSameAsBilling){
		// 		address.create(shippingAddress, function(err, result){
		// 			if (err){
		// 				seriesCallback(err, 'failure');
		// 			}
		// 			currentShippingAddress = result
		// 			seriesCallback(null, 'success');
		// 		});
		// 	}else{
		// 		currentShippingAddress = currentBillingAddress
		// 		seriesCallback(null, 'success');
		// 	}
		// },//createShippingAddress
		// updateUserAddresses : function(seriesCallback){
		// 	user.updateAddresses(currentBillingAddress.id, currentShippingAddress.id, currentUser.id, function(err, result){
		// 		if (err){
		// 			seriesCallback(err, 'failure');
		// 		}
		// 		if (result != null){
		// 			currentUser = result;
		// 			console.log("user updated: ", currentUser)
		// 			seriesCallback(null, 'success');
		// 		}else{
		// 			seriesCallback(err, 'failure');
		// 		}
		// 	});
		// },//updateStatus
		updateOrderShippingDetails : function(seriesCallback){
			
			order.updateShippingDetails(currentBillingAddress, currentShippingAddress, currentUser.id, sessionOrder.id, function(err, result){
				if (err){
					seriesCallback(err, 'failure');
				}
				if (result != null){
					sessionOrder = result;
					console.log("order updated: ", sessionOrder)
					seriesCallback(null, 'success');
				}else{
					seriesCallback(err, 'failure');
				}
			});
		},//updateOrderShippingDetails
		sendNotification : function(seriesCallback){			
			order.getFullProducts(sessionOrder.products, function(err, result){				
				if (!err){
					sessionOrder.fullProducts = result;
				} else {
					sessionOrder.fullProducts = {};
				}
				notifications.OrderPlaced(formData, sessionOrder, function(err, result){
					if (err){
						seriesCallback(err, 'failure');
					} else {
						seriesCallback(null, 'success');
					}
				});
			});			
		}
	},//final callback
	function(err, results) {
		if (err){
			res.send(500, err);
		}
		res.send(sessionOrder)
	});//series

}); //router
