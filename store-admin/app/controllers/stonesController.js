/**
 * stonesController
 */
app.controller('stonesController', function ($scope, $http, $timeout, $upload, $modal, $q, stonesInventoryFactory, DTOptionsBuilder, DTColumnBuilder) {
    getUploadedInventoryFiles();
    $scope.fileReaderSupported = window.FileReader != null;
    $scope.uploadRightAway = true;

    //responses
    $scope.inventoryProcessedResponse;
	$scope.inventoryUploadResponse;
    $scope.submitResponse;
    $scope.inventorySubmitFailed = false;

	$scope.uploadResult = [];
    $scope.inventoryStartOver = false;

	$scope.errorsCollapsed = true;
	$scope.dupCertCollapsed = true;

    $scope.inventoryData = {};

    //getStonesInventory();

    $scope.hasUploader = function (index) {
        return $scope.upload[index] != null;
    };
    $scope.abort = function (index) {
        $scope.upload[index].abort();
        $scope.upload[index] = null;
    };
    //$scope.angularVersion = window.location.hash.length > 1 ? window.location.hash.substring(1) : '1.2.0';
    $scope.onFileSelect = function ($files) {
        $scope.submitResponse = null;
        $scope.selectedFiles = [];
        $scope.progress = [];
        if ($scope.upload && $scope.upload.length > 0) {
            for (var i = 0; i < $scope.upload.length; i++) {
                if ($scope.upload[i] != null) {
                    $scope.upload[i].abort();
                }
            }
        }
        $scope.upload = [];
        $scope.selectedFiles = $files;
        for (var i = 0; i < $files.length; i++) {
            var $file = $files[i];
            //console.log('$file: '+$file.name);
            $scope.progress[i] = -1;
            if ($scope.uploadRightAway) {
                $scope.start(i);
            }
        }
    };

    $scope.start = function (index) {
        $scope.progress[index] = 0;
        $scope.errorMsg = null;
        $scope.upload[index] = $upload.upload({
            url: $scope.baseURL + '/stones/inventory/upload',
            method: 'POST',
            file: $scope.selectedFiles[index],
            fileFormDataName: 'inventory'
        }).then(function (response) {
            $scope.uploadResult.push(response.data);
            $scope.inventoryUploadResponse = response.data;
        }, function (response) {
            if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        }).xhr(function (xhr) {
            xhr.upload.addEventListener('abort', function () {
                console.log('abort complete')
            }, false);
        });
    };

    $scope.resetInputFile = function () {
        var elems = document.getElementsByTagName('input');
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].type == 'file') {
                elems[i].value = null;
            }
        }
    };


    function getUploadedInventoryFiles() {
        stonesInventoryFactory.getUploadedInventoryFiles()
            .then(function (data) {
                $scope.inventoryUploadResponse = data;
            });
    };



    $scope.onProcessClick = function () {
        $scope.processProgessActive = true;
        $scope.inventoryProcessedResponse = null;
        
        stonesInventoryFactory.processUploadedInventory($scope.inventoryData)
            .then(function (data) {
                $scope.processProgessActive = false;
                $scope.inventoryProcessedResponse = data;
            });
    };

    $scope.onStartOverClick = function () {
    	$scope.inventoryProcessedResponse = null;
        stonesInventoryFactory.clearUploadedInventoryFiles()
            .then(function (data) {
                $scope.inventoryUploadResponse = data;
                $scope.inventoryStartOver = true;
            });
    };

    $scope.onSubmitClick = function () {
        $scope.processProgessActive = true;
        //$scope.inventoryProcessedResponse = null;
        stonesInventoryFactory.submitInventory()
            .then(function (data) {
                $scope.processProgessActive = false;
                $scope.submitResponse = data;
                console.log($scope.submitResponse);
                if($scope.submitResponse.status == 'success' && !$scope.submitResponse.errors.length > 0){
                    $scope.inventoryUploadResponse = null;
                    $scope.inventoryProcessedResponse = null;
                    
                }else{
                    $scope.inventorySubmitFailed = true;
                }
            });
    };



    //Stones inventory
    $scope.dtInstance = {};
    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
        var defer = $q.defer();
        stonesInventoryFactory.getStonesInventory()
        .then(function (data) {           
            defer.resolve(data.result);
        });
        return defer.promise;
    }).withOption('rowCallback', rowCallback).withOption('order', [0, 'desc']);

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID'),
        DTColumnBuilder.newColumn('stock_number').withTitle('Stock#'),
        DTColumnBuilder.newColumn('shape').withTitle('Shape'),
        DTColumnBuilder.newColumn('weight').withTitle('Carat'),
        DTColumnBuilder.newColumn('color').withTitle('Color'),
        DTColumnBuilder.newColumn('clarity').withTitle('Clarity'),
        DTColumnBuilder.newColumn('cut').withTitle('Cut'),
        DTColumnBuilder.newColumn('polish').withTitle('Polish'),
        DTColumnBuilder.newColumn('fluorescence').withTitle('Fluorescence'),
        DTColumnBuilder.newColumn('lab').withTitle('Report'),
        DTColumnBuilder.newColumn('price').withTitle('Price')
    ];

    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function() {
            $scope.onEditStoneClick(aData);
        });
        return nRow;
    }

    function getStonesInventory() {
        var defer = $q.defer();
        stonesInventoryFactory.getStonesInventory()
        .then(function (data) {
            //$scope.stonesInventory = JSON.stringify(data.result);
            defer.resolve(data.result);
        });
        return defer.promise;
    }



    $scope.onEditStoneClick = function(stone){
        $scope.onAddStoneClick('lg', stone, true);
    }

    $scope.onAddStoneClick = function (size, stone, isEdit) {
        $scope.isEdit = isEdit;
        var modalInstance = $modal.open({
            templateUrl: 'templates/stone-details-modal.html?v=4.4',
            controller: StoneModalController,
            size: size,
            resolve: {
                stoneTemplate: function($q){
                    var def = $q.defer();
                    if (isEdit){
                        stonesInventoryFactory.getStoneForEdit(stone.id)
                        .then(function (data) {
                            if (data.errors.length > 0){
                                console.log(data.errors);
                            }else{
                                def.resolve(data.stone);
                            }
                        })
                    }else{
                        stonesInventoryFactory.getStoneTemplate()
                        .then(function (data) {
                            if (data.errors.length > 0){
                                console.log(data.errors);
                            }else{
                                def.resolve(data.stone);
                            }
                        })
                    }
                    return def.promise;
                    },
                    isEdit: function(){
                    return isEdit;
                    }
                }
      });
      //called after submit event is handled
      modalInstance.result.then(function () {
        //getStonesInventory();
        $scope.dtInstance.changeData(getStonesInventory());
      }, function(){
        //cancelled
      });
    };//onAddRingClick


});

var StoneModalController = function ($scope, $modalInstance, $timeout, $upload, $window, $q, stonesInventoryFactory, stoneTemplate, isEdit) {
    $scope.isEdit = isEdit;
    $scope.baseURL = "/api";
    $scope.stone = stoneTemplate;

    $scope.selectedItems = {};
    $scope.selectedItems.selectedShapes = getSelectedItems($scope.stone.shapes, true);
    $scope.selectedItems.selectedColors = getSelectedItems($scope.stone.colors, true);
    $scope.selectedItems.selectedClarities = getSelectedItems($scope.stone.clarities, true);
    $scope.selectedItems.selectedCuts = getSelectedItems($scope.stone.cuts, true);
    $scope.selectedItems.selectedLabs = getSelectedItems($scope.stone.labs, true);
    $scope.selectedItems.selectedPolishes = getSelectedItems($scope.stone.polishes, true);
    $scope.selectedItems.selectedFluorescences = getSelectedItems($scope.stone.fluorescences, true);
    $scope.selectedItems.selectedSymmetries = getSelectedItems($scope.stone.symmetries, true);


    $scope.fileReaderSupported = window.FileReader != null;
    $scope.uploadRightAway = false;
    $scope.uploadResult = [];
    $scope.selectedFiles = [];


    $scope.hasUploader = function (index) {
        return $scope.upload[index] != null;
    };
    $scope.abort = function (index) {
        $scope.upload[index].abort();
        $scope.upload[index] = null;
    };

    $scope.clearFiles = function(){
        $scope.selectedFiles = [];
    }

    $scope.onFileSelect = function ($files) {
        //console.log($files)
        $scope.submitResponse = null;
        $scope.selectedFiles = [];
        $scope.progress = [];
        if ($scope.upload && $scope.upload.length > 0) {
            for (var i = 0; i < $scope.upload.length; i++) {
                if ($scope.upload[i] != null) {
                    $scope.upload[i].abort();
                }
            }
        }
        $scope.upload = [];
        $scope.selectedFiles = $files;
        $scope.dataUrls = [];
        for (var i = 0; i < $files.length; i++) {
            var $file = $files[i];
                if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($files[i]);
                    var loadFile = function(fileReader, index) {
                        fileReader.onload = function(e) {
                            $timeout(function() {
                                $scope.dataUrls[index] = e.target.result;
                            });
                        }
                    }(fileReader, i);
                }

            //console.log('$file: '+$file.name);
            $scope.progress[i] = -1;
            /*if ($scope.uploadRightAway) {
                $scope.start(i);
            }*/
        }
    };

    $scope.submitStoneImages = function(files, stone){
        var def = $q.defer();
        //$scope.stone = stone;
        submitImages(files, stone);
        def.resolve(stone); //This is probably wrong.
        return def.promise;
    }
    var submitImages = function(files, stone) {
        //console.log(files);
        
        var file = files.shift();
        if(typeof file === 'undefined'){
            return;
        }
        var index = 1;
        //$scope.progress[index] = 0;
        $scope.errorMsg = null;
        $scope.upload[index] = $upload.upload({
            url: $scope.baseURL + '/stones/inventory/upload/images?id='+stone.id,
            method: 'POST',
            file: file,
            fileFormDataName: 'inventory'
        }).then(function (response) {
            $scope.uploadResult.push(response.data);
            $scope.inventoryUploadResponse = response.data;
            //console.log(file);
            submitImages(files, stone);   //run through files recursively
        }, function (response) {
            if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            //$scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        }).xhr(function (xhr) {
            xhr.upload.addEventListener('abort', function () {
                console.log('abort complete')
            }, false);
        });
    };

    $scope.imageDelete = function(image) {
        stonesInventoryFactory.deleteStoneImage($scope.stone.id, image.id)
            .then(function (data) {
                $scope.stone.images = data.images;
        });
    }
    $scope.imageFavorite = function(image) {
        stonesInventoryFactory.makeImagePrimaryForStone($scope.stone.id, image.id)
            .then(function (data) {
                $scope.stone.images = data.images;
        });
    }


    $scope.delete = function (stone) {
        stonesInventoryFactory.deleteStone(stone.id)
            .then(function (data) {
            /*if (data.errors.length > 0){
                console.log(data.errors);
            }else{*/
                $modalInstance.close();
            //}
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.previewStone = function(stone) {
        $window.open($window.location.origin + '/store/products/diamonds/' + stone.stock_number, '_blank');
    }

    $scope.submit = function (result, files, preview) {

        //console.log(result.published);
        if( $scope.stone.stock_number != null && $scope.stone.price != null && $scope.stone.weight != null &&
            result.selectedLabs != null && result.selectedShapes != null && result.selectedColors != null 
            && result.selectedClarities != null && result.selectedCuts != null
            ){
            console.log(result);
                $scope.stone.lab_id = result.selectedLabs.id;
                $scope.stone.shape_id = result.selectedShapes.id;
                $scope.stone.color_id = result.selectedColors.id;
                $scope.stone.clarity_id = result.selectedClarities.id;
                $scope.stone.cut_id = result.selectedCuts.id;
                $scope.stone.polish_id = result.selectedPolishes === undefined ? 5 : result.selectedPolishes.id;
                $scope.stone.symmetry_id = result.selectedSymmetries === undefined ? 6 : result.selectedSymmetries.id;
                $scope.stone.fluorescence_id = result.selectedFluorescences === undefined ? 8 : result.selectedFluorescences.id;
                

                if(isEdit){
                    stonesInventoryFactory.editStone($scope.stone)
                    .then(function (data) {                        
                        $scope.submitStoneImages(files, $scope.stone).then(function(stone){
                            if (preview) {
                                $scope.previewStone(stone);
                            } else {
                                $modalInstance.close();
                            }
                        })
                    });
                }else{
                    stonesInventoryFactory.submitNewStone($scope.stone)
                    .then(function (data) {
                        console.log(data.stone);
                        $scope.submitStoneImages(files, data.stone).then(function(stone){
                        if (preview) {
                            $scope.previewRing(stone);
                        } else {
                            $modalInstance.close();
                        }
                    })
                    });
                }
        }
    };

}

var getSelectedItems = function(elements, single = false){
  var selectedElements = [];
  angular.forEach(elements, function(element){
    if(element.assigned == true){
      selectedElements.push(element);
    }
  });
  if(single) { return selectedElements[0]; }
  else { return selectedElements; }
}
