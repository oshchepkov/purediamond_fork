app.controller('appController', function ($scope, $location) {
    //console.log("appController is initializing...");
    $scope.baseURL = "/api";
    $scope.startLoading = function () {
        $scope.isLoading = true;
    }
    $scope.stopLoading = function () {
        $scope.isLoading = false;
    }
    $scope.startLoading();

    $scope.goRoute = function (route) {
        $location.path(route);
    };

    $scope.overrideOptions = {
        "bStateSave": true,
        "iCookieDuration": 2419200, /* 1 month */
        "bJQueryUI": true,
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": true,
        "bDestroy": true
    };

});//appController
