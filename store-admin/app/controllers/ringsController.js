app.controller('ringsController', function ($scope, $http, $modal, $timeout, $window, $q, ringsInventoryFactory, DTOptionsBuilder, DTColumnBuilder) {



	//$scope.ringForEditForm;
	//getRingsInventory();

	$scope.onEditRingClick = function(ring){
		$scope.onAddRingClick('lg', ring, true);
	}

	$scope.dtInstance = {};
	$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
        var defer = $q.defer();
       	ringsInventoryFactory.getRingsInventory()
        .then(function (data) {           
            defer.resolve(data.rings);
        });
        return defer.promise;
    }).withOption('rowCallback', rowCallback).withOption('order', [0, 'desc']);

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID'),
        DTColumnBuilder.newColumn('lot_number').withTitle('Lot#'),
        DTColumnBuilder.newColumn('title').withTitle('Title'),
        DTColumnBuilder.newColumn('price').withTitle('Price'),
        DTColumnBuilder.newColumn('stone_weight_min').withTitle('Stone Weight').renderWith(function(data, type, full) {
            return full.stone_weight_min + 'ct - ' + full.stone_weight_max + 'ct';
        })
        // DTColumnBuilder.newColumn('stone_weight_max').withTitle('Metals').renderWith(function(data, type, full) {
        // 	var metals = [];
        // 	for(var i = 0; i < full.metals.length; i++) {
        // 		metals.push(' '+full.metals[i].name);
        // 	}        	
        //     return metals.toString();
        // })
    ];

    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function() {
            $scope.onEditRingClick(aData);
        });
        return nRow;
    }


	$scope.onAddRingClick = function (size, ring, isEdit) {
		$scope.isEdit = isEdit;
	 	var modalInstance = $modal.open({
		    templateUrl: 'templates/ring-details-modal.html?v=1.9',
		    controller: RingsModalController,
		    size: size,
		    resolve: {
			      /*ring: function(){
			        return ring;
			      },*/
			      ringTemplate: function($q){
			      	var def = $q.defer();
			      	if (isEdit){
				      	ringsInventoryFactory.getRingForEdit(ring.id)
				      		.then(function (data) {
				      		if (data.errors.length > 0){
				      			console.log(data.errors);
				      		}else{
				      	    	def.resolve(data.ring);
				      		}
				      	})
			      	}else{
			      		ringsInventoryFactory.getRingTemplate()
			      			.then(function (data) {
			      			if (data.errors.length > 0){
			      				console.log(data.errors);
			      			}else{
			      		    	def.resolve(data.ring);
			      			}
			      		})
			      	}
			      	return def.promise;
			      },
			      isEdit: function(){
			      	return isEdit;
			      }
			    }
	  });
	  //called after submit event is handled
	  modalInstance.result.then(function () {
	  	//getRingsInventory();
	  	$scope.dtInstance.changeData(getRingsInventory());
	  }, function(){
	  	//cancelled
	  });
	};//onAddRingClick

	$scope.onPreviewRing = function(product) {
		$window.open($window.location.origin + '/store/products/'+product.category+'/' + product.id, '_blank');
	};

	function getRingsInventory() {
        var defer = $q.defer();
       	ringsInventoryFactory.getRingsInventory()
        .then(function (data) {           
            defer.resolve(data.rings);
        });
        return defer.promise;
	};

	$scope.rebuild_thumbs = function() {
		ringsInventoryFactory.rebuild_thumbs()
			.then(function (data) {

		});
	}


});


var RingsModalController = function ($scope, $modalInstance, $timeout, $upload, $window, $q, ringsInventoryFactory, ringTemplate, isEdit) {
	$scope.isEdit = isEdit;
	$scope.baseURL = "/api";
	$scope.ring = ringTemplate;

	$scope.categories = [
		{name:'Rings', slug:'rings', assigned:(($scope.ring.category == 'rings')?true:false)}, 
		{name:'Earrings', slug:'earrings', assigned:(($scope.ring.category == 'earrings')?true:false)}, 
		{name:'Pendants', slug:'pendants', assigned:(($scope.ring.category == 'pendants')?true:false)}, 
		{name:'Wedding Bands', slug:'wedding-bands', assigned:(($scope.ring.category == 'wedding-bands')?true:false)}
	];

	$scope.selectedItems = {};
	$scope.selectedItems.selectedMetals = getSelectedItems($scope.ring.metals);
	$scope.selectedItems.selectedShapes = getSelectedItems($scope.ring.shapes);
	$scope.selectedItems.selectedCollections = getSelectedItems($scope.ring.collections);
	$scope.selectedItems.selectedDesigner = getSelectedItems($scope.ring.designers, true);
	$scope.selectedItems.selectedCategory = getSelectedItems($scope.categories, true);
	if(typeof $scope.selectedItems.selectedDesigner === 'undefined') {
		$scope.selectedItems.selectedDesigner = $scope.ring.designers[1];
	}
	if(typeof $scope.selectedItems.selectedCategory === 'undefined') {
		$scope.selectedItems.selectedCategory = $scope.categories[0];
	}

//images upload
	//console.log($files)
    $scope.fileReaderSupported = window.FileReader != null;
    $scope.uploadRightAway = false;
	$scope.uploadResult = [];
	$scope.selectedFiles = [];


    $scope.hasUploader = function (index) {
        return $scope.upload[index] != null;
    };
    $scope.abort = function (index) {
        $scope.upload[index].abort();
        $scope.upload[index] = null;
    };

   $scope.onFileSelect = function ($files) {
   		//console.log($files)
        $scope.submitResponse = null;
        $scope.selectedFiles = [];
        $scope.progress = [];
        if ($scope.upload && $scope.upload.length > 0) {
            for (var i = 0; i < $scope.upload.length; i++) {
                if ($scope.upload[i] != null) {
                    $scope.upload[i].abort();
                }
            }
        }
        $scope.upload = [];
        $scope.selectedFiles = $files;
        $scope.dataUrls = [];
        for (var i = 0; i < $files.length; i++) {
            var $file = $files[i];
				if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
					var fileReader = new FileReader();
					fileReader.readAsDataURL($files[i]);
					var loadFile = function(fileReader, index) {
						fileReader.onload = function(e) {
							$timeout(function() {
								$scope.dataUrls[index] = e.target.result;
							});
						}
					}(fileReader, i);
				}

            //console.log('$file: '+$file.name);
            $scope.progress[i] = -1;
            /*if ($scope.uploadRightAway) {
                $scope.start(i);
            }*/
        }
    };

    $scope.submitRingImages = function(files, ring){
      var def = $q.defer();
    	$scope.ring	= ring;
		  start(files);
      def.resolve(ring); //This is probably wrong.
		  return def.promise;
    }

    var start = function (files) {
		//console.log(files);
    	var file = files.shift();
    	if(typeof file === 'undefined'){
    		return;
    	}
    	var index = 1;
        //$scope.progress[index] = 0;
        $scope.errorMsg = null;
        $scope.upload[index] = $upload.upload({
            url: $scope.baseURL + '/rings/inventory/upload/images?id='+$scope.ring.id,
            method: 'POST',
            file: file,
            fileFormDataName: 'inventory'
        }).then(function (response) {
            $scope.uploadResult.push(response.data);
            $scope.inventoryUploadResponse = response.data;
            //console.log(file);
			start(files);	//run through files recursively
        }, function (response) {
            if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            //$scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        }).xhr(function (xhr) {
            xhr.upload.addEventListener('abort', function () {
                console.log('abort complete')
            }, false);
        });
    };

	$scope.clearFiles = function(){
		$scope.selectedFiles = [];
	}
    //images upload//

	$scope.imageDelete = function(image){
		//console.log(image);
		ringsInventoryFactory.deleteRingImage($scope.ring.id, image.id)
			.then(function (data) {
				$scope.ring.images = data.images;
		});
	}
	$scope.imageFavorite = function(image){
		//console.log(image);
		ringsInventoryFactory.makeImagePrimaryForRing($scope.ring.id, image.id)
			.then(function (data) {
				$scope.ring.images = data.images;
		});
	}

	$scope.submit = function (result, files, preview) {

		//console.log(result.published);
		if(	$scope.ring.title != null && $scope.ring.lot_number != null && $scope.ring.price != null &&
			$scope.ring.stone_weight_min != null && $scope.ring.stone_weight_max != null && result.selectedDesigner != null &&
			result.selectedMetals.length > 0 && result.selectedShapes.length > 0 && result.selectedCollections.length > 0
			){
				$scope.ring.metals = result.selectedMetals;
				$scope.ring.shapes = result.selectedShapes;
				$scope.ring.collections = result.selectedCollections;
				$scope.ring.designers = [];
				$scope.ring.designers.push(result.selectedDesigner);
				$scope.ring.category = result.selectedCategory.slug;

				if(isEdit){
				    ringsInventoryFactory.editRing($scope.ring)
				        .then(function (data) {
                    $scope.submitRingImages(files, $scope.ring).then(function(ring){
                      if (preview) {
                        //Open new window to preview the ring
                        $scope.previewRing(ring);
                      } else {
                        $modalInstance.close();
                      }
                    })
				    });
				}else{
					ringsInventoryFactory.submitNewRing($scope.ring)
					    .then(function (data) {
					    	console.log(data);

					    	$scope.submitRingImages(files, data.ring).then(function(ring){
                  if (preview) {
                        //Open new window to preview the ring
                        $scope.previewRing(ring);
                      } else {
                        $modalInstance.close();
                      }
                })

					});
				}
		}
	};


	$scope.delete = function (ring) {
		ringsInventoryFactory.deleteRing(ring.id)
			.then(function (data) {
			/*if (data.errors.length > 0){
				console.log(data.errors);
			}else{*/
		    	$modalInstance.close();
			//}
		});
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

  $scope.previewRing = function(ring) {
    $window.open($window.location.origin + '/store/products/'+ring.category+'/' + ring.id, '_blank');
  }

};


var getSelectedItems = function(elements, single = false){
  var selectedElements = [];
  angular.forEach(elements, function(element){
    if(element.assigned == true){
      selectedElements.push(element);
    }
  });
  if(single) { return selectedElements[0]; }
  else { return selectedElements; }
}
