app.controller('ordersController', function ($scope, $http, $filter, ordersFactory) {

  $scope.dtOptions = {
    order: [[ 0, "desc" ]]
  }

  getOrders();

  function getOrders() {
    ordersFactory.getOrdersInventory()
    .then(function (data) {
      if (data.errors.lenght > 0){
        console.log(data.errors);
      }else{
        $scope.orders = data.orders;
      }
    });
  };  

  $scope.onDeleteOrderClick = function(order) {
    
    ordersFactory.deleteOrder(order.id)
    .then(function (data) {
      if (data.status != 'Fail'){        
        console.log(data);
        $scope.orders.splice($scope.orders.indexOf(order), 1);
      }
    });
  };



});

