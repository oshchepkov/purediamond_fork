app.controller('designersController', function ($scope, $http, $modal, $timeout, $upload, designersFactory) {

  getDesigners();

  $scope.onEditDesignerClick = function(designer){
    //console.log(designer);
    //$scope.designerForEditForm = designer;
    $scope.onAddDesignerClick('lg', designer, true);
  }



  $scope.onAddDesignerClick = function (size, designer, isEdit) {
    $scope.isEdit = isEdit;
    var modalInstance = $modal.open({
      templateUrl: 'templates/designer-details-modal.html',
      controller: DesignersModalController,
      size: size,
      resolve: {
            /*ring: function(){
              return ring;
            },*/
            designerTemplate: function($q){
              var def = $q.defer();
              if (isEdit){
                designersFactory.getDesignerForEdit(designer.id)
                .then(function (data) {
                  if (data.errors.length > 0){
                    console.log(data.errors);
                  }else{
                    def.resolve(data.designer);
                  }
                })
              }else{
                designersFactory.getDesignerTemplate()
                .then(function (data) {
                  if (data.errors.length > 0){
                    console.log(data.errors);
                  }else{
                    def.resolve(data.designer);
                  }
                })
              }
              return def.promise;           
            },
            isEdit: function(){
              return isEdit;
            }
          }
        });
    //called after submit event is handled
    modalInstance.result.then(function () {
      getDesigners();
    }, function(){
      //cancelled
    });
  };//onAddDesignerClick

  function getDesigners() {
    designersFactory.getDesignersInventory()
    .then(function (data) {
      if (data.errors.length > 0){
        console.log(data.errors);
      }else{
        $scope.designers = data.designers;
      }
    });
  };  



});


var DesignersModalController = function ($scope, $modalInstance, $timeout, designersFactory, designerTemplate, isEdit) {
  $scope.isEdit = isEdit;
  $scope.designer = designerTemplate;
  




  $scope.submit = function (result) {
    //console.log(result.published);

    if ($scope.designer.name != null) {
      if(isEdit){
        designersFactory.editDesigner($scope.designer)
        .then(function (data) {
          $modalInstance.close();
        });
      }else{
        designersFactory.submitNewDesigner($scope.designer)
        .then(function (data) {
          $modalInstance.close();
        });
      }
    }
  };

  $scope.delete = function (designer) {
    designersFactory.deleteDesigner(designer.id)
    .then(function (data) {
      /*if (data.errors.length > 0){
        console.log(data.errors);
      }else{*/
        $modalInstance.close();
      //}
    });   
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.slugGen = function(data) {
    if(data) {
      $scope.designer.slug = data.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
    } else { $scope.designer.slug = null; }
  };

};





