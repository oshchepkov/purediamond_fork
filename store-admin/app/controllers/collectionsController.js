app.controller('collectionsController', function ($scope, $http, $modal, $timeout, $upload, collectionsFactory) {

  getCollections();

  $scope.onEditCollectionClick = function(collection){
    //console.log(collection);
    //$scope.collectionForEditForm = collection;
    $scope.onAddCollectionClick('lg', collection, true);
  }



  $scope.onAddCollectionClick = function (size, collection, isEdit) {
    $scope.isEdit = isEdit;
    var modalInstance = $modal.open({
      templateUrl: 'templates/collection-details-modal.html',
      controller: CollectionsModalController,
      size: size,
      resolve: {
            /*ring: function(){
              return ring;
            },*/
            collectionTemplate: function($q){
              var def = $q.defer();
              if (isEdit){
                collectionsFactory.getCollectionForEdit(collection.id)
                .then(function (data) {
                  if (data.errors.length > 0){
                    console.log(data.errors);
                  }else{
                    def.resolve(data.collection);
                  }
                })
              }else{
                collectionsFactory.getCollectionTemplate()
                .then(function (data) {
                  if (data.errors.length > 0){
                    console.log(data.errors);
                  }else{
                    def.resolve(data.collection);
                  }
                })
              }
              return def.promise;
            },
            isEdit: function(){
              return isEdit;
            }
          }
        });
    //called after submit event is handled
    modalInstance.result.then(function () {
      getCollections();
    }, function(){
      //cancelled
    });
  };//onAddCollectionClick

  function getCollections() {
    collectionsFactory.getCollectionsInventory()
    .then(function (data) {
      if (data.errors.length > 0){
        console.log(data.errors);
      }else{
        $scope.collections = data.collections;
      }
    });
  };



});


var CollectionsModalController = function ($scope, $modalInstance, $timeout, collectionsFactory, collectionTemplate, isEdit) {
  $scope.isEdit = isEdit;
  $scope.collection = collectionTemplate;





  $scope.submit = function (result) {
    //console.log(result.published);

    if ( $scope.collection.name != null) {
      if(isEdit){
        collectionsFactory.editCollection($scope.collection)
        .then(function (data) {
          $modalInstance.close();
        });
      }else{
        collectionsFactory.submitNewCollection($scope.collection)
        .then(function (data) {
          $modalInstance.close();
        });
      }
    }
  };

  $scope.delete = function (collection) {
    collectionsFactory.deleteCollection(collection.id)
    .then(function (data) {
      /*if (data.errors.length > 0){
        console.log(data.errors);
      }else{*/
        $modalInstance.close();
      //}
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

};
