var app = angular.module("pureDiamondAdminApp",['ngRoute','ui.bootstrap','angularFileUpload', 'textAngular', 'datatables'])
	.config(
		function($routeProvider, $locationProvider) {
			//console.log("pureDiamond app is initializing...");

			$routeProvider
				.when('/stones',
					{
						templateUrl: 'app/partials/stones.html?v=4',
						controller: "stonesController"
					})
				.when('/designers',
					{
						templateUrl: 'app/partials/designers.html',
						controller: "designersController"
					})
				.when('/products',
					{
						templateUrl: 'app/partials/rings.html?v=6.7',
						controller: "ringsController"
					})
				.when('/collections',
					{
						templateUrl: 'app/partials/collections.html',
						controller: "collectionsController"
					})
				.when('/orders',
					{
						templateUrl: 'app/partials/orders.html?v=1.8',
						controller: "ordersController"
					})
				.when('/pages/:pageSlug',
					{
						templateUrl: 'app/partials/pages.html?v=2',
						controller: "pagesController"
					})
				.otherwise({ redirectTo: '/stones' });
    		//$locationProvider.html5Mode(true);
		}
	);

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
