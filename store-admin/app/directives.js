/**
 * Directives
 */

/*app.directive('vivoFocusMe', function( $timeout ) {
	return function( scope, elem, attrs ) {
	    scope.$watch(attrs.vivoFocusMe, function( newval ) {
	        $timeout(function() {
	          elem[0].focus();
	        }, 0, false);
	    });
	  };
});*/

/*app.directive('orderStatus', function() {
	return function( scope, elem, attrs ) {
			scope.$watch(attrs.currentTime, function(val) {

				if(!scope.mobileOrder.isDelivered){
					if (val > scope.mobileOrder.pickupTime){
						elem.removeClass('text-success');
						elem.addClass('text-danger');
						scope.orderStatusText = "PAST DUE";
					}else{
						elem.removeClass('text-danger');
						elem.removeClass('text-success');
						scope.orderStatusText = "PENDING";
					}
				}else{
					elem.removeClass('text-danger');
					elem.addClass('text-success');
					scope.orderStatusText = "DELIVERED";
				}

		    });

		scope.$watch(attrs.deliveryStatus, function(val) {
			if(val){
				elem.removeClass('text-danger');
				elem.addClass('text-success');
				scope.orderStatusText = "DELIVERED";
			}
		});
	};
});*/

/*app.directive('sortedColumn', function( $timeout ) {
	return function( scope, elem, attrs ) {
	    scope.$watch(attrs.sortedBy, function(val) {
	    	if (val === attrs.columnName){
	    		if (scope.reverse){
	    			elem.removeClass('glyphicon glyphicon-arrow-down');
	    			elem.addClass('glyphicon glyphicon-arrow-up');
	    		}else{
	    			elem.removeClass('glyphicon glyphicon-arrow-up');
	    			elem.addClass('glyphicon glyphicon-arrow-down');
	    		}
	    	}else{
	    		elem.removeClass('glyphicon glyphicon-arrow-up');
	    		elem.removeClass('glyphicon glyphicon-arrow-down');
	    	}
	    });
	  };
});*/

/*app.directive('vivoCheckInterval', function() {
	return function( scope, elem, attrs ) {
	    scope.$watch(attrs.current, function(val) {
	    	if (val == attrs.set){
	    		elem.addClass('glyphicon glyphicon-ok');
	    	}else{
	    		elem.removeClass('glyphicon glyphicon-ok');
	    	}
	    });
	}
});*/

/*app.directive('vivoSwitchAutoRefresh', function() {
	return function( scope, elem, attrs ) {
	    scope.$watch(attrs.isEnabled, function(val) {
	    	if (val == eval(attrs.set)){
	    		elem.addClass('glyphicon glyphicon-ok');
	    	}else{
	    		elem.removeClass('glyphicon glyphicon-ok');
	    	}
	    });
	}
});*/

/*app.directive('vivoButtonAutoRefresh', function() {
	return function( scope, elem, attrs ) {
	    scope.$watch(attrs.isEnabled, function(val) {
	    	if (val == true){
	    		elem.removeClass('btn btn-danger');
	    		elem.addClass('btn btn-default');
	    	}else{
	    		elem.removeClass('btn btn-default');
	    		elem.addClass('btn btn-danger');
	    	}
	    });
	}
});*/

app.directive('mySpinner', function() {
	var spinner = new Spinner(spinnerOptions);
	return function( scope, elem, attrs ) {
	    scope.$watch(attrs.isLoading, function(val) {
	    	if (val == true){
	    		spinner.spin();
	    		elem.append(spinner.el);
	    	}else{
	    		spinner.stop();
	    	}
	    });
	  };
});

app.directive('responsiveProgress', function() {
	return function( scope, elem, attrs ) {
	    scope.$watch(attrs.isActive, function(val) {
	    	if (val == true){
				elem.addClass('active');
	    	}else{
				elem.removeClass('active');
	    	}
	    });
	  };
});


/*app.directive('vivoButtonHideDelivered', function() {
	return function( scope, elem, attrs ) {
	    scope.$watch(attrs.hide, function(val) {
	    	if (val == true){
	    		elem.removeClass('btn btn-default');
	    		elem.addClass('btn btn-warning');
	    		elem.html('Show delivered');
	    	}else{
	    		elem.removeClass('btn btn-warning');
	    		elem.addClass('btn btn-default');
	    		elem.html('Hide delivered');
	    	}
	    });
	}
});*/
