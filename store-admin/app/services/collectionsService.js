app.factory('collectionsFactory', [ '$http', function ($http) {
    console.log('collectionsFactory initializing...');
    var baseURL = "/api";
    var collectionsFactory = {
        getCollectionsInventory: function () {
            var resp = $http.get(baseURL + "/collections/inventory")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    return response.data;
                });
            return resp;
        },
        getCollectionForEdit: function (collectionId) {
            var resp = $http.get(baseURL + "/collections/inventory/collection", {params: {id: collectionId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        getCollectionTemplate: function () {
            var resp = $http.get(baseURL + "/collections/inventory/collection")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        submitNewCollection: function (collection) {
            var resp = $http.post(baseURL + "/collections/inventory/collection", collection)
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        editCollection: function (collection) {
            var resp = $http.put(baseURL + "/collections/inventory/collection", collection, {params: {id: collection.id}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        deleteCollection: function (collectionId) {
            var resp = $http.delete(baseURL + "/collections/inventory/collection", {params: {id: collectionId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    console.log(response.data)
                    return response.data;
                });
            return resp;
        }

     };
    return collectionsFactory;
}]);