app.factory('ordersFactory', [ '$http', function ($http) {
    console.log('ordersFactory initializing...');
    var baseURL = "/api";
    var ordersFactory = {
        getOrdersInventory: function () {
            var resp = $http.get(baseURL + "/orders/inventory")                
                .then(function (response) {
                    return response.data;
                });
            return resp;
        },
        deleteOrder: function (orderId) {
            var resp = $http.delete(baseURL + "/orders/inventory/order", {params: {id: orderId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    console.log(response.data)
                    return response.data;
                });
            return resp;
        }

     };
    return ordersFactory;
}]);