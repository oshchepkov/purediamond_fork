app.factory('ringsInventoryFactory', [ '$http', function ($http) {
    var baseURL = "/api";
    var ringsInventoryFactory = {
        getRingsInventory: function () {
            var resp = $http.get(baseURL + "/rings/inventory")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    return response.data;
                });
            return resp;
        },
        getRingForEdit: function (ringId) {
            var resp = $http.get(baseURL + "/rings/inventory/ring", {params: {id: ringId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        getRingTemplate: function () {
            var resp = $http.get(baseURL + "/rings/inventory/ring")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        submitNewRing: function (ring) {
            var resp = $http.post(baseURL + "/rings/inventory/ring", ring)
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        editRing: function (ring) {
            var resp = $http.put(baseURL + "/rings/inventory/ring", ring, {params: {id: ring.id}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        deleteRing: function (ringId) {
            var resp = $http.delete(baseURL + "/rings/inventory/ring", {params: {id: ringId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        deleteRingImage: function (ringId, imageId) {
            var resp = $http.delete(baseURL + "/rings/inventory/ring/image", {params: {ringId: ringId, imageId: imageId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        makeImagePrimaryForRing: function (ringId, imageId) {
            var resp = $http.put(baseURL + "/rings/inventory/ring/image/primary", null, {params: {ringId: ringId, imageId: imageId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        rebuild_thumbs: function () {
            var resp = $http.post(baseURL + "/rings/inventory/rebuild_thumbs")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    console.log(response.data)
                    return response.data;
                });
            return resp;
        }



     };
    return ringsInventoryFactory;
}]);
