app.factory('pagesFactory', [ '$http', function ($http) {
    console.log('pagesFactory initializing...');
    var baseURL = "/api";
    var pagesFactory = {
        getPageBySlug: function (pageSlug) {
            var resp = $http.get(baseURL + "/pages/page", {params: {slug: pageSlug}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        getPage: function (pageId) {
            var resp = $http.get(baseURL + "/pages/page", {params: {id: pageId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        editPage: function (page) {
            var resp = $http.put(baseURL + "/pages/page", page, {params: {id: page.id}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        }
     };
    return pagesFactory;
}]);
