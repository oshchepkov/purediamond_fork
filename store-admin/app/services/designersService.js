app.factory('designersFactory', [ '$http', function ($http) {
    console.log('designersFactory initializing...');
    var baseURL = "/api";
    var designersFactory = {
        getDesignersInventory: function () {
            var resp = $http.get(baseURL + "/designers/inventory")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    return response.data;
                });
            return resp;
        },
        getDesignerForEdit: function (designerId) {
            var resp = $http.get(baseURL + "/designers/inventory/designer", {params: {id: designerId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        getDesignerTemplate: function () {
            var resp = $http.get(baseURL + "/designers/inventory/designer")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        submitNewDesigner: function (designer) {
            var resp = $http.post(baseURL + "/designers/inventory/designer", designer)
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        editDesigner: function (designer) {
            var resp = $http.put(baseURL + "/designers/inventory/designer", designer, {params: {id: designer.id}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        deleteDesigner: function (designerId) {
            var resp = $http.delete(baseURL + "/designers/inventory/designer", {params: {id: designerId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    console.log(response.data)
                    return response.data;
                });
            return resp;
        }

     };
    return designersFactory;
}]);