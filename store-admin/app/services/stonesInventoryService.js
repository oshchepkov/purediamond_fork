app.factory('stonesInventoryFactory', [ '$http', function ($http) {
    //console.log("diamondsFactory is initializing...");
    var baseURL = "/api";
    var stonesInventoryFactory = {
        getUploadedInventoryFiles: function () {
            var resp = $http.get(baseURL + "/stones/inventory/getUploadedFiles")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    return response.data;
                });
            return resp;
        },
        processUploadedInventory: function (inventoryData) {
            var resp = $http.post(baseURL + "/stones/inventory/process", {inventoryData: inventoryData})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log("diamondsFactory return response.data...");
                    return response.data;
                });
            return resp;
        },
        clearUploadedInventoryFiles: function () {
            var resp = $http.delete(baseURL + "/stones/inventory/clear")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log("diamondsFactory return response.data...");
                    return response.data;
                });
            return resp;
        },
        submitInventory: function () {
            var resp = $http.post(baseURL + "/stones/inventory/submit")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log("diamondsFactory return response.data...");
                    return response.data;
                });
            return resp;
        },
        getStonesInventory: function () {
            var resp = $http.get(baseURL + "/stones/inventory/getInventory")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    return response.data;
                });
            return resp;
        },
        getStoneForEdit: function (stoneId) {
            var resp = $http.get(baseURL + "/stones/inventory/stone", {params: {id: stoneId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        getStoneTemplate: function () {
            var resp = $http.get(baseURL + "/stones/inventory/stone")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        submitNewStone: function (stone) {
            var resp = $http.post(baseURL + "/stones/inventory/stone", stone)
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        editStone: function (stone) {
            var resp = $http.put(baseURL + "/stones/inventory/stone", stone, {params: {id: stone.id}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        deleteStone: function (stoneId) {
            var resp = $http.delete(baseURL + "/stones/inventory/stone", {params: {id: stoneId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    //console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        deleteStoneImage: function (stoneId, imageId) {
            var resp = $http.delete(baseURL + "/stones/inventory/stone/image", {params: {stoneId: stoneId, imageId: imageId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    console.log(response.data)
                    return response.data;
                });
            return resp;
        },
        makeImagePrimaryForStone: function (stoneId, imageId) {
            var resp = $http.put(baseURL + "/stones/inventory/stone/image/primary", null, {params: {stoneId: stoneId, imageId: imageId}})
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    console.log(response.data)
                    return response.data;
                });
            return resp;
        }
    };
    return stonesInventoryFactory;
}]);