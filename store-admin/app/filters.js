app.filter('hideDeliveredFilter', function() {
  return function(orders, hideDelivered) {
	  var filtered = [];
	    angular.forEach(orders, function(order) {
	    	if (hideDelivered){
	    		if (!order.isDelivered)
	    			filtered.push(order);
	    	}else{
	    		filtered.push(order);
	    	}
	    });
	  return filtered;
  };
});