<?php if ( ! isset( $_SESSION ) ) session_start(); ?>
<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html lang="en"><!--<![endif]-->
<head>
	<meta charset="UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="content-language" content="en">
	<title>Pure Diamond - Best place to get a Diamond Engagement Ring in Vancouver</title>
	<meta name="description" content="Best place to purchase a diamond engagement ring in Vancouver. Shop with the help of expert gemologist, 10 years experience in selecting diamonds. We will ensure that you get an amazing diamond set in a beautiful, stunning and exciting engagement diamond ring."/>
	<meta name="keywords" content="Diamond Ring, Wholesale diamond engagement rings, Diamond solitaire engagement rings, Diamond engagement rings settings, Affordable diamond engagement rings, Diamond rings for engagement, Diamond Earrings Vancouver, Awesome diamonds, Perfect Diamond engagement ring, Best diamond Engagement ring, Engagement ring Vancouver, Diamond ring Vancouver, Round brilliant cut diamonds, Princess cut diamonds, Her diamond ring in Vancouver, GIA triple excellent diamonds Vancouver, AGS certified Diamonds in Vancouver, How to buy a diamond engagement ring, Cheap diamond engagement rings, Why give diamond engagement rings, Black Diamond Engagement ring Vancouver, Halo Diamond engagement ring Vancouver, Solitaire diamond engagement ring Vancouver, Single stone classic diamond engagement ring Vancouver, Awesome diamonds in Vancouver, Stunning diamond engagement ring in Vancouver, Love diamonds, Diamonds are the girls best friends, Synthetic diamonds, Emerald cut diamond engagement ring Vancouver, Where do I buy an engagement ring and not get ripped off, 10K 14K 18K Rose Gold diamond engagement ring in Vancouver, 10K 14K 18K yellow gold diamond engagement in Vancouver, 10K 14K 18K White gold diamond engagement ring in Vancouver"/>

	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta property="og:url" content=""/>
	<meta property="og:title" content=""/>
	<meta property="og:description" content=""/>
	<meta property="og:image" content=""/>

	<base href="/store/">

	<link rel="apple-touch-icon" sizes="57x57" href="/store-assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/store-assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/store-assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/store-assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/store-assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/store-assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/store-assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/store-assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/store-assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/store-assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/store-assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/store-assets/img/favicon/favicon-16x16.png" sizes="16x16">


	<link rel="pingback" href="https://purediamond.ca/xmlrpc.php" />

  <!--[if lt IE 9]>
  	<script src="https://purediamond.ca/wp-content/themes/Divi/js/html5.js" type="text/javascript"></script>
  	<![endif]-->



  	<script type="text/javascript">
  		document.documentElement.className = 'js';
  	</script>



  	<!-- Store scripts and styles -->
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
  	<!-- bootstrap fallback -->
  	<!-- <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"> -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" type="text/css">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/blue.css" type="text/css">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/assets/owl.carousel.min.css" type="text/css">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/assets/owl.theme.default.min.css" type="text/css">
  	<link rel="stylesheet" href="/store-assets/css/owlcarousel/owl.transitions.css" type="text/css">
  	<link rel="stylesheet" href="/store-assets/css/icomoon/style.css?v=2" type="text/css">
  	<!-- <link rel="stylesheet" href="/store-assets/css/custom.css" type="text/css"> -->
  	<link rel="stylesheet" href="/store-assets/css/angular-slider.css" type="text/css">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.19.1/select.min.css" type="text/css">
  	<link rel="stylesheet" href="/store-assets/css/fa/css/font-awesome.min.css" type="text/css">




  	<!-- <script src="//cdn.ravenjs.com/1.1.16/angular/raven.min.js"></script> -->

  	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
  	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-route.min.js"></script>
  	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-sanitize.min.js"></script>
  	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-touch.min.js"></script>
  	<script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.0.1.js"></script>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/spin.js/2.3.2/spin.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.19.1/select.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/owl.carousel.min.js"></script>
  	<script async src="//platform.instagram.com/en_US/embeds.js"></script>




	<!-- <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script>
	    Stripe.setPublishableKey('pk_test_L2MJPmWTlisaxqIgct0iHuXZ');
	</script> -->

	<!-- Loading the app's main library -->
	<script src="/store-assets/app.js?v=6"></script>

	<script type="text/javascript">
		app.config(function($locationProvider){
			$locationProvider.html5Mode(true).hashPrefix('!');
		})
	</script>

	<!-- Loading other components -->

	<script src="/store-assets/js/utils.js"></script>
	<script src="/store-assets/js/rangeInputSupported.js"></script>
	<script src="/store-assets/js/angular-slider.min.js"></script>
	<script src="/store-assets/js/angular-payments.min.js"></script>



	<!-- Store scripts and styles end -->


	<link rel="stylesheet" href="/store-assets/css/pd-global.min.css?v=2">

	<!-- Typekit font. Proxima nova -->
  <!-- <script src="//use.typekit.net/mwz1igx.js"></script>
  <script>try{Typekit.load();}catch(e){}</script> -->

</head>
<body class="home page" data-ng-app="pureDiamondWebApp" data-ng-controller="appController" ngCloak>
	<div id="page-container">

		<header id="main-header" class="site-header">
			<div class="container clearfix">

				<div class="navi-box">
					<div class="site-logo">
						<a href="/"></a>
					</div>
					<a href="#" class="nav-btn" menutoggle><i class="fa fa-bars" aria-hidden="true"></i></a>
					<div class="shopping-bag-status visible-xs">
						<a ng-href="/store/checkout"><i class="fa fa-shopping-bag"></i><span ng-bind="productsCount"></span></a>
					</div>

					<nav id="top-menu-nav">
						<ul class="top-menu nav" ng-show="navpages">
							<li ng-repeat="navpage in navpages"><a href="/store/products/{{navpage.slug}}" ng-bind="navpage.menu_title"></a></li>
						</ul>

						<ul id="menu-main-nav" class="nav top-menu">
							<li class="menu-item has-subnav">
								<a href="" ng-click="j_subnav=!j_subnav; d_subnav=false">Jewellery</a>
								<ul class="main-nav-subnav" ng-class="{active: j_subnav}">
									<li class="menu-item"><a href="/store/products/rings">Rings</a></li>
									<li class="menu-item"><a href="/store/products/pendants">Pendants</a></li>
									<li class="menu-item"><a href="/store/products/earrings">Earrings</a></li>
									<li class="menu-item"><a href="/store/products/wedding-bands">Wedding Bands</a></li>
								</ul>
							</li>
							<li class="menu-item has-subnav">
								<a href="" ng-click="d_subnav=!d_subnav; j_subnav=false">Diamonds</a>
								<ul class="main-nav-subnav" ng-class="{active: d_subnav}">
									<li class="menu-item"><a href="/store/products/diamonds" target="_self">Inventory</a></li>
									<li class="menu-item"><a href="/store/products/specials" target="_self">Specials</a></li>
								</ul>
							</li>
							<li class="menu-item"><a href="/info/about-us">Why Us</a></li>
							<li class="menu-item"><a href="/info/virtual-tour">Virtual Tour</a></li>
							<li class="menu-item"><a href="/info/blog">Blog</a></li>
						</ul>


						<div class="social-icons">
							<a href="https://www.facebook.com/pages/Pure-Diamond/566340603467508" target="_blank"><span class="icon-facebook-letter"></span></a>
							<a href="http://www.pinterest.com/canadadiamond/" target="_blank"><span class="icon-pinterest"></span></a>
							<a href="https://twitter.com/PureDiamond_ca" target="_blank"><span class="icon-twitter"></span></a>
							<a href="http://instagram.com/purediamond.ca" target="_blank"><span class="icon-instagram"></span></a>
							<a href="https://plus.google.com/u/0/+PureDiamondVancouver" target="_blank"><span class="fa fa-google-plus"></span></a>
							<a href="https://www.youtube.com/channel/UClxazhC2oBbr6g4PWdoZ81g" target="_blank"><span class="fa fa-youtube"></span></a>
						</div>
					</nav>
				</div>

				<div class="info-block">
					<div class="social-icons">
						<a href="https://www.facebook.com/pages/Pure-Diamond/566340603467508" target="_blank"><span class="icon-facebook-letter"></span></a>
						<a href="http://www.pinterest.com/canadadiamond/" target="_blank"><span class="icon-pinterest"></span></a>
						<a href="https://twitter.com/PureDiamond_ca" target="_blank"><span class="icon-twitter"></span></a>
						<a href="http://instagram.com/purediamond.ca" target="_blank"><span class="icon-instagram"></span></a>
						<a href="https://plus.google.com/u/0/+PureDiamondVancouver" target="_blank"><span class="fa fa-google-plus"></span></a>
						<a href="https://www.youtube.com/channel/UClxazhC2oBbr6g4PWdoZ81g" target="_blank"><span class="fa fa-youtube"></span></a>
					</div>
					<div class="contacts">
						<div class="phone"><i class="fa fa-phone"></i><a href="tel://1-604-563-9875">+1 (604) 563-9875</a></div>
						<div class="email"><a href="mailto:info@purediamond.ca"><i class="fa fa-envelope"></i>info@purediamond.ca</a></div>
					</div>

					<div class="shopping-bag-status hidden-xs">
						<a ng-href="/store/checkout"><i class="fa fa-shopping-bag"></i>Shopping Bag <span ng-bind="countToShow" ng-show="countToShow"></span></a>
						<!-- <a href="/store/signin" class="signin-btn"><i class="fa fa-sign-in" aria-hidden="true"></i>Sign In</a> -->
					</div>

				</div>

			</div> <!-- .container -->
		</header> <!-- #main-header -->

		<div id="et-main-area">

			<div class="header-title">
				<h1 ng-show="page.title"><span ng-bind="page.title"></span></h1>
				<div class="colored-border two-colors"><span class="blue-line"></span></div>
			</div>
			<div id="main-content" ng-cloak>
				<div data-ng-view keep-scroll-pos id="page-content" class="container"></div>
			</div> <!-- #main-content -->


			<footer id="main-footer">

				<div class="footer-top">

					<div class="container">
						<div class="col-sm-12">
							<div class="designers-link">
								<a href="/store/designers">
									<div><span>We are working with</span></div>
									<h1><span>Jewellery</span> Designers</h1>
								</a>
							</div>
						</div>
						<div class="diamonds-shapes col-sm-12">

							<h4>Diamonds Collections</h4>
							<ul class="shapes">
								<li>
									<a href="/store/products/diamonds?shape=round" class="icon-round"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=princess" class="icon-princess"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=cushion" class="icon-cushion"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=asscher" class="icon-asscher"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=marquise" class="icon-marquise"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=oval" class="icon-oval"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=radiant" class="icon-radiant"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=pear" class="icon-pear"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=emerald" class="icon-emerald"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=heart" class="icon-heart"></a>
								</li>
							</ul>
						</div>
					</div>
				</div>




				<div class="footer-bottom">

					<div class="container">
						<div id="footer-menu" class="col-sm-12">
							<ul class="top-menu nav" ng-show="navpages">
								<li ng-repeat="navpage in navpages"><a href="/store/products/{{navpage.slug}}" ng-bind="navpage.menu_title"></a></li>
							</ul>


							<ul class="nav top-menu">
								<li class="menu-item"><a href="/store/products/rings">Rings</a></li>
								<li class="menu-item"><a href="/store/products/pendants">Pendants</a></li>
								<li class="menu-item"><a href="/store/products/earrings">Earrings</a></li>
								<li class="menu-item"><a href="/store/products/wedding-bands">Wedding Bands</a></li>
								<li class="menu-item"><a href="/store/products/diamonds">Diamonds</a></li>							
								<li class="menu-item"><a href="/info/about-us">Why Us</a></li>
								<li class="menu-item"><a href="/info/virtual-tour">Virtual Tour</a></li>
								<li class="menu-item"><a href="/info/blog">Blog</a></li>
							</ul>


						</div>
						<div class="col-sm-3 nopadding">
							<div class="contacts">
								<div class="address">736 Granville Street, <br/>Vancouver, BC Canada.</div>
								<div class="phone"><a href="tel://1-855-736-2591"><i class="fa fa-phone"></i>+1 (855) 736-2591</a> <br/><a href="tel://1-604-563-9875">+1 (604) 563-9875</a></div>
								<div class="email"><a href="mailto:info@purediamond.ca"><i class="fa fa-envelope"></i>info@purediamond.ca</a></div>
							</div>
						</div>
						<div class="col-sm-6">
						<div class="footer-widget">
						<div class="fwidget col-sm-6">
						<div class="menu-education-container">
						<ul id="menu-education" class="menu">
						<li class="menu-item"><a href="/info/education/learn-about-metals/">Learn About Metals</a></li>
						<li class="menu-item"><a href="/info/education/diamond-certification/">Diamond Certification</a></li>
						<li class="menu-item"><a href="/info/education/diamond-symmetry-grading/">Diamond Polish and Symmetry</a></li>
						<li class="menu-item"><a href="/info/education/history-of-diamond-cuts/">History of Diamond Cuts</a></li>
						<li class="menu-item"><a href="/info/education/ideal-proportions-of-round-brilliant-cut-explained/">Ideal Proportions of Round Brilliant Cut Explained</a></li>
						<li class="menu-item"><a href="/info/education/the-4cs-of-diamonds/">The 4Cs of diamonds</a></li>
						<li class="menu-item"><a href="/info/education/conflict-free-diamonds/">Conflict Free Diamonds</a></li>
						<li class="menu-item"><a href="/info/education/documentation/">Documentation</a></li>
						</ul>
						</div>
						</div>
						<div class="fwidget col-sm-6">
						<div class="menu-customer-care-container">
						<ul id="menu-customer-care" class="menu">
						<li class="menu-item"><a href="/info/customer-care/100-risk-free-shopping/">100% Risk-Free Shopping</a></li>
						<li class="menu-item"><a href="/info/customer-care/30-day-return-policy/">30 Day Return Policy</a></li>
						<li class="menu-item"><a href="/info/customer-care/free-shipping/">Free shipping</a></li>
						<li class="menu-item"><a href="/info/customer-care/30-day-money-back-guarantee/">30 Day Money Back Guarantee</a></li>
						<li class="menu-item"><a href="/info/customer-care/lifetime-warranty/">Lifetime Warranty</a></li>
						<li class="menu-item"><a href="/info/customer-care/120-lifetime-diamond-upgrade/">Diamond Upgrade Promise</a></li>
						<li class="menu-item"><a href="/info/customer-care/wholesale-diamond-price/">Wholesale Diamond Price</a></li>
						<li class="menu-item"><a href="/info/customer-care/price-match/">Our Price Match Guarantee</a></li>
						<li class="menu-item"><a href="/info/customer-care/free-monthly-diamond-give-away/">Free Diamond Give Away</a></li>
						<li class="menu-item"><a href="/info/education/reviews/">Reviews</a></li>
						</ul>
						</div>
						</div>
						</div>
					</div>
					<div class="actions-col col-sm-3 nopadding">
						<div class="social-icons">
							<a href="https://www.facebook.com/pages/Pure-Diamond/566340603467508" target="_blank"><span class="icon-facebook-letter"></span></a>
							<a href="http://www.pinterest.com/canadadiamond/" target="_blank"><span class="icon-pinterest"></span></a>
							<a href="https://twitter.com/PureDiamond_ca" target="_blank"><span class="icon-twitter"></span></a>
							<a href="http://instagram.com/purediamond.ca" target="_blank"><span class="icon-instagram"></span></a>
							<a href="https://plus.google.com/u/0/+PureDiamondVancouver" target="_blank"><span class="fa fa-google-plus"></span></a>
							<a href="https://www.youtube.com/channel/UClxazhC2oBbr6g4PWdoZ81g" target="_blank"><span class="fa fa-youtube"></span></a>
						</div>
						<a textpopup data-mfp-src="#appointment-popup" href="" class="appointment-btn">Talk to a Gemologist</a>
					</div>

					<div class="services col-sm-12">
						<div class="logo visa"><img src="/store-assets/img/logo-visa.png" alt=""></div>
						<div class="logo mastercard"><img src="/store-assets/img/logo-mastercard.png" alt=""></div>
						<div class="logo"><img src="/store-assets/img/logo-amexpress.png" alt=""></div>
						<div class="logo"><img src="/store-assets/img/logo-paypal.png" alt=""></div>
						<div class="logo"><img src="/store-assets/img/logo-bitcoin.png" alt=""></div>
						<div class="logo"><img src="/store-assets/img/logo-fedex.png" alt=""></div>
						<div class="logo ags"><img src="/store-assets/img/logo-ags.png" alt=""></div>
						<div class="logo"><img src="/store-assets/img/logo-gia.png" alt=""></div>
						<div class="logo canrocks"><img src="/store-assets/img/logo-canrocks.png" alt=""></div>
						<div class="logo lights"><img src="/store-assets/img/logo-lights.png" alt=""></div>
					</div>
					<div class="copyright col-sm-12">
						<span>&copy; <?php echo date("Y"); ?> Pure Diamond</span>
					</div>

				</div>

			</div>


		</footer> <!-- #main-footer -->
	</div> <!-- #et-main-area -->


</div> <!-- #page-container -->




<div class="mfp-hide text-popup" id="shipping-popup">
	<h2>FREE SHIPPING</h2>

	<p>PUREDIAMOND.CA offers complimentary shipping on all jewelry shipped in the CANADA or the U.S. via FedEx® Priority Overnight.</p>
	<p>You'll take comfort in knowing precisely what day your order will arrive. If you wish, you can also follow every step it takes along the way with detailed order tracking.</p>

	<a href="/customer-care/free-shipping/">Learn More</a>
</div>

<div class="mfp-hide text-popup" id="returns-popup">
	<h2>30 Day Return Policy</h2>
	<p>The purchase of a diamond can be one of the most difficult decisions you'll ever have to make. That's why purediamond.ca gives you 30 days to return your diamond for a full refund or exchange without charging a restocking fee. No questions asked! If for any reason you want to return your diamond, you've got thirty full days to decide. This 30-day return policy is for the center diamond only, not side stones, three-stone rings or mountings. The center diamond must be in excellent condition, free from breakage and chipping, and be accompanied by certification documents.</p>
	<p>If certification documents are lost or you are not able to provide them, $200 will be deducted from your return amount to cover recertification of the diamond. *In order to give our customers the best pricing, we do not allow comparison purchasing. Comparison purchasing is the act of ordering two or more of the same, or similar item, and then only keeping one of them. If you are unsure, or have detailed questions about a particular item, please call one of our customer service representatives for expert help. *Our 30-day return policy applies to purchases up to $29,000</p>

</div>


<div class="mfp-hide text-popup" id="appointment-popup">
	<form ng-submit="talkToSubmit()" role="form" enctype="multipart/form-data" ng-hide="talkToForm.success">

				<h2>Schedule an appointment</h2>
				<div class="row">
				<div class="col-sm-6">
				<div>
					<label for="sched-name">Your Name <span>(required)</span></label><br>
				    <input type="text" name="name" ng-model="talkToForm.name" value="" size="40" id="sched-name" aria-required="true" aria-invalid="false">
				</div>
				<div>
					<label for="sched-phone">Your phone</label><br>
				    <input type="text" name="phone" ng-model="talkToForm.phone" value="" size="40" id="sched-phone" aria-invalid="false">
				</div>
				</div>
				<div class="col-sm-6">
				<div>
				    <label for="sched-email">Your Email <span>(required)</span></label><br>
				    <input type="email" name="email" ng-model="talkToForm.email" value="" size="40" id="sched-email" aria-required="true" aria-invalid="false">
				</div>				
				</div>
				<div class="col-sm-12">
				    <label for="sched-message">Additional notes</label><br>
				    <textarea name="notes" cols="40" rows="3" ng-model="talkToForm.notes" id="sched-message" aria-invalid="false"></textarea>
					<div class="col-sm-5 nopadding"><input type="submit" value="Send"></div>
					<div class="col-sm-7 note">
						<p>Please feel free to call 1 (855) 736-2591 or 604-563-9875.</p>
						<p>A Pure Diamond specialist will be happy to answer any questions you may have.</p>
					</div>
				</div>
				</div>
				<div class="form-response"></div>
				</form>
				<div class="form-success" ng-show="talkToForm.success">
					<h3>Thank you!</h3>
					<h4>We will get in touch with you shortly.</h4>
				</div>
</div>


<div class="mfp-hide text-popup" id="share-popup">
	<h2 class="centered">Share it</h2>

	<div class="centered share-big">
		<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{share.url}}" id="share-fb" class="icon-facebook"></a>

		<a target="_blank" href="http://twitter.com/intent/tweet?text={{share.text}}&url={{share.url}}" id="share-tw" class="icon-twitter"></a>

		<a target="_blank" href="https://pinterest.com/pin/create/button/?url={{share.url}}&media={{share.media}}&description={{share.text}}" id="share-pin" class="icon-pinterest"></a>

	</div>
</div>



<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
			$.src='//v2.zopim.com/?2UDEnJrWAJt1oXu1onwyldjRUVZWCc2X';z.t=+new Date;$.
			type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
		</script>
		<!--End of Zopim Live Chat Script-->
	</body>
	</html>
