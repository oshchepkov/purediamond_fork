var gulp = require('gulp')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')
var cssmin = require('gulp-cssmin')
var rename = require('gulp-rename');

gulp.task('js', function () {
  gulp.src(['store-assets/app/*.js', 'store-assets/app/**/*.js'])
    .pipe(concat('store-assets/app.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('.'));

  	gulp.src('store-assets/css/pd-global.css')
  		.pipe(cssmin())
  		.pipe(rename({suffix: '.min'}))
  		.pipe(gulp.dest('store-assets/css'));

})
