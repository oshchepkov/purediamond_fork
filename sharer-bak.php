<?php
if(empty($_GET)) exit;

$site_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'];

// Connecting, selecting database
$db = new mysqli("localhost", "purediamond_node", "Sd2hfSCYhZXCBZWK", "purediamond_node");
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}

$prod_id = $db->real_escape_string(trim($_GET['product']));
$stone_id = $db->real_escape_string(trim($_GET['stone']));

if(!empty($prod_id)) {
	$q1 = $db->query(
		"SELECT 
		rings.*, 
		images.file_name,
		images.description as image_desc,  
		rings_images.favorite as image_fav 
		FROM rings 
		LEFT JOIN rings_images ON rings.id = rings_images.ring_id 
		LEFT JOIN images ON images.id = rings_images.image_id 
		WHERE rings.id=$prod_id
		"
	);
	$db->close();
	$product = $q1->fetch_assoc();

	$images = array();

	$product['images'][] = $product['file_name'];
	if($product['image_fav'] == 1) { 
		$product['image_fav'] = $product['file_name']; 
		$images[] = array(
			'url' => $site_url.'/static/images/rings/'.$product['id'].'/'.$product['file_name'],
			'description' => $product['image_desc'],
			'favorite' => true
		);
	} else {
		$images[] = array(
			'url' => $site_url.'/static/images/rings/'.$product['id'].'/'.$product['file_name'],
			'description' => $product['image_desc'],
			'favorite' => false
		);
	}

	while ($row = $q1->fetch_assoc()) {
		if($row['image_fav'] == 1) { 
			$product['image_fav'] = $row['file_name']; 
			$images[] = array(
				'url' => $site_url.'/static/images/rings/'.$product['id'].'/'.$row['file_name'],
				'description' => $row['image_desc'],
				'favorite' => true
			);
		} else {
			$images[] = array(
				'url' => $site_url.'/static/images/rings/'.$product['id'].'/'.$row['file_name'],
				'description' => $row['image_desc'],
				'favorite' => false
			);
		}
	}

	$product['images'] = $images;

	$json_ring = json_encode(array(
		'id' => $product['id'],
		'title' => $product['title'],
		'lot_number' => $product['lot_number'],
		'price' => $product['price'],
		'stone_weight_min' => '',
		'stone_weight_max' => '',
		'description' => empty($product['description']) ? '' : utf8_encode($product['description']),
		'published' => 1,
		'details' => empty($product['details']) ? '' : utf8_encode($product['details']),
		'discount' => empty($product['discount']) ? 0 : $product['discount'],
		'designers' => '',
		'metals' => '',
		'shapes' => '',
		'collections' => '',
		'images' => $images
	));

	$json_images = json_encode($images);

	$product_title = $product['title'];
	$price = (!empty($product['discount']) ? 'C$'.(floatval($product['price'])+floatval($stone['total_price']-$product['discount'])).' (C$'.$product['discount'].' discount)' : 'C$'.(floatval($product['price'])+floatval($stone['total_price'])));
	$share_img = $site_url.'/static/images/products/'.$product['id'].'/'.(($product['image_fav'] != 0) ? $product['image_fav'] : $product['file_name']);
	$share_desc = utf8_encode($product['description']);
}

if(!empty($stone_id)) {
	$q2 = $db->query(
		"SELECT 
		stones.*, 
		shapes.name, 
		shapes.class_name, 
		cuts.friendly_name as cut, 
		clarities.name as clarity, 
		colors.name as color, 
		labs.friendly_name as lab 
		FROM stones 
		LEFT JOIN shapes ON shapes.id = stones.shape_id 
		LEFT JOIN cuts ON cuts.id = stones.cut_id 
		LEFT JOIN clarities ON clarities.id = stones.clarity_id 
		LEFT JOIN colors ON colors.id = stones.color_id 
		LEFT JOIN labs ON labs.id = stones.lab_id 
		WHERE stones.id=$stone_id"
	);
	$db->close();
	$stone = $q2->fetch_assoc();

	$json_stone = json_encode(array(
		'id' => $stone['id'],
		'stock_number' => $stone['stock_number'],
		'shape' => $stone['name'],
		'shape_class_name' => $stone['class_name'],
		'weight' => $stone['weight'],
		'color' => $stone['color'],
		'clarity' => $stone['clarity'],
		'cut' => $stone['cut'],
		'polish' => '',
		'symmetry' => '',
		'measurements' => '',
		'table' => '',
		'lab' => '',
		'cert_number' => '',
		'cert_file' => '',
		'image' => '',
		'price' => $stone['total_price'],
		'depth' => '',
		'girdle' => '',
		'fluorescence' => '',
		'cert_link' => ''
	));

	$product_title = $stone['name'];
	$price = 'C$'.(floatval($stone['total_price']));
	$share_img = (!empty($stone['name']) ? $site_url.'/store-assets/pics/shapes/'.strtolower($stone['name']).'.jpg' : '');
	$share_desc = (!empty($stone['name']) ? $stone['name'].' cut diamond: ' : '');
	$share_desc .= (!empty($stone['weight']) ? $stone['weight'].' carat' : '');
	$share_desc .= (!empty($stone['cut']) ? ', '.$stone['cut'].' cut' : '');
	$share_desc .= (!empty($stone['color']) ? ', '.$stone['color'].' color' : '');
	$share_desc .= (!empty($stone['clarity']) ? ', '.$stone['clarity'].' clarity' : '');
	$share_desc .= (!empty($stone['lab']) ? ', '.$stone['lab'].' certificate' : '');
}

?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<script>

	<?php if(!empty($json_stone)) { ?>

	var stoneParams = <?php echo $json_stone; ?>;
	var stoneReq = new XMLHttpRequest();
	stoneReq.open("POST", <?php echo '"'.$site_url.'/api/orders/addStone"';?>);
	stoneReq.setRequestHeader("Content-type","application/json");
	stoneReq.send(JSON.stringify(stoneParams));
	window.location.replace(<?php echo '"'.$site_url.'/store/products/diamonds/'.$stone['stock_number'].'"';?>);

	<?php } ?>


	<?php if(!empty($json_ring)) { ?>

	var ringParams = <?php echo $json_ring; ?>;	
	var ringReq = new XMLHttpRequest();
	ringReq.open("POST", <?php echo '"'.$site_url.'/api/orders/addRing"';?>);
	ringReq.setRequestHeader("Content-type","application/json");
	ringReq.send(JSON.stringify(ringParams));
	window.location.replace(<?php echo '"'.$site_url.'/store/products/'.$product['category'].'/'.$product['id'].'"';?>);

	<?php } ?>	

	</script>

	<meta charset="UTF-8">
	<title>Pure Diamond - <?php echo $product_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:title" content="<?php echo $product_title.' - '.$price?>">
	<meta property="og:site_name" content="Pure Diamond">
	<meta property="og:type" content="product">
	<meta property="og:url" content="<?php echo $site_url.$_SERVER['REQUEST_URI']?>">
	<meta property="og:description" content="<?php echo $share_desc; ?>">
	<meta property="og:image" content="<?php echo $share_img; ?>">
	<meta property="og:image:width" content="300">
	<meta property="og:image:height" content="300">
	<?php if(!empty($images)) {
		foreach($images as $image) :?>
	<meta property="og:image" content="<?php echo $image['url']; ?>">
	<meta property="og:image:width" content="300">
	<meta property="og:image:height" content="300">
	<?php endforeach; } ?>
	
</head>
<body>
	<h1><?php echo $product_title; ?></h1>
	<img src="<?php echo $share_img; ?>" width="300" alt="">
</body>
</html>



