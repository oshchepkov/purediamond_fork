ALTER TABLE `users` 
ADD INDEX `fk_user_billing_address_idx` (`billing_address_id` ASC),
ADD INDEX `fk_user_shipping_address_idx` (`shipping_address_id` ASC);
ALTER TABLE `diamonds`.`users` 
ADD CONSTRAINT `fk_user_billing_address`
  FOREIGN KEY (`billing_address_id`)
  REFERENCES `diamonds`.`addresses` (`id`)
  ON DELETE SET NULL
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_user_shipping_address`
  FOREIGN KEY (`shipping_address_id`)
  REFERENCES `diamonds`.`addresses` (`id`)
  ON DELETE SET NULL
  ON UPDATE NO ACTION;
