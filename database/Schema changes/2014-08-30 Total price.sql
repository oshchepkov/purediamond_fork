ALTER TABLE stones ADD `total_price` decimal(10,2) DEFAULT NULL;
ALTER TABLE stones_staging ADD `total_price` decimal(10,2) DEFAULT NULL;