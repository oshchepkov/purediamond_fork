ALTER TABLE `orders` 
ADD COLUMN `billing_address` TEXT NULL AFTER `ring_size`,
ADD COLUMN `shipping_address` TEXT NULL AFTER `billing_address`;
