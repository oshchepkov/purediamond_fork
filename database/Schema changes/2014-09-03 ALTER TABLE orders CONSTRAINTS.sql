ALTER TABLE `orders` 
ADD INDEX `fk_orders_user_idx` (`user_id` ASC);
ALTER TABLE `diamonds`.`orders` 
ADD CONSTRAINT `fk_orders_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `diamonds`.`users` (`id`)
  ON DELETE SET NULL
  ON UPDATE NO ACTION;
