ALTER TABLE users ADD `billing_address` text, ADD `shipping_address` text, ADD `subscribe` tinyint(1) DEFAULT NULL;

ALTER TABLE orders ADD `user_id` int(11) DEFAULT NULL, ADD `ring_size` float DEFAULT NULL;