--CREATE DATABASE  IF NOT EXISTS `diamonds` /*!40100 DEFAULT CHARACTER SET utf8 */;
--USE `diamonds`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: diamonds
-- ------------------------------------------------------
-- Server version	5.6.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clarities`
--

DROP TABLE IF EXISTS `clarities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clarities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clarities`
--

LOCK TABLES `clarities` WRITE;
/*!40000 ALTER TABLE `clarities` DISABLE KEYS */;
INSERT INTO `clarities` VALUES (1,1,'IF',NULL,'clarity description'),(2,2,'VVS1',NULL,'clarity description'),(3,3,'VVS2',NULL,'clarity description'),(4,4,'VS1',NULL,'clarity description'),(5,5,'VS2',NULL,'clarity description'),(6,6,'SI1',NULL,'clarity description'),(7,7,'SI2',NULL,'clarity description'),(8,8,'SI3',NULL,'clarity description'),(9,9,'I1',NULL,'clarity description'),(10,10,'I2',NULL,'clarity description'),(11,11,'I3',NULL,'clarity description'),(12,100,'other',NULL,'clarity description');
/*!40000 ALTER TABLE `clarities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collections`
--

DROP TABLE IF EXISTS `collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collections`
--

LOCK TABLES `collections` WRITE;
/*!40000 ALTER TABLE `collections` DISABLE KEYS */;
INSERT INTO `collections` VALUES (2,2,'Gold',NULL),(3,3,'Silver',NULL),(10,0,'Platinum',NULL);
/*!40000 ALTER TABLE `collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,1,'D',NULL,'color D description'),(2,2,'E',NULL,'color E description'),(3,3,'F',NULL,'color F description'),(4,4,'G',NULL,'color G description'),(5,5,'H',NULL,'color H description'),(6,6,'I',NULL,'color I description'),(7,7,'J',NULL,'color J description'),(8,8,'K',NULL,'color K description'),(9,9,'L',NULL,'color L description'),(10,10,'M',NULL,'color M description'),(11,11,'N',NULL,'color N description'),(12,12,'O',NULL,'color O description'),(13,13,'P',NULL,'color P description'),(14,14,'Q',NULL,'color Q description'),(15,15,'R',NULL,'color R description'),(16,16,'S',NULL,'color S description'),(17,17,'T',NULL,'color T description'),(18,18,'U',NULL,'color U description'),(19,19,'V',NULL,'color V description'),(20,20,'W',NULL,'color W description'),(21,21,'X',NULL,'color X description'),(22,22,'Y',NULL,'color Y description'),(23,23,'Z',NULL,'color Z description'),(24,100,'other',NULL,NULL);
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuts`
--

DROP TABLE IF EXISTS `cuts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuts`
--

LOCK TABLES `cuts` WRITE;
/*!40000 ALTER TABLE `cuts` DISABLE KEYS */;
INSERT INTO `cuts` VALUES (1,1,'EX','Super Ideal','super description'),(2,2,'I','Ideal','ideal description'),(3,3,'VG','Very Good','very good description'),(4,4,'G','Good','good description'),(5,5,'F','Fair','fair description'),(6,100,'other',NULL,NULL);
/*!40000 ALTER TABLE `cuts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designers`
--

DROP TABLE IF EXISTS `designers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designers`
--

LOCK TABLES `designers` WRITE;
/*!40000 ALTER TABLE `designers` DISABLE KEYS */;
INSERT INTO `designers` VALUES (16,'Franz Kafka','franz_kafka','kabak3.jpg',NULL),(17,'Master Yoda','master_yoda','kabak2.jpg',NULL),(18,'Artemy Lebedev','artemy_lebedev','kabak4.jpg',NULL),(20,'Jean Claude Van Damme','jean_claude_van_damme','kabak1.jpg',NULL),(21,'Stephen Hawking','stephen_hawking','browdy.jpg',NULL);
/*!40000 ALTER TABLE `designers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fluorescences`
--

DROP TABLE IF EXISTS `fluorescences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fluorescences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fluorescences`
--

LOCK TABLES `fluorescences` WRITE;
/*!40000 ALTER TABLE `fluorescences` DISABLE KEYS */;
INSERT INTO `fluorescences` VALUES (1,1,'N','None',NULL),(2,2,'VSL','Very Slight',NULL),(3,3,'SL','Slight',NULL),(4,4,'F','Faint',NULL),(5,5,'M','Medium',NULL),(6,6,'S','Strong',NULL),(7,7,'VST','Very Strong',NULL),(8,100,'other',NULL,NULL);
/*!40000 ALTER TABLE `fluorescences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metals`
--

DROP TABLE IF EXISTS `metals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metals`
--

LOCK TABLES `metals` WRITE;
/*!40000 ALTER TABLE `metals` DISABLE KEYS */;
INSERT INTO `metals` VALUES (1,1,'Platinum'),(2,2,'Gold'),(3,3,'Silver'),(4,4,'Copper'),(5,5,'Mercury');
/*!40000 ALTER TABLE `metals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) NOT NULL,
  `contents` text,
  `status` varchar(45) DEFAULT NULL,
  `stone_id` int(11) DEFAULT NULL,
  `ring_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_stone_idx` (`stone_id`),
  KEY `fk_orders_ring_idx` (`ring_id`),
  CONSTRAINT `fk_orders_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_stone` FOREIGN KEY (`stone_id`) REFERENCES `stones` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2014-08-05 22:44:00',1200.00,200.00,1400.00,NULL,NULL,NULL,NULL),(2,'2014-08-05 22:44:00',2500.00,200.00,2700.00,NULL,NULL,NULL,NULL),(8,'2014-08-07 21:54:27',0.00,0.00,0.00,NULL,'new',NULL,NULL),(12,'2014-08-07 23:29:32',0.00,0.00,0.00,NULL,'new',1,NULL),(13,'2014-08-10 22:09:24',0.00,0.00,0.00,NULL,'new',2,NULL);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_sessions`
--

DROP TABLE IF EXISTS `orders_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `session_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_sessions_order_idx` (`order_id`),
  CONSTRAINT `fk_orders_sessions_order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_sessions`
--

LOCK TABLES `orders_sessions` WRITE;
/*!40000 ALTER TABLE `orders_sessions` DISABLE KEYS */;
INSERT INTO `orders_sessions` VALUES (6,13,'nOdukUhjwH_AiXTOicGM5mCsoCwF-ujU');
/*!40000 ALTER TABLE `orders_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polishes`
--

DROP TABLE IF EXISTS `polishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `polishes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polishes`
--

LOCK TABLES `polishes` WRITE;
/*!40000 ALTER TABLE `polishes` DISABLE KEYS */;
INSERT INTO `polishes` VALUES (1,1,'EX','Excellent',NULL),(2,3,'VG','Very Good',NULL),(3,4,'G','Good',NULL),(4,5,'F','Fair',NULL),(5,100,'other','',NULL),(6,2,'I','Ideal',NULL);
/*!40000 ALTER TABLE `polishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings`
--

DROP TABLE IF EXISTS `rings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `lot_number` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stone_weight_min` decimal(10,2) NOT NULL,
  `stone_weight_max` decimal(10,2) NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings`
--

LOCK TABLES `rings` WRITE;
/*!40000 ALTER TABLE `rings` DISABLE KEYS */;
INSERT INTO `rings` VALUES (16,'New ring',1,222.00,1.00,5.00,1),(17,'Ring 1',123,123.00,0.10,0.50,1);
/*!40000 ALTER TABLE `rings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings_collections`
--

DROP TABLE IF EXISTS `rings_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings_collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_collections_collection_idx` (`collection_id`),
  KEY `fk_ring_collections_ring_idx` (`ring_id`),
  CONSTRAINT `fk_ring_collections_collection` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ring_collections_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=266 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings_collections`
--

LOCK TABLES `rings_collections` WRITE;
/*!40000 ALTER TABLE `rings_collections` DISABLE KEYS */;
INSERT INTO `rings_collections` VALUES (256,17,2),(263,16,10),(264,16,2),(265,16,3);
/*!40000 ALTER TABLE `rings_collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings_designers`
--

DROP TABLE IF EXISTS `rings_designers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings_designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `designer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_designers_ring_idx` (`ring_id`),
  KEY `fk_ring_designers_designer_idx` (`designer_id`),
  CONSTRAINT `fk_ring_designers_designer` FOREIGN KEY (`designer_id`) REFERENCES `designers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ring_designers_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings_designers`
--

LOCK TABLES `rings_designers` WRITE;
/*!40000 ALTER TABLE `rings_designers` DISABLE KEYS */;
INSERT INTO `rings_designers` VALUES (204,17,18),(207,16,21);
/*!40000 ALTER TABLE `rings_designers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings_images`
--

DROP TABLE IF EXISTS `rings_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `favorite` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rings_images_ring_idx` (`ring_id`),
  KEY `fk_rings_images_image_idx` (`image_id`),
  CONSTRAINT `fk_rings_images_image` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rings_images_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings_images`
--

LOCK TABLES `rings_images` WRITE;
/*!40000 ALTER TABLE `rings_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `rings_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings_metals`
--

DROP TABLE IF EXISTS `rings_metals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings_metals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `metal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_metal_idx` (`metal_id`),
  KEY `fk_ring_metals_ring_idx` (`ring_id`),
  CONSTRAINT `fk_ring_metals_metal` FOREIGN KEY (`metal_id`) REFERENCES `metals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ring_metals_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings_metals`
--

LOCK TABLES `rings_metals` WRITE;
/*!40000 ALTER TABLE `rings_metals` DISABLE KEYS */;
INSERT INTO `rings_metals` VALUES (295,17,2),(306,16,1),(307,16,2),(308,16,3),(309,16,4),(310,16,5);
/*!40000 ALTER TABLE `rings_metals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings_shapes`
--

DROP TABLE IF EXISTS `rings_shapes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings_shapes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `shape_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_shapes_shape_idx` (`shape_id`),
  KEY `fk_ring_shapes_ring_idx` (`ring_id`),
  CONSTRAINT `fk_ring_shapes_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ring_shapes_shape` FOREIGN KEY (`shape_id`) REFERENCES `shapes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings_shapes`
--

LOCK TABLES `rings_shapes` WRITE;
/*!40000 ALTER TABLE `rings_shapes` DISABLE KEYS */;
INSERT INTO `rings_shapes` VALUES (317,17,12),(324,16,12),(325,16,10),(326,16,3);
/*!40000 ALTER TABLE `rings_shapes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text COLLATE utf8_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('nOdukUhjwH_AiXTOicGM5mCsoCwF-ujU',1407820335,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"passport\":{\"user\":1}}');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shapes`
--

DROP TABLE IF EXISTS `shapes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shapes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `class_name` varchar(45) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shapes`
--

LOCK TABLES `shapes` WRITE;
/*!40000 ALTER TABLE `shapes` DISABLE KEYS */;
INSERT INTO `shapes` VALUES (1,4,'Asscher',NULL,NULL,'asscher',1),(2,100,'Baguette',NULL,NULL,'baguette',0),(3,3,'Cushion Modified',NULL,NULL,'cushion',1),(4,9,'Emerald',NULL,NULL,'emerald',1),(5,10,'Heart',NULL,NULL,'heart',1),(6,102,'Kite',NULL,NULL,'kite',0),(7,5,'Marquise',NULL,NULL,'marquise',1),(8,6,'Oval',NULL,NULL,'oval',1),(9,8,'Pear',NULL,NULL,'pear',1),(10,2,'Princess',NULL,NULL,'princess',1),(11,7,'Radiant',NULL,NULL,'radiant',1),(12,1,'Round',NULL,NULL,'round',1),(13,101,'Trilliant',NULL,NULL,'trilliant',0);
/*!40000 ALTER TABLE `shapes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stones`
--

DROP TABLE IF EXISTS `stones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_number` varchar(45) NOT NULL,
  `shape_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `clarity_id` int(11) NOT NULL,
  `cut_id` int(11) NOT NULL,
  `polish_id` int(11) NOT NULL,
  `symmetry_id` int(11) NOT NULL,
  `fluorescence_id` int(11) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `measurements` varchar(20) NOT NULL,
  `certificate_number` varchar(45) DEFAULT NULL,
  `certificate_filename` varchar(45) DEFAULT NULL,
  `diamond_image` varchar(45) DEFAULT NULL,
  `rapnet_price` decimal(10,2) NOT NULL,
  `cash_price` decimal(10,2) DEFAULT NULL,
  `rapnet_discount` decimal(10,2) DEFAULT NULL,
  `cash_price_discount` decimal(10,2) DEFAULT NULL,
  `lab` varchar(45) DEFAULT NULL,
  `fluorescence_color` varchar(20) DEFAULT NULL,
  `treatment` varchar(20) DEFAULT NULL,
  `availability` varchar(20) DEFAULT NULL,
  `fancy_color` varchar(20) DEFAULT NULL,
  `fancy_color_intensity` varchar(20) DEFAULT NULL,
  `fancy_color_overtone` varchar(20) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `table_percent` decimal(10,2) DEFAULT NULL,
  `girdle_thin` varchar(20) DEFAULT NULL,
  `girdle_thick` varchar(20) DEFAULT NULL,
  `girdle` decimal(10,2) DEFAULT NULL,
  `girdle_condition` varchar(45) DEFAULT NULL,
  `cullet_size` varchar(20) DEFAULT NULL,
  `cullet_condition` varchar(45) DEFAULT NULL,
  `crown_height` decimal(10,2) DEFAULT NULL,
  `crown_angle` decimal(10,2) DEFAULT NULL,
  `pavilion_depth` decimal(10,2) DEFAULT NULL,
  `pavilion_angle` decimal(10,2) DEFAULT NULL,
  `laser_inscription` varchar(45) DEFAULT NULL,
  `cert_comment` varchar(255) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `time_to_location` varchar(45) DEFAULT NULL,
  `is_matched_pair_separable` varchar(20) DEFAULT NULL,
  `pair_stock_number` varchar(20) DEFAULT NULL,
  `allow_raplink_feed` int(11) DEFAULT NULL,
  `parcel_stones` varchar(20) DEFAULT NULL,
  `trade_show` varchar(20) DEFAULT NULL,
  `key_to_symbols` varchar(20) DEFAULT NULL,
  `shade` varchar(20) DEFAULT NULL,
  `star_length` varchar(20) DEFAULT NULL,
  `center_inclusion` varchar(20) DEFAULT NULL,
  `black_inclusion` varchar(20) DEFAULT NULL,
  `member_comment` varchar(20) DEFAULT NULL,
  `report_issue_date` varchar(20) DEFAULT NULL,
  `report_type` varchar(20) DEFAULT NULL,
  `lab_location` varchar(20) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `milky` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shape_idx` (`shape_id`),
  KEY `fk_color_idx` (`color_id`),
  KEY `fk_clarity_idx` (`clarity_id`),
  KEY `fk_cut_idx` (`cut_id`),
  KEY `fk_polish_idx` (`polish_id`),
  KEY `fk_symmetry_idx` (`symmetry_id`),
  KEY `fk_fluorescence_idx` (`fluorescence_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2686 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stones`
--

LOCK TABLES `stones` WRITE;
/*!40000 ALTER TABLE `stones` DISABLE KEYS */;
INSERT INTO `stones` VALUES (1,'8EM100J147',11,5,6,3,2,3,1,0.95,'6.45x4.63x3.22','3125224423','8EM100J147.Jpg','8EM100J147-2.Jpg',1760.00,0.00,-0.60,0.00,'EGL ISRAEL','','','','','','',69.60,65.00,'STK','STK',0.00,'','N','',0.00,0.00,0.00,0.00,'','DIASCRIPT ON GIRDLE','USA','','New York','','FALSE','',1,'','','','','','','','','','','','',''),(2,'RA-1423',11,7,3,3,3,4,1,1.07,'6.39x5.48x3.81','514069646','RA-1423.Jpg','RA-1423-2.Jpg',2464.00,0.00,-0.44,0.00,'GIA','','','','','','',69.40,65.00,'','',0.00,'','N','',0.00,0.00,0.00,0.00,'','','USA','','New York','','FALSE','',1,'','','','','','','','','','','','',''),(3,'8R10063-29',11,3,5,3,1,1,1,0.70,'4.74x4.37x3.71','3203347022','8R10063-29.Jpg','8R10063-29-2.Jpg',1344.00,0.00,-0.68,0.00,'EGL ISRAEL','','','','','','',84.90,68.00,'STK','STK',0.00,'','N','',0.00,0.00,0.00,0.00,'','DIASCRIPT ON GIRDLE','USA','','New York','','FALSE','',1,'','','','','','','','','','','','',''),(4,'ARE-19878',10,2,4,3,2,4,1,1.03,'5.60x5.37x4.15','1146480577','PR-2280.Jpg','PR-2280-2.Jpg',4575.00,0.00,-0.39,0.00,'GIA','','','','','','',77.20,75.00,'TN','VTK',0.00,'','N','',0.00,0.00,0.00,0.00,'','','USA','','New York','','FALSE','',1,'','','','','','','','','','','','',''),(5,'8EM7817-98',11,3,5,3,2,4,1,0.58,'4.61x4.21x3.49','2156382646','8EM7817-98.Jpg','8EM7817-98-2.Jpg',1296.00,0.00,-0.52,0.00,'GIA','','','','','','',82.80,67.00,'VTK','XTK',0.00,'','N','',0.00,0.00,0.00,0.00,'','GIA 6147210277','USA','','New York','','FALSE','',1,'','','','','','','','','','','','','');
/*!40000 ALTER TABLE `stones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stones_staging`
--

DROP TABLE IF EXISTS `stones_staging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stones_staging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_number` varchar(45) NOT NULL,
  `shape_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `clarity_id` int(11) NOT NULL,
  `cut_id` int(11) NOT NULL,
  `polish_id` int(11) NOT NULL,
  `symmetry_id` int(11) NOT NULL,
  `fluorescence_id` int(11) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `measurements` varchar(20) NOT NULL,
  `certificate_number` varchar(45) DEFAULT NULL,
  `certificate_filename` varchar(45) DEFAULT NULL,
  `diamond_image` varchar(45) DEFAULT NULL,
  `rapnet_price` decimal(10,2) NOT NULL,
  `cash_price` decimal(10,2) DEFAULT NULL,
  `rapnet_discount` decimal(10,2) DEFAULT NULL,
  `cash_price_discount` decimal(10,2) DEFAULT NULL,
  `lab` varchar(45) DEFAULT NULL,
  `fluorescence_color` varchar(20) DEFAULT NULL,
  `treatment` varchar(20) DEFAULT NULL,
  `availability` varchar(20) DEFAULT NULL,
  `fancy_color` varchar(20) DEFAULT NULL,
  `fancy_color_intensity` varchar(20) DEFAULT NULL,
  `fancy_color_overtone` varchar(20) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `table_percent` decimal(10,2) DEFAULT NULL,
  `girdle_thin` varchar(20) DEFAULT NULL,
  `girdle_thick` varchar(20) DEFAULT NULL,
  `girdle` decimal(10,2) DEFAULT NULL,
  `girdle_condition` varchar(45) DEFAULT NULL,
  `cullet_size` varchar(20) DEFAULT NULL,
  `cullet_condition` varchar(45) DEFAULT NULL,
  `crown_height` decimal(10,2) DEFAULT NULL,
  `crown_angle` decimal(10,2) DEFAULT NULL,
  `pavilion_depth` decimal(10,2) DEFAULT NULL,
  `pavilion_angle` decimal(10,2) DEFAULT NULL,
  `laser_inscription` varchar(45) DEFAULT NULL,
  `cert_comment` varchar(255) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `time_to_location` varchar(45) DEFAULT NULL,
  `is_matched_pair_separable` varchar(20) DEFAULT NULL,
  `pair_stock_number` varchar(20) DEFAULT NULL,
  `allow_raplink_feed` int(11) DEFAULT NULL,
  `parcel_stones` varchar(20) DEFAULT NULL,
  `trade_show` varchar(20) DEFAULT NULL,
  `key_to_symbols` varchar(20) DEFAULT NULL,
  `shade` varchar(20) DEFAULT NULL,
  `star_length` varchar(20) DEFAULT NULL,
  `center_inclusion` varchar(20) DEFAULT NULL,
  `black_inclusion` varchar(20) DEFAULT NULL,
  `member_comment` varchar(20) DEFAULT NULL,
  `report_issue_date` varchar(20) DEFAULT NULL,
  `report_type` varchar(20) DEFAULT NULL,
  `lab_location` varchar(20) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `milky` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shape_idx` (`shape_id`),
  KEY `fk_color_idx` (`color_id`),
  KEY `fk_clarity_idx` (`clarity_id`),
  KEY `fk_cut_idx` (`cut_id`),
  KEY `fk_polish_idx` (`polish_id`),
  KEY `fk_symmetry_idx` (`symmetry_id`),
  KEY `fk_fluorescence_idx` (`fluorescence_id`),
  CONSTRAINT `fk_clarity` FOREIGN KEY (`clarity_id`) REFERENCES `clarities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_color` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cut` FOREIGN KEY (`cut_id`) REFERENCES `cuts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fluorescence` FOREIGN KEY (`fluorescence_id`) REFERENCES `fluorescences` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_polish` FOREIGN KEY (`polish_id`) REFERENCES `polishes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shape` FOREIGN KEY (`shape_id`) REFERENCES `shapes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_symmetry` FOREIGN KEY (`symmetry_id`) REFERENCES `symmetries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stones_staging`
--

LOCK TABLES `stones_staging` WRITE;
/*!40000 ALTER TABLE `stones_staging` DISABLE KEYS */;
/*!40000 ALTER TABLE `stones_staging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `symmetries`
--

DROP TABLE IF EXISTS `symmetries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `symmetries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `symmetries`
--

LOCK TABLES `symmetries` WRITE;
/*!40000 ALTER TABLE `symmetries` DISABLE KEYS */;
INSERT INTO `symmetries` VALUES (1,1,'EX','Excellent',NULL),(2,2,'I','Ideal',NULL),(3,3,'VG','Very Good',NULL),(4,4,'G','Good',NULL),(5,5,'F','Fair',NULL),(6,100,'other',NULL,NULL);
/*!40000 ALTER TABLE `symmetries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'a@a.com','qwerty','John','Smith'),(2,'b@b.com','qwerty','Bob','Dillan');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-10 22:12:42
