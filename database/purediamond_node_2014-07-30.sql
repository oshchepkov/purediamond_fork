# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.34)
# Database: purediamond_node
# Generation Time: 2014-07-30 13:20:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table clarities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clarities`;

CREATE TABLE `clarities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `clarities` WRITE;
/*!40000 ALTER TABLE `clarities` DISABLE KEYS */;

INSERT INTO `clarities` (`id`, `rank`, `name`, `friendly_name`, `description`)
VALUES
	(1,1,'IF',NULL,'clarity description'),
	(2,2,'VVS1',NULL,'clarity description'),
	(3,3,'VVS2',NULL,'clarity description'),
	(4,4,'VS1',NULL,'clarity description'),
	(5,5,'VS2',NULL,'clarity description'),
	(6,6,'SI1',NULL,'clarity description'),
	(7,7,'SI2',NULL,'clarity description'),
	(8,8,'SI3',NULL,'clarity description'),
	(9,9,'I1',NULL,'clarity description'),
	(10,10,'I2',NULL,'clarity description'),
	(11,11,'I3',NULL,'clarity description'),
	(12,100,'other',NULL,'clarity description');

/*!40000 ALTER TABLE `clarities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table collections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `collections`;

CREATE TABLE `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `collections` WRITE;
/*!40000 ALTER TABLE `collections` DISABLE KEYS */;

INSERT INTO `collections` (`id`, `rank`, `name`, `description`)
VALUES
	(1,1,'Platinum',NULL),
	(2,2,'Gold',NULL),
	(3,3,'Silver',NULL);

/*!40000 ALTER TABLE `collections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table colors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `colors`;

CREATE TABLE `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;

INSERT INTO `colors` (`id`, `rank`, `name`, `friendly_name`, `description`)
VALUES
	(1,1,'D',NULL,'color description'),
	(2,2,'E',NULL,'color description'),
	(3,3,'F',NULL,'color description'),
	(4,4,'G',NULL,'color description'),
	(5,5,'H',NULL,'color description'),
	(6,6,'I',NULL,'color description'),
	(7,7,'J',NULL,'color description'),
	(8,8,'K',NULL,'color description'),
	(9,9,'L',NULL,'color description'),
	(10,10,'M',NULL,'color description'),
	(11,11,'N',NULL,'color description'),
	(12,12,'O',NULL,'color description'),
	(13,13,'P',NULL,'color description'),
	(14,14,'Q',NULL,'color description'),
	(15,15,'R',NULL,'color description'),
	(16,16,'S',NULL,'color description'),
	(17,17,'T',NULL,'color description'),
	(18,18,'U',NULL,'color description'),
	(19,19,'V',NULL,'color description'),
	(20,20,'W',NULL,'color description'),
	(21,21,'X',NULL,'color description'),
	(22,22,'Y',NULL,'color description'),
	(23,23,'Z',NULL,'color description'),
	(24,100,'other',NULL,NULL);

/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cuts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cuts`;

CREATE TABLE `cuts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cuts` WRITE;
/*!40000 ALTER TABLE `cuts` DISABLE KEYS */;

INSERT INTO `cuts` (`id`, `rank`, `name`, `friendly_name`, `description`)
VALUES
	(1,1,'EX','Super Ideal','super description'),
	(2,2,'I','Ideal','ideal description'),
	(3,3,'VG','Very Good','very good description'),
	(4,4,'G','Good','good description'),
	(5,5,'F','Fair','fair description'),
	(6,100,'other',NULL,NULL);

/*!40000 ALTER TABLE `cuts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table designers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `designers`;

CREATE TABLE `designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `designers` WRITE;
/*!40000 ALTER TABLE `designers` DISABLE KEYS */;

INSERT INTO `designers` (`id`, `name`, `slug`, `image`, `description`)
VALUES
	(16,'Franz Kafka','franz_kafka','kabak3.jpg',NULL),
	(17,'Master Yoda','master_yoda','kabak2.jpg',NULL),
	(18,'Artemy Lebedev','artemy_lebedev','kabak4.jpg',NULL),
	(20,'Jean Claude Van Damme','jean_claude_van_damme','kabak1.jpg',NULL),
	(21,'Stephen Hawking','stephen_hawking','browdy.jpg',NULL);

/*!40000 ALTER TABLE `designers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fluorescences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fluorescences`;

CREATE TABLE `fluorescences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `fluorescences` WRITE;
/*!40000 ALTER TABLE `fluorescences` DISABLE KEYS */;

INSERT INTO `fluorescences` (`id`, `rank`, `name`, `friendly_name`, `description`)
VALUES
	(1,1,'N','None',NULL),
	(2,2,'VSL','Very Slight',NULL),
	(3,3,'SL','Slight',NULL),
	(4,4,'F','Faint',NULL),
	(5,5,'M','Medium',NULL),
	(6,6,'S','Strong',NULL),
	(7,7,'VST','Very Strong',NULL),
	(8,100,'other',NULL,NULL);

/*!40000 ALTER TABLE `fluorescences` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;

INSERT INTO `images` (`id`, `file_name`, `description`)
VALUES
	(1,'1.jpg',NULL),
	(2,'2.jpg',NULL),
	(3,'3.jpg',NULL),
	(4,'4.jpg',NULL);

/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table metals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `metals`;

CREATE TABLE `metals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `metals` WRITE;
/*!40000 ALTER TABLE `metals` DISABLE KEYS */;

INSERT INTO `metals` (`id`, `rank`, `name`)
VALUES
	(1,1,'Platinum'),
	(2,2,'Gold'),
	(3,3,'Silver'),
	(4,4,'Copper'),
	(5,5,'Mercury');

/*!40000 ALTER TABLE `metals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table polishes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `polishes`;

CREATE TABLE `polishes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `polishes` WRITE;
/*!40000 ALTER TABLE `polishes` DISABLE KEYS */;

INSERT INTO `polishes` (`id`, `rank`, `name`, `friendly_name`, `description`)
VALUES
	(1,1,'EX','Excellent',NULL),
	(2,3,'VG','Very Good',NULL),
	(3,4,'G','Good',NULL),
	(4,5,'F','Fair',NULL),
	(5,100,'other','',NULL),
	(6,2,'I','Ideal',NULL);

/*!40000 ALTER TABLE `polishes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ring_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ring_images`;

CREATE TABLE `ring_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_image_ring_idx` (`ring_id`),
  CONSTRAINT `fk_image_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rings`;

CREATE TABLE `rings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `lot_number` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stone_weight_min` decimal(10,2) NOT NULL,
  `stone_weight_max` decimal(10,2) NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rings` WRITE;
/*!40000 ALTER TABLE `rings` DISABLE KEYS */;

INSERT INTO `rings` (`id`, `title`, `lot_number`, `price`, `stone_weight_min`, `stone_weight_max`, `published`)
VALUES
	(2,'Ring two',2,5.65,0.60,3.60,1),
	(3,'Ring',1,2.00,0.10,0.20,1);

/*!40000 ALTER TABLE `rings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rings_collections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rings_collections`;

CREATE TABLE `rings_collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_collections_collection_idx` (`collection_id`),
  KEY `fk_ring_collections_ring_idx` (`ring_id`),
  CONSTRAINT `fk_ring_collections_collection` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ring_collections_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rings_collections` WRITE;
/*!40000 ALTER TABLE `rings_collections` DISABLE KEYS */;

INSERT INTO `rings_collections` (`id`, `ring_id`, `collection_id`)
VALUES
	(75,3,1),
	(78,2,2);

/*!40000 ALTER TABLE `rings_collections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rings_designers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rings_designers`;

CREATE TABLE `rings_designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `designer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_designers_ring_idx` (`ring_id`),
  KEY `fk_ring_designers_designer_idx` (`designer_id`),
  CONSTRAINT `fk_ring_designers_designer` FOREIGN KEY (`designer_id`) REFERENCES `designers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ring_designers_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rings_designers` WRITE;
/*!40000 ALTER TABLE `rings_designers` DISABLE KEYS */;

INSERT INTO `rings_designers` (`id`, `ring_id`, `designer_id`)
VALUES
	(37,3,16),
	(40,2,17);

/*!40000 ALTER TABLE `rings_designers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rings_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rings_images`;

CREATE TABLE `rings_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `favorite` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rings_images_ring_idx` (`ring_id`),
  KEY `fk_rings_images_image_idx` (`image_id`),
  CONSTRAINT `fk_rings_images_image` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rings_images_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rings_images` WRITE;
/*!40000 ALTER TABLE `rings_images` DISABLE KEYS */;

INSERT INTO `rings_images` (`id`, `ring_id`, `image_id`, `favorite`)
VALUES
	(1,2,1,0),
	(2,2,2,1),
	(3,2,3,0),
	(4,2,4,0);

/*!40000 ALTER TABLE `rings_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rings_metals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rings_metals`;

CREATE TABLE `rings_metals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `metal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_metal_idx` (`metal_id`),
  KEY `fk_ring_metals_ring_idx` (`ring_id`),
  CONSTRAINT `fk_ring_metals_metal` FOREIGN KEY (`metal_id`) REFERENCES `metals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ring_metals_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rings_metals` WRITE;
/*!40000 ALTER TABLE `rings_metals` DISABLE KEYS */;

INSERT INTO `rings_metals` (`id`, `ring_id`, `metal_id`)
VALUES
	(92,3,1),
	(95,2,2);

/*!40000 ALTER TABLE `rings_metals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rings_shapes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rings_shapes`;

CREATE TABLE `rings_shapes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `shape_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_shapes_shape_idx` (`shape_id`),
  KEY `fk_ring_shapes_ring_idx` (`ring_id`),
  CONSTRAINT `fk_ring_shapes_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ring_shapes_shape` FOREIGN KEY (`shape_id`) REFERENCES `shapes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rings_shapes` WRITE;
/*!40000 ALTER TABLE `rings_shapes` DISABLE KEYS */;

INSERT INTO `rings_shapes` (`id`, `ring_id`, `shape_id`)
VALUES
	(102,3,1),
	(105,2,1);

/*!40000 ALTER TABLE `rings_shapes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shapes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shapes`;

CREATE TABLE `shapes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `class_name` varchar(45) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shapes` WRITE;
/*!40000 ALTER TABLE `shapes` DISABLE KEYS */;

INSERT INTO `shapes` (`id`, `rank`, `name`, `friendly_name`, `description`, `class_name`, `visible`)
VALUES
	(1,4,'Asscher',NULL,NULL,'asscher',1),
	(2,100,'Baguette',NULL,NULL,'baguette',0),
	(3,3,'Cushion Modified',NULL,NULL,'cushion',1),
	(4,9,'Emerald',NULL,NULL,'emerald',1),
	(5,10,'Heart',NULL,NULL,'heart',1),
	(6,102,'Kite',NULL,NULL,'kite',0),
	(7,5,'Marquise',NULL,NULL,'marquise',1),
	(8,6,'Oval',NULL,NULL,'oval',1),
	(9,8,'Pear',NULL,NULL,'pear',1),
	(10,2,'Princess',NULL,NULL,'princess',1),
	(11,7,'Radiant',NULL,NULL,'radiant',1),
	(12,1,'Round',NULL,NULL,'round',1),
	(13,101,'Trilliant',NULL,NULL,'trilliant',0);

/*!40000 ALTER TABLE `shapes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stones`;

CREATE TABLE `stones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_number` varchar(45) NOT NULL,
  `shape_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `clarity_id` int(11) NOT NULL,
  `cut_id` int(11) NOT NULL,
  `polish_id` int(11) NOT NULL,
  `symmetry_id` int(11) NOT NULL,
  `fluorescence_id` int(11) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `measurements` varchar(20) NOT NULL,
  `certificate_number` varchar(45) DEFAULT NULL,
  `certificate_filename` varchar(45) DEFAULT NULL,
  `diamond_image` varchar(45) DEFAULT NULL,
  `rapnet_price` decimal(10,2) NOT NULL,
  `cash_price` decimal(10,2) DEFAULT NULL,
  `rapnet_discount` decimal(10,2) DEFAULT NULL,
  `cash_price_discount` decimal(10,2) DEFAULT NULL,
  `lab` varchar(45) DEFAULT NULL,
  `fluorescence_color` varchar(20) DEFAULT NULL,
  `treatment` varchar(20) DEFAULT NULL,
  `availability` varchar(20) DEFAULT NULL,
  `fancy_color` varchar(20) DEFAULT NULL,
  `fancy_color_intensity` varchar(20) DEFAULT NULL,
  `fancy_color_overtone` varchar(20) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `table_percent` decimal(10,2) DEFAULT NULL,
  `girdle_thin` varchar(20) DEFAULT NULL,
  `girdle_thick` varchar(20) DEFAULT NULL,
  `girdle` decimal(10,2) DEFAULT NULL,
  `girdle_condition` varchar(45) DEFAULT NULL,
  `cullet_size` varchar(20) DEFAULT NULL,
  `cullet_condition` varchar(45) DEFAULT NULL,
  `crown_height` decimal(10,2) DEFAULT NULL,
  `crown_angle` decimal(10,2) DEFAULT NULL,
  `pavilion_depth` decimal(10,2) DEFAULT NULL,
  `pavilion_angle` decimal(10,2) DEFAULT NULL,
  `laser_inscription` varchar(45) DEFAULT NULL,
  `cert_comment` varchar(255) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `time_to_location` varchar(45) DEFAULT NULL,
  `is_matched_pair_separable` varchar(20) DEFAULT NULL,
  `pair_stock_number` varchar(20) DEFAULT NULL,
  `allow_raplink_feed` int(11) DEFAULT NULL,
  `parcel_stones` varchar(20) DEFAULT NULL,
  `trade_show` varchar(20) DEFAULT NULL,
  `key_to_symbols` varchar(20) DEFAULT NULL,
  `shade` varchar(20) DEFAULT NULL,
  `star_length` varchar(20) DEFAULT NULL,
  `center_inclusion` varchar(20) DEFAULT NULL,
  `black_inclusion` varchar(20) DEFAULT NULL,
  `member_comment` varchar(20) DEFAULT NULL,
  `report_issue_date` varchar(20) DEFAULT NULL,
  `report_type` varchar(20) DEFAULT NULL,
  `lab_location` varchar(20) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `milky` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shape_idx` (`shape_id`),
  KEY `fk_color_idx` (`color_id`),
  KEY `fk_clarity_idx` (`clarity_id`),
  KEY `fk_cut_idx` (`cut_id`),
  KEY `fk_polish_idx` (`polish_id`),
  KEY `fk_symmetry_idx` (`symmetry_id`),
  KEY `fk_fluorescence_idx` (`fluorescence_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `stones` WRITE;
/*!40000 ALTER TABLE `stones` DISABLE KEYS */;

INSERT INTO `stones` (`id`, `stock_number`, `shape_id`, `color_id`, `clarity_id`, `cut_id`, `polish_id`, `symmetry_id`, `fluorescence_id`, `weight`, `measurements`, `certificate_number`, `certificate_filename`, `diamond_image`, `rapnet_price`, `cash_price`, `rapnet_discount`, `cash_price_discount`, `lab`, `fluorescence_color`, `treatment`, `availability`, `fancy_color`, `fancy_color_intensity`, `fancy_color_overtone`, `depth`, `table_percent`, `girdle_thin`, `girdle_thick`, `girdle`, `girdle_condition`, `cullet_size`, `cullet_condition`, `crown_height`, `crown_angle`, `pavilion_depth`, `pavilion_angle`, `laser_inscription`, `cert_comment`, `country`, `state`, `city`, `time_to_location`, `is_matched_pair_separable`, `pair_stock_number`, `allow_raplink_feed`, `parcel_stones`, `trade_show`, `key_to_symbols`, `shade`, `star_length`, `center_inclusion`, `black_inclusion`, `member_comment`, `report_issue_date`, `report_type`, `lab_location`, `brand`, `milky`)
VALUES
	(1,'ABZ-04553',9,1,6,3,1,3,1,0.24,'3.75x0.89x1.97','2156382646','910489203.Jpg','910489203-2.Jpg',702.00,0.00,-0.35,0.00,'EGL USA','','','G','','','',52.50,58.00,'TN','M',0.00,'','N','',0.00,0.00,0.00,0.00,'','','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),
	(2,'ASZ-01565',7,1,6,3,2,3,1,0.26,'8.50x3.37x1.59','93913803','93913803.jpg','93913803-2.Jpg',864.00,0.00,-0.20,0.00,'EGL USA','','','G','','','',47.20,58.00,'TN','TN',0.00,'','N','',11.30,0.00,32.90,0.00,'','','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),
	(3,'ARE-22371',12,5,6,4,2,4,1,0.30,'4.22-4.24x2.61','2131851939','2131851939.jpg','2131851939-2.Jpg',0.00,0.00,-0.17,0.00,'GIA','','','G','','','',61.60,58.00,'M','VTK',0.00,'','N','',14.50,0.00,41.50,0.00,'','','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),
	(4,'ARE-20771',12,1,5,3,2,3,1,0.30,'4.09-4.13x2.74','904462108','904462108.jpg','904462108-2.Jpg',1740.00,0.00,-0.40,0.00,'EGL USA','','','G','','','',66.70,56.00,'VTN','TK',0.00,'','N','',17.80,0.00,43.10,0.00,'','HEARTS & ARROWS','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),
	(5,'ACL-544',12,1,7,3,2,6,4,0.30,'4.32-4.33x2.65','1169080782','1169080782.Jpg','1169080782-2.Jpg',1800.00,0.00,-0.28,0.00,'GIA','','','G','','','',61.10,55.00,'M','STK',0.00,'','N','',15.00,0.00,42.50,0.00,'','','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),
	(6,'ACL-545',12,4,6,3,1,6,1,0.30,'4.35-4.40x2.54','909650518','909650518.jpg','909650518-2.Jpg',1840.00,0.00,-0.20,0.00,'EGL USA','','','G','','','',58.10,61.00,'TN','M',0.00,'','N','',13.00,34.00,41.40,39.70,'','HEARTS & ARROWS','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','','');

/*!40000 ALTER TABLE `stones` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stones_staging
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stones_staging`;

CREATE TABLE `stones_staging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_number` varchar(45) NOT NULL,
  `shape_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `clarity_id` int(11) NOT NULL,
  `cut_id` int(11) NOT NULL,
  `polish_id` int(11) NOT NULL,
  `symmetry_id` int(11) NOT NULL,
  `fluorescence_id` int(11) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `measurements` varchar(20) NOT NULL,
  `certificate_number` varchar(45) DEFAULT NULL,
  `certificate_filename` varchar(45) DEFAULT NULL,
  `diamond_image` varchar(45) DEFAULT NULL,
  `rapnet_price` decimal(10,2) NOT NULL,
  `cash_price` decimal(10,2) DEFAULT NULL,
  `rapnet_discount` decimal(10,2) DEFAULT NULL,
  `cash_price_discount` decimal(10,2) DEFAULT NULL,
  `lab` varchar(45) DEFAULT NULL,
  `fluorescence_color` varchar(20) DEFAULT NULL,
  `treatment` varchar(20) DEFAULT NULL,
  `availability` varchar(20) DEFAULT NULL,
  `fancy_color` varchar(20) DEFAULT NULL,
  `fancy_color_intensity` varchar(20) DEFAULT NULL,
  `fancy_color_overtone` varchar(20) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `table_percent` decimal(10,2) DEFAULT NULL,
  `girdle_thin` varchar(20) DEFAULT NULL,
  `girdle_thick` varchar(20) DEFAULT NULL,
  `girdle` decimal(10,2) DEFAULT NULL,
  `girdle_condition` varchar(45) DEFAULT NULL,
  `cullet_size` varchar(20) DEFAULT NULL,
  `cullet_condition` varchar(45) DEFAULT NULL,
  `crown_height` decimal(10,2) DEFAULT NULL,
  `crown_angle` decimal(10,2) DEFAULT NULL,
  `pavilion_depth` decimal(10,2) DEFAULT NULL,
  `pavilion_angle` decimal(10,2) DEFAULT NULL,
  `laser_inscription` varchar(45) DEFAULT NULL,
  `cert_comment` varchar(255) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `time_to_location` varchar(45) DEFAULT NULL,
  `is_matched_pair_separable` varchar(20) DEFAULT NULL,
  `pair_stock_number` varchar(20) DEFAULT NULL,
  `allow_raplink_feed` int(11) DEFAULT NULL,
  `parcel_stones` varchar(20) DEFAULT NULL,
  `trade_show` varchar(20) DEFAULT NULL,
  `key_to_symbols` varchar(20) DEFAULT NULL,
  `shade` varchar(20) DEFAULT NULL,
  `star_length` varchar(20) DEFAULT NULL,
  `center_inclusion` varchar(20) DEFAULT NULL,
  `black_inclusion` varchar(20) DEFAULT NULL,
  `member_comment` varchar(20) DEFAULT NULL,
  `report_issue_date` varchar(20) DEFAULT NULL,
  `report_type` varchar(20) DEFAULT NULL,
  `lab_location` varchar(20) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `milky` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shape_idx` (`shape_id`),
  KEY `fk_color_idx` (`color_id`),
  KEY `fk_clarity_idx` (`clarity_id`),
  KEY `fk_cut_idx` (`cut_id`),
  KEY `fk_polish_idx` (`polish_id`),
  KEY `fk_symmetry_idx` (`symmetry_id`),
  KEY `fk_fluorescence_idx` (`fluorescence_id`),
  CONSTRAINT `fk_clarity` FOREIGN KEY (`clarity_id`) REFERENCES `clarities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_color` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cut` FOREIGN KEY (`cut_id`) REFERENCES `cuts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fluorescence` FOREIGN KEY (`fluorescence_id`) REFERENCES `fluorescences` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_polish` FOREIGN KEY (`polish_id`) REFERENCES `polishes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shape` FOREIGN KEY (`shape_id`) REFERENCES `shapes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_symmetry` FOREIGN KEY (`symmetry_id`) REFERENCES `symmetries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table symmetries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `symmetries`;

CREATE TABLE `symmetries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `symmetries` WRITE;
/*!40000 ALTER TABLE `symmetries` DISABLE KEYS */;

INSERT INTO `symmetries` (`id`, `rank`, `name`, `friendly_name`, `description`)
VALUES
	(1,1,'EX','Excellent',NULL),
	(2,2,'I','Ideal',NULL),
	(3,3,'VG','Very Good',NULL),
	(4,4,'G','Good',NULL),
	(5,5,'F','Fair',NULL),
	(6,100,'other',NULL,NULL);

/*!40000 ALTER TABLE `symmetries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `firstname`, `lastname`)
VALUES
	(1,'a@a.com','qwerty','John','Smith'),
	(2,'b@b.com','qwerty','Bob','Dillan');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
