CREATE DATABASE  IF NOT EXISTS `diamonds` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `diamonds`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: diamonds
-- ------------------------------------------------------
-- Server version	5.6.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clarities`
--

DROP TABLE IF EXISTS `clarities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clarities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clarities`
--

LOCK TABLES `clarities` WRITE;
/*!40000 ALTER TABLE `clarities` DISABLE KEYS */;
INSERT INTO `clarities` VALUES (1,1,'IF',NULL,'clarity description'),(2,2,'VVS1',NULL,'clarity description'),(3,3,'VVS2',NULL,'clarity description'),(4,4,'VS1',NULL,'clarity description'),(5,5,'VS2',NULL,'clarity description'),(6,6,'SI1',NULL,'clarity description'),(7,7,'SI2',NULL,'clarity description'),(8,8,'SI3',NULL,'clarity description'),(9,9,'I1',NULL,'clarity description'),(10,10,'I2',NULL,'clarity description'),(11,11,'I3',NULL,'clarity description'),(12,100,'other',NULL,'clarity description');
/*!40000 ALTER TABLE `clarities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collections`
--

DROP TABLE IF EXISTS `collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collections`
--

LOCK TABLES `collections` WRITE;
/*!40000 ALTER TABLE `collections` DISABLE KEYS */;
INSERT INTO `collections` VALUES (1,1,'Platinum',NULL),(2,2,'Gold',NULL),(3,3,'Silver',NULL);
/*!40000 ALTER TABLE `collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,1,'D',NULL,'color description'),(2,2,'E',NULL,'color description'),(3,3,'F',NULL,'color description'),(4,4,'G',NULL,'color description'),(5,5,'H',NULL,'color description'),(6,6,'I',NULL,'color description'),(7,7,'J',NULL,'color description'),(8,8,'K',NULL,'color description'),(9,9,'L',NULL,'color description'),(10,10,'M',NULL,'color description'),(11,11,'N',NULL,'color description'),(12,12,'O',NULL,'color description'),(13,13,'P',NULL,'color description'),(14,14,'Q',NULL,'color description'),(15,15,'R',NULL,'color description'),(16,16,'S',NULL,'color description'),(17,17,'T',NULL,'color description'),(18,18,'U',NULL,'color description'),(19,19,'V',NULL,'color description'),(20,20,'W',NULL,'color description'),(21,21,'X',NULL,'color description'),(22,22,'Y',NULL,'color description'),(23,23,'Z',NULL,'color description'),(24,100,'other',NULL,NULL);
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuts`
--

DROP TABLE IF EXISTS `cuts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuts`
--

LOCK TABLES `cuts` WRITE;
/*!40000 ALTER TABLE `cuts` DISABLE KEYS */;
INSERT INTO `cuts` VALUES (1,1,'EX','Super Ideal','super description'),(2,2,'I','Ideal','ideal description'),(3,3,'VG','Very Good','very good description'),(4,4,'G','Good','good description'),(5,5,'F','Fair','fair description'),(6,100,'other',NULL,NULL);
/*!40000 ALTER TABLE `cuts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designers`
--

DROP TABLE IF EXISTS `designers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designers`
--

LOCK TABLES `designers` WRITE;
/*!40000 ALTER TABLE `designers` DISABLE KEYS */;
INSERT INTO `designers` VALUES (16,'Franz Kafka','franz_kafka','kabak3.jpg',NULL),(17,'Master Yoda','master_yoda','kabak2.jpg',NULL),(18,'Artemy Lebedev','artemy_lebedev','kabak4.jpg',NULL),(20,'Jean Claude Van Damme','jean_claude_van_damme','kabak1.jpg',NULL),(21,'Stephen Hawking','stephen_hawking','browdy.jpg',NULL);
/*!40000 ALTER TABLE `designers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fluorescences`
--

DROP TABLE IF EXISTS `fluorescences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fluorescences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fluorescences`
--

LOCK TABLES `fluorescences` WRITE;
/*!40000 ALTER TABLE `fluorescences` DISABLE KEYS */;
INSERT INTO `fluorescences` VALUES (1,1,'N','None',NULL),(2,2,'VSL','Very Slight',NULL),(3,3,'SL','Slight',NULL),(4,4,'F','Faint',NULL),(5,5,'M','Medium',NULL),(6,6,'S','Strong',NULL),(7,7,'VST','Very Strong',NULL),(8,100,'other',NULL,NULL);
/*!40000 ALTER TABLE `fluorescences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metals`
--

DROP TABLE IF EXISTS `metals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metals`
--

LOCK TABLES `metals` WRITE;
/*!40000 ALTER TABLE `metals` DISABLE KEYS */;
INSERT INTO `metals` VALUES (1,1,'Platinum'),(2,2,'Gold'),(3,3,'Silver');
/*!40000 ALTER TABLE `metals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polishes`
--

DROP TABLE IF EXISTS `polishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `polishes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polishes`
--

LOCK TABLES `polishes` WRITE;
/*!40000 ALTER TABLE `polishes` DISABLE KEYS */;
INSERT INTO `polishes` VALUES (1,1,'EX','Excellent',NULL),(2,3,'VG','Very Good',NULL),(3,4,'G','Good',NULL),(4,5,'F','Fair',NULL),(5,100,'other','',NULL),(6,2,'I','Ideal',NULL);
/*!40000 ALTER TABLE `polishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ring_images`
--

DROP TABLE IF EXISTS `ring_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ring_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_image_ring_idx` (`ring_id`),
  CONSTRAINT `fk_image_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ring_images`
--

LOCK TABLES `ring_images` WRITE;
/*!40000 ALTER TABLE `ring_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `ring_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings`
--

DROP TABLE IF EXISTS `rings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `lot_number` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stone_weight_min` decimal(10,2) NOT NULL,
  `stone_weight_max` decimal(10,2) NOT NULL,
  `designer_id` int(11) NOT NULL,
  `published` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_designer_idx` (`designer_id`),
  CONSTRAINT `fk_ring_designer` FOREIGN KEY (`designer_id`) REFERENCES `designers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings`
--

LOCK TABLES `rings` WRITE;
/*!40000 ALTER TABLE `rings` DISABLE KEYS */;
INSERT INTO `rings` VALUES (1,'Ring 1',345,25.78,0.50,2.50,16,''),(2,'Ring 2',56,46.35,0.60,3.60,17,'\0'),(3,'Ring 3',7657,68.96,0.70,4.70,18,'');
/*!40000 ALTER TABLE `rings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings_collections`
--

DROP TABLE IF EXISTS `rings_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings_collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_collections_collection_idx` (`collection_id`),
  KEY `fk_ring_collections_ring_idx` (`ring_id`),
  CONSTRAINT `fk_ring_collections_collection` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ring_collections_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings_collections`
--

LOCK TABLES `rings_collections` WRITE;
/*!40000 ALTER TABLE `rings_collections` DISABLE KEYS */;
INSERT INTO `rings_collections` VALUES (1,1,1),(2,1,3),(3,2,1),(4,2,2),(5,2,3),(6,3,1),(7,3,2),(8,3,3);
/*!40000 ALTER TABLE `rings_collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings_metals`
--

DROP TABLE IF EXISTS `rings_metals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings_metals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `metal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_metal_idx` (`metal_id`),
  KEY `fk_ring_metals_ring_idx` (`ring_id`),
  CONSTRAINT `fk_ring_metals_metal` FOREIGN KEY (`metal_id`) REFERENCES `metals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ring_metals_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings_metals`
--

LOCK TABLES `rings_metals` WRITE;
/*!40000 ALTER TABLE `rings_metals` DISABLE KEYS */;
INSERT INTO `rings_metals` VALUES (1,1,1),(2,1,2),(3,1,3),(4,2,2),(5,2,3),(6,3,1),(7,3,3);
/*!40000 ALTER TABLE `rings_metals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rings_shapes`
--

DROP TABLE IF EXISTS `rings_shapes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rings_shapes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ring_id` int(11) NOT NULL,
  `shape_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ring_shapes_shape_idx` (`shape_id`),
  KEY `fk_ring_shapes_ring_idx` (`ring_id`),
  CONSTRAINT `fk_ring_shapes_shape` FOREIGN KEY (`shape_id`) REFERENCES `shapes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ring_shapes_ring` FOREIGN KEY (`ring_id`) REFERENCES `rings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rings_shapes`
--

LOCK TABLES `rings_shapes` WRITE;
/*!40000 ALTER TABLE `rings_shapes` DISABLE KEYS */;
INSERT INTO `rings_shapes` VALUES (1,1,3),(2,1,5),(3,1,7),(4,1,9),(5,2,4),(6,2,6),(7,2,8),(8,2,10),(9,3,6),(10,3,11),(11,3,13);
/*!40000 ALTER TABLE `rings_shapes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shapes`
--

DROP TABLE IF EXISTS `shapes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shapes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `class_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shapes`
--

LOCK TABLES `shapes` WRITE;
/*!40000 ALTER TABLE `shapes` DISABLE KEYS */;
INSERT INTO `shapes` VALUES (1,1,'Asscher',NULL,NULL,'asscher'),(2,2,'Baguette',NULL,NULL,'baguette'),(3,3,'Cushion Modified',NULL,NULL,'cushion_modified'),(4,4,'Emerald',NULL,NULL,'emerald'),(5,5,'Heart',NULL,NULL,'heart'),(6,6,'Kite',NULL,NULL,'kite'),(7,7,'Marquise',NULL,NULL,'marquise'),(8,8,'Oval',NULL,NULL,'oval'),(9,9,'Pear',NULL,NULL,'pear'),(10,10,'Princess',NULL,NULL,'princess'),(11,11,'Radiant',NULL,NULL,'radiant'),(12,12,'Round',NULL,NULL,'round'),(13,13,'Trilliant',NULL,NULL,'trilliant');
/*!40000 ALTER TABLE `shapes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stones`
--

DROP TABLE IF EXISTS `stones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_number` varchar(45) NOT NULL,
  `shape_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `clarity_id` int(11) NOT NULL,
  `cut_id` int(11) NOT NULL,
  `polish_id` int(11) NOT NULL,
  `symmetry_id` int(11) NOT NULL,
  `fluorescence_id` int(11) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `measurements` varchar(20) NOT NULL,
  `certificate_number` varchar(45) DEFAULT NULL,
  `certificate_filename` varchar(45) DEFAULT NULL,
  `diamond_image` varchar(45) DEFAULT NULL,
  `rapnet_price` decimal(10,2) NOT NULL,
  `cash_price` decimal(10,2) DEFAULT NULL,
  `rapnet_discount` decimal(10,2) DEFAULT NULL,
  `cash_price_discount` decimal(10,2) DEFAULT NULL,
  `lab` varchar(45) DEFAULT NULL,
  `fluorescence_color` varchar(20) DEFAULT NULL,
  `treatment` varchar(20) DEFAULT NULL,
  `availability` varchar(20) DEFAULT NULL,
  `fancy_color` varchar(20) DEFAULT NULL,
  `fancy_color_intensity` varchar(20) DEFAULT NULL,
  `fancy_color_overtone` varchar(20) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `table_percent` decimal(10,2) DEFAULT NULL,
  `girdle_thin` varchar(20) DEFAULT NULL,
  `girdle_thick` varchar(20) DEFAULT NULL,
  `girdle` decimal(10,2) DEFAULT NULL,
  `girdle_condition` varchar(45) DEFAULT NULL,
  `cullet_size` varchar(20) DEFAULT NULL,
  `cullet_condition` varchar(45) DEFAULT NULL,
  `crown_height` decimal(10,2) DEFAULT NULL,
  `crown_angle` decimal(10,2) DEFAULT NULL,
  `pavilion_depth` decimal(10,2) DEFAULT NULL,
  `pavilion_angle` decimal(10,2) DEFAULT NULL,
  `laser_inscription` varchar(45) DEFAULT NULL,
  `cert_comment` varchar(255) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `time_to_location` varchar(45) DEFAULT NULL,
  `is_matched_pair_separable` varchar(20) DEFAULT NULL,
  `pair_stock_number` varchar(20) DEFAULT NULL,
  `allow_raplink_feed` int(11) DEFAULT NULL,
  `parcel_stones` varchar(20) DEFAULT NULL,
  `trade_show` varchar(20) DEFAULT NULL,
  `key_to_symbols` varchar(20) DEFAULT NULL,
  `shade` varchar(20) DEFAULT NULL,
  `star_length` varchar(20) DEFAULT NULL,
  `center_inclusion` varchar(20) DEFAULT NULL,
  `black_inclusion` varchar(20) DEFAULT NULL,
  `member_comment` varchar(20) DEFAULT NULL,
  `report_issue_date` varchar(20) DEFAULT NULL,
  `report_type` varchar(20) DEFAULT NULL,
  `lab_location` varchar(20) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `milky` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shape_idx` (`shape_id`),
  KEY `fk_color_idx` (`color_id`),
  KEY `fk_clarity_idx` (`clarity_id`),
  KEY `fk_cut_idx` (`cut_id`),
  KEY `fk_polish_idx` (`polish_id`),
  KEY `fk_symmetry_idx` (`symmetry_id`),
  KEY `fk_fluorescence_idx` (`fluorescence_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stones`
--

LOCK TABLES `stones` WRITE;
/*!40000 ALTER TABLE `stones` DISABLE KEYS */;
INSERT INTO `stones` VALUES (1,'ABZ-04553',9,1,6,3,1,3,1,0.24,'3.75x0.89x1.97','2156382646','910489203.Jpg','910489203-2.Jpg',702.00,0.00,-0.35,0.00,'EGL USA','','','G','','','',52.50,58.00,'TN','M',0.00,'','N','',0.00,0.00,0.00,0.00,'','','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),(2,'ASZ-01565',7,1,6,3,2,3,1,0.26,'8.50x3.37x1.59','93913803','93913803.jpg','93913803-2.Jpg',864.00,0.00,-0.20,0.00,'EGL USA','','','G','','','',47.20,58.00,'TN','TN',0.00,'','N','',11.30,0.00,32.90,0.00,'','','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),(3,'ARE-20771',12,1,5,3,2,3,1,0.30,'4.09-4.13x2.74','904462108','904462108.jpg','904462108-2.Jpg',1740.00,0.00,-0.40,0.00,'EGL USA','','','G','','','',66.70,56.00,'VTN','TK',0.00,'','N','',17.80,0.00,43.10,0.00,'','HEARTS & ARROWS','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),(4,'ACL-545',12,4,6,3,1,6,1,0.30,'4.35-4.40x2.54','909650518','909650518.jpg','909650518-2.Jpg',1840.00,0.00,-0.20,0.00,'EGL USA','','','G','','','',58.10,61.00,'TN','M',0.00,'','N','',13.00,34.00,41.40,39.70,'','HEARTS & ARROWS','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),(5,'ACL-544',12,1,7,3,2,6,4,0.30,'4.32-4.33x2.65','1169080782','1169080782.Jpg','1169080782-2.Jpg',1800.00,0.00,-0.28,0.00,'GIA','','','G','','','',61.10,55.00,'M','STK',0.00,'','N','',15.00,0.00,42.50,0.00,'','','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','',''),(6,'ARE-22371',12,5,6,4,2,4,1,0.30,'4.22-4.24x2.61','2131851939','2131851939.jpg','2131851939-2.Jpg',0.00,0.00,-0.17,0.00,'GIA','','','G','','','',61.60,58.00,'M','VTK',0.00,'','N','',14.50,0.00,41.50,0.00,'','','USA','California','Los Angeles','','FALSE','',1,'','','','','','','','','','','','','');
/*!40000 ALTER TABLE `stones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stones_staging`
--

DROP TABLE IF EXISTS `stones_staging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stones_staging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_number` varchar(45) NOT NULL,
  `shape_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `clarity_id` int(11) NOT NULL,
  `cut_id` int(11) NOT NULL,
  `polish_id` int(11) NOT NULL,
  `symmetry_id` int(11) NOT NULL,
  `fluorescence_id` int(11) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  `measurements` varchar(20) NOT NULL,
  `certificate_number` varchar(45) DEFAULT NULL,
  `certificate_filename` varchar(45) DEFAULT NULL,
  `diamond_image` varchar(45) DEFAULT NULL,
  `rapnet_price` decimal(10,2) NOT NULL,
  `cash_price` decimal(10,2) DEFAULT NULL,
  `rapnet_discount` decimal(10,2) DEFAULT NULL,
  `cash_price_discount` decimal(10,2) DEFAULT NULL,
  `lab` varchar(45) DEFAULT NULL,
  `fluorescence_color` varchar(20) DEFAULT NULL,
  `treatment` varchar(20) DEFAULT NULL,
  `availability` varchar(20) DEFAULT NULL,
  `fancy_color` varchar(20) DEFAULT NULL,
  `fancy_color_intensity` varchar(20) DEFAULT NULL,
  `fancy_color_overtone` varchar(20) DEFAULT NULL,
  `depth` decimal(10,2) DEFAULT NULL,
  `table_percent` decimal(10,2) DEFAULT NULL,
  `girdle_thin` varchar(20) DEFAULT NULL,
  `girdle_thick` varchar(20) DEFAULT NULL,
  `girdle` decimal(10,2) DEFAULT NULL,
  `girdle_condition` varchar(45) DEFAULT NULL,
  `cullet_size` varchar(20) DEFAULT NULL,
  `cullet_condition` varchar(45) DEFAULT NULL,
  `crown_height` decimal(10,2) DEFAULT NULL,
  `crown_angle` decimal(10,2) DEFAULT NULL,
  `pavilion_depth` decimal(10,2) DEFAULT NULL,
  `pavilion_angle` decimal(10,2) DEFAULT NULL,
  `laser_inscription` varchar(45) DEFAULT NULL,
  `cert_comment` varchar(255) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `time_to_location` varchar(45) DEFAULT NULL,
  `is_matched_pair_separable` varchar(20) DEFAULT NULL,
  `pair_stock_number` varchar(20) DEFAULT NULL,
  `allow_raplink_feed` int(11) DEFAULT NULL,
  `parcel_stones` varchar(20) DEFAULT NULL,
  `trade_show` varchar(20) DEFAULT NULL,
  `key_to_symbols` varchar(20) DEFAULT NULL,
  `shade` varchar(20) DEFAULT NULL,
  `star_length` varchar(20) DEFAULT NULL,
  `center_inclusion` varchar(20) DEFAULT NULL,
  `black_inclusion` varchar(20) DEFAULT NULL,
  `member_comment` varchar(20) DEFAULT NULL,
  `report_issue_date` varchar(20) DEFAULT NULL,
  `report_type` varchar(20) DEFAULT NULL,
  `lab_location` varchar(20) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `milky` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shape_idx` (`shape_id`),
  KEY `fk_color_idx` (`color_id`),
  KEY `fk_clarity_idx` (`clarity_id`),
  KEY `fk_cut_idx` (`cut_id`),
  KEY `fk_polish_idx` (`polish_id`),
  KEY `fk_symmetry_idx` (`symmetry_id`),
  KEY `fk_fluorescence_idx` (`fluorescence_id`),
  CONSTRAINT `fk_clarity` FOREIGN KEY (`clarity_id`) REFERENCES `clarities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_color` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cut` FOREIGN KEY (`cut_id`) REFERENCES `cuts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fluorescence` FOREIGN KEY (`fluorescence_id`) REFERENCES `fluorescences` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_polish` FOREIGN KEY (`polish_id`) REFERENCES `polishes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shape` FOREIGN KEY (`shape_id`) REFERENCES `shapes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_symmetry` FOREIGN KEY (`symmetry_id`) REFERENCES `symmetries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stones_staging`
--

LOCK TABLES `stones_staging` WRITE;
/*!40000 ALTER TABLE `stones_staging` DISABLE KEYS */;
/*!40000 ALTER TABLE `stones_staging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `symmetries`
--

DROP TABLE IF EXISTS `symmetries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `symmetries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `friendly_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `symmetries`
--

LOCK TABLES `symmetries` WRITE;
/*!40000 ALTER TABLE `symmetries` DISABLE KEYS */;
INSERT INTO `symmetries` VALUES (1,1,'EX','Excellent',NULL),(2,2,'I','Ideal',NULL),(3,3,'VG','Very Good',NULL),(4,4,'G','Good',NULL),(5,5,'F','Fair',NULL),(6,100,'other',NULL,NULL);
/*!40000 ALTER TABLE `symmetries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'a@a.com','qwerty','John','Smith'),(2,'b@b.com','qwerty','Bob','Dillan');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-23 23:01:49
