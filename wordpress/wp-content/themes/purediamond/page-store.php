<?php get_header(); ?>
<div class="header-title">
  <h1 ng-show="page.title"><span ng-bind="page.title"></span></h1>
  <div class="colored-border two-colors"><span class="blue-line"></span></div>
</div>
<div id="main-content" ng-cloak>
  <div data-ng-view keep-scroll-pos id="page-content" class="container"></div>
</div> <!-- #main-content -->

<?php get_footer(); ?>
