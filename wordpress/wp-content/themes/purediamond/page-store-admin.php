<!DOCTYPE html>
<html data-ng-app="pureDiamondAdminApp" data-ng-controller="appController">
<head>
	<title>Pure Diamond - Admin</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/custom.css" type="text/css">
	<link rel="stylesheet" href="css/angular-slider.css" type="text/css">
	<style type="text/css">
	#logo {
		display : block;
		margin : auto;
	}
	</style>
</head>
<body>
	<div class="container-fluid">
		<h2>Admin console</h2>
		<!--
		<div my-spinner is-loading="isLoading"></div>

		<div>
			<img id="logo" ng-src="images/logo.png"/>
		</div>
	-->

	<div class="row">
		<div class="col-xs-2">
			<tabset vertical="true" type="pills">
				<tab heading="Stones" select="goRoute('stones')"></tab>
				<tab heading="Designers" select="goRoute('designers')"></tab>
				<tab heading="Rings" select="goRoute('rings')"></tab>
				<tab heading="Collections" select="goRoute('collections')"></tab>
				<tab heading="Orders" select="goRoute('orders')"></tab>
			</tabset>
		</div>
		<div class="col-xs-10">
			<div data-ng-view></div>
		</div>
	</div>

	<script src="js/angular-file-upload-html5-shim.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular-route.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular-sanitize.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/spin.js/2.0.1/spin.min.js"></script>
	<script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>

	<!-- Loading the app's main library -->
	<script src="app/app.js"></script>

	<!-- Loading controllers -->
 	<script src="app/controllers/appController.js"></script>
 	<script src="app/controllers/stonesController.js"></script>
 	<script src="app/controllers/ringsController.js"></script>
 	<script src="app/controllers/designersController.js"></script>
 	<script src="app/controllers/collectionsController.js"></script>
 	<script src="app/controllers/ordersController.js"></script>
 	<!-- Loading services -->
	<script src="app/services/stonesInventoryService.js"></script>
	<script src="app/services/ringsInventoryService.js"></script>
	<script src="app/services/designersService.js"></script>
	<script src="app/services/collectionsService.js"></script>
	<script src="app/services/ordersService.js"></script>

	<!-- Loading the app's main libraries -->
	<script src="app/directives.js"></script>
	<script src="app/filters.js"></script>
	<script src="js/utils.js"></script>

	<script src="js/angular-file-upload.min.js"></script>
	<script src='//cdnjs.cloudflare.com/ajax/libs/textAngular/1.2.2/textAngular-sanitize.min.js'></script>
	<script src='//cdnjs.cloudflare.com/ajax/libs/textAngular/1.2.2/textAngular.min.js'></script>




</div>
</body>
</html>
