$(function() {

  //Homepage image switch
  var totalDistance = 0;
  var lastSeenAt = {};


  $('.start-pic').on('mousemove', function(e) {

    if (lastSeenAt.x) {
      totalDistance += Math.sqrt(Math.pow(lastSeenAt.y - event.clientY, 2) + Math.pow(lastSeenAt.x - event.clientX, 2));

    }
    lastSeenAt.x = event.clientX;
    lastSeenAt.y = event.clientY;


    if (totalDistance > 60) {

      var curImg = $(this).find('img.active').removeClass('active');

      var nextImg = curImg.next();

      if (!nextImg.length) {
        nextImg = $(this).find('img:first-child')
      };

      nextImg.addClass('active');
      totalDistance = 0;
    }


  })

  if($(".featured-rings").length > 0) {
    $(".latest-item-desc").each(function(){
      
      var s = $(this).children(".title").html();
      var middle = Math.floor(s.length / 2);
      var before = s.lastIndexOf(' ', middle);
      var after = s.indexOf(' ', middle + 1);

      if (before < after) {
          middle = after;
      } else {
          middle = before;
      }
      $(this).children(".title").html(s.substr(0, middle) + '<br/>' + s.substr(middle + 1));
    });
  }


})