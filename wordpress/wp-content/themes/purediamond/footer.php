<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">

				<div class="footer-top">

				      <div class="container">
				      	<div class="col-sm-12">
							<div class="designers-link">
								<a href="/store/designers">
									<div><span>We are working with</span></div>
									<h1><span>Jewelry</span> Designers</h1>
								</a>
							</div>
						</div>
				        <div class="diamonds-shapes col-sm-12">

				            <h4>Diamonds Collections</h4>
				            <ul class="shapes">
								<li>
									<a href="/store/products/diamonds?shape=round" class="icon-round"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=princess" class="icon-princess"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=cushion" class="icon-cushion"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=asscher" class="icon-asscher"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=marquise" class="icon-marquise"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=oval" class="icon-oval"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=radiant" class="icon-radiant"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=pear" class="icon-pear"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=emerald" class="icon-emerald"></a>
								</li>
								<li>
									<a href="/store/products/diamonds?shape=heart" class="icon-heart"></a>
								</li>
							</ul>
				        </div>
				      </div>
				</div>




				<div class="footer-bottom">

					<div class="container">
						<div id="footer-menu" class="col-sm-12">
							<ul class="top-menu nav" ng-show="navpages">
								<li ng-repeat="navpage in navpages"><a href="/store/products/{{navpage.slug}}" ng-bind="navpage.menu_title"></a></li>
							</ul>
							<?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'nav top-menu', 'menu_id' => '') ); ?>
						</div>
						<div class="col-sm-3 nopadding">
							<div class="contacts">
								<div class="address">736 Granville Street, <br/>Vancouver, BC Canada.</div>
								<div class="phone"><a href="tel://1-855-736-2591"><i class="fa fa-phone"></i>+1 (855) 736-2591</a> <br/><a href="tel://1-604-563-9875">+1 (604) 563-9875</a></div>
								<div class="email"><a href="mailto:info@purediamond.ca"><i class="fa fa-envelope"></i>info@purediamond.ca</a></div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="footer-widget">
							<div class="fwidget col-sm-6">
							<div class="menu-education-container">
							<ul id="menu-education" class="menu">
							<li class="menu-item"><a href="/info/education/learn-about-metals/">Learn About Metals</a></li>
							<li class="menu-item"><a href="/info/education/diamond-certification/">Diamond Certification</a></li>
							<li class="menu-item"><a href="/info/education/diamond-symmetry-grading/">Diamond Polish and Symmetry</a></li>
							<li class="menu-item"><a href="/info/education/history-of-diamond-cuts/">History of Diamond Cuts</a></li>
							<li class="menu-item"><a href="/info/education/ideal-proportions-of-round-brilliant-cut-explained/">Ideal Proportions of Round Brilliant Cut Explained</a></li>
							<li class="menu-item"><a href="/info/education/the-4cs-of-diamonds/">The 4Cs of diamonds</a></li>
							<li class="menu-item"><a href="/info/education/conflict-free-diamonds/">Conflict Free Diamonds</a></li>
							<li class="menu-item"><a href="/info/education/documentation/">Documentation</a></li>
							</ul>
							</div>
							</div>
							<div class="fwidget col-sm-6">
							<div class="menu-customer-care-container">
							<ul id="menu-customer-care" class="menu">
							<li class="menu-item"><a href="/info/customer-care/100-risk-free-shopping/">100% Risk-Free Shopping</a></li>
							<li class="menu-item"><a href="/info/customer-care/30-day-return-policy/">30 Day Return Policy</a></li>
							<li class="menu-item"><a href="/info/customer-care/free-shipping/">Free shipping</a></li>
							<li class="menu-item"><a href="/info/customer-care/30-day-money-back-guarantee/">30 Day Money Back Guarantee</a></li>
							<li class="menu-item"><a href="/info/customer-care/lifetime-warranty/">Lifetime Warranty</a></li>
							<li class="menu-item"><a href="/info/customer-care/120-lifetime-diamond-upgrade/">Diamond Upgrade Promise</a></li>
							<li class="menu-item"><a href="/info/customer-care/wholesale-diamond-price/">Wholesale Diamond Price</a></li>
							<li class="menu-item"><a href="/info/customer-care/price-match/">Our Price Match Guarantee</a></li>
							<li class="menu-item"><a href="/info/customer-care/free-monthly-diamond-give-away/">Free Diamond Give Away</a></li>
							<li class="menu-item"><a href="/info/education/reviews/">Reviews</a></li>
							</ul>
							</div>
							</div>
							</div>
						</div>
						<div class="actions-col col-sm-3 nopadding">
							<div class="social-icons">
				        		<a href="https://www.facebook.com/pages/Pure-Diamond/566340603467508" target="_blank"><span class="icon-facebook-letter"></span></a>
				        		<a href="http://www.pinterest.com/canadadiamond/" target="_blank"><span class="icon-pinterest"></span></a>
				        		<a href="https://twitter.com/PureDiamond_ca" target="_blank"><span class="icon-twitter"></span></a>
				        		<a href="http://instagram.com/purediamond.ca" target="_blank"><span class="icon-instagram"></span></a>
										<a href="https://plus.google.com/u/0/+PureDiamondVancouver" target="_blank"><span class="fa fa-google-plus"></span></a>
										<a href="https://www.youtube.com/channel/UClxazhC2oBbr6g4PWdoZ81g" target="_blank"><span class="fa fa-youtube"></span></a>
				        	</div>
				        	<a textpopup data-mfp-src="#appointment-popup" href="" class="appointment-btn">Talk to a Gemologist</a>
				        </div>

						<div class="services col-sm-12">
							<div class="logo visa"><img src="/store-assets/img/logo-visa.png" alt=""></div>
							<div class="logo mastercard"><img src="/store-assets/img/logo-mastercard.png" alt=""></div>
							<div class="logo"><img src="/store-assets/img/logo-amexpress.png" alt=""></div>
							<div class="logo"><img src="/store-assets/img/logo-paypal.png" alt=""></div>
							<div class="logo"><img src="/store-assets/img/logo-bitcoin.png" alt=""></div>
							<div class="logo"><img src="/store-assets/img/logo-fedex.png" alt=""></div>
							<div class="logo ags"><img src="/store-assets/img/logo-ags.png" alt=""></div>
							<div class="logo"><img src="/store-assets/img/logo-gia.png" alt=""></div>
							<div class="logo canrocks"><img src="/store-assets/img/logo-canrocks.png" alt=""></div>
							<div class="logo lights"><img src="/store-assets/img/logo-lights.png" alt=""></div>
				        </div>
				        <div class="copyright col-sm-12">
				          <span>&copy; <?php echo date("Y"); ?> Pure Diamond</span>
				        </div>

			        </div>

			        	<!-- <div><span><a href="http://www.fancycolors.ca" class="fancy-link" target="_blank">webdesign &amp; development <br/> by www.fancycolors.ca</a></span></div> -->

				</div>


			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->



	<?php wp_footer(); ?>


	<div class="mfp-hide text-popup" id="shipping-popup">
		<h2>FREE SHIPPING</h2>

		<p>PUREDIAMOND.CA offers complimentary shipping on all jewelry shipped in the CANADA or the U.S. via FedEx® Priority Overnight.</p>
		<p>You'll take comfort in knowing precisely what day your order will arrive. If you wish, you can also follow every step it takes along the way with detailed order tracking.</p>

		<a href="/customer-care/free-shipping/">Learn More</a>
	</div>

	<div class="mfp-hide text-popup" id="returns-popup">
		<h2>30 Day Return Policy</h2>
		<p>The purchase of a diamond can be one of the most difficult decisions you'll ever have to make. That's why purediamond.ca gives you 30 days to return your diamond for a full refund or exchange without charging a restocking fee. No questions asked! If for any reason you want to return your diamond, you've got thirty full days to decide. This 30-day return policy is for the center diamond only, not side stones, three-stone rings or mountings. The center diamond must be in excellent condition, free from breakage and chipping, and be accompanied by certification documents.</p>
		<p>If certification documents are lost or you are not able to provide them, $200 will be deducted from your return amount to cover recertification of the diamond. *In order to give our customers the best pricing, we do not allow comparison purchasing. Comparison purchasing is the act of ordering two or more of the same, or similar item, and then only keeping one of them. If you are unsure, or have detailed questions about a particular item, please call one of our customer service representatives for expert help. *Our 30-day return policy applies to purchases up to $29,000</p>

	</div>


	<div class="mfp-hide text-popup" id="appointment-popup">
		<form ng-submit="talkToSubmit()" role="form" enctype="multipart/form-data" ng-hide="talkToForm.success">

				<h2>Schedule an appointment</h2>
				<div class="row">
				<div class="col-sm-6">
				<div>
					<label for="sched-name">Your Name <span>(required)</span></label><br>
				    <input type="text" name="name" ng-model="talkToForm.name" value="" size="40" id="sched-name" aria-required="true" aria-invalid="false">
				</div>
				<div>
					<label for="sched-phone">Your phone</label><br>
				    <input type="text" name="phone" ng-model="talkToForm.phone" value="" size="40" id="sched-phone" aria-invalid="false">
				</div>
				</div>
				<div class="col-sm-6">
				<div>
				    <label for="sched-email">Your Email <span>(required)</span></label><br>
				    <input type="email" name="email" ng-model="talkToForm.email" value="" size="40" id="sched-email" aria-required="true" aria-invalid="false">
				</div>				
				</div>
				<div class="col-sm-12">
				    <label for="sched-message">Additional notes</label><br>
				    <textarea name="notes" cols="40" rows="3" ng-model="talkToForm.notes" id="sched-message" aria-invalid="false"></textarea>
					<div class="col-sm-5 nopadding"><input type="submit" value="Send"></div>
					<div class="col-sm-7 note">
						<p>Please feel free to call 1 (855) 736-2591 or 604-563-9875.</p>
						<p>A Pure Diamond specialist will be happy to answer any questions you may have.</p>
					</div>
				</div>
				</div>
				<div class="form-response"></div>
				</form>
				<div class="form-success" ng-show="talkToForm.success">
					<h3>Thank you!</h3>
					<h4>We will get in touch with you shortly.</h4>
				</div>
	</div>


	<div class="mfp-hide text-popup" id="share-popup">
		<h2 class="centered">Share it</h2>

		<div class="centered share-big">
			<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{share.url}}" id="share-fb" class="icon-facebook"></a>

			<a target="_blank" href="http://twitter.com/intent/tweet?text={{share.text}}&url={{share.url}}" id="share-tw" class="icon-twitter"></a>

			<a target="_blank" href="https://pinterest.com/pin/create/button/?url={{share.url}}&media={{share.media}}&description={{share.text}}" id="share-pin" class="icon-pinterest"></a>

		</div>
	</div>



	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
	$.src='//v2.zopim.com/?2UDEnJrWAJt1oXu1onwyldjRUVZWCc2X';z.t=+new Date;$.
	type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
	</script>
	<!--End of Zopim Live Chat Script-->
</body>
</html>
