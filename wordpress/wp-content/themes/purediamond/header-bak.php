<?php if ( ! isset( $_SESSION ) ) session_start(); ?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php elegant_titles(); ?></title>
	<?php elegant_description(); ?>
	<?php elegant_keywords(); ?>
	<?php elegant_canonical(); ?>

	<?php do_action( 'et_head_meta' ); ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php $template_directory_uri = get_template_directory_uri(); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
	<![endif]-->

	

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>


	<!-- Store scripts and styles -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css"> 
  <!-- bootstrap fallback -->
  <!-- <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"> -->
  <link rel="stylesheet" href="/store-assets/css/magnific-popup.css" type="text/css">
  <link rel="stylesheet" href="/store-assets/css/icheck-skin/blue.css" type="text/css">
  <link rel="stylesheet" href="/store-assets/css/owlcarousel/owl.carousel.css" type="text/css">
  <link rel="stylesheet" href="/store-assets/css/owlcarousel/owl.theme.css" type="text/css">
  <link rel="stylesheet" href="/store-assets/css/owlcarousel/owl.transitions.css" type="text/css">
  <link rel="stylesheet" href="/store-assets/css/icomoon/style.css?v=2" type="text/css">
  <link rel="stylesheet" href="/store-assets/css/custom.css" type="text/css">
  <link rel="stylesheet" href="/store-assets/css/angular-slider.css" type="text/css">
  <link rel="stylesheet" href="/store-assets/css/select.min.css" type="text/css">




	<script src="//cdn.ravenjs.com/1.1.16/angular/raven.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular-route.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular-sanitize.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular-touch.min.js"></script>
	<script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/spin.js/2.0.1/spin.min.js"></script>
	<script src="/store-assets/js/magnific-popup.js"></script>
	<script src="/store-assets/js/icheck.js"></script>
	<script src="/store-assets/js/owl.carousel.min.js"></script>




	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script>
	    Stripe.setPublishableKey('pk_test_L2MJPmWTlisaxqIgct0iHuXZ');
	</script>

	<!-- bootstrap fallback -->
	<!--<script src="/store-assets/js/bootstrap.min.js"></script>-->

	<!-- Loading the app's main library -->
	<script src="/store-assets/app/app.js?v=1"></script>

	<!-- Loading controllers -->
	<script src="/store-assets/app/controllers/appController.js"></script>
	<script src="/store-assets/app/controllers/stoneSearchController.js"></script>
	<script src="/store-assets/app/controllers/stoneDetailsController.js"></script>
	<script src="/store-assets/app/controllers/ringSearchController.js"></script>
	<script src="/store-assets/app/controllers/ringDetailsController.js"></script>
	<script src="/store-assets/app/controllers/shippingDetailsController.js"></script>
	<script src="/store-assets/app/controllers/checkoutController.js"></script>
	<script src="/store-assets/app/controllers/paymentDetailsController.js"></script>
	<script src="/store-assets/app/controllers/homepageController.js"></script>
	<script src="/store-assets/app/controllers/designersController.js"></script>


	<!-- Loading services -->
	<script src="/store-assets/app/services/diamondsFactory.js"></script>
	<script src="/store-assets/app/services/authFactory.js"></script>
	<script src="/store-assets/app/services/ordersFactory.js"></script>
	<script src="/store-assets/app/services/ringsFactory.js"></script>
	<script src="/store-assets/app/services/designersFactory.js"></script>
	<script src="/store-assets/app/services/cacheFactory.js"></script>

	<!-- Loading other components -->
	<script src="/store-assets/app/directives.js"></script>
	<script src="/store-assets/app/filters.js"></script>
	<script src="/store-assets/js/utils.js"></script>
	<script src="/store-assets/js/rangeInputSupported.js"></script>
	<script src="/store-assets/js/angular-slider.min.js"></script>
	<script src="/store-assets/js/angular-payments.min.js"></script>
	<script src="/store-assets/js/select.min.js"></script>



	<!-- Store scripts and styles end -->



	
	<link rel="stylesheet" href="/store-assets/css/pd-global.css?v=2">

	<!-- Typekit font. Proxima nova -->
  <script src="//use.typekit.net/mwz1igx.js"></script>
  <script>try{Typekit.load();}catch(e){}</script>


</head>
<body <?php body_class(); ?> data-ng-app="pureDiamondWebApp" data-ng-controller="appController">
	<div id="page-container">
<?php
	if ( is_page_template( 'page-template-blank.php' ) ) {
		return;
	}

	$et_secondary_nav_items = et_divi_get_top_nav_items();

	$et_phone_number = $et_secondary_nav_items->phone_number;

	$et_email = $et_secondary_nav_items->email;

	$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;

	$show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;

	$et_secondary_nav = $et_secondary_nav_items->secondary_nav;

	$primary_nav_class = 'et_nav_text_color_' . et_get_option( 'primary_nav_text_color', 'dark' );

	$secondary_nav_class = 'et_nav_text_color_' . et_get_option( 'secondary_nav_text_color', 'light' );

	$et_top_info_defined = $et_secondary_nav_items->top_info_defined;
?>

	<?php if ( $et_top_info_defined ) : ?>
		<div id="top-header" class="<?php echo esc_attr( $secondary_nav_class ); ?>">
			<div class="container clearfix">

			<?php if ( $et_contact_info_defined ) : ?>

				<div id="et-info">
				<?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
					<span id="et-info-phone"><?php echo esc_html( $et_phone_number ); ?></span>
				<?php endif; ?>

				<?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
					<a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
				<?php endif; ?>

				<?php
				if ( true === $show_header_social_icons ) {
					get_template_part( 'includes/social_icons', 'header' );
				} ?>
				</div> <!-- #et-info -->

			<?php endif; // true === $et_contact_info_defined ?>

				<div id="et-secondary-menu">
				<?php
					if ( ! $et_contact_info_defined && true === $show_header_social_icons ) {
						get_template_part( 'includes/social_icons', 'header' );
					} else if ( $et_contact_info_defined && true === $show_header_social_icons ) {
						ob_start();

						get_template_part( 'includes/social_icons', 'header' );

						$duplicate_social_icons = ob_get_contents();

						ob_end_clean();

						printf(
							'<div class="et_duplicate_social_icons">
								%1$s
							</div>',
							$duplicate_social_icons
						);
					}

					if ( '' !== $et_secondary_nav ) {
						echo $et_secondary_nav;
					}

					et_show_cart_total();
				?>
				</div> <!-- #et-secondary-menu -->

			</div> <!-- .container -->
		</div> <!-- #top-header -->
	<?php endif; // true ==== $et_top_info_defined ?>

		<header id="main-header" class="site-header <?php echo esc_attr( $primary_nav_class ); ?>">
			<div class="container clearfix">
			<?php
				$logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo
					? $user_logo
					: $template_directory_uri . '/images/logo.png';
			?>
				<div class="site-logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<!-- <img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo" /> -->
					</a>
				</div>

				<?php if (is_front_page()) { ?>
				<h1 class="header-subtitle"><?php the_field('main_subtitle'); ?></h1>
				<?php } ?>

				<div id="et-top-navigation">
					<nav id="top-menu-nav">
					<?php
						$menuClass = 'nav';
						if ( 'on' == et_get_option( 'divi_disable_toptier' ) ) $menuClass .= ' et_disable_top_tier';
						$primaryNav = '';

						$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false ) );

						if ( '' == $primaryNav ) :
					?>
						<ul id="top-menu" class="<?php echo esc_attr( $menuClass ); ?>">
							<?php if ( 'on' == et_get_option( 'divi_home_link' ) ) { ?>
								<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>
							<?php }; ?>

							<?php show_page_menu( $menuClass, false, false ); ?>
							<?php show_categories_menu( $menuClass, false ); ?>
						</ul>
					<?php
						else :
							echo( $primaryNav );
						endif;
					?>
					</nav>

					<?php
					if ( ! $et_top_info_defined ) {
						et_show_cart_total( array(
							'no_text' => true,
						) );
					}
					?>

					<?php if ( false !== et_get_option( 'show_search_icon', true ) ) : ?>
					<div id="et_top_search">
						<span id="et_search_icon"></span>
						<form role="search" method="get" class="et-search-form et-hidden" action="<?php echo esc_url( home_url( '/' ) ); ?>">
						<?php
							printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
								esc_attr_x( 'Search &hellip;', 'placeholder', 'Divi' ),
								get_search_query(),
								esc_attr_x( 'Search for:', 'label', 'Divi' )
							);
						?>
						</form>
					</div>
					<?php endif; // true === et_get_option( 'show_search_icon', false ) ?>

					<div class="social-icons">
						<a href="https://www.facebook.com/pages/Pure-Diamond/566340603467508" target="_blank"><span class="icon-facebook-letter"></span></a>

		        		<a href="http://www.pinterest.com/canadadiamond/" target="_blank"><span class="icon-pinterest"></span></a>

		        		<a href="https://twitter.com/PureDiamond_ca" target="_blank"><span class="icon-twitter"></span></a>

		        		<a href="http://instagram.com/purediamond.ca" target="_blank"><span class="icon-instagram"></span></a>
					</div>

					<?php do_action( 'et_header_top' ); ?>
				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
		</header> <!-- #main-header -->

		<div id="et-main-area">