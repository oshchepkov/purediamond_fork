<?php if ( ! isset( $_SESSION ) ) session_start(); ?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php elegant_titles(); ?></title>
	<?php elegant_description(); ?>
	
	<meta name="keywords" content="Diamond Ring, Wholesale diamond engagement rings, Diamond solitaire engagement rings, Diamond engagement rings settings, Affordable diamond engagement rings, Diamond rings for engagement, Diamond Earrings Vancouver, Awesome diamonds, Perfect Diamond engagement ring, Best diamond Engagement ring, Engagement ring Vancouver, Diamond ring Vancouver, Round brilliant cut diamonds, Princess cut diamonds, Her diamond ring in Vancouver, GIA triple excellent diamonds Vancouver, AGS certified Diamonds in Vancouver, How to buy a diamond engagement ring, Cheap diamond engagement rings, Why give diamond engagement rings, Black Diamond Engagement ring Vancouver, Halo Diamond engagement ring Vancouver, Solitaire diamond engagement ring Vancouver, Single stone classic diamond engagement ring Vancouver, Awesome diamonds in Vancouver, Stunning diamond engagement ring in Vancouver, Love diamonds, Diamonds are the girls best friends, Synthetic diamonds, Emerald cut diamond engagement ring Vancouver, Where do I buy an engagement ring and not get ripped off, 10K 14K 18K Rose Gold diamond engagement ring in Vancouver, 10K 14K 18K yellow gold diamond engagement in Vancouver, 10K 14K 18K White gold diamond engagement ring in Vancouver"/>

	<?php //do_action( 'et_head_meta' ); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta property="og:url" content=""/>
	<meta property="og:title" content=""/>
	<meta property="og:description" content=""/>
	<meta property="og:image" content=""/>

	<meta property="fb:pages" content="566340603467508" />

	

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php $template_directory_uri = get_template_directory_uri(); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
	<![endif]-->



	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php //wp_head(); ?>


  	<!-- <script src="//cdn.ravenjs.com/1.1.16/angular/raven.min.js"></script> -->

	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
  	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-route.min.js"></script>
  	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-sanitize.min.js"></script>
  	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-touch.min.js"></script>
	<script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.0.1.js"></script>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.19.1/select.min.js"></script>

	<!-- Loading other components -->
	<script src="/store-assets/js/utils.js"></script>	
	<script src="/store-assets/js/rangeInputSupported.js"></script>
	<script src="/store-assets/js/angular-slider.min.js"></script>
	<script src="/store-assets/js/angular-payments.min.js"></script>
	<script src="/store-assets/app.js"></script>



	<!-- Store scripts and styles end -->

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" type="text/css">
	<link rel="stylesheet" href="/store-assets/css/icomoon/style.css?v=2" type="text/css">
  	<link rel="stylesheet" href="/store-assets/css/fa/css/font-awesome.min.css" type="text/css">
	<link href="/store-assets/css/pd-global.css?v=2" media="all" rel="stylesheet" type="text/css">

	<!-- Typekit font. Proxima nova -->
  <!-- <script src="//use.typekit.net/mwz1igx.js"></script>
  <script>try{Typekit.load();}catch(e){}</script> -->


</head>
<body <?php body_class(); ?> data-ng-app="pureDiamondWebApp" data-ng-controller="appController" ngCloak>
	<div id="page-container">

		<header id="main-header" class="site-header <?php echo esc_attr( $primary_nav_class ); ?>">
			<div class="container clearfix">

				<div class="navi-box">
					<div class="site-logo">
						<a href="/">
							<!-- <img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo" /> -->
						</a>
					</div>
					<a href="#" class="nav-btn" menutoggle><i class="fa fa-bars" aria-hidden="true"></i></a>
					<div class="shopping-bag-status visible-xs">
						<a ng-href="/store/checkout"><i class="fa fa-shopping-bag"></i><span ng-bind="productsCount"></span></a>
					</div>

					<nav id="top-menu-nav">
						<ul class="top-menu nav" ng-show="navpages">
							<li ng-repeat="navpage in navpages"><a href="/store/products/{{navpage.slug}}" ng-bind="navpage.menu_title"></a></li>
						</ul>
					<?php //wp_nav_menu(array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'nav top-menu', 'menu_id' => '')); ?>
						<ul id="menu-main-nav" class="nav top-menu">
							<li class="menu-item has-subnav">
								<a href="" ng-click="j_subnav=!j_subnav; d_subnav=false">Jewellery</a>
								<ul class="main-nav-subnav" ng-class="{active: j_subnav}">
									<li class="menu-item"><a href="/store/products/rings">Rings</a></li>
									<li class="menu-item"><a href="/store/products/pendants">Pendants</a></li>
									<li class="menu-item"><a href="/store/products/earrings">Earrings</a></li>
									<li class="menu-item"><a href="/store/products/wedding-bands">Wedding Bands</a></li>
								</ul>
							</li>
							<li class="menu-item has-subnav">
								<a href="" ng-click="d_subnav=!d_subnav; j_subnav=false">Diamonds</a>
								<ul class="main-nav-subnav" ng-class="{active: d_subnav}">
									<li class="menu-item"><a href="/store/products/diamonds" target="_self">Inventory</a></li>
									<li class="menu-item"><a href="/store/products/specials" target="_self">Specials</a></li>
								</ul>
							</li>
							<li class="menu-item"><a href="/info/about-us">Why Us</a></li>
							<li class="menu-item"><a href="/info/virtual-tour">Virtual Tour</a></li>
							<li class="menu-item"><a href="/info/blog">Blog</a></li>
						</ul>
						<div class="social-icons">
							<a href="https://www.facebook.com/pages/Pure-Diamond/566340603467508" target="_blank"><span class="icon-facebook-letter"></span></a>
			        		<a href="http://www.pinterest.com/canadadiamond/" target="_blank"><span class="icon-pinterest"></span></a>
			        		<a href="https://twitter.com/PureDiamond_ca" target="_blank"><span class="icon-twitter"></span></a>
			        		<a href="http://instagram.com/purediamond.ca" target="_blank"><span class="icon-instagram"></span></a>
									<a href="https://plus.google.com/u/0/+PureDiamondVancouver" target="_blank"><span class="fa fa-google-plus"></span></a>
									<a href="https://www.youtube.com/channel/UClxazhC2oBbr6g4PWdoZ81g" target="_blank"><span class="fa fa-youtube"></span></a>
						</div>
					</nav>
				</div>

				<div class="info-block">
					<div class="social-icons">
								<a href="https://www.facebook.com/pages/Pure-Diamond/566340603467508" target="_blank"><span class="icon-facebook-letter"></span></a>
		        		<a href="http://www.pinterest.com/canadadiamond/" target="_blank"><span class="icon-pinterest"></span></a>
		        		<a href="https://twitter.com/PureDiamond_ca" target="_blank"><span class="icon-twitter"></span></a>
		        		<a href="http://instagram.com/purediamond.ca" target="_blank"><span class="icon-instagram"></span></a>
								<a href="https://plus.google.com/u/0/+PureDiamondVancouver" target="_blank"><span class="fa fa-google-plus"></span></a>
								<a href="https://www.youtube.com/channel/UClxazhC2oBbr6g4PWdoZ81g" target="_blank"><span class="fa fa-youtube"></span></a>
					</div>
					<div class="contacts">
						<div class="phone"><i class="fa fa-phone"></i><a href="tel://1-604-563-9875">+1 (604) 563-9875</a></div>
						<div class="email"><a href="mailto:info@purediamond.ca"><i class="fa fa-envelope"></i>info@purediamond.ca</a></div>
					</div>

					<div class="shopping-bag-status hidden-xs">
						<a ng-href="/store/checkout"><i class="fa fa-shopping-bag"></i>Shopping Bag <span ng-bind="countToShow" ng-show="countToShow"></span></a>
						<!-- <a href="/store/signin" class="signin-btn"><i class="fa fa-sign-in" aria-hidden="true"></i>Sign In</a> -->
					</div>

				</div>
					<?php //do_action( 'et_header_top' ); ?>
			</div> <!-- .container -->
		</header> <!-- #main-header -->

		<div id="et-main-area">
