<?php get_header(); ?>

<div id="main-content"   ng-cloak>

  <div class="order-panel" ng-show="showOrderPanel">
    <div id="order-block">
      <div class="order-items">
  
        <!-- Stone -->
        <div class="item" ng-show="orderForCurrentSession.stone">
          <div class="pic" ng-show="orderForCurrentSession.stone.shape_class_name"><img ng-src="/store-assets/pics/shapes/{{orderForCurrentSession.stone.shape_class_name}}.jpg" alt=""></div>
          <div class="desc">
            <h4>{{orderForCurrentSession.stone.shape}} Diamond</h4>
            <div>Price: <span class="price">CAD {{orderForCurrentSession.stone.price | number:2}}</span></div>
            <div>
              <a ng-href="#/create-ring/diamond/{{orderForCurrentSession.stone.stock_number}}" class="button white">View</a>
              <a ng-href="#/create-ring/diamond-search" class="button white">Change</a>
            </div>
          </div>
        </div>
  
        <!-- Ring -->
        <div class="item" ng-show="orderForCurrentSession.ring">
          <div class="pic"><img ng-src="{{orderForCurrentSession.ring.images[0].url}}" alt=""/></div>
          <div class="desc">
            <h4>{{orderForCurrentSession.ring.title}}</h4>
            <div>Price: 
              <span class="price" ng-show="orderForCurrentSession.ring.discount == 0">
                CAD {{orderForCurrentSession.ring.price | number:2}}
              </span>
              <span class="price" ng-show="orderForCurrentSession.ring.discount > 0">
                <s>CAD {{orderForCurrentSession.ring.price | number:2}}</s>
                <span>CAD {{orderForCurrentSession.ring.price - orderForCurrentSession.ring.discount | number:2}}</span>
              </span>
            </div>

            <div>  
              <a ng-href="#/create-ring/ring/{{orderForCurrentSession.ring.id}}" class="button white">View</a>
              <a ng-href="#/create-ring/rings" class="button white">Change</a>
            </div>
          </div>
        </div>
  
        <div class="sum">
          <div class="subtotal">
            <span class="left">Subtotal</span>
            <span class="right">{{orderForCurrentSession.subtotal | number:2}}</span>
          </div>
  
          <div class="tax">
            <span class="left">Tax</span>
            <span class="right">{{orderForCurrentSession.tax | number:2}}</span>
          </div>
  
          <div class="before" ng-show="orderForCurrentSession.ring && orderForCurrentSession.ring.discount > 0">
            <span class="left">Price before discount</span>
            <span class="right">{{orderForCurrentSession.subtotal+orderForCurrentSession.tax+orderForCurrentSession.ring.discount | number:2}}</span>
          </div>
  
          <div class="total">
            <span class="left">Your price</span>
            <span class="right">{{orderForCurrentSession.total | number:2}}</span>
          </div>
        </div>
  
      </div>
  
      <!-- <div class="step1 hide">
        <span>Step 1</span>
        <a href=""><span class="underline">Search for a diamond</span></a>
      </div>
      <div class="step2 hide">
        <span>Step 2</span>
        <a href=""><span class="underline">Search for a ring</span></a>
      </div> -->
  
      <a ng-href="#checkout" class="place-order-btn button">Place order</a>
    </div>
  </div>
  <!-- end of Order view -->

  <div data-ng-view keep-scroll-pos id="page-content" class="row nomargin"></div>


</div> <!-- #main-content -->

<?php get_footer(); ?>