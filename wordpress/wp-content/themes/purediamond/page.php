<?php

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">



  <?php if (!is_front_page()): ?>
    <?php if (has_post_thumbnail()) : ?>
      <div class="header-pic">
        <?php the_post_thumbnail(); ?>
      </div>
    <?php endif; ?>


    <div class="header-title">
      <h1><span><?php the_title(); ?></span></h1>
      <div class="colored-border two-colors"><span class="blue-line"></span></div>
    </div>
  <?php endif; ?>




<?php if ( ! $is_page_builder_used ) : ?>

  <div class="container">
    <div id="content-area" class="col-sm-12">
      <div id="left-area">

<?php endif; ?>

      <?php if (is_front_page()) { ?>

        <div id="start" class="section row">
          <div class="col-xs-6 left-block">
            <div class="start-content">
              <a href="/store/#!/products/diamonds" class="start-label">Start with a diamond</a>
              <div class="start-pic">
                <img class="active" src="/store-assets/pics/home-slider/stone1.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/stone2.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/stone3.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/stone4.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/stone5.jpg" alt=""/>
              </div>
            </div>
            <!-- <div class="start-desc">
                <h2><?php the_field('diamond_section_title'); ?></h2>
                <?php the_field('diamond_section_text'); ?>
            </div> -->
          </div>
          <div class="col-xs-6 right-block">
            <div class="start-content">
              <a href="/store/#!/products/rings" class="start-label">Start with a setting</a>
              <div class="start-pic">
                <img class="active" src="/store-assets/pics/home-slider/ring2.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/ring3.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/ring4.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/ring5.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/ring6.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/ring10.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/ring11.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/ring12.jpg" alt=""/>
                <img src="/store-assets/pics/home-slider/ring13.jpg" alt=""/>
              </div>

            </div>
            <!-- <div class="start-desc">
                <h2><?php the_field('ring_section_title'); ?></h2>
                <?php the_field('ring_section_text'); ?>
            </div> -->
          </div>
        </div>


        <?php

        $resp = json_decode(file_get_contents('http://'.$_SERVER['HTTP_HOST'].'/api/rings/featured'));

        ?>

        <script type="text/javascript">
        $(document).ready(function() {
          $("#carousel").owlCarousel({
            stagePadding: 100,
            autoplay: true,
            items:5,
            loop: true,
            autoplaySpeed: 1000,
            center: true,
            responsive: {
              0: { items: 1, stagePadding: 100},
              480: {items: 3, stagePadding: 50},
              768: {items: 3, stagePadding: 100},
              1024: {items: 5}
            }
          });
        });
        </script>


        <!-- <div class="section-strip row nomargin"><div class="col-xs-6 left-strip"></div><div class="col-xs-6 right-strip"></div></div> -->
        <hr class="featured-border">
        <div id="latest" class="section row nomargin product-list">

          <wrap-owlcarousel class="owl-carousel featured-rings" id="carousel">

              <?php foreach ($resp->rings as $product) { ?>
              <div class="latest-item product-list-item">
                <a href="/store/#!/products/<?php echo $product->category ?>/<?php echo $product->id ?>">
                  <div class="price">
                    <?php if($product->discount == 0) { ?>
                    <span>C$</span> <?php echo $product->price ?>
                    <?php } else { ?>
                    <span>C$</span> <?php echo ($product->price - $product->discount) ?>
                    <?php } ?>
                  </div>
                  <div class="latest-item-pic product-list-item-pic"><img src="<?php echo $product->images[0]->url ?>" alt=""/></div>
                  <div class="latest-item-desc product-list-item-desc">
                    <div class="title"><?php echo $product->title ?></div>
                  </div>
                </a>
              </div>
              <?php } ?>

          </wrap-owlcarousel>


        </div>
        <hr class="featured-border">

        <div id="warranties" class="section row">
          <div class="col-sm-2 col-sm-offset-1">
              <span><a href="/customer-care/100-risk-free-shopping">Risk Free<br/>shopping</a></span>
          </div>
          <div class="col-sm-2">
              <span><a href="/customer-care/lifetime-warranty">Life time<br/>warranty</a></span>
          </div>
          <div class="col-sm-2">
              <span><a href="/customer-care/30-day-return-policy">30-day<br/>return policy</a></span>
          </div>
          <div class="col-sm-2">
              <span><a href="/customer-care/30-day-money-back-guarantee">100%<br/>full refund</a></span>
          </div>
          <div class="col-sm-2">
              <span><a href="/customer-care/wholesale-diamond-price">Whole sale<br/>Diamond price</a></span>
          </div>

        </div>


        <div id="bottom-cta" class="section row">

          <?php
            // $pid = 119;
            // $cta = &get_page($pid);
            // setup_postdata( $cta, null, false );
            // the_content();
            // wp_reset_postdata();
          ?>
          <div class="col-sm-4"><a href="#"><img src="/store-assets/img/cta-bridal.jpg" alt=""></a></div>

          <div class="col-sm-4"><a href="#"><img src="/store-assets/img/cta-pop-rings.jpg" alt=""></a></div>

          <div class="col-sm-4"><a href="#"><img src="/store-assets/img/cta-collections.jpg" alt=""></a></div>

        </div>


      <?php } else { ?>

      <?php while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php if ( ! $is_page_builder_used ) : ?>

          <!-- <h1 class="main_title"><?php the_title(); ?></h1> -->
        <?php
          $thumb = '';

          $width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

          $height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
          $classtext = 'et_featured_image';
          $titletext = get_the_title();
          $thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
          $thumb = $thumbnail["thumb"];

          if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
            print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
        ?>

        <?php endif; ?>

          <div class="entry-content">
          <?php
            the_content();

            if ( ! $is_page_builder_used )
              wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
          ?>
          </div> <!-- .entry-content -->

        <?php
          if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
        ?>

        </article> <!-- .et_pb_post -->

      <?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

      </div> <!-- #left-area -->

      <?php get_sidebar(); ?>
    </div> <!-- #content-area -->
  </div> <!-- .container -->

<?php endif; } ?>

</div> <!-- #main-content -->

<?php get_footer(); ?>
