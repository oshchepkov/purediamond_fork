<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('WP_CACHE', true); //Added by WP-Cache Manager
//define( 'WPCACHEHOME', '/srv/www/purediamond.ca/wordpress/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'purediamond_wp');

/** MySQL database username */
define('DB_USER', 'purediamond_wp');

/** MySQL database password */
define('DB_PASSWORD', 'Rsty1320o9');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


//FOR DEV ONLY
define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/info/');
define('WP_HOME',    'http://' . $_SERVER['HTTP_HOST'] . '/info/');

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O}CuKuTJC=L.!eCu pbV)t::@!fF>Cpy_zlC#Q*^%`+^.q.-v*Dr *Rh78RSV_Ud');
define('SECURE_AUTH_KEY',  'l hv4D:e_n/1R)@=pk]]Xa_cDCD`vs|J+{}<aK!<IcSTjVa%fqW*<,~-{GG2seTr');
define('LOGGED_IN_KEY',    '!(41E(a`7uV<FXZL1i~$zO#wK7-8*+%ZK})@G|X]etm9YWKb?)d_iW/.y/%<8nB#');
define('NONCE_KEY',        'Amh#P79m,.kR|SMr!-a#zn[|G3s+BttHZwDP!@&DALS!i_stJ3Or}SCD)n;NUd$G');
define('AUTH_SALT',        'nu{Kgl|we,/>H=TZNRCb-wr+8VIQR2~-i$2x+#gwdU^_^-(; 080ZmG4((Yzc$|~');
define('SECURE_AUTH_SALT', '$.(DO4h#maWV<?[FhEP?+e>:.eDmQ,U|A~z[`.&JuFT}}0G1i+2RJ_(/s|9U&#3C');
define('LOGGED_IN_SALT',   'Zo$2_-N;NNJTJGO]vWREa>EdGiCt-llm#%:AQ2^UiG5}/HOUu]aQq+sQB(<+VN*4');
define('NONCE_SALT',       'zxkH99o^F;3F-=Q-kJ,j5V54[owF]7/p&ZJ/}|Yqs~R)-kF&`Reh<%|r_-{K@Vw?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
