

$(function() {


    $('.slider').slider()
    .on('slide', function(ev){

    })
    .on('slideStop', function(ev){

    });

    $('.slider-range').slider()
  	.on('slide', function(ev){
    
      var labels = $(this).parents().siblings('.labels');
  		$(labels).find('.min-val.float').html(ev.value[0].toFixed(1));  		
      $(labels).find('.max-val.float').html(ev.value[1].toFixed(1)); 
      $(labels).find('.min-val.int').html(ev.value[0]);      
      $(labels).find('.max-val.int').html(ev.value[1]); 

      $(labels).find('.mark').each(function(index) {
        if(index>=ev.value[0] && index<ev.value[1]) {
          $(labels).find('.mark:eq('+index+')').addClass('selected');
        } else {
          $(labels).find('.mark:eq('+index+')').removeClass('selected');
        }
      }); 
      
      

  	})
    .on('slideStop', function(ev){

    });

    
    $('.shapes li a').click(function(e) {
    	e.preventDefault();
    	$('.shapes li.selected').removeClass('selected');
    	$(this).parents('li').addClass('selected');
    });

    var orderBlock = $('#order-block').offset().top; 
    var orderBlockHeight = $('#order-block').height()+50; 
    var mainBlockHeight = $('#buy-process').height();
    var limit = mainBlockHeight-orderBlockHeight;
    

    $(window).scroll(function() {       
      var orderBlockOffset = $('#order-block').offset().top;
      var windowTop = $(window).scrollTop();
      

      if (orderBlock < windowTop){
        if(windowTop < limit) {
        	$('#order-block').css({ position: 'fixed', top: 0 });
        } else {
	        $('#order-block').css({position: 'absolute', top: limit});
	      }
      } else {
      	$('#order-block').css('position', 'relative');
      }
      
 
    });

    $(".search-toggle").on("click", function(e){
      e.preventDefault();
      $(this).parents('#stones-search').toggleClass('basic advanced');
      if($(this).hasClass('advanced')) {
        $(this).removeClass('advanced').children('span').html('Advanced search');
      } else { $(this).addClass('advanced').children('span').html('Basic search'); }
    });

    $(".sort-price").on("click", function(e){
      e.preventDefault();
      $(this).toggleClass('down up');
    });
    $(".checkbox").on("click", function(e){
      e.preventDefault();
      $(this).toggleClass('checked');
    });

});
