$(function() {
  
  var uploader = new plupload.Uploader({
  runtimes : 'html5,flash,silverlight,html4',
  
  browse_button : 'pickfiles', // you can pass in id...
  container: document.getElementById('container'), // ... or DOM Element itself
  
  url : "/fileupload/"+$("#files-folder").val(),
  
  filters : {
    max_file_size : '20mb',
    mime_types: [ { extensions: file_upload_exts } ]
    // mime_types: [
    //   {title : "Image files", extensions : "jpg,gif,png"},
    //   {title : "Zip files", extensions : "zip"}
    // ]
  },

  // Flash settings
  flash_swf_url : '/plupload/js/Moxie.swf',

  // Silverlight settings
  silverlight_xap_url : '/plupload/js/Moxie.xap',
  

  init: {
    PostInit: function() {
      document.getElementById('filelist').innerHTML = '';

      // document.getElementById('uploadfiles').onclick = function() {
      //   uploader.start();
      //   return false;
      // };

    },

    FilesAdded: function(up, files) {
      plupload.each(files, function(file) {
        document.getElementById('filelist').innerHTML = '<div id="' + file.id + '">' + file.name + '&nbsp;<b></b></div>';
      });
      uploader.start();
    },

    UploadProgress: function(up, file) {
      document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
    },

    FileUploaded: function(up, file, data) {
      $("#files-uploaded-field").val('1');
      if(data.response) {
        $("#filelist").hide();
        $("#filelist").parents("form").find(".file-error").html(data.response);
      } else {
        $("#filelist").html(file.name);
      }
      
    },

    Error: function(up, err) {
      document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
    }
  }
});

uploader.init();

});