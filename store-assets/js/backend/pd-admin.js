$(function() {

    $(".addnew-btn").on("click", function(){
      if($("#addnew-box").hasClass("active")) { $("#addnew-box").slideUp(100).removeClass("active"); }
      else { 
        $("#addnew-box input[type=text]").val('');
        $("#addnew-box input[type=file]").val('');
        $("#addnew-box .file-input-name").html('');
        $("#addnew-box").slideDown(100).addClass("active"); 
      }
    });
    $(".cancel-btn").on("click", function(e){
      e.preventDefault();
      $("#addnew-box").slideUp(100).removeClass("active");
    });
    $(".submit-btn").on("click", function(e){
      e.preventDefault();
      $form = $(this).parents("form");
      $.post("", $form.serialize(), false, "json")
      .done(function(data){
        if(data.errors){
          $.each(data.errors, function(index, value){
            $form.find("."+index).html(value).fadeIn(200);
          });
        } else {
          if($("#to-list").val()) {
            $("."+$("#to-list").val()).prepend(data.content);
          }
        }
      });
    });
    $("input[type=text], textarea").keypress(function(){
      $(this).parents(".form-group").find(".error").fadeOut(200);
    });

    function showList(url) {
      $(".data-list").load(url, function() {
        alert( "Load was performed." );
      });
    }

    

    $('.data-table-column-filter').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "/admin/stones"
    } );

});
