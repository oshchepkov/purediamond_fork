/**
 * Directives
 */

/*app.directive('sortedColumn', function( $timeout ) {
	return function( scope, elem, attrs ) {
	    scope.$watch(attrs.sortedBy, function(val) {
	    	if (val === attrs.columnName){
	    		if (scope.reverse){
	    			elem.removeClass('glyphicon glyphicon-arrow-down');
	    			elem.addClass('glyphicon glyphicon-arrow-up');
	    		}else{
	    			elem.removeClass('glyphicon glyphicon-arrow-up');
	    			elem.addClass('glyphicon glyphicon-arrow-down');
	    		}
	    	}else{
	    		elem.removeClass('glyphicon glyphicon-arrow-up');
	    		elem.removeClass('glyphicon glyphicon-arrow-down');
	    	}
	    });
	  };
	});*/


app.directive('mySpinner', function() {
	var spinner = new Spinner(spinnerOptions);
	return function( scope, elem, attrs ) {
		scope.$watch(attrs.isLoading, function(val) {
			if (val == true){
				spinner.spin();
				elem.append(spinner.el);
			}else{
				spinner.stop();
			}
		});
	};
});

/**
* Create a new module for icheck so that it can be injected into an existing
* angular program easily.
*/
angular.module('ui.check', [])
.directive('icheck', function ($timeout, $parse) {
	return {
		require: 'ngModel',
		link: function($scope, element, $attrs, ngModel) {
			return $timeout(function() {
				var value;
				value = $attrs['value'];

				$scope.$watch($attrs['ngModel'], function(newValue){
					$(element).iCheck('update');
				})

				return $(element).iCheck({
					checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					labelHover: true

				}).on('ifChanged', function(event) {
					if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
						$scope.$apply(function() {
							return ngModel.$setViewValue(event.target.checked);
						});
					}
					if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
						return $scope.$apply(function() {
							return ngModel.$setViewValue(value);
						});
					}
				});
			});
		}
	};
});

//Atomatically focus and select text inside the input
app.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {
      scope.$watch(attrs.focusMe, function(value) {
        if(value === true) {
          $timeout(function() {
            element[0].focus();
            element[0].select();
            scope.trigger = false;
          });
        }
      });
    }
  };
});


//For inputs to block non-numeric characters
app.directive('numeric', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^0-9.]/g, '');
        if(transformedInput !== text) {
          ngModelCtrl.$setViewValue(transformedInput);
          ngModelCtrl.$render();
        }
        return transformedInput;  // or return Number(transformedInput)
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
});


app.directive('wrapOwlcarousel', function ($timeout) {
  return {
    restrict: 'AEC',
    link: function (scope, element, attrs) {
      var options = {
        stagePadding: 100,
        autoplay: true,
        items:5,
        loop: true,
        lazyLoad : true,
        autoplaySpeed: 1000,
        center: true,
        responsive: {
          0: { items: 1, stagePadding: 100},
          480: {items: 4, stagePadding: 50},
          768: {items: 4, stagePadding: 100},
          1024: {items: 5}
        }
      };

      $timeout(function(){
        $(element).owlCarousel(options).on('loaded.owl.lazy', function(event){
          event.element.parents('.product-list-item').show();
        })
      })
    }
  };
});


app.directive('lightbox', function ($timeout) {
  return {
    restrict: 'AEC',

    link: function (scope, element, attrs) {
      $(element).magnificPopup({
        type:'image', 
        verticalFit: true,
        gallery: { enabled: true },
        mainClass: 'mfp-fade'
      });

      if (scope.$last) {
        $('.lightbox').magnificPopup({type:'image', gallery: { enabled: true }});
      };

    }
  };
});


app.directive('textpopup', function ($timeout) {
  return {
    restrict: 'AEC',

    link: function (scope, element, attrs) {
      $(element).magnificPopup({
        type: 'inline'
      });
    }
  };
});

app.directive('videopopup', function ($timeout) {
  return {
    restrict: 'AEC',
    link: function (scope, element, attrs) {
      $(element).magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
        iframe: {
          patterns: {
              youtube: {
                  index: 'youtube.com',
                  id: 'v=',
                  src: '//www.youtube.com/embed/%id%?autoplay=1'              
              }
          }
        }
      });
    }
  };
});



app.directive('sticky', ['$timeout', function($timeout){
  return {
    restrict: 'A',
    scope: {
      offset: '@',
      stickyClass: '@'
    },
    link: function($scope, $elem, $attrs){
      $timeout(function(){
        var offsetTop = $scope.offset || 0,
          stickyClass = $scope.stickyClass || '',
          $window = angular.element(window),
          doc = document.documentElement,
          initialPositionStyle = $elem.css('position'),
          stickyLine,
          scrollTop;


        // Set the top offset
        //
        $elem.css('top', offsetTop+'px');


        // Get the sticky line
        //
        function setInitial(){
          stickyLine = $elem[0].offsetTop - offsetTop;
          checkSticky();
        }

        // Check if the window has passed the sticky line
        //
        function checkSticky(){
          scrollTop = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

          if ( scrollTop >= stickyLine ){
            $elem.addClass(stickyClass);
            $elem.css('position', 'fixed');
          } else {
            $elem.removeClass(stickyClass);
            $elem.css('position', initialPositionStyle);
          }
        }


        // Handle the resize event
        //
        function resize(){
          $elem.css('position', initialPositionStyle);
          $timeout(setInitial);
        }


        // Attach our listeners
        //
        $window.on('scroll', checkSticky);
        $window.on('resize', resize);

        setInitial();
      });
    },
  };
}]);



app.directive("keepScrollPos", function($route, $window, $timeout, $location, $anchorScroll) {

    // cache scroll position of each route's templateUrl
    var scrollPosCache = {};

    // compile function
    return function(scope, element, attrs) {

        scope.$on('$routeChangeStart', function() {
            // store scroll position for the current view
            if ($route.current) {
                scrollPosCache[$route.current.loadedTemplateUrl] = [ $window.pageXOffset, $window.pageYOffset ];
            }
        });

        scope.$on('$routeChangeSuccess', function() {
            // if hash is specified explicitly, it trumps previously stored scroll position
            if ($location.hash()) {
                $anchorScroll();

            // else get previous scroll position; if none, scroll to the top of the page
            } else {
                var prevScrollPos = scrollPosCache[$route.current.loadedTemplateUrl] || [ 0, 0 ];
                $timeout(function() {
                    $window.scrollTo(prevScrollPos[0], prevScrollPos[1]);
                }, 1);
            }
        });
    }
});



app.directive('showpopup', function() {
    return {
      link: function($scope, $element, $attrs) {
        $element.bind('mouseenter', function() {
          $(".stone-popup").show();
          $(".stone-popup").css('top', $element[0].offsetTop);
        });
        $element.bind('mouseleave', function() {
          $(".stone-popup").hide();
        });
      }
    }
});


app.directive('menutoggle', function() {
    return {
      link: function($scope, $element, $attrs) {
        $element.on('click', function(e) {
          e.preventDefault();
          $('#top-menu-nav').toggleClass('active');
        });
      }
    }
});


app.directive('instagram', function($http) {
    return {
      link: function($scope, $element, $attrs) {
        $element.html($scope.ringDetails.instagram);
        instgrm.Embeds.process();
      }
    }
});
