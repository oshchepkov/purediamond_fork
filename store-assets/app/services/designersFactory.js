app.factory('designersFactory', [ '$http', function ($http) {
    var baseURL = "/api";
    var designersFactory = {
        getDesignersInventory: function () {
            var resp = $http.get(baseURL + "/designers/inventory")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    return response.data;
                });
            return resp;
        },
        getDesignerTemplate: function () {
            var resp = $http.get(baseURL + "/designers/inventory/designer")
                .error(function (data, status) {
                    alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
                })
                .then(function (response) {
                    return response.data;
                });
            return resp;
        }

     };
    return designersFactory;
}]);