app.factory('ringsFactory', [ '$http', function($http) {

	var baseURL = "/api";
	var ringsFactory = {
		getSearchCriteria : function() {
			var searchCriteria = $http.get(baseURL + "/rings/search")
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return searchCriteria;
		},
		searchRings : function(body) {
			var rings = $http.post(baseURL + "/rings/search", body)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return rings;
		},
		getFeaturedRings : function() {
			var rings = $http.get(baseURL + "/rings/featured")
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return rings;
		},
		getDesignerRings : function(designerSlug) {
			var rings = $http.get(baseURL + "/rings/designer", {params: {slug: designerSlug}})
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return rings;
		},
		getSaleRings : function() {
			var rings = $http.get(baseURL + "/rings/sale")
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return rings;
		},
		getRingById : function(params) {
			var ringDetails = $http.get(baseURL + "/rings/details?id="+params)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return ringDetails;
		}


	};
	return ringsFactory;
}]);
