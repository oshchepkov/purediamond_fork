app.factory('pagesFactory', [ '$http', function($http) {

	var baseURL = "/api";
	var pagesFactory = {

		getPageBySlug : function(pageSlug) {
			var page = $http.get(baseURL + "/pages/page", {params: {slug: pageSlug}})
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return page;
	},

		getAllPages : function() {
			var pages = $http.get(baseURL + "/pages")
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return pages;
		}

	};
	return pagesFactory;
}]);
