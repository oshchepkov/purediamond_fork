app.factory('ordersFactory', [ '$http', function($http) {
	//consothis module is responsible for providindle.log("diamondsFactory is initializing...");
	var baseURL = "/api";
	var ordersFactory = {
		getOrderForSession : function(body) {
			var order = $http.get(baseURL + "/orders/session")
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return order;
		},

		orderUpdate : function(body) {
			var order = $http.post(baseURL + "/orders/updateOrder", body)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return order;
		},

		orderGetProducts : function(productList) {
			var order = $http.post(baseURL + "/orders/getProducts", productList)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return order;
		},

		// orderStone : function(body) {
		// 	var order = $http.post(baseURL + "/orders/addStone", body)
		// 		.error(function(data, status) {
		// 			alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
		// 		})
		// 		.then(function(response) {
		// 			return response.data;
		// 		});
		// return order;
		// },
		// orderRing : function(body) {
		// 	var order = $http.post(baseURL + "/orders/addRing", body)
		// 		.error(function(data, status) {
		// 			alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
		// 		})
		// 		.then(function(response) {
		// 			return response.data;
		// 		});
		// return order;
		// },
		updateOrderOnCheckout : function(body) {
			var order = $http.put(baseURL + "/orders/checkout", body)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return order;
		},
		shippingDetails : function(body) {
			var order = $http.post(baseURL + "/orders/shippingDetails", body)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return order;
		},
		placeOrder : function(body) {
			var order = $http.post(baseURL + "/orders/placeOrder", body)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return order;
		}

	};
	return ordersFactory;
}]);
