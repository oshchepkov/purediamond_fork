app.factory('authFactory', [ '$http', function($http) {
	var baseURL = "/api";
	var authFactory = {
		login : function(credentials) {
			var user = $http.post(baseURL + "/auth/login",credentials)
			.error(function(response) {
				//alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				return response;
			})
			.then(function(response) {
					return response.data;
				});
			return user;
		},
		logout : function(credentials) {
			var user = $http.post(baseURL + "/auth/logout")
			.error(function(data, status) {
				alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
			})
			.then(function(response) {
					return response.data;
				});
			return user;
		},
		signUp : function(credentials) {
			var user = $http.post(baseURL + "/auth/signup",credentials)
			.error(function(response) {
				//alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				return response;
			})
			.then(function(response) {
					return response.data;
				});
			return user;
		},

	};
	return authFactory;
}]);