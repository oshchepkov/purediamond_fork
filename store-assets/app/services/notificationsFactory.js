app.factory('notificationsFactory', [ '$http', function($http) {
	var baseURL = "/api";
	var notificationsFactory = {
		toGemologist : function(form) {
			var message = $http.post(baseURL + "/notifications/togemologist",form)
			.error(function(response) {
				//alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				return response;
			})
			.then(function(response) {
					return response.data;
				});
			return message;
		}
	};
	return notificationsFactory;
}]);