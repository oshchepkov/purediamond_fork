app.factory('productsFactory', [ '$http', function($http) {

	var baseURL = "/api";
	var productsFactory = {
		getSearchCriteria : function() {
			var searchCriteria = $http.get(baseURL + "/rings/search")
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return searchCriteria;
		},
		searchRings : function(body) {
			var rings = $http.post(baseURL + "/products", body)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return rings;
		},
		getFeaturedRings : function() {
			var rings = $http.get(baseURL + "/rings/featured")
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return rings;
		},
		getDesignerRings : function(designerSlug) {
			var rings = $http.get(baseURL + "/rings/designer", {params: {slug: designerSlug}})
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return rings;
		},
		getSale : function() {
			var rings = $http.get(baseURL + "/products/sale")
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return rings;
		},
		getCategoryProducts : function(category) {
			var products = $http.get(baseURL + "/products/list", {params: {category: category}})
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});

		return products;
		},
		getRingById : function(params) {
			var ringDetails = $http.get(baseURL + "/products/details?id="+params)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return ringDetails;
		}


	};
	return productsFactory;
}]);
