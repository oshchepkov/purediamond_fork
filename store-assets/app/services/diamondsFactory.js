//this module is responsible for providing data for the Angular app from any kind of a resource.
//In this case it is communicating with REST API provided by our NodeJs app.
//Returns objects that are being consumed by the controllers.


app.factory('diamondsFactory', [ '$http', function($http) {
	//consothis module is responsible for providindle.log("diamondsFactory is initializing...");
	var baseURL = "/api";
	var diamondsFactory = {
		getSearchCriteria : function() {
			var searchCriteria = $http.get(baseURL + "/stones/search")
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return searchCriteria;
		},
		searchStones : function(body) {
			var stones = $http.post(baseURL + "/stones/search", body)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return stones;
		},
		getStoneByLotNumber : function(params) {
			var stoneDetails = $http.get(baseURL + "/stones/details?lot="+params)
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return stoneDetails;
		},
		getStonesByProperty : function(property) {
			var stones = $http.get(baseURL + "/stones/search/property", {params: {property: property}})
				.error(function(data, status) {
					alert("ERROR!!!\ndata: " + data + "\nstatus: " + status);
				})
				.then(function(response) {
					return response.data;
				});
		return stones;
		},
		
	};
	return diamondsFactory;
}]);
