//The entry point for our app. All routes are here

var app = angular.module("pureDiamondWebApp",['ngRoute','ui.bootstrap','vr.directives.slider', 'ngSanitize', 'angularPayments', 'ui.check', 'ui.select'])
	.config(
		function($routeProvider, $locationProvider, $provide) {
			$routeProvider
				.when('/products/diamonds',
					{
						templateUrl: '/store-assets/app/partials/stone_search.html?v=2',
						controller: "stoneSearchController",
						resolve: {
				            res: function ($q, $route, diamondsFactory) {
				            	var def = $q.defer();
				            	//Getting search criteria
								$route.current.params.category = 'diamonds';
				            	diamondsFactory.getSearchCriteria()
				            	.then(function (asyncData) {
									def.resolve(asyncData);
								});
				            	return def.promise;
				        	},
				        	shape: function($route) {
				        		return $route.current.params.shape;
				        	}

				        }
					})
				.when('/products/diamonds/:stoneLot',
					{
						templateUrl: "/store-assets/app/partials/stone_details.html?v=2",
						controller: "stoneDetailsController",
						resolve: {
				            res: function ($q, $route, diamondsFactory) {
				            	var def = $q.defer();
				            		diamondsFactory.getStoneByLotNumber($route.current.params.stoneLot)
				            			.then(function (asyncData) {
											def.resolve(asyncData);
										});
				            	return def.promise;
				        	}
				        }
					})
				.when('/products/:category',
					{
						templateUrl: "/store-assets/app/partials/ring_search.html?v=2",
						controller: "productsController",
						resolve: {
	            criteria: function ($q, productsFactory) {
	            	var def = $q.defer();
	            	productsFactory.getSearchCriteria()
	            	.then(function (asyncData) {
									def.resolve(asyncData);
								});
	            	return def.promise;
	        		},
							category: function($route) {
								var category = {category:$route.current.params.category};
								return category;
							},
							pageData: function($q, $route, pagesFactory) {
								var def = $q.defer();
								pagesFactory.getPageBySlug($route.current.params.category)
								.then(function (asyncData) {
									def.resolve(asyncData);
								});
	            	return def.promise;
							}
				    }
					})
				.when('/products/:category/:productId',
					{
						templateUrl: "/store-assets/app/partials/ring_details.html?v=2",
						controller: "ringDetailsController",
						resolve: {
				            ringById: function ($q, $route, productsFactory) {
				            	var def = $q.defer();
				            	productsFactory.getRingById($route.current.params.productId, $route.current.params.category)
				            	.then(function (asyncData) {
				            		def.resolve(asyncData);
								});
				            	return def.promise;
				        	}
				        }
					})
				.when('/collections/:slug',
					{
						templateUrl: "/store-assets/app/partials/ring_search.html?v=2",
						controller: "productsController",
						resolve: {
							criteria: function ($q, productsFactory) {
	            	var def = $q.defer();
	            	productsFactory.getSearchCriteria()
	            	.then(function (asyncData) {
									def.resolve(asyncData);
								});
	            	return def.promise;
	        		},
							category: function($route) {
								var category = {collection:$route.current.params.slug};
								return category;
							},
							pageData: function($q, $route, pagesFactory) {
								var def = $q.defer();
								pagesFactory.getPageBySlug($route.current.params.category)
								.then(function (asyncData) {
									def.resolve(asyncData);
								});
	            	return def.promise;
							}
				    }
					})
				.when('/designers',
					{
						templateUrl: "/store-assets/app/partials/designers.html?v=2",
						controller: "designersController"
					})
				.when('/designers/:slug',
					{
						templateUrl: "/store-assets/app/partials/designer-collection.html?v=2",
						controller: "designersController"

					})
				.when('/signin',
					{
						templateUrl: "/store-assets/app/partials/signin.html?v=2"
					})
				.when('/checkout',
					{
						templateUrl: "/store-assets/app/partials/checkout.html?v=2",
						controller: "checkoutController",
						resolve: {
				            orderForSession: function ($q, $route, ordersFactory) {
				            	var def = $q.defer();
											$route.current.params.category = 'shopping bag';
				            		ordersFactory.getOrderForSession()
				            			.then(function (asyncData) {
											def.resolve(asyncData);
										});
				            	return def.promise;
				        	}
				        }

					})
				.when('/shipping-details',
					{
						templateUrl: "/store-assets/app/partials/address_forms.html?v=2",
						controller: "shippingDetailsController",
						resolve: {
				            orderForSession: function ($q, $route, ordersFactory) {
				            	var def = $q.defer();
				            		ordersFactory.getOrderForSession()
				            			.then(function (asyncData) {
											def.resolve(asyncData);
										});
				            	return def.promise;
				        	}
				        }
					})
				.when('/payment-details',
					{
						templateUrl: "/store-assets/app/partials/payment_details.html?v=2",
						controller: "paymentDetailsController",
						resolve: {
				            orderForSession: function ($q, $route, ordersFactory) {
				            	var def = $q.defer();
				            		ordersFactory.getOrderForSession()
				            			.then(function (asyncData) {
											def.resolve(asyncData);
										});
				            	return def.promise;
				        	}
				        }
					})
				.when('/review-order',
					{
						templateUrl: "/store-assets/app/partials/review-order.html?v=2",
						controller: "paymentDetailsController",
						resolve: {
				            orderForSession: function ($q, $route, ordersFactory) {
				            	var def = $q.defer();
				            		ordersFactory.getOrderForSession()
				            			.then(function (asyncData) {
											def.resolve(asyncData);
										});
				            	return def.promise;
				        	}
				        }
					})
				
			// $locationProvider.html5Mode(true).hashPrefix('!');
			//$locationProvider.hashPrefix('!');
				//.otherwise({ redirectTo: '/create-ring/rings' });
    		//$locationProvider.html5 + '?v=' + Math.random() * 1000Mode(true);
    		//$locationProvider.hashPrefix('!');

    	// $provide.decorator('$uiViewScroll', function ($delegate) {
  	  //   return function (uiViewElement) {
  	  //     var top = uiViewElement.getBoundingClientRect().top;
  	  //     window.scrollTo(0, (top - 30));
  	  //   };
  	  // });


		}
	)
	.run(function ($rootScope, $window) {		
		$rootScope.$on('$routeChangeSuccess', function () {
			var title;
			var desc;
			var intrvl = setInterval(function () {
				if (document.readyState == 'complete') {
					var elem = document.createElement('div');
					var meta = document.querySelector('meta[name=description]');
					elem.innerHTML = $rootScope.pageDescription;
					desc = elem.innerText;
					elem.innerHTML = $rootScope.pageTitle;
					title = elem.innerText;
					if(title !== 'undefined') {
						document.title = title.trim().charAt(0).toUpperCase() + title.slice(1);						
						clearInterval(intrvl);
					}
					if(desc !== 'undefined') {
						meta.setAttribute('content', desc.trim());
						clearInterval(intrvl);
					}
				}
			}, 200);
		});
	});



