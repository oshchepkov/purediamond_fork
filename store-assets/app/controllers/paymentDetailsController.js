/**********************************************************************************************
 * paymentDetailsController
 *********************************************************************************************/
app.controller('paymentDetailsController', function($scope, $location, $http, ordersFactory, orderForSession) {

  $scope.orderForCurrentSession = orderForSession

  var goRoute = $scope.goRoute = function(route) {
    $location.url(route);
  };

  getOrderForSession();


  function getOrderForSession(){
    ordersFactory.getOrderForSession()
    .then(function (data) {
      $scope.orderForCurrentSession = data;
    });
  };

  $scope.handleStripe = function(status, response) {
    var $form = $('#payment-details');

    if (response.error) {
      // Show the errors on the form
      $form.find('.payment-errors').text(response.error.message);
      $form.find('button').prop('disabled', false);
    } else {
      // response contains id and card, which contains additional card details
      var token = response.id;
      // Insert the token into the form so it gets submitted to the server
      $form.append($('<input type="hidden" name="stripeToken" />').val(token));
      // and submit
      //$form.get(0).submit();


      //This method should be called after Review Order page.
      //We should save the token in session istead of charging right away

      $http.post('/api/stripe/purchase', {
        stripeToken: token,
        amount: $scope.orderForCurrentSession.total * 100
      }).success(function(data, status){
        $scope.paymentComplete = true;
      })
    }
  }

console.log($scope.orderForCurrentSession)

  var shipStone = 0;
  var shipRing = 0;
  if($scope.orderForCurrentSession.stone) {
      shipStone = 5;
  }
  if($scope.orderForCurrentSession.ring) {
      shipRing = 10;
  }

  var oDate = new Date();
  var rDate = new Date();
  var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
  var dayNames = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];

  rDate.setDate(oDate.getDate() + (shipStone + shipRing));

  $scope.orderDate = dayNames[oDate.getDay()] + ', ' + monthNames[oDate.getMonth()] + ' ' + oDate.getDate();
  $scope.receiveDate = dayNames[rDate.getDay()] + ', ' + monthNames[rDate.getMonth()] + ' ' + rDate.getDate();






}); //paymentDetailsController