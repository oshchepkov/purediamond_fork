
/**********************************************************************************************
 * ringDetailsController
 *********************************************************************************************/
 app.controller('checkoutController', function ($scope, $location, ordersFactory, orderForSession, $rootScope) {

    $scope.orderForCurrentSession = orderForSession;
    $rootScope.page = {title: 'Shopping Bag'};
    $scope.products = [];
    $scope.showEmpty = true;

    var goRoute = $scope.goRoute = function (route) {
        $location.url(route);
    };

    if(typeof $scope.orderForCurrentSession.products !='undefined' && $scope.orderForCurrentSession.products.length > 0) {
      $scope.showEmpty = false;
      bagBuilding();
      calcShipping();
    }

    function bagBuilding() {

      if($scope.orderForCurrentSession.extras) {
          var extras = JSON.parse($scope.orderForCurrentSession.extras)
          if(extras.metalPrice) {
              $scope.orderForCurrentSession.ring.price += extras.metalPrice
          }
      }

      //collections. might be also fetched from the server
      var ringSizeMin = 3;
      var ringSizeMax = 9;
      var ringSizeStep = 0.25;
      $scope.ringSizes = [];

      //Building sizes array
      $scope.ringSizes.push("Less than " + ringSizeMin);
      for (var i = ringSizeMin; i <= ringSizeMax; i+=ringSizeStep) {
          $scope.ringSizes.push(i);
      };
      $scope.ringSizes.push("Greater than " + ringSizeMax);

      $scope.checkoutDetails = {}

      ordersFactory.orderGetProducts($scope.orderForCurrentSession.products)
      .then(function (data) {
        var fullProducts = data;
        var products = [];
        angular.forEach($scope.orderForCurrentSession.products, function(value, key) {
          var prod = {};
          for(var e in fullProducts) {
              if(fullProducts[e].id === value.product_id && fullProducts[e].category === value.category) {
                prod.order_product_id = value.id;
                prod.size = value.size;
                prod.metal_id = value.metal_id;
                if(value.category === 'diamonds') {
                  prod.stone = true;
                  prod.title = fullProducts[e].shape + ' Diamond';
                }
                angular.extend(prod, fullProducts[e]);
                this.push(prod);
                break;
              }
            }
        }, products);

        $scope.products = products;
      });
    }

    function calcShipping() {

      var shipStone = 0;
      var shipRing = 0;

      for(var i in $scope.orderForCurrentSession.products) {
        if($scope.orderForCurrentSession.products[i].category === 'diamonds') { shipStone = 5; }
        else { shipRing = 10; }
      }

      var oDate = new Date();
      var rDate = new Date();
      var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
      var dayNames = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];

      rDate.setDate(oDate.getDate() + (shipStone + shipRing));

      $scope.orderDate = dayNames[oDate.getDay()] + ', ' + monthNames[oDate.getMonth()] + ' ' + oDate.getDate();
      $scope.receiveDate = dayNames[rDate.getDay()] + ', ' + monthNames[rDate.getMonth()] + ' ' + rDate.getDate();

    }



    $scope.onUpdateOrderItem = function(product) {
      product.update = true;
      console.log(product);
      ordersFactory.orderUpdate(product)
      .then(function (data) {
        $scope.orderForCurrentSession = data;
      });
    }

    $scope.onDeleteOrderItem = function(product, index) {
        var prod = {}
        product.remove = true;
        ordersFactory.orderUpdate(product)
        .then(function (data) {
          $scope.orderForCurrentSession = data;
          if(index > -1) {
            $scope.products.splice(index, 1);
          }
          $scope.bagUpdate($scope.orderForCurrentSession);
          calcShipping();
          if($scope.orderForCurrentSession.products < 1) {
            $scope.showEmpty = true;
          }
        });
        // if('stone' === item) {
        //     ordersFactory.orderStone(prod)
        //     .then(function (data) {
        //         $scope.orderForCurrentSession = data;
        //         $scope.bagUpdate($scope.orderForCurrentSession);
        //         console.log($scope.orderForCurrentSession);
        //         if(!$scope.orderForCurrentSession.ring) {
        //             goRoute('create-ring/diamond-search');
        //         }
        //     });
        // }
        // if('ring' === item) {
        //     ordersFactory.orderRing(prod)
        //     .then(function (data) {
        //         $scope.orderForCurrentSession = data;
        //         $scope.bagUpdate($scope.orderForCurrentSession);
        //         if(!$scope.orderForCurrentSession.stone) {
        //             goRoute('create-ring/diamond-search');
        //         }
        //     });
        // }
    }

    $scope.submit = function(){
        //1. submit ring size
        console.log($scope.orderForCurrentSession);
        if($scope.orderForCurrentSession.products.length) {
          ordersFactory.updateOrderOnCheckout($scope.orderForCurrentSession)
          .then(function (data) {
              $scope.orderForCurrentSession = data;
              goRoute('shipping-details');
          });
        }

    }

    if($scope.orderForCurrentSession.stone && $scope.orderForCurrentSession.ring) {
        $scope.shareURL = window.encodeURIComponent($location.protocol()+'://'+$location.host()+'/'+'sharer.php?ring='+$scope.orderForCurrentSession.ring.id);
    }

});//stoneDetailsController
