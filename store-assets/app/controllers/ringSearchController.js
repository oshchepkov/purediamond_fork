app.controller('ringSearchController', function ($scope, $timeout, pdCache, criteria, ringsFactory) {
	var searchCriteria = $scope.searchCriteria = criteria;
	$scope.searchCriteriaForRequest = angular.copy(searchCriteria);

    $scope.userSelectedMetals = [];
    $scope.userSelectedDesigners = [];
    $scope.userSelectedPriceRanges = [];
    $scope.userSelectedCollections = [];

	$scope.rings;
    var timeout = null;


    var cache = pdCache.get('cachedData');
    if (cache && cache.rings) {
        $scope.rings = cache.rings;
    } else {
        cache = {};
        sendSearchRequest(); 
    }
    


    var debounceRequest = function (newVal, oldVal) {
        if (newVal != oldVal) {
            if (timeout) {
                $timeout.cancel(timeout);
            }
            timeout = $timeout(sendSearchRequest, 1000); //timeout before the api call is fired
        }
    };

    function sendSearchRequest() {        
        $scope.startLoading();
        //set the user's selections
        
        setUserSelectionForSearch();
        ringsFactory.searchRings($scope.searchCriteriaForRequest)
            .then(function (data) {
                $scope.rings = data.rings;
                // cache.rings = data.rings;
                // pdCache.put('cachedData', cache);
                $scope.stopLoading();
                
            });
    };


    $scope.filterByObject = function(originalArray, selectedArray, object){
        selectedArray.length = 0;
        
        angular.forEach(originalArray, function(object, index) {
            if (object.selected) {
                selectedArray.push(object);
            };
        });

    }

    $scope.filter = {}
    $scope.filter.designers = {}
    $scope.filter.collections = {}

    $scope.ringsFilter = function(ring) {

        var matches = true;
        for (var prop in $scope.filter) {
           
            if(noFilter($scope.filter[prop])) continue;
            if(ring[prop].length > 1) {
                for (var opt in ring[prop]) {                
                    if (!$scope.filter[prop][ring[prop][opt].name]) {
                        matches = false;
                    } else {
                        matches = true;
                        break;
                    }
                } 
            } else {
                for (var opt in ring[prop]) {                
                    if (!$scope.filter[prop][ring[prop][opt].name]) {
                        matches = false;  
                        break;
                    }
                } 
            }
            if(!matches) { break; }

        }
        return matches;

    };
    function noFilter(filterObj) {
        for (var key in filterObj) {
            if (filterObj[key]) {
                // There is at least one checkbox checked
                return false;
            }
        }
        // No checkbox was found to be checked
        return true;
    }

    // $scope.$watch('userSelectedMetals', debounceRequest, true);
    // $scope.$watch('userSelectedDesigners', debounceRequest, true);
    // $scope.$watch('userSelectedCollections', debounceRequest, true);
    $scope.$watch('userSelectedPriceRanges', debounceRequest, true);

    //var setUserSelectionForSearch = function () {
    function setUserSelectionForSearch() {        
        $scope.searchCriteriaForRequest.metals = $scope.userSelectedMetals.length > 0 ? $scope.userSelectedMetals : searchCriteria.metals;
        $scope.searchCriteriaForRequest.designers = $scope.userSelectedDesigners.length > 0 ? $scope.userSelectedDesigners : searchCriteria.designers;
        $scope.searchCriteriaForRequest.collections = $scope.userSelectedCollections.length > 0 ? $scope.userSelectedCollections : searchCriteria.collections;
        $scope.searchCriteriaForRequest.priceRanges = $scope.userSelectedPriceRanges.length > 0 ? $scope.userSelectedPriceRanges : searchCriteria.priceRanges;
    }

    /*function init(){
        sendSearchRequest();
    }*/


});//controller