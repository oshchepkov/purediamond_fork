app.controller('stoneSearchController', function ($scope, $timeout, $filter, res, shape, diamondsFactory) {
    $scope.searchCriteria = res;

    //getting previous user selection (if exists) into a variable and clear it
    var userSelection = res.userSelection ? res.userSelection : $scope.searchCriteria;
    res.userSelection = null;

    //Uncomment to reset search Criteria
    //userSelection = $scope.searchCriteria;

    $scope.$root.pageTitle = 'Diamonds Inventory';

    $scope.searchCriteriaForRequest = angular.copy(res);
    $scope.stones;
    $scope.searchResultsCount;
    $scope.showResultsCount = userSelection.resultsPerPage;
    $scope.triggerResultsCount = false;
    $scope.keepResultsCount = false;
    //$scope.resultsPerPage = userSelection.resultsPerPage;
    var timeout = null;
    $scope.sortingOrder = userSelection.sortingOrder;
    $scope.sortingOrderDirection = userSelection.sortingOrderDirection;
    $scope.stopLoading();

    $scope.tableHeaders = ['shape', 'carat', 'color', 'clarity', 'cut', 'report', 'polish', 'price']


    setControls(userSelection);

    function setControls(userSelection) {

        //apply previous user selected shapes to current search
        if (shape) {

            $scope.userSelectedShapes = userSelection.shapes = $filter('filter')($scope.searchCriteria.shapes, {'class_name' : shape});

            //makeObjectsSelected($scope.searchCriteria.shapes, );

        } else {
            $scope.userSelectedShapes = userSelection.shapes;

        }

        $scope.userSelectedLabs = userSelection.labs;

        angular.forEach($scope.searchCriteria.shapes, function (object, index) {
                object.selected = false;
        });

        //make shapes selected based on a previous choice
        if($scope.searchCriteria.shapes.length != userSelection.shapes.length){ //do not select all of them
            makeObjectsSelected($scope.searchCriteria.shapes, userSelection.shapes);
        }


        if($scope.searchCriteria.labs.length != userSelection.labs.length){ //do not select all of them
            makeObjectsSelected($scope.searchCriteria.labs, userSelection.labs)
        }

        //Slider for a price range
        $scope.priceRangeSlider = {
            floor: 0,
            ceiling: 100,
            min: toSlider(userSelection.priceRange.min, $scope.searchCriteria.priceRange),
            max: toSlider(userSelection.priceRange.max, $scope.searchCriteria.priceRange),
            step: 0.00001,
            precision: 10,
            buffer: 1
        };

        $scope.priceRange = {
            min: userSelection.priceRange.min,
            max: userSelection.priceRange.max
        };
        $scope.priceRangeInput = {};
        $scope.priceRangeMinEdit = false;
        $scope.priceRangeMaxEdit = false;

        //Slider for a carat range
        $scope.caratRangeSlider = {
            floor: 0,
            ceiling: 100,
            min: userSelection.caratRange.min,
            max: userSelection.caratRange.max,
            min: toSlider(userSelection.caratRange.min, $scope.searchCriteria.caratRange),
            max: toSlider(userSelection.caratRange.max, $scope.searchCriteria.caratRange),
            step: 0.00001,
            precision: 10,
            buffer: 1
        };

        $scope.caratRange = {
            min: userSelection.caratRange.min,
            max: userSelection.caratRange.max
        };
        $scope.caratRangeInput = {};
        $scope.caratRangeMinEdit = false;
        $scope.caratRangeMaxEdit = false;


        //Slider for cuts
        $scope.cutSlider = {
            floor: 1,
            ceiling: $scope.searchCriteria.cuts.length + 1,
            min: getMinMax(userSelection.cuts).min,
            max: getMinMax(userSelection.cuts).max + 1,
            step: 1,
            precision: 1,
            buffer: 1,
            stickiness: 3,
            labelWidth: 100 / $scope.searchCriteria.cuts.length
        };

        //Slider for clarities
        $scope.claritySlider = {
            floor: 1,
            ceiling: $scope.searchCriteria.clarities.length + 1,
            min: getMinMax(userSelection.clarities).min,
            max: getMinMax(userSelection.clarities).max + 1,
            step: 1,
            precision: 1,
            buffer: 1,
            stickiness: 3,
            labelWidth: 100 / $scope.searchCriteria.clarities.length
        };

        //Slider for colors
        $scope.colorSlider = {
            floor: 1,
            ceiling: $scope.searchCriteria.colors.length + 1,
            min: getMinMax(userSelection.colors).min,
            max: getMinMax(userSelection.colors).max + 1,
            step: 1,
            precision: 1,
            buffer: 1,
            stickiness: 3,
            labelWidth: 100 / $scope.searchCriteria.colors.length
        };

    }


    var debounceRequest = function (newVal, oldVal) {
        if (newVal != oldVal) {
            if (timeout) {
                $timeout.cancel(timeout);
            }
            timeout = $timeout(sendStonesSearchRequest, 1000); //timeout before the api call is fired
        }
    };


    //initial request
    sendStonesSearchRequest()

    function sendStonesSearchRequest(criteria) {
        if(!criteria) { criteria = $scope.searchCriteriaForRequest }
        $scope.startLoading();
        //set the user's selections
        setUserSelectionForSearch();

        diamondsFactory.searchStones(criteria)
            .then(function (data) {
                $scope.stones = data.stones;
                $scope.searchResultsCount = data.searchResultsCount;
                if($scope.showResultsCount > $scope.searchResultsCount){
                    $scope.showResultsCount = $scope.searchResultsCount
                }
                //$scope.searchCriteriaForRequest = angular.copy(res); //restoring state of the request for future use
                $scope.stopLoading();
            });
    };


    $scope.$watch('cutSlider', debounceRequest, true);
    $scope.$watch('claritySlider', debounceRequest, true);
    $scope.$watch('colorSlider', debounceRequest, true);
    $scope.$watch('priceRange', debounceRequest, true);
    $scope.$watch('caratRange', debounceRequest, true);
    $scope.$watch('caratRangeSlider', debounceRequest, true);
    $scope.$watch('userSelectedShapes', debounceRequest, true);
    $scope.$watch('userSelectedLabs', debounceRequest, true);
    $scope.$watch('sortingOrder', debounceRequest, true);
    $scope.$watch('sortingOrderDirection', debounceRequest, true);
    $scope.$watch('triggerResultsCount', debounceRequest, true);

    $scope.$watch('priceRangeSlider', setPriceRange, true);
    $scope.$watch('caratRangeSlider', setCaratRange, true);


    function setUserSelectionForSearch() {
        //set price range
        $scope.searchCriteriaForRequest.priceRange.min = $scope.priceRange.min;
        $scope.searchCriteriaForRequest.priceRange.max = $scope.priceRange.max;
        //set carat range
        $scope.searchCriteriaForRequest.caratRange.min = $scope.caratRange.min;
        $scope.searchCriteriaForRequest.caratRange.max = $scope.caratRange.max;

        //set cut range
        $scope.searchCriteriaForRequest.cuts = []; //cleaning colors array before inserting user selection
        angular.forEach($scope.searchCriteria.cuts, function (object, index) {
            if (object.rank >= $scope.cutSlider.min && object.rank <= $scope.cutSlider.max - 1) {
                $scope.searchCriteriaForRequest.cuts.push(object);
            };
        });

        //set clarity range
        $scope.searchCriteriaForRequest.clarities = []; //cleaning colors array before inserting user selection
        angular.forEach($scope.searchCriteria.clarities, function (object, index) {
            if (object.rank >= $scope.claritySlider.min && object.rank <= $scope.claritySlider.max - 1) {
                $scope.searchCriteriaForRequest.clarities.push(object);
            };
        });

        //set color range
        $scope.searchCriteriaForRequest.colors = []; //cleaning colors array before inserting user selection
        angular.forEach($scope.searchCriteria.colors, function (object, index) {
            if (object.rank >= $scope.colorSlider.min && object.rank <= $scope.colorSlider.max - 1) {
                $scope.searchCriteriaForRequest.colors.push(object);
            };
        });

        $scope.searchCriteriaForRequest.shapes = $scope.userSelectedShapes.length > 0 ? $scope.userSelectedShapes : $scope.searchCriteria.shapes;
        $scope.searchCriteriaForRequest.labs = $scope.userSelectedLabs.length > 0 ? $scope.userSelectedLabs : $scope.searchCriteria.labs;

        $scope.searchCriteriaForRequest.sortingOrder = $scope.sortingOrder
        $scope.searchCriteriaForRequest.sortingOrderDirection = $scope.sortingOrderDirection


        if(!$scope.keepResultsCount){
            $scope.showResultsCount = userSelection.resultsPerPage
        }else{
            $scope.keepResultsCount = false
        }
        $scope.searchCriteriaForRequest.limitTo = $scope.showResultsCount
    }

    $scope.onStoneMouseOver = function(stone){
        $scope.stoneForDetailsPanel = stone;
    }

    $scope.onResetCriteriaClick = function(){
        sendStonesSearchRequest(res);
        setControls(res);
    }

    $scope.toggleSelectedShape = function(shape){
        $scope.userSelectedShapes = [];
        if (shape.selected) {
            shape.selected = false;
        } else {
            shape.selected = true;
        }

        angular.forEach($scope.searchCriteria.shapes, function(object, index) {
            if (object.selected) {
                $scope.userSelectedShapes.push(object);
            };
        });
    }

    function getIDs(array){
        var ids = []
        angular.forEach(array, function (object, index) {
            ids.push(object.id);
        });
        return ids
    }

    function getMinMax(array){
        var ids = getIDs(array)
        var minMax = {}
        minMax.max = Math.max.apply( Math, ids )
        minMax.min = Math.min.apply( Math, ids )
        return minMax
    }

    function makeObjectsSelected(target, array){
        var ids = getIDs(array)
        angular.forEach(target, function (object, index) {
            if (ids.indexOf(object.id) > -1){
                object.selected = true;
            }
        });
    }

    //Convert real value to slider
    function toSlider(value, criteria) {
        var cmin =  Math.log(criteria.min);
        var cmax =  Math.log(criteria.max);
        var scale = (cmax - cmin) / (100);
        return (Math.log(value) - cmin) / scale;
    }

    //Convert slider value to real
    function fromSlider(value, criteria) {
        var cmin =  Math.log(criteria.min);
        var cmax =  Math.log(criteria.max);
        var scale = (cmax - cmin) / (100);
        return Math.exp(cmin + scale * (value));
    }


    function setPriceRange() {
        $scope.priceRange.min = Math.round(fromSlider($scope.priceRangeSlider.min, $scope.searchCriteria.priceRange));
        $scope.priceRange.max = Math.round(fromSlider($scope.priceRangeSlider.max, $scope.searchCriteria.priceRange));
    }

    function setCaratRange() {
        $scope.caratRange.min = Number(fromSlider($scope.caratRangeSlider.min, $scope.searchCriteria.caratRange).toFixed(2));
        $scope.caratRange.max = Number(fromSlider($scope.caratRangeSlider.max, $scope.searchCriteria.caratRange).toFixed(2));
    }

    $scope.sortClick = function(value){
        if ($scope.sortingOrder == value && $scope.sortingOrderDirection == 'DESC'){
            $scope.sortingOrderDirection = 'ASC'
        }else{
            $scope.sortingOrderDirection = 'DESC'
        }
        $scope.sortingOrder = value
    }

    $scope.onLoadMoreClick = function(){
        $scope.keepResultsCount = true
        $scope.triggerResultsCount = !$scope.triggerResultsCount
        $scope.showResultsCount += userSelection.resultsPerPage;
        if ($scope.showResultsCount > $scope.searchResultsCount){
         //do not request more results than search count
         $scope.showResultsCount = $scope.searchResultsCount
        }

    }


    //Manually edit price
    $scope.editPriceRangeMin = function() {
        $scope.priceRangeMinEdit = true;
        $scope.priceRangeInput.min = $scope.priceRange.min;
    }

    $scope.editPriceRangeMax = function() {
        $scope.priceRangeMaxEdit = true;
        $scope.priceRangeInput.max = $scope.priceRange.max;
    }

    //Unfocus or press enter to submit price
    $scope.priceRangeMinSubmit = function(e) {
        if (e && e.which !== 13) {
            return false;
        };

        $scope.priceRangeMinEdit = false;

        if (!$scope.priceRangeInput.min) {
            $scope.priceRangeInput.min = $scope.searchCriteria.priceRange.min;
        };
        $scope.priceRangeSlider.min = toSlider($scope.priceRangeInput.min, $scope.searchCriteria.priceRange);
    }

    $scope.priceRangeMaxSubmit = function(e) {
        if (e && e.which !== 13) {
            return false;
        };
        $scope.priceRangeMaxEdit = false;

        if (!$scope.priceRangeInput.max) {
            $scope.priceRangeInput.max = $scope.searchCriteria.priceRange.max;
        };
        $scope.priceRangeSlider.max = toSlider($scope.priceRangeInput.max, $scope.searchCriteria.priceRange);
    }

    $scope.editCaratRangeMin = function() {
        $scope.caratRangeMinEdit = true;
        $scope.caratRangeInput.min = $scope.caratRange.min;
    }

    $scope.editCaratRangeMax = function() {
        $scope.caratRangeMaxEdit = true;
        $scope.caratRangeInput.max = $scope.caratRange.max;
    }

    $scope.caratRangeMinSubmit = function(e) {
        if (e && e.which !== 13) {
            return false;
        };
        $scope.caratRangeMinEdit = false;

        if (!$scope.caratRangeInput.min) {
            $scope.caratRangeInput.min = $scope.searchCriteria.caratRange.min;
        };
        $scope.caratRangeSlider.min = toSlider($scope.caratRangeInput.min, $scope.searchCriteria.caratRange);
    }

    $scope.caratRangeMaxSubmit = function(e) {
        if (e && e.which !== 13) {
            return false;
        };
        $scope.caratRangeMaxEdit = false;

        if (!$scope.caratRangeInput.max) {
            $scope.caratRangeInput.max = $scope.searchCriteria.caratRange.max;
        };
        $scope.caratRangeSlider.max = toSlider($scope.caratRangeInput.max, $scope.searchCriteria.caratRange);
    }


    var shipStone = 5;

    var oDate = new Date();
    var rDate = new Date();
    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var dayNames = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];

    rDate.setDate(oDate.getDate() + shipStone);

    $scope.orderDate = dayNames[oDate.getDay()] + ', ' + monthNames[oDate.getMonth()] + ' ' + oDate.getDate();
    $scope.receiveDate = dayNames[rDate.getDay()] + ', ' + monthNames[rDate.getMonth()] + ' ' + rDate.getDate();


});
