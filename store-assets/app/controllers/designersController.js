app.controller('designersController', function ($scope, $rootScope, designersFactory, ringsFactory, $route) {

    
    $rootScope.page = {title: 'Jewelry designers'};

    $rootScope.$on('$routeChangeSuccess', function() {
      $rootScope.page.title = 'Jewelry designers';
      $scope.page.title = 'Jewelry designers';
      if($scope.designerRings) {
        $scope.page.subtitle = $scope.designerRings[0].designers[0].name;
      }
    });

    if($route.current.params.slug) {
      getDesignerRings();
    } else {
      getDesigners();
    }

    function getDesigners() {
        designersFactory.getDesignersInventory()
        .then(function (data) {
          if (data.errors.length > 0){
            console.log(data.errors);
          }else{
            $scope.designers = data.designers;
          }
        });
    }; 

    function getDesignerRings() {
        ringsFactory.getDesignerRings($route.current.params.slug)
        .then(function (data) {
          if (data.errors.length > 0){
            console.log(data.errors);
          }else{            
            $scope.designerRings = data.rings;
            $rootScope.page.subtitle = $scope.designerRings[0].designers[0].name;
          }          
        });
    };


});//controller