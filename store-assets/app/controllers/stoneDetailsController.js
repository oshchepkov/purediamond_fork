/**********************************************************************************************
 * stoneDetailsController
 *********************************************************************************************/
app.controller('stoneDetailsController', function($scope, $sce, $location, $rootScope, res, diamondsFactory, ordersFactory) {
  $scope.stoneDetails = res.stone;
  $scope.stoneDetails.category = 'diamonds';
  $scope.searchCriteria;
  $scope.stoneDetails.reportLink = false;
  $scope.stoneDetails.primaryPic = false;
  $scope.stoneDetails.otherImages = $scope.stoneDetails.images;
  getSearchCriteria();

  if($scope.stoneDetails.youtube) {
    $scope.stoneDetails.youtubeEmbed = $sce.trustAsResourceUrl('https://www.youtube.com/embed/'+$scope.stoneDetails.youtube.match(/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i)[1]);
  }
  


  $scope.$root.pageTitle = $scope.stoneDetails.weight + ' Carat ' + $scope.stoneDetails.color + '-' + $scope.stoneDetails.clarity + ' ' + $scope.stoneDetails.cut + ' Cut ' + $scope.stoneDetails.shape + ' Diamond';
  // $scope.$root.pageDescription = ringDetails.description;
  var primePic = 0;
  for(var i in $scope.stoneDetails.images) {
    if($scope.stoneDetails.images[i].favorite != false) {
      $scope.stoneDetails.primaryPic = $scope.stoneDetails.images[i];
      primePic = i;
    } else {
      $scope.stoneDetails.primaryPic = $scope.stoneDetails.images[0];
    }
  }
  $scope.stoneDetails.otherImages.splice(primePic, 1);

  function getSearchCriteria() {
    diamondsFactory.getSearchCriteria()
      .then(function(data) {
        $scope.searchCriteria = data;
      });
  };

  $scope.stoneDetails.reportLink = $scope.stoneDetails.cert_link;

  $rootScope.share = {
      url: encodeURIComponent(window.location.href),      //Will change it to a html snapshot url
      text: encodeURIComponent('Check out this diamond'),
      media: encodeURIComponent(window.location.origin + '/store-assets/pics/shapes/' + $scope.stoneDetails.shape_class_name + '.jpg'),
      fb: encodeURIComponent($location.protocol()+'://'+$location.host()+'/'+'sharer.php?stone='+$scope.stoneDetails.id)
  }

  

  //look for it in appController
  /*$scope.orderStoneClick = function(){
		ordersFactory.orderStone($scope.stoneDetails)
		.then(function (data) {
		});

	};*/


}); //stoneDetailsController
