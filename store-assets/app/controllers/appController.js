//All global logic is here. Order clicks etc.

app.controller('appController', function ($scope, $rootScope, $location, $uibModal, $route, $routeParams, authFactory, ordersFactory, pagesFactory, notificationsFactory) {


    getOrderForSession();
    $scope.orderForCurrentSession;

    $scope.$on('$routeChangeSuccess', function() {
      var category = $routeParams.category;
      if(category) {
        $scope.page = {title:category.replace('-', ' ')};
        $scope.category = category;
        $scope.subnav = false;
      }
    });


    $scope.startLoading = function () {
        $scope.isLoading = true;
    }
    $scope.stopLoading = function () {
        $scope.isLoading = false;
    }

    //get custom pages for navigations
    pagesFactory.getAllPages()
    .then(function (data) {
      $scope.navpages = data.pages;
    });


    //Order Panel visibility
    // $scope.$on('$routeChangeSuccess', function() {

    //   var path = $location.path();
    //   $scope.showOrderPanel = false;

    //   if(path.substring(0,7) === '/create') {
    //      $scope.showOrderPanel = true;
    //   }
    // });




    var goRoute = $scope.goRoute = function (route) {
        $location.url(route);
    };



    $scope.bagUpdate = function(order) {
      $scope.productsCount = 0;
      $scope.countToShow = false;
      if(order) {
        $scope.productsCount = order.products.length;
      } else if($scope.orderForCurrentSession.products) {
        $scope.productsCount = $scope.orderForCurrentSession.products.length;
      }
      if($scope.productsCount > 0) {
        $scope.countToShow = '['+$scope.productsCount+']';
      }
    }



    function getOrderForSession(){
      ordersFactory.getOrderForSession()
      .then(function (data) {
        if(data.ring && 'additionalPrice' in data.ring) {
          data.ring.price += data.ring.additionalPrice
        }
        $scope.orderForCurrentSession = data;
        $scope.bagUpdate(data);
      });
    };

    // $scope.orderStoneClick = function(stoneDetails){
    //   ordersFactory.orderStone(stoneDetails)
    //   .then(function (data) {
    //     $scope.orderForCurrentSession = data;
    //     $scope.bagUpdate(data);
    //     if($scope.orderForCurrentSession.ring) {
    //       goRoute('/checkout');
    //     } else {
    //       goRoute('/create-ring/rings');
    //     }
    //   });
    // };

    // $scope.orderRingClick = function(ringDetails){
    //   ordersFactory.orderRing(ringDetails)
    //   .then(function (data) {
    //     data.ring.price = ringDetails.price;
    //     data.ring.additionalPrice = ringDetails.additionalPrice;
    //     $scope.orderForCurrentSession = data;
    //     $scope.bagUpdate(data);
    //     if($scope.orderForCurrentSession.stone) {
    //       goRoute('/checkout');
    //     } else {
    //       goRoute('/create-ring/diamond-search');
    //     }
    //   });
    // };

    $scope.orderAddProduct = function(productDetails){
      if(!productDetails.size) { productDetails.size = 0; }
      if(!productDetails.metal_id) { productDetails.metal_id = 0; }
      ordersFactory.orderUpdate(productDetails)
      .then(function (data) {
        // data.ring.price = ringDetails.price;
        // data.ring.additionalPrice = ringDetails.additionalPrice;
        $scope.orderForCurrentSession = data;
        $scope.bagUpdate(data);

      });
    };

    $scope.onShippingDetailsDone = function(shippingDetails){
      ordersFactory.addShipping(shippingDetails)
      .then(function (data) {
        $scope.orderForCurrentSession = data;
        goRoute('payment-details');
      });
    };




  //
    $scope.userCredentials = {
      username: '',
      password: ''
    };
    $scope.onLogoutClick = function(){
      authFactory.logout()
          .then(function (data) {
          });
    }

    $scope.onLoginClick = function () {

        authFactory.login($scope.userCredentials)
            .then(function (data) {
              if(data.err) {
                $scope.signInErr = data.err.message;
              }
            });

    };

    $scope.signupCredentials = {
      firstName: '',
      lastName: '',
      email: '',
      password: ''
    };

    $scope.onSignUpClick = function () {

        authFactory.signUp($scope.signupCredentials)
            .then(function (data) {
              if(data.err) {
                $scope.signUpErr = data.err.message;
              }
            });

    };

    $scope.talkToForm = {};
    $scope.talkToSubmit = function () {
      if(!$scope.talkToForm.name || !$scope.talkToForm.email) {

      } else {
        notificationsFactory.toGemologist($scope.talkToForm)
        .then(function (data) {
          if(data.err) {
            //$scope.signInErr = data.err.message;
          }
          if(data === 'OK') {            
            $scope.talkToForm.success = true;
          }
        });
      }
    };

    $scope.fileSelected = function(element){
        $scope.talkToForm.file = new FormData();
        $scope.talkToForm.file.append('file', element.files[0]); 
        $scope.$apply();
    }


    //$scope.startLoading();
});//appController


var ModalInstanceCtrl = function ($scope, $uibModalInstance) {

  $scope.login = function (result) {
    $modalInstance.close(result);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};
