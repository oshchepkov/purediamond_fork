
/**********************************************************************************************
 * ringDetailsController
 *********************************************************************************************/
app.controller('shippingDetailsController', function ($scope, $location, $rootScope, ordersFactory, orderForSession) {
    
    $scope.orderForCurrentSession = orderForSession
    $rootScope.page = {title: 'Shopping Bag'};
    var goRoute = $scope.goRoute = function (route) {
        $location.url(route);
    };


    //collections. might be also fetched from the server
    // $scope.countries = ['Canada','United States','Mexico']
    // $scope.provinces = ['Alberta','British Columbia','California','Ontario','New York'] //??? to be separated to provinces/states ???

    // $scope.address = {}
    // $scope.address.billing = {}
    // $scope.address.shipping = {}

    // $scope.orderForm = {
    //     name: null,
    //     phone: null,
    //     email: null
    // }

    //////////DELETE////////////
    // $scope.address.billing.firstName = "John"
    // $scope.address.billing.lastName = "Doe"
    // $scope.address.billing.addressLine1 = "125-4569 Elm St."
    // $scope.address.billing.addressLine2 = "2nd floor"
    // $scope.address.billing.country = "Canada"
    // $scope.address.billing.province = "British Columbia"
    // $scope.address.billing.city = "Vancouver"
    // $scope.address.billing.postalCode = "V6G1H8"
    // $scope.address.billing.phone = "(555)555-55-55"
    // $scope.address.billing.email = "b@a.com"
    // $scope.address.billing.confirmEmail = "b@a.com"
    // $scope.address.billing.subscribe = true
    // $scope.address.billing.shippingSameAsBilling = false//true

    // $scope.address.shipping.firstName = "Angelina"
    // $scope.address.shipping.lastName = "Jolie"
    // $scope.address.shipping.addressLine1 = "5-233 Maple St."
    // $scope.address.shipping.addressLine2 = "5nd floor"
    // $scope.address.shipping.country = "Canada"
    // $scope.address.shipping.province = "British Columbia"
    // $scope.address.shipping.city = "Burnaby"
    // $scope.address.shipping.postalCode = "V1G1D0"
    // $scope.address.shipping.phone = "(666)666-77-88"
    
    
    //////////////////////


    // $scope.shippingChange = function() {
    //     if($scope.address.billing.shippingSameAsBilling) {
    //         $scope.address.shipping.firstName = $scope.address.billing.firstName;
    //         $scope.address.shipping.lastName = $scope.address.billing.lastName;
    //         $scope.address.shipping.addressLine1 = $scope.address.billing.addressLine1;
    //         $scope.address.shipping.addressLine2 = $scope.address.billing.addressLine2;
    //         $scope.address.shipping.country = $scope.address.billing.country;
    //         $scope.address.shipping.province = $scope.address.billing.province;
    //         $scope.address.shipping.city = $scope.address.billing.city;
    //         $scope.address.shipping.postalCode = $scope.address.billing.postalCode;
    //         $scope.address.shipping.phone = $scope.address.billing.phone;
    //     }
    //     if(!$scope.address.billing.shippingSameAsBilling) { 
    //         $scope.address.shipping.firstName = null;
    //         $scope.address.shipping.lastName = null;
    //         $scope.address.shipping.addressLine1 = null;
    //         $scope.address.shipping.addressLine2 = null;
    //         $scope.address.shipping.country = null;
    //         $scope.address.shipping.province = null;
    //         $scope.address.shipping.city = null;
    //         $scope.address.shipping.postalCode = null;
    //         $scope.address.shipping.phone = null;
    //     }
    // }
    

    $scope.submit = function(){
        //1. check if all required fields are filled out !!!
        // if(!$scope.address.billing.shippingSameAsBilling){
        //     if(!validateAddress($scope.address.shipping, false))    
        //         return
        // }else{
        //     $scope.address.shipping = null
        // }

        if ($scope.orderForm.$valid){
            ordersFactory.placeOrder($scope.orderFormData)
            .then(function (data) {
                $scope.orderForCurrentSession = data;
                if($scope.orderForCurrentSession.status === 'placed') {
                    $scope.showPlaced = true;
                }
                //goRoute('payment-details');
            });
        }
    }





});//stoneDetailsController


// function validateForm(formData, billing){
//     if (billing){
//         if(address.email && address.confirmEmail && address.email == address.confirmEmail){
//             //go on
//         }else{
//             return false
//         }
//     }
//     if( address.firstName && address.lastName && address.addressLine1 && address.country
//         && address.province && address.city && address.postalCode && address.phone){
//         return true
//     }else{
//         return false
//     }
// }

