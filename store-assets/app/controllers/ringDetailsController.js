
/**********************************************************************************************
 * ringDetailsController
 *********************************************************************************************/
app.controller('ringDetailsController', function ($scope, $rootScope, $location, ringById, productsFactory) {
    var ringDetails = $scope.ringDetails = ringById.ring[0];

    if(typeof ringDetails == 'undefined') { $location.url('products/'+$scope.category); return; }

    $scope.$root.pageTitle = ringDetails.title;
    $scope.$root.pageDescription = ringDetails.description;
    
    $scope.primaryRingImage;
    $scope.otherRingImages = [];

    $scope.metals = $scope.ringDetails.metals;

    $scope.ringDetails.suggested_shapes = $.map(ringDetails.shapes, function(shape){ return shape.name }).join(', ');

    $scope.ringDetails.collections_list = $.map(ringDetails.collections, function(collection){ return collection.name }).join(', ');

    var share_image = false;
    if(ringDetails.images.length) {
        share_image = encodeURIComponent(window.location.origin + ringDetails.images[0].url);
        
        angular.forEach(ringDetails.images, function (image, index) {
            if(index == 0){ //taking the first image as primary. It is usually a primary one if it's set
                $scope.primaryRingImage = image
            }else{
                $scope.otherRingImages.push(image)
            }
        });
    }
    
    $rootScope.share = {
        url: encodeURIComponent(window.location.href),      //Will change it to a html snapshot url
        text: encodeURIComponent("Check out this jewellery "),
        media: share_image,
        fb: encodeURIComponent($location.protocol()+'://'+$location.host()+'/'+'sharer.php?product='+ringDetails.id)
    }

    
    if(angular.isUndefined($scope.ringDetails.metal)) {
        $scope.ringDetails.metal = $scope.metals[1];
    }


    $scope.metalSelect = function() {
        additionalPrice = 0;
        console.log($scope.ringDetails);

        // if($scope.ringDetails.additionalPrice > 0) { $scope.ringDetails.price -= $scope.ringDetails.additionalPrice }
        //
        // if($scope.ringDetails.metal.name === "Platinum") {
        //     additionalPrice = 300;
        // }
        // $scope.ringDetails.price += additionalPrice;
        // $scope.ringDetails.additionalPrice = additionalPrice;
    }

});//stoneDetailsController
