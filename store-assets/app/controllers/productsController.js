app.controller('productsController', function ($scope, $rootScope, $timeout, pdCache, criteria, category, pageData, productsFactory, diamondsFactory) {
	var searchCriteria = $scope.searchCriteria = criteria;
	$scope.searchCriteriaForRequest = angular.copy(searchCriteria);

    $scope.$root.pageTitle = 'Pure Diamond - '+$scope.category.charAt(0).toUpperCase() + $scope.category.slice(1);
    //$scope.$root.pageDescription = '';

    $scope.userSelectedMetals = [];
    $scope.userSelectedDesigners = [];
    $scope.userSelectedPriceRanges = [];
    $scope.userSelectedCollections = [];

		$scope.rings;
    var timeout = null;

		$rootScope.page = $scope.page = pageData.page;

		//$scope.searchCriteriaForRequest.category = category;

		if(category.collection) {
			for(var i in $scope.searchCriteria.collections) {
				if($scope.searchCriteria.collections[i].name.toLowerCase().replace(/\s+/g, "-") === category.collection) {
					$scope.searchCriteriaForRequest.collections = [$scope.searchCriteria.collections[i]];
					console.log($scope.searchCriteria.collections[i]);
				}
			}
		} else {
			$scope.searchCriteriaForRequest.category = category.category;
		}

		console.log($scope.searchCriteriaForRequest);

    var cache = pdCache.get('cachedData');
    if (cache && cache.rings) {
        $scope.rings = cache.rings;
    } else {
        cache = {};
		if(category.category === 'sale') {
			productsFactory.getCategoryProducts(category.category)
            .then(function (data) {
                $scope.filterHide = true;
                $scope.withLabel = true;
                $scope.rings = data;
            });
		} else if(category.category === 'specials') {
            diamondsFactory.getStonesByProperty('specials')
            .then(function (data) {
                var stones = data.stones;
                $scope.filterHide = true;
                $scope.withLabel = true;
                for(var i in data.stones) {
                    var primaryImage = false;
                    for(var im in data.stones[i].images) {
                        if(data.stones[i].images[im].favorite === 1) {
                             console.log('yes', data.stones[i])
                            primaryImage = '/static/images/stones/'+data.stones[i].id+'/'+data.stones[i].images[im].file_name;
                        }
                    }
                    if(!primaryImage && data.stones[i].images.length > 0) {
                        console.log('no', data.stones[i].images)
                        primaryImage = '/static/images/stones/'+data.stones[i].id+'/'+data.stones[i].images[0].file_name;
                    } else if(data.stones[i].images.length < 1) {
                        primaryImage = '/store-assets/pics/shapes/'+data.stones[i].shape_class_name+'.jpg';
                    }
                    data.stones[i].id = data.stones[i].stock_number;
                    data.stones[i].title = data.stones[i].weight+' Carat '+ data.stones[i].color +'-'+data.stones[i].clarity + ' ' + data.stones[i].cut + ' Cut ' + data.stones[i].shape + ' Diamond';
                    data.stones[i].primaryImage = primaryImage;
                }

                $scope.rings = data.stones;
            });
        }
        else {
			sendSearchRequest();
		}

    }



    var debounceRequest = function (newVal, oldVal) {
        if (newVal != oldVal) {
            if (timeout) {
                $timeout.cancel(timeout);
            }
            timeout = $timeout(sendSearchRequest, 1000); //timeout before the api call is fired
        }
    };

    function sendSearchRequest() {
        $scope.startLoading();
        //set the user's selections

        setUserSelectionForSearch();
        productsFactory.searchRings($scope.searchCriteriaForRequest)
            .then(function (data) {
                $scope.rings = data.rings;
                // cache.rings = data.rings;
                // pdCache.put('cachedData', cache);
                $scope.stopLoading();
            });
    };


    $scope.filterByObject = function(originalArray, selectedArray, object){
        selectedArray.length = 0;

        angular.forEach(originalArray, function(object, index) {
            if (object.selected) {
                selectedArray.push(object);
            };
        });

    }

    $scope.filter = {}
    $scope.filter.designers = {}
    $scope.filter.collections = {}

    $scope.ringsFilter = function(ring) {

        var matches = true;
        for (var prop in $scope.filter) {

            if(noFilter($scope.filter[prop])) continue;
            if(ring[prop].length > 1) {
                for (var opt in ring[prop]) {
                    if (!$scope.filter[prop][ring[prop][opt].name]) {
                        matches = false;
                    } else {
                        matches = true;
                        break;
                    }
                }
            } else {
                for (var opt in ring[prop]) {
                    if (!$scope.filter[prop][ring[prop][opt].name]) {
                        matches = false;
                        break;
                    }
                }
            }
            if(!matches) { break; }

        }
        return matches;

    };
    function noFilter(filterObj) {
        for (var key in filterObj) {
            if (filterObj[key]) {
                // There is at least one checkbox checked
                return false;
            }
        }
        // No checkbox was found to be checked
        return true;
    }

    // $scope.$watch('userSelectedMetals', debounceRequest, true);
    // $scope.$watch('userSelectedDesigners', debounceRequest, true);
    // $scope.$watch('userSelectedCollections', debounceRequest, true);
    $scope.$watch('userSelectedPriceRanges', debounceRequest, true);

    //var setUserSelectionForSearch = function () {
    function setUserSelectionForSearch() {
        $scope.searchCriteriaForRequest.metals = $scope.userSelectedMetals.length > 0 ? $scope.userSelectedMetals : searchCriteria.metals;
        $scope.searchCriteriaForRequest.designers = $scope.userSelectedDesigners.length > 0 ? $scope.userSelectedDesigners : searchCriteria.designers;
        $scope.searchCriteriaForRequest.collections = $scope.userSelectedCollections.length > 0 ? $scope.userSelectedCollections : searchCriteria.collections;
        $scope.searchCriteriaForRequest.priceRanges = $scope.userSelectedPriceRanges.length > 0 ? $scope.userSelectedPriceRanges : searchCriteria.priceRanges;
    }

    /*function init(){
        sendSearchRequest();
    }*/



});//controller
